#ifndef DICTIONARY_H
#define DICTIONARY_H

#include <QList>
#include <QMultiMap>
#include <QMetaType>
#include "utils.h"

enum partOfSpeech {
    Unknown         = 0x0000,
    Preposition     = 0x0001,
    Verb            = 0x0002,
    Noun            = 0x0004,
    Adverb          = 0x0008,
    Pronoun         = 0x0010,
    ProperNoun      = 0x0020,
    Compound        = 0x0040,
    Number          = 0x0080,
    AuxVerb         = 0x0100,
    Adjective       = 0x0200,
    Conjunction     = 0x0400,
    PrepPhrase      = 0x0800,
    Interrogative   = 0x1000
};

struct angelicalWord {
    QString word;
    QString definition;
    int type;
    int occurances;
    bool newWord;
};
Q_DECLARE_METATYPE( angelicalWord )

QList<angelicalWord> words_a() {
    QList<angelicalWord> out;

    out.append((angelicalWord){"aai", "amongst (you)", Preposition, 4, false});
    out.append((angelicalWord){"aaiom", "amongst (us)", Preposition, 2, false});
    out.append((angelicalWord){"aaf", "amongst", Preposition, 1, false});
    out.append((angelicalWord){"aala", "to place", Verb, 1, false});
    out.append((angelicalWord){"aao", "amongst", Preposition, 1, false});
    out.append((angelicalWord){"aath", "works/deeds", Noun, 0, true});
    out.append((angelicalWord){"ababalond", "horlot", Noun, 1, false});
    out.append((angelicalWord){"abai", "to stoop", Verb, 0, true});
    out.append((angelicalWord){"abaivonin", "stooping dragons", Compound, 1, false});
    out.append((angelicalWord){"aboapri", "to serve", Verb, 1, false});
    out.append((angelicalWord){"abramig", "to prepare", Verb, 1, false});
    out.append((angelicalWord){"abramg", "to prepare", Verb, 2, false});
    out.append((angelicalWord){"abraasa", "to provide", Verb, 1, false});
    out.append((angelicalWord){"acam", "7699", Number, 1, false});
    out.append((angelicalWord){"achildao", "diamond", Noun, 1, false});
    out.append((angelicalWord){"acocasb", "time", Noun, 1, false});
    out.append((angelicalWord){"acroodzi", "beginning", Noun, 1, false});
    out.append((angelicalWord){"adgmach", "much glory (adoration, praise)", Unknown, 0, false});
    out.append((angelicalWord){"adgt", "can", AuxVerb, 1, false});
    out.append((angelicalWord){"adna", "obediance", Noun, 1, false});
    out.append((angelicalWord){"adohi", "kingdom", Noun, 1, false});
    out.append((angelicalWord){"adoian", "face", Noun, 1, false});
    out.append((angelicalWord){"adphaht", "unspeakable", Adjective, 1, false});
    out.append((angelicalWord){"adroch", "olive mount", Noun, 1, false});
    out.append((angelicalWord){"adrpan", "to cast down", Verb, 1, false});
    out.append((angelicalWord){"af", "19", Number, 1, false});
    out.append((angelicalWord){"affa", "empty", Adjective, 1, false});
    out.append((angelicalWord){"ag", "no, none, no one", Adjective|Pronoun, 0, false});
    out.append((angelicalWord){"agl", "no one", Compound|Pronoun, 0, true});
    out.append((angelicalWord){"agltoltorn", "no one creature", Unknown, 1, false});
    out.append((angelicalWord){"agtoltorn", "no creature", Compound, 1, false});
    out.append((angelicalWord){"aisro", "to promise", Verb, 1, false});
    out.append((angelicalWord){"alar", "to settle, to place", Verb, 1, false});
    out.append((angelicalWord){"alca", "to signify (?)", Verb, 0, true});
    out.append((angelicalWord){"aldi", "to gather", Verb, 1, false});
    out.append((angelicalWord){"aldon", "gird up, to gather together", Verb, 3, false});
    out.append((angelicalWord){"allar", "to bind up", Verb, 1, false});
    out.append((angelicalWord){"amgedpha", "I will begin anew", Verb, 0, false});
    out.append((angelicalWord){"amipzi", "to fasten", Verb, 1, false});
    out.append((angelicalWord){"amiran", "yourselves", Pronoun, 1, false});
    out.append((angelicalWord){"amma", "cursed", Adjective, 1, false});
    out.append((angelicalWord){"amzes", "those that fear God (?)", Noun, 0, true});
    out.append((angelicalWord){"ananael", "secret wisdom", Noun, 1, false});
    out.append((angelicalWord){"anetab", "(in) government", Noun, 1, false});
    out.append((angelicalWord){"angelard", "thoughts", Noun, 1, false});
    out.append((angelicalWord){"aoiveae", "stars", Noun, 1, false});
    out.append((angelicalWord){"apachana", "the slimy things made of dust", Noun, 0, false});
    out.append((angelicalWord){"apila", "to live", Verb, 0, true});
    out.append((angelicalWord){"aqlo", "thy, your", Pronoun, 1, false});
    out.append((angelicalWord){"ar", "that", Pronoun, 1, false});
    out.append((angelicalWord){"ar", "to van (?)", Verb, 0, true});
    out.append((angelicalWord){"arcaosgi", "(to van?) the earth", Compound, 1, false});
    out.append((angelicalWord){"arcoazior", "that increase", Compound, 1, false});
    out.append((angelicalWord){"argedco", "with humility we call thee, with adoration of the trinity", Verb, 0, false});
    out.append((angelicalWord){"arn", "the second aethyr", ProperNoun, 1, false});
    out.append((angelicalWord){"arphe", "I desire thee, o God", Verb, 0, false});
    out.append((angelicalWord){"artabas", "that govern", Compound, 1, false});
    out.append((angelicalWord){"arzulgh", "spirit opposing Befafes", ProperNoun, 0, false});
    out.append((angelicalWord){"ascha", "n/a(?)", Unknown, 0, false});
    out.append((angelicalWord){"asobam", "(on) whom", Pronoun, 1, false});
    out.append((angelicalWord){"asp", "the twenty-first aethyr", ProperNoun, 1, false});
    out.append((angelicalWord){"asp", "Quality(?)", Unknown, 0, true});
    out.append((angelicalWord){"aspian", "qualities, characteristics", Noun, 1, false});
    out.append((angelicalWord){"aspt", "before, in front", Preposition, 1, false});
    out.append((angelicalWord){"astel", "n/a", Unknown, 0, false});
    out.append((angelicalWord){"asymp", "another", Pronoun|Adjective, 1, false});
    out.append((angelicalWord){"atraah", "girdles", Noun, 1, false});
    out.append((angelicalWord){"audcal", "gold, the mercury of the philosopher's stone", Unknown, 0, false});
    out.append((angelicalWord){"avav", "pomp", Noun, 0, true});
    out.append((angelicalWord){"avavago", "thunders (of increase)", ProperNoun, 2, false});
    out.append((angelicalWord){"avavox", "his pomp", Compound, 1, false});
    out.append((angelicalWord){"aviny", "millstones", Noun, 1, false});
    out.append((angelicalWord){"azia", "like (unto)", Preposition, 0, true});
    out.append((angelicalWord){"aziagiar", "like unto the harvest", Compound, 1, false});
    out.append((angelicalWord){"aziazor", "likeness of", Noun, 1, false});
    out.append((angelicalWord){"azien", "hands", Noun, 1, false});

    return out;
}

QList<angelicalWord> words_b() {
    QList<angelicalWord> out;

    out.append((angelicalWord){"bab", "dominion", Noun, 0, true});
    out.append((angelicalWord){"babage", "south", Noun, 2, false});
    out.append((angelicalWord){"babagen", "south", Noun, 1, false});
    out.append((angelicalWord){"babalon", "wicked", Noun|Adjective, 1, false});
    out.append((angelicalWord){"baeovib", "righteousness", ProperNoun, 1, false});
    out.append((angelicalWord){"bag", "the twenty-eighth aethyr", ProperNoun, 1, false});
    out.append((angelicalWord){"bag", "fury?", Unknown, 0, true});
    out.append((angelicalWord){"bagie", "fury", Noun, 1, false});
    out.append((angelicalWord){"bagle", "for, wherefore, because", Conjunction, 11, false});
    out.append((angelicalWord){"baglen", "because", Conjunction, 1, false});
    out.append((angelicalWord){"bahal", "to cry loudly, to yell", Unknown, 1, false});
    out.append((angelicalWord){"balit", "the just", Noun, 1, false});
    out.append((angelicalWord){"balt", "justice", Noun, 3, false});
    out.append((angelicalWord){"baltan", "justice", Noun, 1, false});
    out.append((angelicalWord){"baltim", "extreme justice, fury", Noun, 1, false});
    out.append((angelicalWord){"baltle", "righteousness", Noun, 1, false});
    out.append((angelicalWord){"baltoh", "righteousness", Unknown, 1, false});
    out.append((angelicalWord){"baltoha", "(my) righteousness", Unknown, 1, false});
    out.append((angelicalWord){"balye", "salt", Noun, 1, false});
    out.append((angelicalWord){"balzarg", "stewards", Noun, 1, false});
    out.append((angelicalWord){"balzizras", "judgement", Noun, 1, false});
    out.append((angelicalWord){"bams", "to forget", Verb, 1, false});
    out.append((angelicalWord){"barees", "n/a", Noun, 1, false});
    out.append((angelicalWord){"basgim", "day", Noun, 1, false});
    out.append((angelicalWord){"bazem", "midday", Noun, 0, true});
    out.append((angelicalWord){"bazemlo", "midday the first", Unknown, 1, false});
    out.append((angelicalWord){"bafefes", "light from light", ProperNoun, 0, false});
    out.append((angelicalWord){"bafefes", "heptarchic prince of tuesday", ProperNoun, 0, false});
    out.append((angelicalWord){"bafefes", "heptarchic prince of mars", ProperNoun, 0, false});
    out.append((angelicalWord){"bafes", "heptarchic prince of tuesday (vocative)", ProperNoun, 0, false});
    out.append((angelicalWord){"bia", "voices", Noun, 1, false});
    out.append((angelicalWord){"biab", "to stand", Verb, 1, false});
    out.append((angelicalWord){"bial", "voice", Noun, 1, false});
    out.append((angelicalWord){"bien", "(my) voice", Noun, 1, false});
    out.append((angelicalWord){"bigl", "comforter (?)", Noun, 0, true});
    out.append((angelicalWord){"bigliad", "(in our) comforter", Compound, 1, false});
    out.append((angelicalWord){"blans", "to harbor", Verb, 1, false});
    out.append((angelicalWord){"bliard", "comfort", Noun, 0, true});
    out.append((angelicalWord){"blior", "comfort", Noun, 1, false});
    out.append((angelicalWord){"bliora", "comfort", Noun, 1, false});
    out.append((angelicalWord){"bliorax", "shalt comfort, to comfort", Verb, 1, false});
    out.append((angelicalWord){"bliorb", "comfort", Noun, 1, false});
    out.append((angelicalWord){"bliors", "comfort", Noun, 1, false});
    out.append((angelicalWord){"bliort", "comfort", Noun, 0, true});
    out.append((angelicalWord){"bobagelzod", "heptarchic king of sunday", ProperNoun, 0, false});
    out.append((angelicalWord){"bobagelzod", "heptarchic king of sol?", ProperNoun, 0, false});
    out.append((angelicalWord){"bogpa", "to reign", Verb, 1, false});
    out.append((angelicalWord){"bolp", "(be) thou", Verb|Pronoun, 1, false});
    out.append((angelicalWord){"booapis", "to serve", Verb, 1, false});
    out.append((angelicalWord){"bornogo", "heptarchic prince of sunday", ProperNoun, 0, false});
    out.append((angelicalWord){"bornogo", "heptarchic prince of sol", ProperNoun, 0, false});
    out.append((angelicalWord){"bransg", "guard", Noun, 1, false});
    out.append((angelicalWord){"brgda", "to sleep", Verb, 1, false});
    out.append((angelicalWord){"brin", "have", Verb, 0, true});
    out.append((angelicalWord){"brint", "has", Verb, 0, true});
    out.append((angelicalWord){"brints", "to have", Verb, 1, false});
    out.append((angelicalWord){"brita", "to speak of", Verb, 1, false});
    out.append((angelicalWord){"busd", "glory, glorious", Noun|Adjective, 2, false});
    out.append((angelicalWord){"busdir", "glory", Noun, 1, false});
    out.append((angelicalWord){"busdirtilb", "glory (of) her", Compound, 1, false});
    out.append((angelicalWord){"butmon", "mouth", Noun, 1, false});
    out.append((angelicalWord){"butmoni", "mouths", Noun, 1, false});
    return out;
}

QList<angelicalWord> words_c() {
    QList<angelicalWord> out;

    out.append((angelicalWord){"ca", "therefore", Adverb, 4, false});
    out.append((angelicalWord){"ca", "as?", Preposition, 0, true});
    out.append((angelicalWord){"cab", "rod, scepter", Noun, 1, false});
    out.append((angelicalWord){"caba", "to govern", Verb, 1, false});
    out.append((angelicalWord){"cabanladan", "n/a", Unknown, 0, false});
    out.append((angelicalWord){"cacacom", "to flourish", Verb, 1, false});
    out.append((angelicalWord){"cacocasb", "another while", Noun, 0, true});
    out.append((angelicalWord){"cacrg", "until", Preposition|Conjunction, 2, false});
    out.append((angelicalWord){"cafafam", "n/a", Unknown, 0, true});
    out.append((angelicalWord){"calz", "firmaments, heavens", Noun, 1, false});
    out.append((angelicalWord){"camascheth", "n/a", Unknown, 0, false});
    out.append((angelicalWord){"camikas", "n/a", Unknown, 0, false});
    out.append((angelicalWord){"camliax", "spake, spoke", Verb, 1, false});
    out.append((angelicalWord){"canse", "mighty", Adjective, 0, false});
    out.append((angelicalWord){"caosg", "earth", Noun, 3, false});
    out.append((angelicalWord){"caosga", "earth", Noun, 1, false});
    out.append((angelicalWord){"caosgi", "earth", Noun, 5, false});
    out.append((angelicalWord){"caosgin", "earth", Noun, 1, false});
    out.append((angelicalWord){"caosgo", "of the earth", PrepPhrase, 6, false});
    out.append((angelicalWord){"caosgon", "unto the earth", PrepPhrase, 1, false});
    out.append((angelicalWord){"capimali", "successively", Adverb, 1, false});
    out.append((angelicalWord){"capimao", "while (period of time)", Noun, 0, true});
    out.append((angelicalWord){"capimaon", "number of time", Noun, 1, false});
    out.append((angelicalWord){"capmiali", "successively", Unknown, 1, false});
    out.append((angelicalWord){"carbaf", "to sink", Verb, 1, false});
    out.append((angelicalWord){"carma", "to come out, to arrive from, to arise", Verb, 0, true});
    out.append((angelicalWord){"carmara", "n/a", ProperNoun, 1, true});
    out.append((angelicalWord){"casarm", "whom", Pronoun, 2, false});
    out.append((angelicalWord){"casarma", "whom", Pronoun, 1, false});
    out.append((angelicalWord){"casarman", "whom, (under) whose", Pronoun|Adjective, 2, false});
    out.append((angelicalWord){"casarmg", "in whom", Pronoun, 5, false});
    out.append((angelicalWord){"casarmi", "(under) whom", Pronoun, 1, false});
    out.append((angelicalWord){"casasam", "abiding", Noun, 1, false});
    out.append((angelicalWord){"ceph", "letter Z", ProperNoun, 0, false});
    out.append((angelicalWord){"chiis", "are (they)", Verb, 0, true});
    out.append((angelicalWord){"chirlan", "to rejoice", Verb, 2, false});
    out.append((angelicalWord){"chis", "are", Verb, 19, false});
    out.append((angelicalWord){"chisda", "are there", Compound, 0, true});
    out.append((angelicalWord){"chisholq", "are measured", Compound, 1, false});
    out.append((angelicalWord){"chismicaolz", "are mighty", Compound, 1, false});
    out.append((angelicalWord){"chiso", "shall be", Verb, 1, false});
    out.append((angelicalWord){"chista", "are as", Compound, 0, true});
    out.append((angelicalWord){"chistad", "are the third", Compound, 1, false});
    out.append((angelicalWord){"chr", "the twentieth aethyr", ProperNoun, 1, false});
    out.append((angelicalWord){"chr", "to be?, to exist?", Verb, 0, true});
    out.append((angelicalWord){"chramsa", "be it made with power", Verb, 0, true});
    out.append((angelicalWord){"christeos", "let there be...", Verb, 3, false});
    out.append((angelicalWord){"cial", "9996", Number, 1, false});
    out.append((angelicalWord){"ciaofi", "terror", Noun, 1, false});
    out.append((angelicalWord){"cicle", "mysteries", Noun, 1, false});
    out.append((angelicalWord){"cicles", "mysteries", Noun, 1, false});
    out.append((angelicalWord){"cinxir", "to mingle", Verb, 1, false});
    out.append((angelicalWord){"cla", "456", Number, 2, false});
    out.append((angelicalWord){"cnila", "blood", Noun, 1, false});
    out.append((angelicalWord){"cnoqod", "(unto) servants", Noun, 1, false});
    out.append((angelicalWord){"cnoquodi", "(with) ministers", Noun, 1, false});
    out.append((angelicalWord){"cnoquol", "servants", Noun, 1, false});
    out.append((angelicalWord){"coazior", "to increase", Verb, 0, true});
    out.append((angelicalWord){"cocasb", "time", Noun, 2, false});
    out.append((angelicalWord){"cocasg", "times", Noun, 2, false});
    out.append((angelicalWord){"collal", "sleeves, sheaths", Noun, 1, false});
    out.append((angelicalWord){"commah", "to truss together, join", Verb, 1, false});
    out.append((angelicalWord){"comselh", "circle", Noun, 1, false});
    out.append((angelicalWord){"como", "window", Noun, 0, true});
    out.append((angelicalWord){"comobliort", "Window of Comfort", Compound, 1, false});
    out.append((angelicalWord){"congamphlgh", "faith, Holy Ghost", Noun, 0, false});
    out.append((angelicalWord){"conisbra", "the work of man", Noun, 1, false});
    out.append((angelicalWord){"const", "thunders", Noun, 1, false});
    out.append((angelicalWord){"coraxo", "thunders of judgement and wrath", ProperNoun, 2, false});
    out.append((angelicalWord){"cordziz", "mankind", Noun, 1, false});
    out.append((angelicalWord){"cormf", "number", Noun, 1, false});
    out.append((angelicalWord){"cormfa", "numbers", Noun, 1, false});
    out.append((angelicalWord){"cormp", "to number", Verb, 2, false});
    out.append((angelicalWord){"cormpo", "hath (yet) numbered", Verb, 1, false});
    out.append((angelicalWord){"cormpt", "(are) numbered", Verb, 1, false});
    out.append((angelicalWord){"coronzom", "satan, the devil, the enemy", ProperNoun, 0, false});
    out.append((angelicalWord){"cors", "such", Adjective, 1, false});
    out.append((angelicalWord){"corsi", "such", Adjective, 1, false});
    out.append((angelicalWord){"corsta", "such as", Compound, 2, false});
    out.append((angelicalWord){"crip", "but", Conjunction, 1, false});
    out.append((angelicalWord){"croodzi", "beginning (of things)", Noun, 1, false});
    out.append((angelicalWord){"crp", "but", Conjunction, 0, true});
    out.append((angelicalWord){"crpl", "but one", Compound, 1, false});
    out.append((angelicalWord){"crus", "more, greater (?)", Adjective, 0, true});
    out.append((angelicalWord){"cruscanse", "more mighty", Compound, 0, false});
    return out;
}

QList<angelicalWord> words_d() {
    QList<angelicalWord> out;

    out.append((angelicalWord){"d", "third", Noun|Adjective, 1, false});
    out.append((angelicalWord){"da", "there", Pronoun, 0, true});
    out.append((angelicalWord){"damploz", "variety", Noun, 1, false});
    out.append((angelicalWord){"daox", "5678", Number, 1, false});
    out.append((angelicalWord){"darbs", "to obey", Verb, 1, false});
    out.append((angelicalWord){"darg", "6739", Number, 1, false});
    out.append((angelicalWord){"darr", "the philosopher's stone", Noun, 0, true});
    out.append((angelicalWord){"darsar", "wherefore, therefore", Adverb, 1, false});
    out.append((angelicalWord){"dax", "loins", Noun, 0, true});
    out.append((angelicalWord){"daxil", "thy loins", Compound, 0, false});
    out.append((angelicalWord){"dazis", "heads", Noun, 2, false});
    out.append((angelicalWord){"de", "of", Preposition, 1, false});
    out.append((angelicalWord){"deo", "the seventh aethyr", ProperNoun, 1, false});
    out.append((angelicalWord){"des", "the twenty-sixth aethyr", ProperNoun, 1, false});
    out.append((angelicalWord){"dialprt", "third flame", Compound, 1, false});
    out.append((angelicalWord){"div", "angle", Noun, 0, true});
    out.append((angelicalWord){"dlasod", "sulfur", Noun, 0, false});
    out.append((angelicalWord){"dluga", "to give", Verb, 1, false});
    out.append((angelicalWord){"dlugam", "given", Verb, 0, true});
    out.append((angelicalWord){"dlugar", "to give", Verb, 2, false});
    out.append((angelicalWord){"daolim", "sin", Noun, 1, false});
    out.append((angelicalWord){"dobix", "to fall", Verb, 1, false});
    out.append((angelicalWord){"dodpal", "to vex", Verb, 1, false});
    out.append((angelicalWord){"dodrmni", "vexed", Adjective, 1, false});
    out.append((angelicalWord){"dods", "to vex", Verb, 1, false});
    out.append((angelicalWord){"dodsih", "vexation", Noun, 1, false});
    out.append((angelicalWord){"don", "Letter R", ProperNoun, 0, false});
    out.append((angelicalWord){"donasdoga", "n/a", Unknown, 0, true});
    out.append((angelicalWord){"donasdogamatastos", "hellfire", Noun, 0, false});
    out.append((angelicalWord){"dooain", "name", Noun, 2, false});
    out.append((angelicalWord){"dooaip", "(in the) name", Noun, 1, false});
    out.append((angelicalWord){"dooiap", "(in the) name", Noun, 1, false});
    out.append((angelicalWord){"dorpha", "to look about", Verb, 1, false});
    out.append((angelicalWord){"dorphal", "to look upon (with gladness)", Verb, 1, false});
    out.append((angelicalWord){"dosig", "night", Noun, 1, false});
    out.append((angelicalWord){"drilpa", "great", Adjective, 2, false});
    out.append((angelicalWord){"drilpi", "great", Adjective, 2, false});
    out.append((angelicalWord){"drix", "to bring down", Verb, 1, false});
    out.append((angelicalWord){"droln", "any", Adjective|Adverb, 1, false});
    out.append((angelicalWord){"drux", "Letter N", ProperNoun, 0, false});
    out.append((angelicalWord){"ds", "which, that", Pronoun, 16, false});
    out.append((angelicalWord){"dsabramg", "which prepared", Compound, 1, false});
    out.append((angelicalWord){"dsbrin", "which have", Compound, 3, false});
    out.append((angelicalWord){"dschis", "which are", Compound, 2, false});
    out.append((angelicalWord){"dsi", "which is", Compound, 1, false});
    out.append((angelicalWord){"dsinsi", "which walkest", Compound, 1, false});
    out.append((angelicalWord){"dsium", "which (is) called", Compound, 1, false});
    out.append((angelicalWord){"dsoado", "which weave", Compound, 1, false});
    out.append((angelicalWord){"dsom", "that understand", Compound, 1, false});
    out.append((angelicalWord){"dsonf", "which reign", Compound, 2, false});
    out.append((angelicalWord){"dspaaox", "which remain", Compound, 1, false});
    out.append((angelicalWord){"dspraf", "which dwell", Compound, 1, false});
    out.append((angelicalWord){"dst", "which (also)", Pronoun, 2, false});
    out.append((angelicalWord){"duiv", "third angle", Compound, 1, false});

    return out;
}

QList<angelicalWord> words_e() {
    QList<angelicalWord> out;

    out.append((angelicalWord){"eai", "amongst", Preposition, 0, true});
    out.append((angelicalWord){"ecrin", "praise", Noun, 0, true});
    out.append((angelicalWord){"ednas", "receivers", Noun, 1, false});
    out.append((angelicalWord){"ef", "to visit", Verb, 1, false});
    out.append((angelicalWord){"efafafe", "vials", Noun, 1, false});
    out.append((angelicalWord){"el", "the first", ProperNoun, 1, false});
    out.append((angelicalWord){"elzap", "course", Noun, 0, true});
    out.append((angelicalWord){"elzaptilb", "her course", Compound, 1, false});
    out.append((angelicalWord){"em", "nine", Number, 1, false});
    out.append((angelicalWord){"emetgis", "seal", Noun, 1, false});
    out.append((angelicalWord){"emna", "here", Noun, 1, false});
    out.append((angelicalWord){"emod", "8763", Noun, 1, false});
    out.append((angelicalWord){"enay", "lord", Noun, 2, false});
    out.append((angelicalWord){"eol", "made", Verb, 1, false});
    out.append((angelicalWord){"eolis", "to make", Verb, 1, false});
    out.append((angelicalWord){"eophan", "lamentation", Noun, 1, false});
    out.append((angelicalWord){"eors", "n/a", Unknown, 0, true});
    out.append((angelicalWord){"eran", "6332", Number, 1, false});
    out.append((angelicalWord){"erm", "ark, refuge, haven", Noun, 1, false});
    out.append((angelicalWord){"es", "fourth", Noun, 1, false});
    out.append((angelicalWord){"esiach", "brothers", Noun, 1, false});
    out.append((angelicalWord){"ethamz", "to cover", Verb, 1, false});
    out.append((angelicalWord){"etharzi", "peace", Noun, 0, true});
    return out;
}

QList<angelicalWord> words_f() {
    QList<angelicalWord> out;

    out.append((angelicalWord){"f", "to visit", Verb, 0, true});
    out.append((angelicalWord){"faaip", "voices, voicings, (psalms?)", Noun, 1, false});
    out.append((angelicalWord){"faboan", "poison", Noun, 1, false});
    out.append((angelicalWord){"fafen", "intent", Noun, 1, false});
    out.append((angelicalWord){"fam", "Letter S/Sh", ProperNoun, 0, true});
    out.append((angelicalWord){"faonts", "to dwell (within)", Verb, 1, false});
    out.append((angelicalWord){"faorgt", "dwelling place", Noun, 0, true});
    out.append((angelicalWord){"fargt", "dwelling places", Noun, 1, false});
    out.append((angelicalWord){"farzm", "uplifted voices, to speak up", Verb, 1, false});
    out.append((angelicalWord){"fbliard", "visit (with) comfort", Compound, 1, false});
    out.append((angelicalWord){"fcaosga", "visit the earth", Compound, 1, false});
    out.append((angelicalWord){"fetharzi", "visit in peace", Compound, 1, false});
    out.append((angelicalWord){"fifalz", "weed out", Verb, 1, false});
    out.append((angelicalWord){"fisis", "to execute, to carry out", Verb, 1, false});

    return out;
}

QList<angelicalWord> words_g() {
    QList<angelicalWord> out;

    out.append((angelicalWord){"ga", "n/a", ProperNoun, 1, false});
    out.append((angelicalWord){"ga", "31", Number, 0, true});
    out.append((angelicalWord){"gah", "spirits", Noun, 1, false});
    out.append((angelicalWord){"gahire", "(a name of God?)", ProperNoun, 0, true});
    out.append((angelicalWord){"gahoachma", "I am that I am", ProperNoun, 0, false});
    out.append((angelicalWord){"gal", "Letter D", ProperNoun, 0, false});
    out.append((angelicalWord){"galaht", "n/a", Unknown, 0, false});
    out.append((angelicalWord){"galgol", "n/a", Unknown, 0, false});
    out.append((angelicalWord){"galsagen", "divine power creating the angel of the sun", ProperNoun|Verb, 0, false});
    out.append((angelicalWord){"galsuageth", "n/a", Unknown, 0, false});
    out.append((angelicalWord){"galvah", "the end, the omega", ProperNoun, 0, false});
    out.append((angelicalWord){"ganiurax", "n/a", Unknown, 0, false});
    out.append((angelicalWord){"garil", "n/a", Unknown, 0, false});
    out.append((angelicalWord){"garmal", "n/a", Unknown, 0, false});
    out.append((angelicalWord){"garnastel", "n/a", Unknown, 0, false});
    out.append((angelicalWord){"gascampho", "why didst thou so?", Interrogative, 0, false});
    out.append((angelicalWord){"gazavaa", "n/a", ProperNoun, 0, false});
    out.append((angelicalWord){"gchis", "are", Verb, 0, true});
    out.append((angelicalWord){"gchisge", "are not", Compound, 1, false});
    out.append((angelicalWord){"ge", "not", Adverb, 0, true});
    out.append((angelicalWord){"gebofal", "the practice of the 49 gates of understanding", Noun, 0, false});
    out.append((angelicalWord){"ged", "Letter G/J", ProperNoun, 0, false});
    out.append((angelicalWord){"geh", "are, are", Verb, 1, false});
    out.append((angelicalWord){"geiad", "lord and master", ProperNoun, 1, false});
    out.append((angelicalWord){"gel", "n/a", Unknown, 0, false});
    out.append((angelicalWord){"gemeganza", "your will be done, as you wish", Unknown, 0, false});
    out.append((angelicalWord){"gemp", "n/a", Unknown, 0, false});
    out.append((angelicalWord){"gephna", "n/a", Unknown, 0, false});
    out.append((angelicalWord){"ger", "Letter Q/Qu", ProperNoun, 0, false});
    out.append((angelicalWord){"geta", "there", Adverb, 0, false});
    out.append((angelicalWord){"gethog", "a divine name from the sigillum emeth", ProperNoun, 0, false});
    out.append((angelicalWord){"giar", "harvest", Noun, 0, true});
    out.append((angelicalWord){"gigipah", "(living) breath", Noun, 1, false});
    out.append((angelicalWord){"gil", "n/a", Unknown, 0, false});
    out.append((angelicalWord){"gisg", "Letter T", ProperNoun, 0, false});
    out.append((angelicalWord){"githgulcag", "(lucifer, satan)?", ProperNoun, 0, false});
    out.append((angelicalWord){"givi", "stronger", Adjective, 1, false});
    out.append((angelicalWord){"gizyax", "earthquakes", Noun, 1, false});
    out.append((angelicalWord){"gmicalzo", "in power (and presence?)", Noun, 1, false});
    out.append((angelicalWord){"gmicalzoma", "power of understanding", Compound, 1, false});
    out.append((angelicalWord){"gnay", "doth, does", Verb, 2, false});
    out.append((angelicalWord){"gnetaab", "(your) governments", Noun, 1, false});
    out.append((angelicalWord){"gnonp", "to garnish", Verb, 1, false});
    out.append((angelicalWord){"gohed", "one everlasting, all things descending upon one", ProperNoun, 0, false});
    out.append((angelicalWord){"gohel", "sayeth the first", Compound, 1, false});
    out.append((angelicalWord){"gohia", "(we) say", Verb, 1, false});
    out.append((angelicalWord){"goho", "to say", Verb, 2, false});
    out.append((angelicalWord){"gohoiad", "sayeth the lord", Compound, 1, false});
    out.append((angelicalWord){"gohol", "to say", Verb, 2, false});
    out.append((angelicalWord){"goholor", "lift up", Verb, 1, false});
    out.append((angelicalWord){"gohon", "have spoken", Verb, 1, false});
    out.append((angelicalWord){"gohulim", "(it is) said", Verb, 1, false});
    out.append((angelicalWord){"gohus", "(I) say", Verb, 2, false});
    out.append((angelicalWord){"gon", "Letter I/Y", ProperNoun, 0, false});
    out.append((angelicalWord){"gono", "faith, trust, loyalty", Noun, 1, false});
    out.append((angelicalWord){"gosaa", "stranger", Noun, 1, false});
    out.append((angelicalWord){"graa", "moon", Noun, 1, false});
    out.append((angelicalWord){"graph", "Letter E", ProperNoun, 0, false});
    out.append((angelicalWord){"grosb", "(bitter) sting", Noun|Verb, 1, false});
    out.append((angelicalWord){"grsam", "admiration", Noun, 1, false});
    out.append((angelicalWord){"gru", "to cause, bring about, result", Noun|Verb, 0, false});
    return out;
}

QList<angelicalWord> words_h() {
    QList<angelicalWord> out;

    out.append((angelicalWord){"hagonel", "n/a", ProperNoun, 0, false});
    out.append((angelicalWord){"hamuthz", "n/a", Unknown, 0, false});
    out.append((angelicalWord){"hardeh", "to read (?)", Verb, 0, false});
    out.append((angelicalWord){"harg", "to plant", Verb, 1, false});
    out.append((angelicalWord){"helech", "n/a", Unknown, 0, false});
    out.append((angelicalWord){"hoath", "true worshipper", Noun, 1, false});
    out.append((angelicalWord){"holdo", "to groan", Verb, 1, false});
    out.append((angelicalWord){"holq", "to measure", Verb, 2, false});
    out.append((angelicalWord){"hom", "to live", Verb, 1, false});
    out.append((angelicalWord){"homil", "(true) ages", Noun, 1, false});
    out.append((angelicalWord){"homin", "age", Noun, 1, false});
    out.append((angelicalWord){"hoxmarch", "fear (stand in awe of) God", Verb, 0, false});
    out.append((angelicalWord){"hubaio", "lanterns", Noun, 1, false});
    out.append((angelicalWord){"hubaro", "(living/burning) lamps", Noun, 1, false});
    out.append((angelicalWord){"hucacha", "n/a", Unknown, 0, false});
    out.append((angelicalWord){"huseh", "n/a", Unknown, 0, false});

    return out;
}

QList<angelicalWord> words_i() {
    QList<angelicalWord> out;

    out.append((angelicalWord){"i", "is/are", Verb, 2, false});
    out.append((angelicalWord){"ia", "n/a", Unknown, 0, true});
    out.append((angelicalWord){"iad", "God", ProperNoun, 1, false});
    out.append((angelicalWord){"iadbaltoh", "(triumphant) God of righteousness", Compound, 1, false});
    out.append((angelicalWord){"iadnah", "knowledge", Noun, 1, false});
    out.append((angelicalWord){"iadnamad", "pure knowledge", Compound, 1, false});
    out.append((angelicalWord){"iadoias", "eternal God", ProperNoun, 0, true});
    out.append((angelicalWord){"iadoiasmomar", "God eternally crowned", Compound, 1, false});
    out.append((angelicalWord){"iadpil", "(unto) him", ProperNoun, 1, false});
    out.append((angelicalWord){"iaiadix", "honor", Noun, 1, false});
    out.append((angelicalWord){"iaial", "to conclude, to judge", Verb, 1, false});
    out.append((angelicalWord){"iaida", "the highest", ProperNoun, 2, false});
    out.append((angelicalWord){"iaidon", "the all powerful", Noun, 1, false});
    out.append((angelicalWord){"iaisg", "everlasting one and indivisible god", ProperNoun, 0, false});
    out.append((angelicalWord){"ialpirgah", "flames of the first glory", Compound, 1, false});
    out.append((angelicalWord){"ialpirt", "light", Noun, 1, false});
    out.append((angelicalWord){"ialpon", "to burn", Verb, 1, false});
    out.append((angelicalWord){"ialpor", "flaming", Adjective, 1, false});
    out.append((angelicalWord){"ialprg", "burning flame", Compound|ProperNoun, 1, false});
    out.append((angelicalWord){"ialprt", "flame", Noun, 0, true});
    out.append((angelicalWord){"ialprg", "burning flame", Compound|ProperNoun, 1, false});
    out.append((angelicalWord){"iaod", "beginning", Noun, 2, false});
    out.append((angelicalWord){"iaodaf", "(in the) beginning", Noun, 1, false});
    out.append((angelicalWord){"ich", "the eleventh aethyr", ProperNoun, 1, false});
    out.append((angelicalWord){"ichis", "are", Verb, 0, true});
    out.append((angelicalWord){"ichisge", "are not", Compound, 1, false});
    out.append((angelicalWord){"icorsca", "is such as", Compound, 1, false});
    out.append((angelicalWord){"idlugam", "is given", Compound, 1, false});
    out.append((angelicalWord){"idoigo", "him who sits upon the holy throne", ProperNoun, 1, false});
    out.append((angelicalWord){"iehusoz", "(God's) mercies", Noun, 1, false});
    out.append((angelicalWord){"iga", "is 31", Compound, 1, false});
    out.append((angelicalWord){"il", "is one", Compound, 1, false});
    out.append((angelicalWord){"ili", "(the) first, (at) first", Noun, 1, false});
    out.append((angelicalWord){"yls", "thou", Pronoun, 5, false});
    out.append((angelicalWord){"ylsi", "thee", Pronoun, 1, false});
    out.append((angelicalWord){"imvamar", "to apply unto", Verb, 1, false});
    out.append((angelicalWord){"inoas", "are, have become", Compound, 1, false});
    out.append((angelicalWord){"insi", "to walk", Verb, 0, true});
    out.append((angelicalWord){"ioiad", "him that liveth forever", ProperNoun, 1, false});
    out.append((angelicalWord){"ip", "not", Adverb, 1, false});
    out.append((angelicalWord){"ip", "her", Pronoun, 0, true});
    out.append((angelicalWord){"ipam", "is not", Compound, 1, false});
    out.append((angelicalWord){"ipamis", "cannot be", Compound, 1, false});
    out.append((angelicalWord){"ipuran", "shall not see", Compound, 0, true});
    out.append((angelicalWord){"irgil", "how many", Compound, 0, true});
    out.append((angelicalWord){"irgilchisda", "how many are there", Compound, 1, false});
    out.append((angelicalWord){"islman", "is a house", Compound, 1, false});
    out.append((angelicalWord){"isro", "promise of", Compound, 1, false});
    out.append((angelicalWord){"ita", "is as", Compound, 1, false});
    out.append((angelicalWord){"iudra", "n/a", Unknown, 0, false});
    out.append((angelicalWord){"ium", "(is) called", Verb, 0, true});
    out.append((angelicalWord){"iumd", "(is) called", Verb, 2, false});
    out.append((angelicalWord){"ivonph", "is wrath", Compound, 1, false});
    out.append((angelicalWord){"ivonpovnph", "is wrath in anger", Compound, 1, false});
    out.append((angelicalWord){"iurehoh", "what christ did in hell", Noun, 0, true});
    out.append((angelicalWord){"iusmach", "begotten", Adjective, 0, false});
    out.append((angelicalWord){"ix", "let", Verb, 1, true});
    out.append((angelicalWord){"ixomaxip", "let her be known", Compound, 1, false});
    out.append((angelicalWord){"izazaz", "to frame, to form", Verb, 1, false});
    out.append((angelicalWord){"izizop", "(your?) vessels", Noun, 1, false});

    return out;
}

QList<angelicalWord> words_j() {
    QList<angelicalWord> out;

    out.append((angelicalWord){"jabes", "n/a", Unknown, 0, false});

    return out;
}

QList<angelicalWord> words_k() {
    QList<angelicalWord> out;

    out.append((angelicalWord){"kelpadman", "n/a", Unknown, 0, false});
    out.append((angelicalWord){"kures", "n/a", Unknown, 0, false});
    return out;
}

QList<angelicalWord> words_l() {
    QList<angelicalWord> out;

    out.append((angelicalWord){"l", "the first", ProperNoun, 2, false});
    out.append((angelicalWord){"la", "the first", Noun, 1, false});
    out.append((angelicalWord){"laiad", "secrets of truth", Noun, 1, false});
    out.append((angelicalWord){"lang", "those who serve", ProperNoun, 0, true});
    out.append((angelicalWord){"lansh", "exalted power", Noun, 1, false});
    out.append((angelicalWord){"lap", "for, because", Conjunction, 3, false});
    out.append((angelicalWord){"larag", "neither, nor", Conjunction, 1, false});
    out.append((angelicalWord){"las", "rich", Adjective, 0, true});
    out.append((angelicalWord){"lasdi", "(my) feet", Noun, 1, false});
    out.append((angelicalWord){"lasollor", "rich man", Compound, 1, false});
    out.append((angelicalWord){"lava", "fervency, humility?", Noun, 0, true});
    out.append((angelicalWord){"lcapimao", "one while", Compound, 1, false});
    out.append((angelicalWord){"lea", "the sixteenth aethyr", ProperNoun, 1, false});
    out.append((angelicalWord){"lel", "same", Adverb, 1, false});
    out.append((angelicalWord){"lephe", "n/a", Unknown, 0, false});
    out.append((angelicalWord){"levithmong", "beasts of the field", Noun, 1, false});
    out.append((angelicalWord){"lialprt", "first flame", Compound, 1, false});
    out.append((angelicalWord){"life", "n/a", Unknown, 0, false});
    out.append((angelicalWord){"lil", "the first aethyr", ProperNoun, 1, false});
    out.append((angelicalWord){"lilonon", "branches", Noun, 1, false});
    out.append((angelicalWord){"limlal", "treasure", Noun, 1, false});
    out.append((angelicalWord){"lin", "the twenty-second aethyr", ProperNoun, 1, false});
    out.append((angelicalWord){"lit", "the fifth aethyr", ProperNoun, 1, false});
    out.append((angelicalWord){"lnibm", "one season", Compound, 1, false});
    out.append((angelicalWord){"lo", "the first", Noun, 0, true});
    out.append((angelicalWord){"loagaeth", "speech from God", Noun, 0, false});
    out.append((angelicalWord){"loe", "the twelfth aethyr", ProperNoun, 1, false});
    out.append((angelicalWord){"logaah", "n/a", Unknown, 0, false});
    out.append((angelicalWord){"loholo", "to shine", Verb, 1, false});
    out.append((angelicalWord){"lolcis", "bucklers", Noun, 0, true});
    out.append((angelicalWord){"loncho", "to fall", Verb, 1, false});
    out.append((angelicalWord){"londoh", "kingdoms", Noun, 1, false});
    out.append((angelicalWord){"lonsa", "power", Noun, 1, false});
    out.append((angelicalWord){"lonshi", "power", Noun, 1, false});
    out.append((angelicalWord){"lonshin", "powers", Noun, 0, true});
    out.append((angelicalWord){"lonshitox", "his power", Compound, 1, false});
    out.append((angelicalWord){"lpatralx", "one rock", Compound, 1, false});
    out.append((angelicalWord){"lrasd", "to dispose, to place", Verb, 1, false});
    out.append((angelicalWord){"lring", "to stir up", Verb, 1, false});
    out.append((angelicalWord){"lsmnad", "one another", Compound, 1, false});
    out.append((angelicalWord){"lu", "from one", Preposition, 0, false});
    out.append((angelicalWord){"luas", "those who praise, the triumphant", ProperNoun, 0, false});
    out.append((angelicalWord){"lucal", "north", Noun, 1, false});
    out.append((angelicalWord){"luciftian", "(ornaments of) brightness", Noun, 1, false});
    out.append((angelicalWord){"luciftias", "brightness", Noun, 1, false});
    out.append((angelicalWord){"luiahe", "song of honor", Noun, 1, false});
    out.append((angelicalWord){"lulo", "tartar, mother of vinegar", Noun, 1, false});
    out.append((angelicalWord){"lurfando", "n/a", Unknown, 0, false});
    out.append((angelicalWord){"lusd", "(your) feet", Noun, 1, false});
    out.append((angelicalWord){"lusda", "(their) feet", Noun, 1, false});
    out.append((angelicalWord){"lusdan", "(with) feet", Noun, 1, false});
    out.append((angelicalWord){"luseroth", "n/a", Unknown, 0, false});
    out.append((angelicalWord){"lzar", "courses", Noun, 0, true});
    out.append((angelicalWord){"lzirn", "wonders", Noun, 0, true});

    return out;
}

QList<angelicalWord> words_m() {
    QList<angelicalWord> out;

    out.append((angelicalWord){"m", "except", Conjunction|Preposition, 1, false});
    out.append((angelicalWord){"maasi", "laid up, stored up", Verb, 1, false});
    out.append((angelicalWord){"mabberan", "how now(?)", Adverb, 0, false});
    out.append((angelicalWord){"mabza", "coat", Noun, 1, false});
    out.append((angelicalWord){"mad", "(your) God, pure, undefiled", Noun, 5, false});
    out.append((angelicalWord){"madriax", "heavens", Noun, 1, false});
    out.append((angelicalWord){"madrid", "iniquity", Noun, 2, false});
    out.append((angelicalWord){"madzilodarp", "God of stretch forth and conquor", Compound, 1, false});
    out.append((angelicalWord){"mal", "thrust, arrow, increase", Noun, 0, true});
    out.append((angelicalWord){"malpirgi", "fires of life and increase", Compound, 1, false});
    out.append((angelicalWord){"malprg", "through-thrusting fire, fiery arrow", Compound, 1, false});
    out.append((angelicalWord){"malpurg", "fiery darts/arrows", Compound, 1, false});
    out.append((angelicalWord){"mals", "Letter P/Ph", ProperNoun, 0, false});
    out.append((angelicalWord){"manin", "(in the) mind", Noun, 1, false});
    out.append((angelicalWord){"maoffas", "measureless", Adjective, 1, false});
    out.append((angelicalWord){"mapm", "9639", Number, 1, false});
    out.append((angelicalWord){"mapsama", "he that speaks", ProperNoun, 0, false});
    out.append((angelicalWord){"marb", "according to", Adjective, 1, false});
    out.append((angelicalWord){"marmara", "n/a", ProperNoun, 0, true});
    out.append((angelicalWord){"masch", "n/a", Unknown, 0, false});
    out.append((angelicalWord){"matastos", "n/a", Unknown, 0, true});
    out.append((angelicalWord){"matb", "one thousand", Noun, 2, false});
    out.append((angelicalWord){"matorb", "long (period of time)", Compound, 1, false});
    out.append((angelicalWord){"maz", "the sixth aethyr", ProperNoun, 1, false});
    out.append((angelicalWord){"med", "Letter O", ProperNoun, 0, false});
    out.append((angelicalWord){"miam", "continuance", Noun, 1, false});
    out.append((angelicalWord){"mian", "continuance", Noun, 0, true});
    out.append((angelicalWord){"mian", "3663", Number, 1, false});
    out.append((angelicalWord){"micalp", "mightier", Adjective, 1, false});
    out.append((angelicalWord){"micalzo", "mighty, power", Noun|Adjective, 3, false});
    out.append((angelicalWord){"micaoli", "mighty", Adjective, 1, false});
    out.append((angelicalWord){"micaolz", "mighty", Adjective, 2, false});
    out.append((angelicalWord){"micma", "behold", Verb, 5, false});
    out.append((angelicalWord){"miinoag", "corners, boundries", Noun, 1, false});
    out.append((angelicalWord){"miketh", "the true measure of the will of god in judegement which is by visdom", Noun, 0, false});
    out.append((angelicalWord){"mir", "torment", Noun, 1, false});
    out.append((angelicalWord){"mirc", "upon", Preposition, 3, false});
    out.append((angelicalWord){"molvi", "surges", Noun, 1, false});
    out.append((angelicalWord){"mom", "moss (dross?)", Noun, 1, false});
    out.append((angelicalWord){"momao", "crowns", Noun, 1, false});
    out.append((angelicalWord){"momar", "to crown", Verb, 0, true});
    out.append((angelicalWord){"monasci", "great name", Noun, 1, false});
    out.append((angelicalWord){"momons", "heart", Noun, 1, false});
    out.append((angelicalWord){"moooah", "to repent", Verb, 1, false});
    out.append((angelicalWord){"mospleh", "horns", Noun, 1, true});
    out.append((angelicalWord){"moz", "joy, joy of God", Noun, 1, false});
    out.append((angelicalWord){"murifri", "n/a", ProperNoun, 0, false});

    return out;
}

QList<angelicalWord> words_n() {
    QList<angelicalWord> out;

    out.append((angelicalWord){"na", "the name of the trinity, one seperable for a while", ProperNoun, 0, false});
    out.append((angelicalWord){"na", "Letter H", ProperNoun, 0, false});
    out.append((angelicalWord){"naghezes", "worthiness (?)", Noun, 0, false});
    out.append((angelicalWord){"nalvage", "earth-fleer", ProperNoun, 0, false});
    out.append((angelicalWord){"nanaeel", "(my) power", Noun, 1, false});
    out.append((angelicalWord){"nanba", "thorns", Noun, 1, false});
    out.append((angelicalWord){"nepeai", "swords", Noun, 1, false});
    out.append((angelicalWord){"napta", "(two-edged) sword", Noun, 1, false});
    out.append((angelicalWord){"nazarth", "pillars (of gladness)", Noun, 1, false});
    out.append((angelicalWord){"nazavabh", "(hyacinth) pillars", Noun, 1, false});
    out.append((angelicalWord){"nazpsad", "sword", Noun, 1, false});
    out.append((angelicalWord){"ne", "n/a", Unknown, 0, false});
    out.append((angelicalWord){"netaab", "government", Noun, 1, false});
    out.append((angelicalWord){"netaaib", "government", Noun, 1, false});
    out.append((angelicalWord){"ni", "28", Number, 1, false});
    out.append((angelicalWord){"nia", "the twenty-fourth aethyr", ProperNoun, 1, false});
    out.append((angelicalWord){"nibm", "season", Noun, 0, true});
    out.append((angelicalWord){"nidali", "noises", Noun, 1, false});
    out.append((angelicalWord){"niis", "come (here)", Verb, 2, false});
    out.append((angelicalWord){"niisa", "come away", Verb, 1, false});
    out.append((angelicalWord){"niiso", "come away", Verb, 5, false});
    out.append((angelicalWord){"noaln", "may be", Verb, 1, false});
    out.append((angelicalWord){"noan", "to become", Verb, 2, false});
    out.append((angelicalWord){"noar", "(is) become", Verb, 1, false});
    out.append((angelicalWord){"noas", "to become", Verb, 2, false});
    out.append((angelicalWord){"noasmi", "(let) become", Verb, 1, false});
    out.append((angelicalWord){"nobloh", "palms, palms of", Noun, 1, false});
    out.append((angelicalWord){"noch", "n/a", Unknown, 0, true});
    out.append((angelicalWord){"noco", "servant", Noun, 1, false});
    out.append((angelicalWord){"noib", "yea, yes", Adverb, 1, false});
    out.append((angelicalWord){"nomig", "even (as)", Adverb|Adjective, 1, false});
    out.append((angelicalWord){"nonca", "(to) you", Unknown, 1, false});
    out.append((angelicalWord){"noncf", "you (plural)", Pronoun, 2, false});
    out.append((angelicalWord){"nonci", "you", Pronoun, 1, false});
    out.append((angelicalWord){"noncp", "you", Pronoun, 1, false});
    out.append((angelicalWord){"nor", "son", Noun, 0, true});
    out.append((angelicalWord){"norm", "son, sons", Noun, 0, true});
    out.append((angelicalWord){"normolap", "sons of men", Compound, 1, false});
    out.append((angelicalWord){"noromi", "sons", Noun, 1, false});
    out.append((angelicalWord){"norquasahi", "sons of pleasure", Compound, 1, false});
    out.append((angelicalWord){"norz", "six", Noun, 1, false});
    out.append((angelicalWord){"nostoah", "it was the beginning", Compound, 0, false});
    out.append((angelicalWord){"nothoa", "amidst", Preposition, 1, false});

    return out;
}

QList<angelicalWord> words_o() {
    QList<angelicalWord> out;

    out.append((angelicalWord){"o", "five", Number, 1, false});
    out.append((angelicalWord){"o", "come and bear witness", Verb, 0, false});
    out.append((angelicalWord){"oado", "to weave", Verb, 0, true});
    out.append((angelicalWord){"oadriax", "lower heavens", Noun, 1, false});
    out.append((angelicalWord){"oai", "amongst", Preposition, 0, true});
    out.append((angelicalWord){"oali", "to place", Verb, 1, false});
    out.append((angelicalWord){"oanio", "moment", Noun, 1, false});
    out.append((angelicalWord){"ob", "28", Number, 1, false});
    out.append((angelicalWord){"obelison", "pleasant deliverer, a name of Befafes", ProperNoun, 0, false});
    out.append((angelicalWord){"obelisong", "pleasant deliverers", Noun, 1, false});
    out.append((angelicalWord){"obloc", "garland", Noun, 1, false});
    out.append((angelicalWord){"oboleh", "garments", Noun, 1, false});
    out.append((angelicalWord){"obza", "half", Noun, 1, false});
    out.append((angelicalWord){"od", "and", Conjunction, 75, false});
    out.append((angelicalWord){"odapila", "and liveth", Compound, 1, false});
    out.append((angelicalWord){"odbrint", "and has", Compound, 1, false});
    out.append((angelicalWord){"odcacocasb", "and another while", Compound, 1, false});
    out.append((angelicalWord){"odchis", "and are", Compound, 1, false});
    out.append((angelicalWord){"odecrin", "and the praise (of)", Compound, 1, false});
    out.append((angelicalWord){"odes", "and fourth", Compound, 1, false});
    out.append((angelicalWord){"odfaorgt", "and the dwelling place", Compound, 1, false});
    out.append((angelicalWord){"odipuran", "and shall not see", Compound, 1, false});
    out.append((angelicalWord){"odlonshin", "and powers", Compound, 1, false});
    out.append((angelicalWord){"odmiam", "and continuance", Compound, 2, false});
    out.append((angelicalWord){"odo", "to open", Verb, 2, false});
    out.append((angelicalWord){"odquasb", "and destroy", Compound, 1, false});
    out.append((angelicalWord){"odugeg", "and wax strong", Compound, 1, false});
    out.append((angelicalWord){"odvooan", "and truth", Compound, 1, false});
    out.append((angelicalWord){"odzamran", "and appear", Compound, 2, false});
    out.append((angelicalWord){"oecrimi", "to sing praises", Verb, 2, false});
    out.append((angelicalWord){"ofafafe", "vials", Noun, 1, false});
    out.append((angelicalWord){"oh", "come and bear witness (?)", Verb, 0, false});
    out.append((angelicalWord){"ohio", "woe", Noun, 1, false});
    out.append((angelicalWord){"ohorela", "to legislate", Verb, 1, false});
    out.append((angelicalWord){"oi", "this", Adjective|Pronoun, 0, true});
    out.append((angelicalWord){"oiad", "(of) God", Noun, 4, false});
    out.append((angelicalWord){"oisalman", "this house", Compound, 1, false});
    out.append((angelicalWord){"ol", "I", Pronoun, 1, false});
    out.append((angelicalWord){"ol", "24", Number, 2, false});
    out.append((angelicalWord){"olani", "two times, twice", Adverb, 1, false});
    out.append((angelicalWord){"olap", "men", Noun, 0, true});
    out.append((angelicalWord){"olcordziz", "made mankind", Compound, 1, false});
    out.append((angelicalWord){"ollog", "men", Noun, 1, false});
    out.append((angelicalWord){"ollor", "man", Noun, 0, true});
    out.append((angelicalWord){"oln", "made (of)", Verb, 1, false});
    out.append((angelicalWord){"olora", "(of) man", Noun, 1, false});
    out.append((angelicalWord){"om", "to know, to understand", Verb, 2, false});
    out.append((angelicalWord){"oma", "(of) understanding", Noun, 0, true});
    out.append((angelicalWord){"omaos", "names", Noun, 1, false});
    out.append((angelicalWord){"omax", "to know", Verb, 1, false});
    out.append((angelicalWord){"omicaolz", "(be) mighty", Verb, 1, false});
    out.append((angelicalWord){"omp", "understanding", Noun, 0, true});
    out.append((angelicalWord){"omptilb", "her understanding", Compound, 1, false});
    out.append((angelicalWord){"ooanoan", "eyes", Noun, 1, false});
    out.append((angelicalWord){"ooaona", "eyes", Noun, 1, false});
    out.append((angelicalWord){"ooge", "chamber", Noun, 1, false});
    out.append((angelicalWord){"op", "22", Number, 1, false});
    out.append((angelicalWord){"oq", "but, except", Preposition|Conjunction, 1, false});
    out.append((angelicalWord){"or", "Letter F", ProperNoun, 0, true});
    out.append((angelicalWord){"orh", "the second spirit of darkness", ProperNoun, 0, false});
    out.append((angelicalWord){"oroch", "under", Preposition, 1, false});
    out.append((angelicalWord){"orocha", "beneath", Preposition, 1, false});
    out.append((angelicalWord){"orri", "(baren) stone", Noun, 1, false});
    out.append((angelicalWord){"ors", "darkness", Noun, 1, false});
    out.append((angelicalWord){"orsba", "drunken", Adjective, 2, false});
    out.append((angelicalWord){"orscatbl", "buildings", Noun, 1, false});
    out.append((angelicalWord){"orscor", "dryness", Noun, 1, false});
    out.append((angelicalWord){"os", "12", Number, 0, true});
    out.append((angelicalWord){"osf", "discord", Noun, 0, true});
    out.append((angelicalWord){"oslondoh", "12 kingdoms", Compound, 1, false});
    out.append((angelicalWord){"ot", "and", Conjunction, 0, true});
    out.append((angelicalWord){"othil", "seats (of), to set", Noun|Verb, 2, false});
    out.append((angelicalWord){"othilrit", "seats of mercy", Compound, 1, false});
    out.append((angelicalWord){"oucho", "to confound", Verb, 1, false});
    out.append((angelicalWord){"ovoars", "center", Noun, 1, false});
    out.append((angelicalWord){"ovof", "to magnify", Verb, 1, false});
    out.append((angelicalWord){"ox", "26", Number, 1, false});
    out.append((angelicalWord){"oxox", "to vomit, to hurl forth", Verb, 1, false});
    out.append((angelicalWord){"oxiayal", "mighty seat, the divine throne", Compound, 1, false});
    out.append((angelicalWord){"oxo", "the fifteenth aethyr", Noun, 1, false});
    out.append((angelicalWord){"ozazm", "to make (me)", Verb, 1, false});
    out.append((angelicalWord){"ozazma", "to make (us)", Verb, 1, false});
    out.append((angelicalWord){"ozien", "(mine own) hand", Noun, 1, false});
    out.append((angelicalWord){"ozol", "hands (_maybe_ heads?)", Noun, 1, false});
    out.append((angelicalWord){"ozongon", "manifold winds", Noun, 1, false});

    return out;
}

QList<angelicalWord> words_p() {
    QList<angelicalWord> out;

    out.append((angelicalWord){"p", "eight", Number, 1, false});
    out.append((angelicalWord){"pa", "Letter B", ProperNoun, 0, false});
    out.append((angelicalWord){"paaox", "to remain", Verb, 0, true});
    out.append((angelicalWord){"paaoxt", "to remain", Verb, 1, false});
    out.append((angelicalWord){"pacaduasam", "n/a", Unknown, 0, false});
    out.append((angelicalWord){"pacaph", "n/a", Unknown, 0, false});
    out.append((angelicalWord){"pacaduasam", "n/a", Unknown, 0, true});
    out.append((angelicalWord){"padgze", "justice from divine power without defect", Noun, 0, true});
    out.append((angelicalWord){"paeb", "oak", Noun, 1, false});
    out.append((angelicalWord){"page", "to rest", Verb, 0, true});
    out.append((angelicalWord){"pageip", "rest not", Compound, 1, false});
    out.append((angelicalWord){"paid", "always", Adverb, 1, false});
    out.append((angelicalWord){"pal", "Letter X", ProperNoun, 0, false});
    out.append((angelicalWord){"pala", "two, separated", Noun, 0, false});
    out.append((angelicalWord){"pam", "not", Adverb, 0, true});
    out.append((angelicalWord){"pambt", "unto (me)", Preposition, 1, false});
    out.append((angelicalWord){"pamis", "cannot", Verb, 0, true});
    out.append((angelicalWord){"panpir", "to pour down (rain)", Verb, 1, false});
    out.append((angelicalWord){"paombd", "members, parts, appendages", Noun, 1, false});
    out.append((angelicalWord){"papnor", "remembrance, memory", Noun, 1, false});
    out.append((angelicalWord){"par", "(in) them", Pronoun, 1, false});
    out.append((angelicalWord){"parach", "equal", Adjective, 1, false});
    out.append((angelicalWord){"paracleda", "wedding", Noun, 1, false});
    out.append((angelicalWord){"paradial", "living dwellings", Noun, 1, false});
    out.append((angelicalWord){"paradiz", "virgins", Noun, 1, false});
    out.append((angelicalWord){"parm", "to run", Verb, 1, false});
    out.append((angelicalWord){"parmgi", "(let) run", Verb, 1, false});
    out.append((angelicalWord){"pasbs", "daughters", Noun, 1, false});
    out.append((angelicalWord){"patralx", "rock", Noun, 0, true});
    out.append((angelicalWord){"paz", "the fourth aethyr", ProperNoun, 1, false});
    out.append((angelicalWord){"pd", "33", Number, 1, false});
    out.append((angelicalWord){"peleh", "worker of wonders", Unknown, 0, true});
    out.append((angelicalWord){"peral", "69636", Number, 1, false});
    out.append((angelicalWord){"phama", "I will give", Verb, 0, true});
    out.append((angelicalWord){"pi", "she", Pronoun, 0, true});
    out.append((angelicalWord){"piad", "(your) God", Noun, 1, false});
    out.append((angelicalWord){"piadph", "the depths of (my) jaws", Noun, 1, false});
    out.append((angelicalWord){"piamol", "n/a", Unknown, 0, true});
    out.append((angelicalWord){"piap", "balance", Noun, 1, false});
    out.append((angelicalWord){"piatol", "n/a", Unknown, 0, true});
    out.append((angelicalWord){"pibliar", "places of comfort", Noun, 1, false});
    out.append((angelicalWord){"pidiai", "marble", Noun, 1, false});
    out.append((angelicalWord){"pii", "she is", Compound, 1, false});
    out.append((angelicalWord){"pilah", "moreover", Adverb, 1, false});
    out.append((angelicalWord){"pild", "continually", Adverb, 1, false});
    out.append((angelicalWord){"pilzin", "firmaments of waters", Noun, 1, false});
    out.append((angelicalWord){"pinzu", "n/a", Unknown, 0, false});
    out.append((angelicalWord){"pir", "holy (ones)", Noun, 1, false});
    out.append((angelicalWord){"pirgah", "the first glory, flames", Noun, 0, true});
    out.append((angelicalWord){"pirgi", "fires", Noun, 0, true});
    out.append((angelicalWord){"piripsax", "heavens", Noun, 1, false});
    out.append((angelicalWord){"piripsol", "heavens", Noun, 2, false});
    out.append((angelicalWord){"piripson", "(third?) heaven", Noun, 1, false});
    out.append((angelicalWord){"plapli", "partakers (of)", Noun, 2, false});
    out.append((angelicalWord){"plosi", "as many", Unknown, 1, false});
    out.append((angelicalWord){"poamal", "palace", Noun, 1, false});
    out.append((angelicalWord){"poilp", "to divide", Verb, 1, false});
    out.append((angelicalWord){"pola", "two together, couple", Noun, 0, false});
    out.append((angelicalWord){"pop", "the nineteenth aethyr", ProperNoun, 1, false});
    out.append((angelicalWord){"praf", "to dwell", Verb, 0, true});
    out.append((angelicalWord){"pragma", "n/a", Unknown, 0, false});
    out.append((angelicalWord){"prdzar", "to diminish", Verb, 1, false});
    out.append((angelicalWord){"prg", "flame", Noun, 0, true});
    out.append((angelicalWord){"prge", "fire", Noun, 1, false});
    out.append((angelicalWord){"prgel", "fire", Noun, 1, false});
    out.append((angelicalWord){"priaz", "those", Pronoun|Adjective, 1, false});
    out.append((angelicalWord){"priazi", "those", Pronoun|Adjective, 1, false});
    out.append((angelicalWord){"pugo", "as unto", Preposition, 1, false});
    out.append((angelicalWord){"puin", "(sharp) sickles", Noun, 0, true});
    out.append((angelicalWord){"puran", "to see", Verb, 0, true});
    out.append((angelicalWord){"purg", "flames", Noun, 1, false});

    return out;
}

QList<angelicalWord> words_q() {
    QList<angelicalWord> out;

    out.append((angelicalWord){"q", "or", Conjunction, 1, false});
    out.append((angelicalWord){"qaa", "creation, garments", Noun, 3, false});
    out.append((angelicalWord){"qaal", "creator", Compound, 1, false});
    out.append((angelicalWord){"qaan", "creation", Noun, 1, false});
    out.append((angelicalWord){"qaaon", "creation", Noun, 1, false});
    out.append((angelicalWord){"qaas", "creation", Noun, 1, false});
    out.append((angelicalWord){"qadah", "creation", Compound, 1, false});
    out.append((angelicalWord){"qanis", "olives", Noun, 0, false});
    out.append((angelicalWord){"qcocasb", "contents of time", Noun, 1, false});
    out.append((angelicalWord){"qmospleh", "or the horns", Compound, 1, false});
    out.append((angelicalWord){"qrasahi", "pleasure", Noun, 0, true});
    out.append((angelicalWord){"qta", "or as", Compound, 1, false});
    out.append((angelicalWord){"qting", "rotten", Noun|Adjective, 1, false});
    out.append((angelicalWord){"quar", "1636", Number, 1, false});
    out.append((angelicalWord){"quasb", "to destroy", Verb, 0, true});
    out.append((angelicalWord){"quiin", "wherein", Conjunction, 2, false});
    out.append((angelicalWord){"qurlst", "handmaid", Noun, 1, false});
    out.append((angelicalWord){"qzmoz", "n/a", Unknown, 0, true});

    return out;
}

QList<angelicalWord> words_r() {
    QList<angelicalWord> out;

    out.append((angelicalWord){"raas", "east", Noun, 1, false});
    out.append((angelicalWord){"rassy", "east", Noun, 1, false});
    out.append((angelicalWord){"raclir", "weeping", Noun|Verb, 1, false});
    out.append((angelicalWord){"remiges", "n/a", Unknown, 0, false});
    out.append((angelicalWord){"restil", "to praise (him?)", Verb, 1, false});
    out.append((angelicalWord){"rii", "the twenty-ninth aethyr", ProperNoun, 1, false});
    out.append((angelicalWord){"rior", "widow", Noun, 1, false});
    out.append((angelicalWord){"ripir", "no place", Noun, 1, false});
    out.append((angelicalWord){"rit", "mercy", Noun, 1, false});
    out.append((angelicalWord){"rlodnr", "alchemical furnace, athanor(?)", Noun, 0, false});
    out.append((angelicalWord){"ror", "sun", Noun, 1, false});
    out.append((angelicalWord){"roxtan", "(rectified?) wine", Noun, 0, false});
    out.append((angelicalWord){"rudna", "n/a", Unknown, 0, false});

    return out;
}

QList<angelicalWord> words_s() {
    QList<angelicalWord> out;

    out.append((angelicalWord){"s", "fourth", Noun|Adjective, 0, true});
    out.append((angelicalWord){"saanir", "parts", Noun, 2, false});
    out.append((angelicalWord){"saba", "whose", Adjective, 0, true});
    out.append((angelicalWord){"sabaooaona", "whose eyes", Compound, 1, false});
    out.append((angelicalWord){"sach", "the establishers, supporters", ProperNoun, 0, false});
    out.append((angelicalWord){"sagacor", "in one number", Noun, 1, false});
    out.append((angelicalWord){"salbrox", "live (burning) sulfur", Noun, 1, false});
    out.append((angelicalWord){"sald", "wonder", Noun, 1, false});
    out.append((angelicalWord){"salman", "house", Noun, 2, false});
    out.append((angelicalWord){"samhampors", "n/a", Unknown, 0, true});
    out.append((angelicalWord){"samvelg", "the righteous", Noun, 1, false});
    out.append((angelicalWord){"sapah", "mighty sounds, thunders", ProperNoun, 1, false});
    out.append((angelicalWord){"sdiv", "fourth angle", Compound, 1, false});
    out.append((angelicalWord){"sem", "in this place", Noun, 0, true});
    out.append((angelicalWord){"semhaham", "n/a", Unknown, 0, false});
    out.append((angelicalWord){"ser", "sorrow", Noun, 0, false});
    out.append((angelicalWord){"siaion", "temple", Noun, 1, false});
    out.append((angelicalWord){"siatris", "scorpions", Noun, 1, false});
    out.append((angelicalWord){"sibsi", "covenant", Noun, 0, true});
    out.append((angelicalWord){"smnad", "another", Pronoun|Adjective, 0, true});
    out.append((angelicalWord){"soba", "whose", Adjective, 8, false});
    out.append((angelicalWord){"sobaiad", "whose God", Compound, 1, false});
    out.append((angelicalWord){"sobam", "Whom", Pronoun, 3, false});
    out.append((angelicalWord){"sobca", "whose", Adjective, 2, false});
    out.append((angelicalWord){"sobha", "whose", Adjective, 0, true});
    out.append((angelicalWord){"sobhaath", "whose works", Compound, 1, false});
    out.append((angelicalWord){"sobo", "whose", Adjective, 0, true});
    out.append((angelicalWord){"soboln", "west", Noun, 1, false});
    out.append((angelicalWord){"sobolzar", "whose courses", Compound, 1, false});
    out.append((angelicalWord){"sobra", "whose", Adjective, 1, false});
    out.append((angelicalWord){"sola", "whose", Unknown, 0, true});
    out.append((angelicalWord){"solamian", "whose countinuance", Compound, 1, false});
    out.append((angelicalWord){"solpeth", "harken unto, listen to", Verb, 1, false});
    out.append((angelicalWord){"sonf", "to reign", Verb, 1, false});
    out.append((angelicalWord){"sor", "action (esp. taken by a king)", Noun, 0, false});
    out.append((angelicalWord){"surzas", "to swear, promise", Verb, 1, false});
    out.append((angelicalWord){"symp", "another", Pronoun|Adjective, 1, false});

    return out;
}

QList<angelicalWord> words_t() {
    QList<angelicalWord> out;

    out.append((angelicalWord){"t", "it", Pronoun, 0, true});
    out.append((angelicalWord){"ta", "as", Preposition|Conjunction, 9, false});
    out.append((angelicalWord){"tabaan", "governor", Noun, 1, false});
    out.append((angelicalWord){"tabaord", "(be) governed", Verb, 1, false});
    out.append((angelicalWord){"tabaori", "to govern", Verb, 1, false});
    out.append((angelicalWord){"tabas", "to govern", Verb, 0, true});
    out.append((angelicalWord){"tabges", "caves", Noun, 1, false});
    out.append((angelicalWord){"tablior", "as comforters", Compound, 1, false});
    out.append((angelicalWord){"tad", "as the third", Compound, 0, true});
    out.append((angelicalWord){"tage", "as (is) not", Compound, 1, false});
    out.append((angelicalWord){"tal", "Letter M", ProperNoun, 0, false});
    out.append((angelicalWord){"talho", "cups", Noun, 1, false});
    out.append((angelicalWord){"talo", "as the first", Compound, 1, false});
    out.append((angelicalWord){"talolcis", "as bucklers", Compound, 1, false});
    out.append((angelicalWord){"tan", "the seventh aethyr", Noun, 1, false});
    out.append((angelicalWord){"tapuin", "as (sharp) sickles", Unknown, 1, false});
    out.append((angelicalWord){"taqanis", "as olives", Compound, 1, false});
    out.append((angelicalWord){"tastax", "going before (to procede)", Verb, 1, false});
    out.append((angelicalWord){"tatan", "wormwood", Noun, 1, false});
    out.append((angelicalWord){"taviv", "as the second", Compound, 1, false});
    out.append((angelicalWord){"taxs", "7336", Number, 1, false});
    out.append((angelicalWord){"teloah", "death", Noun, 1, false});
    out.append((angelicalWord){"teloch", "death", Noun, 2, false});
    out.append((angelicalWord){"telocvovim", "death dragons", Compound|ProperNoun, 1, false});
    out.append((angelicalWord){"tex", "the thirteenth aethyr", Noun, 1, false});
    out.append((angelicalWord){"thil", "seats", Noun, 2, false});
    out.append((angelicalWord){"thild", "seats", Noun, 1, false});
    out.append((angelicalWord){"thiln", "seats", Noun, 0, true});
    out.append((angelicalWord){"thilnos", "12 seats (of)", Compound, 1, false});
    out.append((angelicalWord){"ti", "it is", Compound, 1, false});
    out.append((angelicalWord){"tia", "unto (us)", Preposition, 1, false});
    out.append((angelicalWord){"tianta", "bed", Noun, 1, false});
    out.append((angelicalWord){"tibibp", "sorrow", Noun, 1, false});
    out.append((angelicalWord){"tilb", "her", Adjective|Pronoun, 1, false});
    out.append((angelicalWord){"tiobl", "(within) her", Pronoun, 2, false});
    out.append((angelicalWord){"tliob", "to separate (classify?) creatures", Verb, 1, false});
    out.append((angelicalWord){"toatar", "harken, to listen", Verb, 1, false});
    out.append((angelicalWord){"tofglo", "all (things)", Noun, 1, false});
    out.append((angelicalWord){"toh", "to triumph", Verb, 1, false});
    out.append((angelicalWord){"toha", "(my) triumph", Noun, 0, true});
    out.append((angelicalWord){"tohcoth", "nature spirits", Noun, 0, false});
    out.append((angelicalWord){"tolham", "all creatures", Noun, 1, false});
    out.append((angelicalWord){"tolhami", "(upon) all creatures", Noun, 1, false});
    out.append((angelicalWord){"toltorg", "creatures", Noun, 1, false});
    out.append((angelicalWord){"toltorgi", "(with) creatures", Noun, 1, false});
    out.append((angelicalWord){"toltorn", "creature", Noun, 0, true});
    out.append((angelicalWord){"ton", "all", Adjective, 1, false});
    out.append((angelicalWord){"tonug", "to deface", Verb, 1, false});
    out.append((angelicalWord){"tooat", "to furnish", Verb, 1, false});
    out.append((angelicalWord){"tor", "the twenty-third aethyr", ProperNoun, 1, false});
    out.append((angelicalWord){"tor", "to rise(?)", Verb, 0, true});
    out.append((angelicalWord){"torb", "one hundred", Number, 1, false});
    out.append((angelicalWord){"torgu", "arise", Verb, 1, false});
    out.append((angelicalWord){"torzu", "arise", Verb, 3, false});
    out.append((angelicalWord){"torzul", "(shall) arise", Verb, 1, false});
    out.append((angelicalWord){"torzulp", "to rise", Verb, 1, false});
    out.append((angelicalWord){"tox", "his, him", Pronoun|Adjective, 1, false});
    out.append((angelicalWord){"tranan", "marrow", Noun, 1, false});
    out.append((angelicalWord){"trian", "shall be", Verb, 2, false});
    out.append((angelicalWord){"trint", "to sit", Verb, 1, false});
    out.append((angelicalWord){"trof", "as building", Noun|Verb, 1, false});
    out.append((angelicalWord){"turbs", "(in) beauty", Noun, 1, false});

    return out;
}

QList<angelicalWord> words_u() {
    QList<angelicalWord> out;

    out.append((angelicalWord){"ucim", "frown not (to smile)", Verb, 1, false});
    out.append((angelicalWord){"udl", "the rest", Noun, 0, true});
    out.append((angelicalWord){"ugear", "strength (of men)", Noun, 1, false});
    out.append((angelicalWord){"ugeg", "become strong", Verb, 1, false});
    out.append((angelicalWord){"Ugegi", "to wax (become) strong", Verb, 1, false});
    out.append((angelicalWord){"ul", "end", Noun, 1, false});
    out.append((angelicalWord){"ulcinin", "happy", Adjective, 1, false});
    out.append((angelicalWord){"uls", "ends", Noun, 1, false});
    out.append((angelicalWord){"umadea", "strong towers", Noun, 1, false});
    out.append((angelicalWord){"uml", "to add", Verb, 1, false});
    out.append((angelicalWord){"umplif", "strength", Noun, 1, false});
    out.append((angelicalWord){"un", "Letter A", ProperNoun, 0, false});
    out.append((angelicalWord){"unal", "these", Pronoun, 1, false});
    out.append((angelicalWord){"unalah", "skirts", Noun, 1, false});
    out.append((angelicalWord){"unalchis", "these are", Compound, 1, false});
    out.append((angelicalWord){"unchi", "to confound", Verb, 1, false});
    out.append((angelicalWord){"undl", "the rest", Noun, 1, false});
    out.append((angelicalWord){"unig", "to require", Verb, 1, false});
    out.append((angelicalWord){"uniglag", "to descend", Verb, 1, false});
    out.append((angelicalWord){"upaah", "wings", Noun, 3, false});
    out.append((angelicalWord){"upaahi", "wings", Noun, 1, false});
    out.append((angelicalWord){"ur", "Letter L", ProperNoun, 0, false});
    out.append((angelicalWord){"uran", "elders", Noun, 1, false});
    out.append((angelicalWord){"urbs", "to beautify", Verb, 1, false});
    out.append((angelicalWord){"urch", "the confusers", ProperNoun, 0, true});
    out.append((angelicalWord){"urelp", "(a strong) seething", Noun, 1, false});
    out.append((angelicalWord){"uta", "the fourteenth aethyr", ProperNoun, 1, false});
    out.append((angelicalWord){"uti", "the twenty-fifth aethyr", ProperNoun, 1, false});
    out.append((angelicalWord){"ux", "42", Number, 1, false});

    return out;
}

QList<angelicalWord> words_v() {
    QList<angelicalWord> out;

    out.append((angelicalWord){"vaa", "Vaa, the last of first, of the fourth", ProperNoun, 1, false});
    out.append((angelicalWord){"vabzir", "the eagle", Noun, 1, false});
    out.append((angelicalWord){"van", "Letter U/V", ProperNoun, 0, false});
    out.append((angelicalWord){"vaoan", "truth", Noun, 1, false});
    out.append((angelicalWord){"varpax", "n/a", Unknown, 0, false});
    out.append((angelicalWord){"vasedg", "n/a", Unknown, 1, false});
    out.append((angelicalWord){"vau", "to work", Verb, 0, true});
    out.append((angelicalWord){"vaulzirn", "work wonders", Compound, 1, false});
    out.append((angelicalWord){"vaun", "to work", Verb, 1, false});
    out.append((angelicalWord){"viiv", "second", Noun, 1, false});
    out.append((angelicalWord){"virq", "nests", Noun, 1, false});
    out.append((angelicalWord){"viv", "second", Noun, 1, false});
    out.append((angelicalWord){"vivdiv", "second angle", Compound, 1, false});
    out.append((angelicalWord){"vivialprt", "second flame", Compound, 1, false});
    out.append((angelicalWord){"vnph", "anger", Noun, 0, true});
    out.append((angelicalWord){"vohim", "mighty", Adjective, 1, false});
    out.append((angelicalWord){"vomzarg", "every one, all", Pronoun, 1, false});
    out.append((angelicalWord){"vonin", "dragons", Noun, 0, true});
    out.append((angelicalWord){"vonph", "wrath", Noun, 1, false});
    out.append((angelicalWord){"vonpho", "(of) wrath", Noun, 2, false});
    out.append((angelicalWord){"vonpo", "wrath", Noun, 0, true});
    out.append((angelicalWord){"vonpovnph", "wrath in anger", Compound, 0, true});
    out.append((angelicalWord){"vooan", "truth", Noun, 1, false});
    out.append((angelicalWord){"vors", "over", Preposition, 1, false});
    out.append((angelicalWord){"vorsg", "over (you)", Preposition, 1, false});
    out.append((angelicalWord){"vovim", "dragon", Noun, 0, true});
    out.append((angelicalWord){"vovina", "dragon", Noun, 1, false});

    return out;
}

QList<angelicalWord> words_w() {
    QList<angelicalWord> out;

    return out;
}

QList<angelicalWord> words_x() {
    QList<angelicalWord> out;

    return out;
}

QList<angelicalWord> words_y() {
    QList<angelicalWord> out;

    out.append((angelicalWord){"yarry", "providence", Noun, 1, false});
    out.append((angelicalWord){"yl", "thy", Pronoun, 0, true});
    out.append((angelicalWord){"yolcam", "bring forth, to bear", Verb, 1, false});
    out.append((angelicalWord){"yolci", "to bring forth", Verb, 1, false});
    out.append((angelicalWord){"yor", "to roar", Verb, 1, false});
    out.append((angelicalWord){"yrpoil", "division", Noun, 1, false});
    out.append((angelicalWord){"vep", "flame", Noun, 1, false});
    out.append((angelicalWord){"", "", Unknown, 1, false});
    out.append((angelicalWord){"", "", Unknown, 1, false});
    out.append((angelicalWord){"", "", Unknown, 1, false});
    out.append((angelicalWord){"", "", Unknown, 1, false});

    return out;
}

QList<angelicalWord> words_z() {
    QList<angelicalWord> out;

    out.append((angelicalWord){"za", "n/a", ProperNoun, 0, false});
    out.append((angelicalWord){"zaa", "the seventh aethyr", ProperNoun, 1, false});
    out.append((angelicalWord){"zacam", "to move", Verb, 1, false});
    out.append((angelicalWord){"zacar", "move", Verb, 8, false});
    out.append((angelicalWord){"zamran", "to appear", Verb, 7, false});
    out.append((angelicalWord){"zax", "the tenth aethyr", ProperNoun, 1, false});
    out.append((angelicalWord){"zchis", "(they) are", Verb, 3, false});
    out.append((angelicalWord){"zed", "n/a", Unknown, 0, false});
    out.append((angelicalWord){"zen", "the eighteenth aethyr", ProperNoun, 1, false});
    out.append((angelicalWord){"zid", "the eighth aethyr", ProperNoun, 1, false});
    out.append((angelicalWord){"zien", "hands", Noun, 1, false});
    out.append((angelicalWord){"zildar", "fly into", Verb, 1, false});
    out.append((angelicalWord){"zilodarp", "stretch forth, conquest", Noun, 0, true});
    out.append((angelicalWord){"zim", "the eighteenth aethyr", ProperNoun, 1, false});
    out.append((angelicalWord){"zim", "entrance(?) or territory(?)", ProperNoun, 1, false});
    out.append((angelicalWord){"zimii", "to enter", Verb, 1, false});
    out.append((angelicalWord){"zimz", "Vestures, territories", Noun, 1, false});
    out.append((angelicalWord){"zip", "the ninth aethyr", ProperNoun, 1, false});
    out.append((angelicalWord){"zir", "am, were, was", Verb, 2, false});
    out.append((angelicalWord){"zirdo", "am", Verb, 1, false});
    out.append((angelicalWord){"zirenaiad", "I am the Lord (your) God", Compound, 1, false});
    out.append((angelicalWord){"zirom", "were", Verb, 1, false});
    out.append((angelicalWord){"zirop", "was", Verb, 1, false});
    out.append((angelicalWord){"zixlay", "to stir up", Verb, 1, false});
    out.append((angelicalWord){"zizop", "vessels", Noun, 1, false});
    out.append((angelicalWord){"zilda", "to water", Verb, 1, false});
    out.append((angelicalWord){"zna", "motion, action", Adjective, 0, true});
    out.append((angelicalWord){"znrza", "to swear", Verb, 1, false});
    out.append((angelicalWord){"zol", "hands", Noun, 1, false});
    out.append((angelicalWord){"zom", "the third aethyr", ProperNoun, 1, false});
    out.append((angelicalWord){"zom", "to encompass(?)", Verb, 0, true});
    out.append((angelicalWord){"zomdux", "amidst, encomapssed by", Preposition, 1, false});
    out.append((angelicalWord){"zonc", "appareled", Verb, 1, false});
    out.append((angelicalWord){"zong", "winds", Noun, 1, false});
    out.append((angelicalWord){"zonrensg", "to deliver", Verb, 1, false});
    out.append((angelicalWord){"zorge", "be friendly unto me", Verb, 1, false});
    out.append((angelicalWord){"zumvi", "seas", Noun, 1, false});
    out.append((angelicalWord){"zuraah", "prayer(?)", Unknown, 0, false});
    out.append((angelicalWord){"zurah", "n/a", Unknown, 0, false});
    out.append((angelicalWord){"zure", "n/a", Unknown, 0, true});
    out.append((angelicalWord){"zuresch", "n/a", Unknown, 0, true});
    out.append((angelicalWord){"zylna", "within (itself)", Preposition, 1, false});

    return out;
}

QList<angelicalWord> wordList()
{
    QList<angelicalWord> words;
    words.append(words_a());
    words.append(words_b());
    words.append(words_c());
    words.append(words_d());
    words.append(words_e());
    words.append(words_f());
    words.append(words_g());
    words.append(words_h());
    words.append(words_i());
    words.append(words_j());
    words.append(words_k());
    words.append(words_l());
    words.append(words_m());
    words.append(words_n());
    words.append(words_o());
    words.append(words_p());
    words.append(words_q());
    words.append(words_r());
    words.append(words_s());
    words.append(words_t());
    words.append(words_u());
    words.append(words_v());
    words.append(words_w());
    words.append(words_x());
    words.append(words_y());
    words.append(words_z());
    return words;
}

QMultiMap<QString, angelicalWord> dictionary()
{
    QMultiMap<QString, angelicalWord> out;
    foreach( angelicalWord word, wordList() ) {
        out.insert(word.word,word);
    }
    return out;
}

#endif // DICTIONARY_H
