#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QFile>
#include <QDebug>
#include "database.h"

Database::Database()
{
}

QList<QString> Database::uniqueLetterCombinations(Locations locations, int number)
{
    QSqlDatabase db = QSqlDatabase::database();
    QList<QString> out;
//    if(locations & Loagaeth) {
//        if(number == -1) {
            QSqlQuery combinationsQuery("select distinct COMBINATION from COMBINATIONS ORDER BY COMBINATION ASC;",db);
            if(!combinationsQuery.exec()) {
                qDebug() << "query failed";
                return out;
            }
            while(combinationsQuery.next()) {
                out << combinationsQuery.value(0).toString();
            }
//        }
//    }
    return out;
}

bool Database::setFilename(const QString &filename)
{
    if(m_filename != filename) {
        emit status(tr("Opening database file %1...").arg(filename));
        QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
        db.setDatabaseName(filename);
        if(!db.open()) {
            emit error(tr("Could not open database file %1").arg(filename));
            m_filename.clear();
            return false;
        }
        emit status(tr("Opened database file %1").arg(filename));
        m_filename = filename;
    }
    emit error(tr("Database is already open."));
    return true;
}

void Database::rebild()
{
    clear();
    createStructure();
    populateAllTables();
}

void Database::clear()
{
    if(m_filename.isEmpty()) {
        emit error(tr("No database file specified."));
        return;
    }
    QString oldFilename(m_filename);
    QFile dbFile(m_filename);
    if(dbFile.exists()) {
        if(dbFile.remove()) {
            m_filename.clear();
            setFilename(oldFilename);
            emit status(tr("Database data cleared."));
            return;
        }
        else {
            emit error(tr("Could not delete database file %1").arg(m_filename));
            return;
        }
    }
    else {
        emit error(tr("Database file doesn't exist."));
        return;
    }
}

void Database::createStructure()
{
    QSqlDatabase db = QSqlDatabase::database();
    emit status(tr("Creating database tables..."));
    db.exec("CREATE TABLE COMBINATIONS("
            "COMBINATION    TEXT    NOT NULL," //The letter combination
            "TABLENUMBER    INT     NOT NULL," //The table number (0-98)
            "ROWNUMBER      INT     NOT NULL," //The row number (0 indexed)
            "STARTCOLUMN    INT     NOT NULL," //The column the combination starts in (0 indexed)
            "ENDCOLUMN      INT     NOT NULL"  //The column the combination ends in (0 indexed)
            ");");
    if(db.lastError().isValid()) {
        emit error(tr("Database error: %1").arg(db.lastError().text()));
        return;
    }

    db.exec("CREATE TABLE SCORES("
            "COMBINATION   TEXT     NOT NULL," //The letter combination
            "LENGTH        INT      NOT NULL," //Length of the combination
            "FREQUENCY     INT      NOT NULL," //Frequency in Loagaetch
            "INCALLS       INT      NOT NULL," //Frequency in the 48 calls
            "SCORE         INT      NOT NULL"  //The computed score
            ");");
    if(db.lastError().isValid()) {
        emit error(tr("Database error: %1").arg(db.lastError().text()));
        return;
    }

    db.exec("CREATE INDEX idx_COMBINATIONS ON COMBINATIONS (COMBINATION ASC);");
    if(db.lastError().isValid()) {
        emit error(tr("Database error: %1").arg(db.lastError().text()));
        return;
    }

    db.exec("CREATE INDEX idx_COMBINATIONS2 ON COMBINATIONS (COMBINATION ASC, TABLENUMBER ASC);");
    if(db.lastError().isValid()) {
        emit error(tr("Database error: %1").arg(db.lastError().text()));
        return;
    }

    db.exec("CREATE INDEX idx_SCORES_COMBO ON SCORES (COMBINATION ASC);");
    if(db.lastError().isValid()) {
        emit error(tr("Database error: %1").arg(db.lastError().text()));
        return;
    }

    db.exec("CREATE INDEX idx_SCORES_LENGTH ON SCORES (LENGTH ASC);");
    if(db.lastError().isValid()) {
        emit error(tr("Database error: %1").arg(db.lastError().text()));
        return;
    }

    db.exec("CREATE INDEX idx_SCORES_FREQUENCY ON SCORES (FREQUENCY ASC);");
    if(db.lastError().isValid()) {
        emit error(tr("Database error: %1").arg(db.lastError().text()));
        return;
    }

    db.exec("CREATE INDEX idx_SCORES_INCALLS ON SCORES (INCALLS ASC);");
    if(db.lastError().isValid()) {
        emit error(tr("Database error: %1").arg(db.lastError().text()));
        return;
    }

    db.exec("CREATE INDEX idx_SCORES_SCORE ON SCORES (SCORE ASC);");
    if(db.lastError().isValid()) {
        emit error(tr("Database error: %1").arg(db.lastError().text()));
        return;
    }

    emit status(tr("Database tables created."));
}

void Database::populateAllTables()
{

}

void Database::populateCombinations()
{

}

void Database::populateScores()
{

}
