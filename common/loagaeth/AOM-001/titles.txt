01A:
01B:
02A: alla opnay qviemmah. [ "alla opnay qveemmah" ]
02B: zvrebth aho dan lanfal cramza [ "zurebth aho ^ lanfal g? ramza" ]
03A: pandobna ox adroh azimcholdrux. [ "pandôbna o~ x á droh á zim' chol drux" ]
03B: dlod Alged zvrem. [ "dlod allged zvram' " ]
04A: Zvbla ox arnogan Algers aclo. [ "zvbla ôx arnó gan algé rs aclô" ]
04B: Danfal gest Axamph acrosta. [ "dá nfal gest á xamph acrôsta" ]
05A: Gonzahoh alch arge oho Adanch. [ "gonzahoh alch argé oho~ á danch" ]
05B: Zvchastors plohodmax argednon acho [ "zuchá stors plôhodmax argé dnon á cho" ]
06A: Sancgonfal aldex, Ave goh adatqvan, [ "sá ncgonfal á ldex á ue gôh adá tquan" ]
06B: pvrcha ges maxgem adroth vaxox ahó [ "purchá ges' m' á xgem adrôth vá xox ahô" ]
07A: Dam lethgath onzar avoxalgeth [ "dam lé thgath onzá r auoxá lgeth" ]
07B: chvmaxchaberexmapha [ "chumá xchabé ré xmapha" ]
08A: algebadreth [ "á lgé ba~ dré th" ]
08B: Oylzongs [ "ôylzõngs" ]
09A: pagesgem [ "págésgém^" ]
09B: Avallacax [ "auállãcãx" ]
10A: Gorvemgemps [ "goruémgém^ ps" ]
10B: Bacap Laffos [ "bãcãp l áffos" (sic) ]
11A: Ozimba londorh [ "ôzim^ ba lôndôrh" ]
11B: ylchvzzapg [ "ylchvzzâpg" ]
12A: Nopham [ "nôphám" ]
12B: Signeh gax [ "signéh gâx" ]
13A: t-lallaah gethnoh [ "tallaah géthnôh" ]
13B: Iaialgh lercol zinrox
14A: Pincal vexlan
14B: Phin potagar giron
15A: Se ger pcopalph
15B: Oroh Zvn.compvxoh
16A: Dadavar gedrong
16B: varahhatraglax pligeo
17A: Hidrahah glazipvagel
17B: Engidexol neolchiph
18A: Polacax cvbagod
18B: Zad, ron anchal
19A: Gedmarg alpon
19B: Bvzalg long arnap
20A: Zicha lezach
20B: Drem phingel oxah oho
21A: algonzib virbalox
21B: Avriz ommaphily geld
22A: Cehergol naoal
22B: Fál mexicamps vrom
23A: Conar vomfagal
23B: Toxarxh nerhoh gel podnon
24A: Zichidpha lvziph
24B: Nervag pranclan
25A: Demphoz prang oho
25B: Harodan lempric dohoh
26A: Chy pled sagnaronph
26B: Draxph intayfalg
27A: Vlnen razo vilcomb
27B: Vincal leorna rvh
28A: Dababel gel zozaah
28B: Larvh gohonp babbabor
29A: Famfax lep axax.
29B: Zirzach bvmazon
30A: Tar, vin gabax orho
30B: Glonz alnoptd
31A: Gemnarv Hvncol
31B: Rynh zichzor chalan
32A: yayger balpaoeh
32B: Car vanal geldons
33A: Vio nilg onpho
33B: Toxhencol ylnorox ziborh
34A: Balvomph chiphan
34B: Vingelg laxih parcan
35A: Zvda vig pancar
35B: Dexvlng chirony gavv
36A: Qnabazed vil pvdar
36B: Xanpa phaphingeth
37A: Ronlox bapvabap orh
37B: Calbahhah genrox
38A: Dohvnam gethgol axah
38B: Vantavong nargax
39A: Pvlgaah ner gisch
39B: Archi septh lorox
40A: Damponpha nexo gel
40B: Dexph geld onchen
41A: Ellaxor Natoglan
41B: Fam filgisch larvouch
42A: Cemgealg ralphos
42B: Zodagrap zilpob
43A: Necprilga lvpvarn
43B: Depsah onge phialox
44A: Nelziar pol dolgon
44B: Parni volchemph
45A: Acvirzilg chiparal
45B: Alged on chipráxal
46A: Clarn nancal
46B: Lexrox pingh lardol
47A: Zvrzvh genvox
47B: Chiromonph zarchan olinorg
48A: Calgs sedph panglox
48B: Bapporgel bvrioldepnay
49:
