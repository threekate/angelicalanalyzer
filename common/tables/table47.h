#ifndef TABLE47_H
#define TABLE47_H
#include <QList>

QList<QString> table47a(){
    QList<QString> out;
    out << "az";
    out << "loga";
    out << "dexopu";
    out << "hvgrunga";
    out << "lexorunadu";
    out << "phicholaxoro";
    out << "zidbalgrunphux";
    out << "lemgebeduxnelzoc";
    out << "chidrunphungephdul";
    out << "yxurnu'bu'rgdethcongal";
    out << "lvduxabothuxlethgrunom";
    out << "axrepogaldextalophinador";
    out << "geonavaxlexargnelpiodrunch";
    out << "arnetalzichlaxarnopthadroxur";
    out << "nervaxalunphdethcolnurpchirodu";
    out << "phioglaxdeothalbruxdemnubluntoxa";
    out << "neophirucher?unvaldemuronpzirchu'lg";
    out << "vnvoxubbodelarnupporundexturgvxarnol";
    out << "pargalangvarzechlorichlamuxapolgarbdur";
    out << "chvrunchzilchuxabrunphurolpdextorudgeona";
    out << "vilcholadornavorgdulbaxlethgompthgerbruxox";
    out << "arnothumnogulleprouchchipodruphlerdonappotha";
    out << "regemptaxalapurunphchidnopurglebornu'nchonduxor";
    out << "nerboroxalgalglethnurgdeoncharnumphu'ppvrubruxnal";
    out << "aglexolalpgendrunnexolungavaxremavihchirdornamph";
    out << "olphigonuxdetharganzachchironpurgonnabbvruxlap";
    out << "gethnargdemnochalblothurcvthgethgalphichurno";
    out << "deopharglemnoganrvxodurbvu'xlonchzichorunch";
    out << "denhondrobvirnoxzvrvchilorfacvmpnaplunga";
    out << "nemanpungeronalbanchdichargeonchconvon";
    out << "dedohurbvixnalgeophunnepthvruphruxal";
    out << "virufargdexnupvizubuldroxzinvuncho";
    out << "piduboncharzunbdvrunphchilontuxo";
    out << "nagvrununvunchzivrvicloviviach";
    out << "zidrumphulgothchurnonloxarno";
    out << "pivu'draxlexarbvirnaxlegont";
    out << "pachustorzamblothdexanga";
    out << "vitobluxvxniholpranzib";
    out << "duxaronglephigozelva";
    out << "crinoxalpongepuxar";
    out << "exaponepzirdnxre";
    out << "phichussemeron";
    out << "du'xlabongala";
    out << "zibomunold";
    out << "chipuged";
    out << "dvlvam";
    out << "orno";
    out << "de";
    return out;
}

QList<QString> table47b(){
    QList<QString> out;
    out << "xi";
    out << "rgeo";
    out << "pvrana";
    out << "decorgal";
    out << "hidorophun";
    out << "zalpulgdexto";
    out << "uxamedulphlexo";
    out << "necrohgenzipharn";
    out << "alnorphdvvanglouxa";
    out << "phadolmuxgethnabalan";
    out << "viodruxlebronurdvrzanc";
    out << "chaduvurgnexophirinnanzi";
    out << "rednexapolgrapdexolalnorga";
    out << "zidruphurcludonvungnecrohleb";
    out << "vuntompgelphzarrunchu'lonphduxo";
    out << "nurvochuphundermu'xtepotrunurbalg";
    out << "liduncrunzvchapolydnopthulvungleno";
    out << "phicuxremaroxvlchexlurvanchdexolphia";
    out << "lvmurchdethcorgemphlanchoxzidopolphurg";
    out << "lanqvozichfimagelpoduchironnarcszidbraxt";
    out << "vortnoxglvganphzebrubruchzvrgegolpzachsvrg";
    out << "phimnaxploxrecrardexmalonderqnoptzibvlvargeb";
    out << "vurneydunpumgelduvorchzichrozirnoptzubvhohchur";
    out << "rexnopphicongnahaheonzipolumuchexdexnolagripalga";
    out << "vichornaldexoronclethbvrnzadroxzirvahgvlumphaxor";
    out << "decosvixorneldrunchlexorgnebronphingenphzadcha";
    out << "remugemphzalhixnorgrepungabvdoxalneffrogenph";
    out << "dexolnachlanphzicarnabaldexorbvonhornzanva";
    out << "chvdupurghlexarbivodovachgethbrunvrgbana";
    out << "dochavanlemoxaronphduxalpzirdoptharget";
    out << "zacarizahgethmunczeldolvundvxnargaar";
    out << "vingalpoxrongdextolphixardeclumner";
    out << "phiangaolphelgarbvnachu'domnabonb";
    out << "argednopzidruxupvolgethzircoth";
    out << "allvbavunpdelclungzirnobaxal";
    out << "phichastorremagemphalzedph";
    out << "arnobuvolchuldunpargeong";
    out << "nuxalloxdethcormucoxta";
    out << "phichuphurnepothnuru";
    out << "dexcoldurvanchlemb";
    out << "axupoholmichanph";
    out << "dethconchlexoi";
    out << "zecongepaldo";
    out << "vicharreon";
    out << "anchalde";
    out << "puldal";
    out << "getl";
    out << "zv";
    return out;
}

QList< QList<QString> > table47() {
    QList< QList<QString> > out;
    out << table47a();
    out << table47b();
    return out;
}

#endif // TABLE47_H
