#ifndef TABLE9_H
#define TABLE9_H
#include <QList>

QList<QString> table9a(){
    QList<QString> out;

    out << "1234567sednachanzaclanzabvlachodangahzvcha2345678";
    out << "2134567lathnaclongatoxardnachaphnodolphaho3245678";
    out << "3214567archanvahgenodalzachenachanzacladon4325678";
    out << "4321567orgednachalzanchalabvzachefnoxadnar5432678";
    out << "5432167voltiblahnoxdargephaldvrganzachyoxa6543278";
    out << "6543217plazengethaldextohvorthangephadonge7654328";
    out << "7654321nazaldahzanchanzaoldangalahvonroxap8765432";
    out << "axarmaclohnotaxvargemnatophadolgemphadonchadonzah";
    out << "parchephnochadnadonclazangehozadmachvaldehadarged";
    out << "volzangephaclanzacloxarnahvohadronchyadnahvotarge";
    out << "pragednolganzanclaoxarnachovongangaprahlodnaxorah";
    out << "vonzangephazolmahzvreblohaclaxnochappangensanolsa";
    out << "vaxargempadolzachephaxardrahgednaclanvartesnochar";
    out << "radoxagranzaclanvongiblesadrvngahvolzyblohacharso";
    out << "algemenohadnacodvonoxg121234oedaenoadonzachadnaca";
    out << "abvargednopahoadras78IBAGAFA56cagafolchapnadoxard";
    out << "delchansanoxahpras6IBAGAFAIBAG7alghvomfangetoxage";
    out << "vorgemvagehadoton5A6543217654FA8ofaalothagethnoga";
    out << "acladaxvarteohna4IA6543217654AGA9tapradantageldaa";
    out << "achanzachephaho3F666543217654333A1bolzanchadaxorg";
    out << "nolzadadraxpahx2A777aulauaxar777I2oalbvzanchahoda";
    out << "zyblachephango1AI111sBnaBcvBl666BA3nobvlagesmacoh";
    out << "algadnaclansam9IB222ooOdOaOam555AB4vandabrahadmac";
    out << "domzibladaxvro8BA333gxaRRReth444GA5vlzangephaxarg";
    out << "algednopalgeta7AG444aBORNOGOd333AG6nochanaldaphna";
    out << "pagentaphengoh6GA555ngaOOOpha222FA7geongeporaxarg";
    out << "alzadchardaxol5AF666ddGnGaGaq111AF8notaslaphanagl";
    out << "plvduachanpoonAFA777aOoxOamOa777IA9onphangephalze";
    out << "orchanzandalala3I111qantalget666B1aocardaxadmache";
    out << "ondimachesdohox2B222345671234565A2cneoplaxvargeta";
    out << "apragensodahopon1BF23456712345AA3odolzachanzangeh";
    out << "myclahhvdrabahona9A23456712345F4anapolhadganvarte";
    out << "gongergalaxarotaxr8BAGAFAIBAGI5oxadolzadnahziblah";
    out << "tantogapvargemaltaz76AFAIBAG76clanggolzadnahgebar";
    out << "arlaovagephadednoclad5432198fadoxahchodnocapvargo";
    out << "nolzadnachephaldedmacheolchadnadoxarnepacheldaxor";
    out << "valgedadrvxadnodlaphzvgalgahlonzanchanachansanols";
    out << "apransanadarcholzadnahachirahdoxaldagahzangephnon";
    out << "orcladorvorbemaradontagelaxarnohprodalgarozangefa";
    out << "laxardroamnachondanclaxazvrbbalohadnodanorgephals";
    out << "argladronclanzadroxzinchaadranchlogedgaachadnacho";
    out << "valchepnadoxargempnaphealbvndagedlohaxargehnotals";
    out << "2345678sanglifafrangehnoxaldehgebladahocha7654321";
    out << "3245678londansadantalmahochansanoxalgethvo6543217";
    out << "4325678gempnadoxarchansanapvradednolatonsa5432167";
    out << "5432678englorzabmvchaaphinodalzadcharzahod4321567";
    out << "6543278aglancladonvanrablidoxagephacladnoh3214567";
    out << "7654328gethvdradolziblaxvorchapnachaxaldla2134567";
    out << "8765432noxtadgmaolzangeolchadnaphavonronge1234567";

    return out;
}

QList<QString> table9b(){
    QList<QString> out;

    out << "olgathanedganzamachadnachoalzachadaxparonaschalda";
    out << "mayaphinochaphblardongansadnageachadnahgalsapaxah";
    out << "gehadnazenachandazazochalzachanlocharmachalpalgeh";
    out << "achirdaxalgabrodnaphaaxargedalodnaxaphagendaxargh";
    out << "lochadnahvoxardagagonziblazanchvonzidonacholchans";
    out << "voxordranaxpalchadnazadolzichadmachervangenadnaho";
    out << "dorgednaphanedolgapathathemblohgarzadnachanvolzah";
    out << "voltafnaxzorgalsahnoxadnahvontomblohaxargedethnoh";
    out << "goldaxvornaxprangendachalondaxorchadnachamvangert";
    out << "omordalpagnaplasvolsebladonzvrehacladanzavorchads";
    out << "vorsanmahvsadnachalzagiblohaphradmahnochastorador";
    out << "phadanzvreghcheldaxcladexcoapvlzadahnodanvagaoxar";
    out << "zvrathaladnamvontalpaxorzangenalzvchonodomvahplic";
    out << "noxofolzinchadmachephnaphinadaxplaschoalphosarnox";
    out << "arneyaxalgethadradnavonsangladolgeffadospricalmoh";
    out << "nevodmahvetagimnapholadraxvblahgeltholachandoxollz";
    out << "varchenzaphahnoladnachaoxarnabehvalzaganocchidona";
    out << "vottvplesapresmoachenvahnoxadnamvachastorcalbalge";
    out << "vartoxanagedvompongraphasclvmadaxnoxagreondrohvol";
    out << "zingethadanzacladazadaxvorgendahnochanzazaclonaci";
    out << "doddormachvalgenlvdasyprehvoxylgadeorgaphamonsoan";
    out << "damnadnageongaladvfaynatonevmaclapqvergehnaxvarod";
    out << "panfegehdorchansanogeldazanchvraholzadnarzvchalzo";
    out << "arphimacherolchapplachastorymnadapheolchadvdradoh";
    out << "comgelangvngalaxarnachorpangendogadloxlonvarteffa";
    out << "onchanphaneodanzagefaldvnzachadzydamnachonzidnasf";
    out << "aldadgeprahgedlocaphaldraydalahgensonacholdanneca";
    out << "lohodnadoxpragmachodaxvargefaffynahgeongafanoholz";
    out << "admichabvolzenbahgalpohvrnodragohvartafeoxargerfa";
    out << "londelappugnegonagvndranganfalsahoxartahvolzantax";
    out << "chrantalzabaxartrahnochastorpragendonachephnadabA";
    out << "chvrnachefalzendagemnolhodnageralzedaproxcavolath";
    out << "agignadaxvargedoxpalchefforvrdranvagempancholahod";
    out << "vdranvagehgascohalchynzadahvdrastnotaxvanvnchalph";
    out << "vocasternavordachafoxardagennolzavorgempnochaphan";
    out << "lvncavobnamqvatolzabblgexgerdaroarchandahvontanth";
    out << "zendaxvaronzenbulmehachednahiehadnoalhydblaxordro";
    out << "amnadvagendcanglahgeodmahcansaxvaroxangethalidvah";
    out << "colzadnachefadmagelorchonzadonhidraxvargenvahodah";
    out << "nolhaxmaclaxpronsanplaqragnaglexorgehvaxargethals";
    out << "adglaxodentalpagednoadexochanzeaphyoclazagemnaxoh";
    out << "voialdalahonchaltahlexohnongranvaopxanclovdgelgna";
    out << "varargedraorgethandraxolapladfaghenlvnzahabchadva";
    out << "ruhaxalsaxlonclansethandolambalgethachadnvxvorget";
    out << "darziblaxdohanganphaldnxargevobnadahzvnchalaxorga";
    out << "aprischallongedvronaphynsagethladanqvahnotalpahox";
    out << "zanchvchavfalvdrahaprexonzichalgenoxadnavorgemvah";
    out << "proclingephachvlzaclinodorvorgemnealdladorglehvon";
    out << "alcleaxylgeroardradnacheoldapxagnahiehodzanaholno";
    return out;
}


QList< QList<QString> > table9() {
    QList< QList<QString> > out;
    out << table9a();
    out << table9b();
    return out;
}

#endif // TABLE9_H
