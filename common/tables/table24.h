#ifndef TABLE24_H
#define TABLE24_H
#include <QList>

QList<QString> table24a(){
    QList<QString> out;

    out << "hapvanachalzidgoldachiuxnachaphzintogenalhadlacha";
    out << "vartaxandulzugentolaxmadunsadagnochapnuchodnoclux";
    out << "vustulzuchaclodzuchnechurdanonhvdrathaxonulphichu";
    out << "zuntafnaclanyntalAgelnohehebnataxnocronacloxadoth";
    out << "aldoxapongalonchinagruntontagnocharpzintaxoclantu";
    out << "vidablaxnotaxtuloxarnuchathzidnopnlaxzinophunzach";
    out << "ulchidachasvorgefafordothzidnochadantantoxuzilnad";
    out << "volilvachaantagnocharuldethuschunteontugathzaduch";
    out << "aschodnaxtalgethalduntagagalzadonvadrabraxtaiotha";
    out << "nolgethudnobalaxtaxalpeglaxtrantonacheldraburchul";
    out << "nazalgenoxandoxachanvahohanzulbagentuxarnophanchi";
    out << "dalzidubruxachennochaduxvornabruchchinaxtargepang";
    out << "aldolvulassolydnotalaxpichalzichacordunzucolzvrah";
    out << "viandrocothlabagonzvrtaxangraphahvluxorgeodnachup";
    out << "oxurnachgedaxantantaxalatvotabranhvnuduxolchaluge";
    out << "nodranvadaxvbluxachephnachornablexundruhzintulget";
    out << "adorgaluxvdraxocornonzinulvaguthnotublothzinoduxt";
    out << "vargethacharnoxtalzadnonchurvaxpluchastorazontaca";
    out << "nolgephalundagothaclandantuxagolyntonfagathanzoch";
    out << "leotazanchadaxnatoranchiudnatahzinudaplaxpargenta";
    out << "contugethnogalalaxadrudegolzachanchinoxtarganupha";
    out << "alzichantagethagethunlapuxantulgathuxnotalgethaxa";
    out << "vorconbrahzednoxalgraduxvolgethuchepnaxurnochupal";
    out << "adnaxturnoculzintachephagalluxundroxudnachathneot";
    out << "alanzanoturgunaxvaldethachanzadoelzintugenaxalnah";
    out << "lexodnachastruxaprinadabraxylzadchanoxtalgednodux";
    out << "vianzachephudnoxadorpethaclaxaltotathgeongazalzio";
    out << "chidnantagathzidnaxacharnobalzintagnaphutnachanza";
    out << "adnazantolvdraxudnaphalexadnoludringephuxulnachux";
    out << "chiadnahgeondaxalaxtontuxanaphgeontaltuxnoduphans";
    out << "achanzodachchinabulontalgedvanhuduxnoduxyldlachyo";
    out << "talgednocunluphartuntolplidormuclinaxalgethnoxcas";
    out << "traxaplantugnochathnozadnavantuxalgethnodugalzuch";
    out << "uprunzachaalzednodabruxtoteroxzichastorunzadnoxah";
    out << "chivantugnoteronzinachealzednochaxvaltamulziduoph";
    out << "anzintoglaxandaxudurgendoluxturgethnuxadroxagenta";
    out << "pichalzintaxnochutorontaxulgethorgethuldextogurta";
    out << "upurgethonuxtulunchinatonaxtoluxaduntaxnagethalzu";
    out << "clontauxurdruxtalgethuchapnacothclaxurdronchednac";
    out << "asconuxtargennadoxalphadnochanzachhibuthucheladza";
    out << "chionzachadroxadnachephalcydnodanvalgethaxinophur";
    out << "chyrsadgorsadaxluthugnochuteronchivansuchloxandac";
    out << "levandarchansantagelaxardacuxvichontaxudnuchgorta";
    out << "nyxolgradaxtaxtorporonzintuluzvrtogalzidnocuthulo";
    out << "ypnachadoxtalgethroxuclinzuchuphnazadnogulaxardot";
    out << "chantanzinabalatcharsugnochurtulzantagnolziburoth";
    out << "alhanzachchadnopalaxchidontantalaxazintoxnaphanza";
    out << "chiontalmuchantaxandraxnolzidnuchephulcharnocafah";
    out << "geontalchuxtarnuchupvalgednoduxadphungethuntoxurd";
    return out;
}

QList<QString> table24b(){
    QList<QString> out;
    out << "phiontaxagrudraxacontantugnocladunzvchastoryndaxo";
    out << "nozulnocloxzintogalazodnochapalchanoxtoulchadnono";
    out << "vidraxnochastorantantoxaldachanadoxaplazodalcagno";
    out << "ziuntaglaxzinbanotalgednochapapartoxonupviatrazoh";
    out << "nechastorohacladnogentaalglatagnoclonoxaltaperoxa";
    out << "nacladaxtraxonzontalzichanzachadolnachazoclaxvura";
    out << "chephnolduzudrontalugladnochuphucrantuxuldegraxoh";
    out << "vorchanzadnochastoridnochadanzucluxyntlagrudoxara";
    out << "parchadnocladaxvarnograhadnocharylzintagnacloxara";
    out << "gealthurzechudranzachaclaxtapheronsuntagnoxalgaho";
    out << "vioxnadrapheaxulgethnolzidnochuryntoclaxuronzacla";
    out << "axarnaphunephalonzantaxorchadnaxurnobagendeodraxt";
    out << "ziltugethagrannozantaloxardeohnechurchaluxteotrac";
    out << "neopeloxtarchunonanclunchanozanucloxadnogruphaxol";
    out << "adragraxtafrugnolzidnonalgehodunzaglaxybluxtachon";
    out << "chiduxtrohchifrugnolaxidaxurdroxahnochastoracludo";
    out << "zibbalgabuthzibvlantantuxlargednochanchianzochunt";
    out << "viadaclaxtexurnachadnochunuldaxtroclaxarnocanusch";
    out << "lochustrohzantyngalaporgenoxydnacloxarvacohpurcha";
    out << "noxardraxadolgransadoxtalaxneopvantaxachaludubrux";
    out << "nalgethuchandaxneobvadorgenchidaxuronturtaraxtron";
    out << "zanluchaturgeloxydiblaxarcellaynotaxalanzednoxurb";
    out << "cataxadnochephvoltuclantoxarnochephusnodultuxoroh";
    out << "volchefarothnatobladaxvaxernagraxchansadnaphiulca";
    out << "nadvbalaclionzintolchadaxoronzignogrutaxmatacholp";
    out << "vocarpaxnonsoldaxarvdnuchovixutnateolgathneochado";
    out << "adracaxnoltathzidnoxtuhaclaxtorynubvluxornagohulz";
    out << "gethugnoclaxasclontonoxappraxdolzadlaxydortagenza";
    out << "clitaxarohvolgethonabaldaxyndongalaxfiaxdarohzint";
    out << "nomeronalchennapahlochantanaxynsahcluxardroxyblur";
    out << "urnopaletathanuclaxnatorzidnogeltalantophasarchal";
    out << "zidnochasturohvaltublaxphiunzachiubruhgelzemblucu";
    out << "aldonaxurnaphaandonzantagathnotugarnuchabulpvlach";
    out << "zednoprutoxzintolgedathardroxudnachordontagnoxulo";
    out << "pacednochaphalchadaxnpzubnlabvalgethacordacuthzic";
    out << "olzachuclaxtorontantuxaraxgeograxtulanneogaraxpuc";
    out << "lodnogethaxamerontulpathychustorgniabludoxyndutho";
    out << "zinopathzichunzoaldenvahzichastoryntagnocluxavayt";
    out << "tolzaclaxyntogrunadoxvalgethunophanzignabadroxach";
    out << "zicholzalamvigunuhoxaltuplaxloxadnatorpuluxynzuch";
    out << "alexturoudnazalonduzynsapvlantantoxnudluxvidonsac";
    out << "nochonvuchulplagnochastoroxybluxtortofmalodongeot";
    out << "nalgaduxvzachonclanvaxanatonzuchoaldvbranadothanz";
    out << "ychadaxalchonodabrachulzednoparzemnehuzvdaxodolzu";
    out << "chasanchanoltaxonapavolguvaxnotuxarohadnugephanux";
    out << "vichálbaurdrunzábachionzumachlodaxvagenzabbvlaxar";
    out << "nochanzaloxyntolapronachonzidnoxtaladronzubvluxor";
    out << "chinotragunchaprugnadadagenalchednochadnaxaloxtah";
    out << "vorgethaxuclantaxnophururdontugenmalzibathnadoxtu";
    return out;
}

QList< QList<QString> > table24() {
    QList< QList<QString> > out;
    out << table24a();
    out << table24b();
    return out;
}

#endif // TABLE24_H
