#ifndef TABLE49_H
#define TABLE49_H
#include <QList>

QList<QString> table49a(){
    QList<QString> out;

    out << "loagaeth seg loxi brincr";
    out << "larzed dox ner habzilb adnor";
    out << "doncha larb vors hirobra";
    out << "exi ur zedmp taiip chimvane";
    out << "chermach lendix nor zandox";
    return out;
}

QList<QString> table49b(){
    QList<QString> out;
    out << "";
    out << "";
    out << "alla opnay qviemmah";
    out << "zvrebth aho dan lanfal cramza";
    out << "pandobna ox adroh azimcholdrux";
    out << "dlod allged zvrem";
    out << "zvbla ox arnogan algers aclo";
    out << "danfal gest axamph acrosta";
    out << "gonzahoh alch arge oho adanch";
    out << "zvchastors plohodmax argednon acho";
    out << "sancgonfal aldex, ave goh adatqvan";
    out << "pvrcha ges maxgem adroth vaxox ahó";
    out << "dam lethgath onzar avoxalgeth";
    out << "chvmaxchaberexnapha";
    out << "algebadreth";
    out << "oylzongs";
    out << "pagesgem";
    out << "avallacax";
    out << "gorvemgemps";
    out << "bacap laffos";
    out << "ozimba londorh";
    out << "ylchvzzapg";
    out << "nopham";
    out << "signeh gax";
    out << "t-lallaah gethnoh";
    out << "iaialgh lercol zinrox";
    out << "pincal vexlan";
    out << "phin potagar giron";
    out << "se ger pcopalph";
    out << "oroh zvn. compvxoh";
    out << "dadavar gedrong";
    out << "varahhatraglax pligeo";
    out << "hidrahah glazipvagel";
    out << "engidexol; neolchiph";
    out << "polacax cvbagod";
    out << "zad, ron anchal";
    out << "gedmarg alpon";
    out << "bvzalg long arnap";
    out << "zicha lezach";
    out << "drem phingel oxah oho";
    out << "algonzib virbalox";
    out << "avriz ommaphily geld";
    out << "cehergol naoal";
    out << "fál mexicamps vrom";
    out << "conar vomfagal";
    out << "toxarxh nerhoh gel poodnon";
    out << "zichidpha lvziph";
    out << "nervag pranclan";
    out << "demphoz prag oho";
    out << "harodan lempric dohoh";
    out << "chy pled sagnaronph";
    out << "draxph intayfalg";
    out << "vlnen razo vilcomb";
    out << "vincal leorna rvh";
    out << "dababel gel zozaah";
    out << "larvh gohonp babbabor";
    out << "famfax lep axax";
    out << "zirzach bvmazon";
    out << "tar, vin gabax orho";
    out << "glonz alnoptd";
    out << "gemnarv hvncol";
    out << "rynh zichzor chalan";
    out << "yayger balpaoeth";
    out << "car vanal geldons";
    out << "vio nilg onpho";
    out << "toxhencol ylnorox ziborh";
    out << "balvomph chiphan";
    out << "vingelg laxih parcan";
    out << "zvda vig pancar";
    out << "dexvlng chirony gavv";
    out << "qnabazeb vil pvdar";
    out << "xanpa phaphingeth";
    out << "ronlox bapvabap";
    out << "calbahhah genrox";
    out << "dohvnam gethgol axah";
    out << "vantavong natgx";
    out << "pvlgaao ner gisch";
    out << "archi septh lorox";
    out << "damponpha nexo gel";
    out << "dexph geld onchen";
    out << "ellaxor natoglan";
    out << "fam filgisch larvouch";
    out << "cemgealg ralphos";
    out << "zodagrap zilpob";
    out << "necprilga lvpvarn";
    out << "depsah onge phialox";
    out << "nelziar pol dolgon";
    out << "parni volchemph";
    out << "acvirzilg chiparal";
    out << "alged on chipráxal";
    out << "clarn nancal";
    out << "lexrox pingh lardol";
    out << "zvrzvh genvox";
    out << "chiromonph zarchan olinorg";
    out << "calgs sedph panglox";
    out << "bapporgel bvrioldenay";
    return out;
}

QList< QList<QString> > table49() {
    QList< QList<QString> > out;
    out << table49a();
    out << table49b();
    return out;
}

#endif // TABLE49_H
