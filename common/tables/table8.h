#ifndef TABLE8_H
#define TABLE8_H
#include <QList>

QList<QString> table8a(){
    QList<QString> out;

    out << "chelvaxargacholdanachardmachaxladnachedadolvachon";
    out << "darorGANSANBAHZVRACHADOMVAGENNARBAVODNACHAPHNOTOR";
    out << "valzibvlahchaphadnochastorzvlehnabvladaxranglahod";
    out << "zarchanvalchemphnoadrabahnabvlahgehoxachornatorta";
    out << "ledoxadrvmacraxnapziphagemachanzachonaldadgmahora";
    out << "zalgenvahzancrahcaliclalvadnagemnageolzimaronphag";
    out << "dolhapnachoalgedvannechastrohaclonsavorttangedoha";
    out << "noxembloxachardanzachintozannehalpagephordanvahno";
    out << "clanchvmachonaldrvxachapnahdoxylvageffalsadexarge";
    out << "nadoltaganvancephadranganoldonclyapromycaxvorcono";
    out << "adrodlahgenoxagepatragemvansegoradvnahachaldampha";
    out << "notablagaharchaparlvmanolamphynochapnacothaldexoh";
    out << "agedvarahzodchadnachapnapvlantafacalcedonzaclynor";
    out << "amphanotalzachefnodarahzvmblhahadedgaphnagemnacha";
    out << "drvxadnachaxparchefnadonzabarhadnochaphaclaxargef";
    out << "adzvlzeacronzachaloncastrogahiclandrvlnadvagamplo";
    out << "adrvganaolzangephalchadnochadempnaphealbvzadahcho";
    out << "algvdabraoxarnachefnalcomaldvlongervarvcarhvclaxo";
    out << "noldanvagemanchvdanvanchasdribazahcladnotabnaphor";
    out << "chediadnophaorchaZVRchanolzanclonapnadvangentahoa";
    out << "iadediaynolgeaxargotacarmahselgadednoponalednache";
    out << "chaphanzacheoldamvacharzachaldvbrahgednochadaxdro";
    out << "alpadnacorzaphychasornabvaladaxargamalodavahzyble";
    out << "amphionadantagmacladzadnadecastorredoxamahgelgeze";
    out << "adnaphanodacalzachacladnolchyopontanzachafranzvch";
    out << "alzadblahchorgenadvxaldeodnachophadnadoxalgebadvr";
    out << "chansabnahoroxachydrothadnahcladnapnagemadraphado";
    out << "oxchadanzachadoldachnochanchavansabnlahgonilahora";
    out << "vorgednohchapnodladohachanzaclodoxarchephzvblahod";
    out << "dromzanclohaprangenodalzanchanoxarchavaxorgelsado";
    out << "polzidonaachorsaplangaphoaldophadaxarnageanlansah";
    out << "crodnolgedrapragnarbzonclaxorpraxadvadraholzadmac";
    out << "olzybladaxvortvmangentoncladongaphaolchydonaxalge";
    out << "prahchansavonsadnahcorsangethvdraoxarnehaplazador";
    out << "gednahcloddanchaorgemnadorplangedadrvdabahvolzymp";
    out << "archefnadoxangranolzabnachephalzvbalandrantaxvals";
    out << "aprongenohadnachandabblahvongenvagehzvbaclazargeb";
    out << "olzadnachaacrydoachadnaphzyngladoxandrahphangenva";
    out << "oclaxvarontanzacladvongethnatoblohgehvdachanzvrah";
    out << "priclaizachnogempnadodgalgrahhvetolangertadranvar";
    out << "rvmadnachephaxilganahprangefadvlgephnoclaxtarvang";
    out << "admachvadrvbanohacharzadahpronaclazangedaclaxtorn";
    out << "nadvongelzadnahoclavaxcorgaladhvgethaldednaclango";
    out << "algesvargednadonchinzaaprinochaphalphangenohadnoh";
    out << "clantagvnzachaaldehadnaclansasablaphangednoachado";
    out << "chephlathgednachoraldaxrohnebvangedoxaldingaphacl";
    out << "acloxtohvoregmanohclanzvachastorragvdabrazymclaph";
    out << "alchadmachnogalzaclydaxoclappanorgempnapheadrvdah";
    out << "chadaxmanochadoxargemehavongenohapladnachaphoxalg";

    return out;
}

QList<QString> table8b(){
    QList<QString> out;

    out << "gartalzaonzangephacladnachoaclasadnogoramphangcho";
    out << "achardangeodnadaxalgehdoxarmachoalibagepholzadors";
    out << "alzvdachopangenvclydaxvrgedadraxoclanzacamvangesa";
    out << "lorgednachefamgemplagedadolgehgarzanlachanzvghach";
    out << "sabiodachoaldenvagedmacoldavanvrgednachastraphal";
    out << "deodragordangeolapradmagelzachampalgednagephadro";
    out << "capnagethalzealpachansamotalzazemblaxvartalgedoh";
    out << "ariplagennoxolaprancevalzegorambvrgadvarnongalza";
    out << "cordaphadrvngahphiangalbrachoidachadlansagmagesa";
    out << "brandlangrinoplagephantenoxarmichanoxaldinodaxax";
    out << "copnagemvardedogahvolgedahvontaglahproxadnagemph";
    out << "advnglahvorgemvaloxpabtephaphrongellodangephange";
    out << "catascoloxdethongedagronzachalzemnachephaldrahos";
    out << "prodgnahagvrahaclansanochastrahgeblangephadolged";
    out << "avondegranoxaldegramahpracastorarqvatantogonnaxo";
    out << "plidonvorgensanocallvgempnageorgeralvoldangefaxo";
    out << "aprangenohcalzadnachefavornagendantafvolgedhabvr";
    out << "galvolgadradoxprangesoxanglanochastoramphallddar";
    out << "gedahodchamanapphilmagenalzadnadoxardephaalgedox";
    out << "vrgephadronchanvolzednachornazadnachacoxadvachar";
    out << "olzadnamvongemochadnapladaxvorzachaphnatolzagoch";
    out << "achmargahvalzadmachondaxolachvrahvonsamplahochal";
    out << "samahgemnosylaldegohadnolethalgephadranvontargem";
    out << "galasnadoxvaltephagemnaxzodaxrohadantagennexordn";
    out << "voltagmagenzabalchaphnochaphlatongradoxvrnogladn";
    out << "axilgoarbagentardranphalocastoxvoblantagemnotalz";
    out << "lepalgvargosromtangehacheldonachephneydansednahg";
    out << "ladoxvargemnaphoaldengephaclanzanochastargaldage";
    out << "charzadnachapvolzarvdrangeholzamplagnaxvanoplase";
    out << "dagransahoxalgeadednacheondanvagephoxvidranclanor";
    out << "antplavingalfatoytantoyalgedrababrachlocohvoxalge";
    out << "nablvpagemvatolagervadrangemnadaclangephaldraxpa";
    out << "zvrablagoantognaphealchvadrossoaprahgednoxadetho";
    out << "prageffadefalchadaxlaxargedadolchadanrabavomraga";
    out << "daxlagendathedolphegmacoplaxyrvoxtargemnavrbrast";
    out << "vorttalgefabramnachohclaxdraxvagelvdrahnochastor";
    out << "tontagnagoddnachepholzadnachansvamphangendanclad";
    out << "olclastervoxalvadolzyblonamphirodoxnochanzadahol";
    out << "vatongephoamglaxvorgempacheforzednacnonsapvotton";
    out << "gahlvzazednahohahadahvorgemphanoldanzadednoclanso";
    out << "hvndaxvarondanchanpeaglahvdonsahvorbablonvrnaxva";
    out << "rahsadglahoxaredahdonchapnaclyontanochapnochapha";
    out << "sednobangempnadoncladoxardrahgaongohaxclvagorlido";
    out << "pracadnadongenvageodnachapvornagentageladrvxachap";
    out << "colzachapnoxadnogemvadnohtempalzarongensadronchap";
    out << "nosadnachansalgemphadaxorchaysabalgephanochasroz";
    out << "pragnadgramahvndanqvanolzantahvolzagethrohahorgo";
    out << "mavrangethorzanzedaxvarohpricahocladnadgvargedrag";
    out << "corzydanvalcoblahgeongraphanlapvlagednocapvonsada";
    return out;
}


QList< QList<QString> > table8() {
    QList< QList<QString> > out;
    out << table8a();
    out << table8b();
    return out;
}

#endif // TABLE8_H
