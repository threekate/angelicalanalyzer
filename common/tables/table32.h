#ifndef TABLE32_H
#define TABLE32_H
#include <QList>

QList<QString> table32a(){
    QList<QString> out;

    out << "da";
    out << "clax";
    out << "amvage";
    out << "nobalaxo";
    out << "aporgadaxo";
    out << "nogrohcronza";
    out << "ambvclohhanchv";
    out << "uavornagantantox";
    out << "aploxoronzingaloph";
    out << "abradaxalgedarchepha";
    out << "dabladnachadnapolgadax";
    out << "vorcothnoxalgoradmachalp";
    out << "devamphanchanzaclaxvrchono";
    out << "dehelgolaudraxyutaglaxablata";
    out << "chonodonzidnaphachadanvahchinz";
    out << "aplohtorcohamahpenroxtatotoplaxo";
    out << "chanzabvaraxydnaxalgathzvrastonget";
    out << "apladnochapadnobagenaxoldcartonmaxar";
    out << "rodamanhvntoxalpaghcharhacnochdonzotho";
    out << "vvorbgobhorarxargnanobalgoanclopriucligh";
    out << "algenopolitanoxamvamnochadaxoruodoxancheph";
    out << "chevamgelaldlaxtoxantonoxvnafradeoxabloxarah";
    out << "gednopadaxvrnaxtalgethadgnofautontaxarbdothcah";
    out << "getholaxalglobamvagalhadnochapozaznochlorgrachim";
    out << "vonabligadnocalaxamerontautoxalgethnaplexvlcheph";
    out << "arvongaldraplaphanzangvriohalploxaxaldagathlox";
    out << "arnoxarpraxallebafeszvchastornadchalalglaxod";
    out << "divamauchanzachlochastoraucherzachalvidrax";
    out << "plidoxlapronopasvrchemvoxnoxalpalgarnsah";
    out << "chiontaxapladmaxolchidnopranvornexalpa";
    out << "chiontalouzadchiduoxalgedvamdegrohad";
    out << "plichastorqvalsipouapdravtoughabvo";
    out << "gorhangahloxarnaxalcothvortanoxa";
    out << "leutoxuaaplaxoradnoxvlgethalva";
    out << "houzipodaxlapvlantautomvagel";
    out << "hvbariouchanvoxlordrougold";
    out << "amvantoclaxploxvdraxaduo";
    out << "dahanbahchipornoclanpo";
    out << "axandraxvlgathabvarg";
    out << "lobvnachopaxnolgat";
    out << "lednavaxorchanda";
    out << "lopraxtagemvou";
    out << "hablatavamph";
    out << "geougradox";
    out << "alchvmad";
    out << "hvnzac";
    out << "leca";
    out << "au";
    return out;
}

QList<QString> table32b(){
    QList<QString> out;

    out << "o";
    out << "nax";
    out << "danza";
    out << "zvlblot";
    out << "avontaget";
    out << "apvllantalp";
    out << "hacodadnoxaco";
    out << "volzidmachephax";
    out << "anzinolantagladox";
    out << "argevoxaldranhvuzac";
    out << "nochastorandoxarbvolc";
    out << "nodadaxprinchastorgalal";
    out << "dehvgranvahchelplaxorchal";
    out << "levoderohhaxammarontantaxol";
    out << "pvnapharalzednopopnlaucheuvah";
    out << "alchidnochadnachapvorgethalblax";
    out << "lodouaxaudomvagelaxonachorbalgoth";
    out << "chidraxplichastoralgemnaveloxuadrax";
    out << "chadnapaxtalonzvngethaclouzochaldraxt";
    out << "valgebraxplichaduapachonzachorgethalzar";
    out << "donvongelblaxamaroutautavolvdraxarchinorp";
    out << "vorbageladnoxtaxonzachvidautaxopolgalodoxar";
    out << "gethmanchanoxtaltpaxvarargethdolchastorguaxor";
    out << "volchendoxapnagalpaxhardoutageladnaxaplidvorbax";
    out << "vrnzachdolchastorarvinodalparchaloxnaxvlnolpazolp";
    out << "zvnadochauantagnoxaplapnladeodmaxvrnoxaldouchal";
    out << "vionadaxalchephaxadaxvorgalodongalaxaldotharg";
    out << "nobvlabvlarvauglaxantagrangemnagolandoxnorp";
    out << "avompnadoxoladmauacharantaxalplaxadorpazo";
    out << "ziduonaqvartonolchixaxantantaxoralglaxt";
    out << "viodabrachlabvlangehougrahvolchastorg";
    out << "amnathacoxalgegraxyblouvarnoxdapval";
    out << "axarnoxtaxarnaxdiograxplaxtaroxad";
    out << "chionochaphaxalplaxnoggeongadal";
    out << "hvbnrnohapvoutortadnoxamphgen";
    out << "voblaphaxhanzachadnodauzach";
    out << "gvdargalaxalpalaxvlchinop";
    out << "deongraphloxadnopothalg";
    out << "ladnothachalpraxadaxo";
    out << "viodarbachansabvang";
    out << "lohauzidoxantagep";
    out << "ardrvmachephaxa";
    out << "uochadnaplaxt";
    out << "vongonaplan";
    out << "alpvdraxt";
    out << "vorgant";
    out << "hvdro";
    out << "alg";
    out << "b";
    return out;
}

QList< QList<QString> > table32() {
    QList< QList<QString> > out;
    out << table32a();
    out << table32b();
    return out;
}

#endif // TABLE32_H
