#ifndef TABLE42_H
#define TABLE42_H
#include <QList>

QList<QString> table42a(){
    QList<QString> out;
    out << "vl";
    out << "poth";
    out << "nochar";
    out << "zichaval";
    out << "exexrocgbn";
    out << "radvirdorfay";
    out << "lebbachirsogno";
    out << "oporratamvurampa";
    out << "nexlugynduladornap";
    out << "viodurglethgonchacho";
    out << "pionaphorglaxongtharga";
    out << "rvmazednuvargcurracolhid";
    out << "advirduthnexoldrunpungeldu";
    out << "cornoxudvornadomphadglizvbba";
    out << "curtoxlizinophruntaltoxzepurra";
    out << "feriaxnolgaxurmunchlahnozanhidna";
    out << "purgelartvorocohhidunchaphrubnaxyl";
    out << "geromuronphilodruhzizcocolocorhvbbor";
    out << "rapicalmaronzinarumphgerthroxulchimvor";
    out << "lalapodhincoyminyngedlarduthaclonzacharn";
    out << "donchalpuluxornuvrunzidblahchirdrumpulurph";
    out << "zinorbuxolgethnoxolggedromphazidnoxlapvramax";
    out << "chiroduxydnorbvorgempnoxylzichoruxpirgethalzic";
    out << "lvrgethavoncromphzichastorchalchescrimubalapthar";
    out << "virnoprumzidnuxorgalpogalzipurgcalcorchurnobvorc";
    out << "ledvnaphaxzibulaphzilchinorzachloxuvrongemphar";
    out << "gervadpioclaxprinzuchlavanrophtaxnolgzichorg";
    out << "virzadchplionvarontampraphargetorgvichasto";
    out << "nalgethaxonzipothanchloxarnnechruxtancax";
    out << "philophaludrunchnorvanchlorchastorgpia";
    out << "cadorgemnordoxlepuganzichalzucharrox";
    out << "tapornafruncolunhidzichlarpruxdolp";
    out << "zidronphalchedzicharonclonchzido";
    out << "purunvagarbdoxelbardronpiuncha";
    out << "donaphrahidmacovarnexaldroth";
    out << "ziclocroghargelbadnavaronc";
    out << "degruhoxzilcaxurbrunchlo";
    out << "cuplugathzidnurgonluch";
    out << "phonzichalzichloxarg";
    out << "arclonvaronchzibbo";
    out << "viannochaduxloxt";
    out << "crulproxvitnar";
    out << "genruxpolcax";
    out << "marnodvarg";
    out << "degruhol";
    out << "vincam";
    out << "clao";
    out << "ze";
    return out;
}

QList<QString> table42b(){
    QList<QString> out;
    out << "m";
    out << "ala";
    out << "pharg";
    out << "lordemp";
    out << "nechoraxo";
    out << "tantoxoronc";
    out << "vialgerharfra";
    out << "golcaxlebarchae";
    out << "nazirilpharneplau";
    out << "duxdupdoldedrahorna";
    out << "vichanzachledvorggeor";
    out << "runchathnaxlaphzidvonox";
    out << "hirculumphinalpdethnophar";
    out << "ulzednudruxzidcoxuldruxnero";
    out << "priclaxviorgephalnopvotargalg";
    out << "ruvanconruxtapornaphiomadroxiro";
    out << "proncaxlaxzimvamranzeduapolonhorz";
    out << "gedronaldurgethlarmaxviodubruxchelx";
    out << "zinvanchlethcomphalgethaxoruxrecoxnel";
    out << "curmupulchardrophrunglaxurgvichargaxorn";
    out << "punvmuruxtanglonhvdvirnothduncoxarglarcvt";
    out << "lemnochargzidnapthaxornaphrumlethgolglarvax";
    out << "zimutorgnervoxuvolzichacruntantomvagethaxarox";
    out << "philgetharnodrunvirozararpulgethloxorbadnocaldo";
    out << "vongaloxhvdrumphanzednoxdurcvthapulgetharnoxtarga";
    out << "zidnurbvarmathzibonduxamecrontalpluxorzechalnox";
    out << "chenvanchnochastorgviandrathloganphalzenbaxno";
    out << "virgethaulardruxoropprunzachloxarnopdedronp";
    out << "zinographaxchalzechvrnochvolchastorpurcon";
    out << "dunvachnoluzachchidbruxzidnophaluxumato";
    out << "nedorgalchephlechanvarbanothzirbrchad";
    out << "lervongulzachaphvlnoxdecroxlarvanch";
    out << "udnanvagemphzirdoxulcoxucrothleth";
    out << "zixorglabargedorgvarcaxnarcloxt";
    out << "phiungenphzednurgbluxornapvac";
    out << "pogracolgvornoxydnopporcolz";
    out << "ucornolemduxoronlebvmazic";
    out << "zachoruxvilzidnaphornox";
    out << "ulcruxurunchnorgalpha";
    out << "zechoruxnoldunchnox";
    out << "viodurgulcvtarnal";
    out << "hvranchloxamvar";
    out << "nodrucolpiong";
    out << "zvrchealdop";
    out << "vinzechle";
    out << "crazaze";
    out << "phina";
    out << "dea";
    out << "x";
    return out;
}

QList< QList<QString> > table42() {
    QList< QList<QString> > out;
    out << table42a();
    out << table42b();
    return out;
}

#endif // TABLE42_H
