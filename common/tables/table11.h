#ifndef TABLE11_H
#define TABLE11_H
#include <QList>

QList<QString> table11a(){
    QList<QString> out;

    out << "chilmapheladnochanzaalderoganzachaxvolgranzachaho";
    out << "nopharnachoadrochadoxvarnotophanoxadnochonzadnoch";
    out << "volgemnaphaxlodoxvorgetholadnochapvdraxvortaxoclo";
    out << "vngennaphanzantagephvchalmachapnachoandongaalzico";
    out << "apransachaphnoxtalphalahvndatarganphaondethadvrac";
    out << "vlgabrahnoxvrnohvalcapnachonzydnochoardvrangaphax";
    out << "nataxoladnochapvdrazadnogahvalzechonadrouzachadox";
    out << "chiadnochaphadlantavahgedothachanzaclauapraxorcha";
    out << "valsvgeneharchalsvadrossonodalgavxarnaphavontenbl";
    out << "onchymarondaxalgraxvorgentaphansvorgabladrazidnol";
    out << "prachastnorachynzachapnothangephvlzadnagephvalget";
    out << "vdanvanoltapladexacloxachanzachiorchaxvdrantaphax";
    out << "voldyblanoxammerozardvragethvolgemnachaordrosadna";
    out << "doxvlgapavollatolzoapnacopraltaadnongeivtraxvacha";
    out << "asbamanoxaldragradnotangephvoxargetothnothastarab";
    out << "zichadanzachavoaxagelzavorganzohahzecondavalgetho";
    out << "aphicodampachodconpalgatnargathacoalchydonapvatax";
    out << "vdraxvalgednochlozantagmahvonsalpavldangepaxladox";
    out << "adraxalpatraxohcvraxcoagangahvolsoarchvdravoltage";
    out << "nochanzadantageapralqvarioxadnaholachalzachansadn";
    out << "abnagendaclancogemnaclypatradaxorcvonsapnadoxange";
    out << "apnacladvaxantaloxanvanogephagensoalgemplaxadraxo";
    out << "nochastoralzidocapvlagemardrvnagetnatladgonaxardr";
    out << "vortantahcaplaccorzadmacoxalnahodagehvdrahvoxalga";
    out << "oxordrahnoldaxovorgendonchanzacahocolgednachapnox";
    out << "vagingathalzedhoncansapahnolgathoxalchymacoxadrax";
    out << "aclarnannotaxapvongamvalpragnocapsvoltagmacladant";
    out << "vorhanzachapnatardexondadnacopnatazangethnotadmag";
    out << "vynsanglaplonsovodranvargemnacholaacharnapheascho";
    out << "vdrangephachedaprvtanzachvdaxnohaladranzazichapna";
    out << "geographanalgedgemnavedradothaldahgshvdaxlogednog";
    out << "olchadvargedoxapragnocaphamzvragaxpragednoclaaxoh";
    out << "algvdranzodlaphzandanvangethaxargavansatnagefvdra";
    out << "axamerpblatoxarnotaxtoralendoxachonabladontagenno";
    out << "balzigonzanotaxargephalattradnacheoxenocladaxolan";
    out << "tangonzephachednochancladdoragephanalgednochasaro";
    out << "taxlochadaxvongentanaxorgednachanaldevagethzyblat";
    out << "orchanvahnochastoradolphadoxondrahvagednachazacho";
    out << "palconatongaphanvlaxvargetnoganzagaldraxvrdnahado";
    out << "nochalzachamapladnochapvontagnaplionzacladoxadoxa";
    out << "gemnachodarnachapnodoxvorgentaxalcharoldadnachons";
    out << "chydronsaclazadnocaprvnzaclyohadnargahodaxorplach";
    out << "orzyblethzichastoramphvnarathvldaxachonzoaldeprax";
    out << "lanchadaxoplpngephnetoxarondextogelalchephnachodo";
    out << "zantagnoxaldvnzachanalchenoxalolahvoonohischnocha";
    out << "alchadnachanardrosaclaxtophangempssoldaxnochonsoa";
    out << "vartalapnodaxgehvdraxarchonzanochastorziblethadna";
    out << "gehordaalchadaxoralchidonadontagnachaalzachephaha";
    out << "zemphagnaclonzachanzadnacholvontagnocapcaplaoddah";

    return out;
}

QList<QString> table11b(){
    QList<QString> out;

    out << "zvranchosnachoalgednachoalphinadoxadganzechastoro";
    out << "platerohgenochadaxvargenzohacvldalabvartnagadrage";
    out << "pradaxolzidahchordaxvornotalgemphnadaxvrnablahooh";
    out << "achastamaynolgethaxargephnolzachydorgenzachaxtoro";
    out << "prachansodanzentagloxolgahvorontagenulsednachenso";
    out << "placheffalohzechadnacholvansanganaoxardrahvolzach";
    out << "acherbahnochastorazycladaxvongephesandravadnavahl";
    out << "corzacheochadnavontantaglazachydragemdaxolchadnac";
    out << "gefnadoizanagolmilonparfransoalchiplagethodnaclao";
    out << "dronsalgednogefabcloclydanzaphavovanganfalotadgar";
    out << "grodomodanzycharnapheaxylzachablapadaxnochadlazah";
    out << "vornopolonchvdrahvoxargehnolzydnabavarzeazedalydo";
    out << "chansangedoxanechensadnopavioxdaxmaxlandohgemnapa";
    out << "orzabvlagehvdachanloxardacheaphrodnaxzydadgnochas";
    out << "vorgymachoaplidonchalzixparchefnaolzachonzachbrvn";
    out << "clatontagephnolaxvargemnacohgehvdrahvofmarracaxal";
    out << "genodaxpladadgmaoxzyblandvansermocynositationesso";
    out << "axarchaplansoapnattoalgethorzychasavornaclahzydla";
    out << "valgedlochaorzanchednopoloxadaxvargempamchlahgesn";
    out << "aldaxvarangehodapharazabvlondaxarnachochastorlang";
    out << "axpharahvongenohgemnachodnachaalzyllesadortomaxto";
    out << "vlzednabarvarconazyntagleszvrastorahphinaddonulax";
    out << "zychansoalzydnagefvortanblagehalprvnagdohorgnalph";
    out << "zvzangeagendaglahoxendolpantoreeptonalzyntonzvcha";
    out << "legednagordexdolsedmananziclionprascoadridopachan";
    out << "sangeahnoxavonsapladoxachestoxaclydnodabladnagemp";
    out << "asphangethnoxalglahvolzydnachynoxachydonachynzapa";
    out << "lomelonhorhaxzyngephacornotleppazdolzydochansvarh";
    out << "longensodanoxydlycharvarzensoachansolepvantagenda";
    out << "olzadchaphnolzydnacharzammeronaclydaxangethaxplog";
    out << "onzachatnazacohvalcapnahoxednohalgethalchydnapaon";
    out << "zachadnachadonzechavlmazachvrahvorsethprinsathmah";
    out << "golsagatvrdrazablizodonzyphvraldolanosephcvladosc";
    out << "sangesnophicaszvresgetholdmvangolsedarphicolzaadr";
    out << "onsagepnachonsagednaclansatagnagethaclvzachephall";
    out << "gethcohalchyadmachadavortaxmodeohalzychapachadnac";
    out << "nodalgamanoxadmachladvangendoalchyxachapchalzeddo";
    out << "volgedadroxvarladrvdahazichadanchaxalgedothnachad";
    out << "sednoblaxmacharsalzedaldazidnochapacharnodohvolze";
    out << "aivdebraxplicaloxadranclanzednaphangethalgednaxor";
    out << "angednaoxarnachavoltablanoxadnavlzachacharmachedo";
    out << "achadeoxglanzoachangoalzentogaphagepalgethaxamexo";
    out << "archednachoalzecoxvargehnaxadvagethlabchathalclax";
    out << "zvrachaclidnazendolgethanzachadnachaxchadnagohalz";
    out << "chonsadgmachalzichastrahochaldagemphnabvlaxvarget";
    out << "archednaxvolgethnaxonchenadolgedhoxangethlatqvaso";
    out << "capnachonzvchasstoxaphagnachzenochadonamphydonzan";
    out << "calzacapzichodnacarsocapgeneodahvolzidaplaxorched";
    out << "apalzachenadoxtaramvargendalstgornachohalgvdaphar";

    return out;
}

QList< QList<QString> > table11() {
    QList< QList<QString> > out;
    out << table11a();
    out << table11b();
    return out;
}

#endif // TABLE11_H
