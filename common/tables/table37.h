#ifndef TABLE37_H
#define TABLE37_H
#include <QList>

QList<QString> table37a(){
    QList<QString> out;
    out << "ax";
    out << "orno";
    out << "chadno";
    out << "pugelvac";
    out << "neophragal";
    out << "vdraxtempuxo";
    out << "natolabamachep";
    out << "vdraxnolaxarnoxo";
    out << "vizantaxameroxydno";
    out << "avvlzachathnoxuroxol";
    out << "amarvaduxzintagnoxonol";
    out << "pufioduxolgedroxuplichas";
    out << "semnedophaxulnoxvdrathaxon";
    out << "michardunvahgvlazamanchalpla";
    out << "nochatarvdroxuvomnaphavxornach";
    out << "gesnachaldroxuxurnopuxzinbolduxo";
    out << "navargalophhanvaxarggonuplaxarchem";
    out << "pulvdraxtexurnecothaldoxuchepthadgno";
    out << "aldoxanaprinadonaxvnopolopugraphgethar";
    out << "duxurbalvoxcordulchapvdnoxumvangloxneopu";
    out << "deodlamongepurarnoxandnaxvrnohtalqvaxablax";
    out << "zarapuehnocuntantolaponacaxviodraxvixnablanc";
    out << "horvmaprachalzechaldempndcothareoxnoplaxzvdnox";
    out << "vircataplacodungeontalaphchancoduxuplaxadraloxal";
    out << "viorchathnochephaxurnoduxcolgathaconzachvdrachod";
    out << "locastomarchlarvageladnopthaxorgallodumaxvnzac";
    out << "avonzachlozidnopolgarzamenchaphapogellancvth";
    out << "arnobalonfamgalbaroxvxurnoxtempnaphealgeld";
    out << "avornaphaxcaduxnolatviocaptcotaxulpolget";
    out << "adoxnorvicholadvontaphanneprachephvlza";
    out << "chivonzavldoryxcortadnobaradoxcadvao";
    out << "nazodpuxulgurontalbathorchastornac";
    out << "ynzachanzachnoluduxichornochadax";
    out << "aplanchandunvaxordrexvrnodudvo";
    out << "chephaxadonphlatnrgenapolgat";
    out << "vocastorundroxvldlohchidno";
    out << "duxurochamvargethalcotha";
    out << "dumnechoxuldoxvonchalg";
    out << "labaxtoplalmalpunzet";
    out << "ungethadornotychal";
    out << "deodracaxminocax";
    out << "vorgebarchcaod";
    out << "negapluphamp";
    out << "zidnoxtarg";
    out << "alchodno";
    out << "viaxar";
    out << "chan";
    out << "ta";
    return out;
}

QList<QString> table37b(){
    QList<QString> out;
    out << "a";
    out << "bad";
    out << "codux";
    out << "napvram";
    out << "vongethar";
    out << "neporanchad";
    out << "volcothachonz";
    out << "deodnacoxaloxor";
    out << "phiadothappvlapha";
    out << "demoschalonphadmach";
    out << "axandumgelalnothaxara";
    out << "phicotaxablapugethalzac";
    out << "noriminontapulmacothaxorn";
    out << "viodraxalgethabranziduxloth";
    out << "nacopvageladnorbdunzachgvdrax";
    out << "apvanglaxcampurnachephaxulplart";
    out << "nopulonunchinocaxmalzednopullonto";
    out << "vorngolpladhidragennavaraxalvorbaxo";
    out << "varzephnochalaldrannobvoxargethchalub";
    out << "vnzgalplaxfiorafronpaoldumoncompardrolp";
    out << "ziradracranchaxugenvaxchelsochaclaxurnopu";
    out << "vnachadoxyblontaxolgraxpromaphararmionaplox";
    out << "charzachlorcastoramphagelzablinoxtoraclupurdo";
    out << "vixolgathlabvlungozadaplidovanarzongelasqvethan";
    out << "olmadoxonraphaxclaxtargugednophcriontoxalprtgnorb";
    out << "vochendoxarmachaphlabulonzintaxtalorzachallacax";
    out << "nabaroxalgethazvblaxardrontaxonocalarzinpuget";
    out << "zidraphalchansoxmichastordraxnopruxulphedno";
    out << "uvamnachionzednapnroncalduchacloxaranchal";
    out << "carzidpogennachephaxurnolaxynzochalpuzi";
    out << "carmaxarolgedmanazaryntogluxavioramph";
    out << "lexongaludvanchzinbvlgetharzvduchar";
    out << "nolugraphadnoxdegostorphivanmacha";
    out << "zinpolaxonochaphaldroxarnoborot";
    out << "viotarbgethnelandvmachuchabla";
    out << "vicorgelphcoxurbadnacolchal";
    out << "axornaphrontemanchiladnoc";
    out << "vrvahgethmaxmavaralzech";
    out << "zintoxaloxupothvehgel";
    out << "alchandotpionapvorn";
    out << "udronzachchilvach";
    out << "apraxorglalputh";
    out << "zichaxornoran";
    out << "temanachand";
    out << "alnotharg";
    out << "alphang";
    out << "luxor";
    out << "amp";
    out << "a";
    return out;
}

QList< QList<QString> > table37() {
    QList< QList<QString> > out;
    out << table37a();
    out << table37b();
    return out;
}

#endif // TABLE37_H
