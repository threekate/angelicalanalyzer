#ifndef TABLE43_H
#define TABLE43_H
#include <QList>

QList<QString> table43a(){
    QList<QString> out;
    out << "i";
    out << "lga";
    out << "loxaz";
    out << "chephnn";
    out << "ordonponn";
    out << "gethlaxornl";
    out << "cacholapharcx";
    out << "neotapharchlorb";
    out << "tempnacorgaxalgax";
    out << "renzampnavartniodur";
    out << "hilzichavoncruxnelcoi";
    out << "parnoldeobadnaxorgenatc";
    out << "hindzuchlonzampnorbviorga";
    out << "alcothardongethalclaxyrnopn";
    out << "vidrabachlagvaxurnopthvilgada";
    out << "haxurnzidnachastorylplugnurbaxu";
    out << "lempnovarchzidnachaphaxlexcochalg";
    out << "pirudruxvirgethafolmnaphlidonpaxart";
    out << "renophrunzalcluxornoxurmvrupothtulcov";
    out << "remzidnuxulpluxlarmichantantolphzichaso";
    out << "navroxuxpiruvanchlardroxnepvlurvadexorgun";
    out << "runnuxteugaludonvarylchethapornuxzichlorgao";
    out << "remupharharnuxtaldethlodgamphzilovthanzichlob";
    out << "tutuvarchasrinzabvoroxuldrunchlarnonchcharzachx";
    out << "lvbamuronptuxorpvarcrostulcutharurbvulclurlizachr";
    out << "remphanchlordonvargsepagouaxnorgremugephalduxta";
    out << "rexarbvargepthuaxtolphaxornorunchzidnophaxylb";
    out << "cvrothnorduxrochzidconopthalcothaxargrenvon";
    out << "axunvurnoxtulgvathaxurnodpinavurbacloxarn";
    out << "dongruphzidnoxzabvluthaxuxuronpongrupha";
    out << "zemclunpvrnolarphulturphzidnuxochilla";
    out << "pianchlorvanchachoromozachlifdronar";
    out << "zicharzenvulchiruphurnzinturddxor";
    out << "gethalbadnordothchironchlabvurc";
    out << "cupuxurnorvugethalphorglichon";
    out << "umphvrucorsulpolgsehonrncro";
    out << "viduburgelpuxurnolphalzir";
    out << "rocunzurydnumbuddvruxal";
    out << "lironphgaxalglordrudp";
    out << "virganolglvrunvucha";
    out << "piurnuzvburchclux";
    out << "rocronzachalnor";
    out << "rubporbvaclvm";
    out << "virbornoxar";
    out << "cruxroclo";
    out << "phichan";
    out << "ruxal";
    out << "dox";
    out << "y";
    return out;
}

QList<QString> table43b(){
    QList<QString> out;
    out << "ur";
    out << "noll";
    out << "geraxi";
    out << "pvrufamo";
    out << "debolapluo";
    out << "grunzirbruxd";
    out << "lurtonupvrgrua";
    out << "halcopvonchvrump";
    out << "ducolnuprulzednnro";
    out << "charporggeldroharixe";
    out << "tumpnuxapothchilophams";
    out << "zirdoxubvornoxuldronthal";
    out << "cherzachlachastorvirgeduxv";
    out << "nepharongzelchipolduxurbufot";
    out << "zilamnogerrudroxyxornomphalbon";
    out << "vornapheaccabraxcampheronhvlzicx";
    out << "lordopolobruvunculgalphurnoxvdnurr";
    out << "drdoclanzachalhvtubuxirnodgudnurbulc";
    out << "revuronphzicluxurgenphzidnaxuldorunpti";
    out << "garhvnguthlexrumphchalguthaxurnuvaxlexou";
    out << "nubunzenphchvdruphunclonzichrifulgodepolap";
    out << "halzotraxnvrumvonluxydnuphvzunchalepotharnea";
    out << "gelduvunvortonphururgnerochasoxorndoxrobupurgo";
    out << "hvrdopzeclinroxzipvurbruhluduvulhidnurbzochethad";
    out << "vomronphdvbarmodilarinconphzilduxlaruguxpilgexol";
    out << "vurnoxzilzicopranlamrednorhoxylnurbvirgalglexo";
    out << "puphamnugrundolgethumornoxzichubrunchculcrop";
    out << "zulupulguxzirophurnhunzedchlapvrunumphzirc";
    out << "lardungulhvxarpvirgephuldoxumnervalpotur";
    out << "croplumachrunhvlcothelnevugroldvruphan";
    out << "nexorgulgednuxnochuroxzrulperncarvun";
    out << "onbolsullvdru'nphnuxurghvdronvugeld";
    out << "ludruxybvu'rnophurdexorglarzedcur";
    out << "urpvronebvagendoxeculgnauffuph";
    out << "vxclorhernaxlvbaltexupruthal";
    out << "zidnophuremcolurdonbuchnex";
    out << "zidupvrggubvlornunzvrdun'";
    out << "viobruxlethcolzilzacho";
    out << "necorgenphzimetrexer";
    out << "nurvibulcronzirban";
    out << "puruphrumulchvdu";
    out << "polgednuvuroxt";
    out << "ylcothuxurps";
    out << "uvrvdothnu";
    out << "zamnavon";
    out << "chivun";
    out << "zico";
    out << "du";
    return out;
}

QList< QList<QString> > table43() {
    QList< QList<QString> > out;
    out << table43a();
    out << table43b();
    return out;
}

#endif // TABLE43_H
