#ifndef TABLE38_H
#define TABLE38_H
#include <QList>

QList<QString> table38a(){
    QList<QString> out;
    out << "ul";
    out << "podu";
    out << "navomu";
    out << "chioduxo";
    out << "napnacolzv";
    out << "arnavarantax";
    out << "vpraxapraxtolp";
    out << "vichastorardroth";
    out << "ampvgethoxuldredno";
    out << "conzanraxvlchaxardro";
    out << "phichastorapinolaxzint";
    out << "noprinotaxacnehgeographa";
    out << "demnapheargvnaplaxchironph";
    out << "getharbarzaalzinconoxargetha";
    out << "lobvriattentaxalpvicolzachvrac";
    out << "deontaxapluntafnochaxlaporionged";
    out << "chadnoxardroxviodraxaloxynsunblaxo";
    out << "charmionathaxurnophuthchilontapnrget";
    out << "alcvthzintalgatorneybadhidnochephqvars";
    out << "nevolchethcharvmagelpvlaphaduxnorgedvorb";
    out << "cvthaxulnochaphzintaxamarochulpluxtaxurnac";
    out << "rovarnopvdroxalgethnochanzvnzidvarchaxolaldr";
    out << "anziputronchabnlotargrcnzalpvdronynmarpmaryqva";
    out << "gehelmondanceslarvnzempurnephadvonzochchadunzach";
    out << "leopulontorotoxuxurpviobrachchibladgehenzachchid";
    out << "dranvalpulmednopralgesrachvdnoxumvorgethaxamer";
    out << "phipheriontantaxulchednopthaxardroxclanzacha";
    out << "viotarbangenphnochastorghalzechornoxayvalg";
    out << "godoglaxvidobrachalnethaxorpurgethnolton";
    out << "limafaxchanzachnoratalphimadrophzvdlap";
    out << "viradnochargalzichazvdnoncunruchhvnz";
    out << "alphanchothgethnaxameronalpartoxon";
    out << "mandapnapvixornadydnoxulgraxurpu";
    out << "chiaduxorapviodrumaplaprinzabl";
    out << "archimancoxnevarothargenphax";
    out << "nerocothalnodvidlaxapvalga";
    out << "hvlzidnaqrangexrapodnorb";
    out << "vomtacvthalcothaxonzac";
    out << "nebalothchadvangetha";
    out << "viodraxzilbathavon";
    out << "claxargrahnorbra";
    out << "viornaphexalge";
    out << "arvantoxaplo";
    out << "neogradont";
    out << "lapnlaca";
    out << "virdex";
    out << "oxad";
    out << "na";
    return out;
}

QList<QString> table38b(){
    QList<QString> out;
    out << "y";
    out << "blo";
    out << "napod";
    out << "uxoluxa";
    out << "ruvurmuch";
    out << "alzcdpontal";
    out << "deonzedvarbar";
    out << "axurnocathdegra";
    out << "vollidnothaxorgat";
    out << "axtvpluxnevongothax";
    out << "anzenphadolvunchvduth";
    out << "aplupunoxtardothaxvrbac";
    out << "lograbalonazamphoapvmacho";
    out << "narvedgnupvlcathandunvartah";
    out << "gunchalplichoruxonocuphazvrga";
    out << "lothomvagethaxalbluxtempuxorgat";
    out << "vinzichlepoladaxonaphamphdegrodon";
    out << "azildnorunzechnorchidrodhaxvarnapux";
    out << "zimphaxadnaterothchiduxtarvannechasto";
    out << "chaligluhaxzintoxupluphrugnodrahgebarch";
    out << "lemphidunanchoraxalgluxphiocharzachcoraxo";
    out << "norzadvachaclaxzenodruthaldrogelziraphramph";
    out << "hvracathchadnompthaclintoxzvbracharzvnchaloxo";
    out << "remodugelurtelpuxvnnothavolnephzencrothamnothan";
    out << "ulplotharvaxropranchoduxvrzednopuxulphvrunzichaph";
    out << "narvagenvacothalchvmaphaxuldnurbvorzedhabaxtorg";
    out << "lethmacolandempnaphaxalchednupurzibaradgedhon";
    out << "cholcolupharnanvahchephrexabsipithclordrvxo";
    out << "nargroxzimachladruntogulhipurgthlonzancho";
    out << "nevornocloxuppvdrafaxhilungergathchedno";
    out << "pirabruxladnaldechoralzedunchorzachad";
    out << "vilbolgethlordumvadglirionchzarphor";
    out << "vrneduvanloproxtarbalbrudduxolgar";
    out << "virnoxuproxnopharchinzachchilon";
    out << "clanpuxugrunzvbluxpithcolgurd";
    out << "viotruszildroxumvorchlonvst";
    out << "uxophornogrothzirycliphox";
    out << "talqvaxlvrbrothgemvadgh";
    out << "sarraqvaurichmvlgalzi";
    out << "ronsoschalpuxanvarc";
    out << "revornadronsablor";
    out << "hetrogabaxlixar";
    out << "pelhaxornobva";
    out << "gemnaldutha";
    out << "chardoton";
    out << "abvarac";
    out << "ydnot";
    out << "teo";
    out << "a";
    return out;
}

QList< QList<QString> > table38() {
    QList< QList<QString> > out;
    out << table38a();
    out << table38b();
    return out;
}

#endif // TABLE38_H
