#ifndef TABLE33_H
#define TABLE33_H
#include <QList>

QList<QString> table33a(){
    QList<QString> out;
    out << "ax";
    out << "alga";
    out << "loxang";
    out << "laponara";
    out << "axanclaxoh";
    out << "zachenzolaxo";
    out << "novinodaxalcho";
    out << "natorgnochalchor";
    out << "anvarnapalgethalga";
    out << "loxanvarontantoxalgo";
    out << "amvarnocaltalpaxordrah";
    out << "nepormachoraxaladonzepha";
    out << "shinochalaxalgempodothaxal";
    out << "vdrodnochaldaxvrnosclaxalpal";
    out << "geodraphaxalcothanchordnobgath";
    out << "alzendoxlangafacoxargoloxantolga";
    out << "viontarmaxynzachephalzedondoxacoha";
    out << "viodrabaxvlcothachenvahzedlopaplaxor";
    out << "nebzedgoaxabnavarhvraschaxarprahlochas";
    out << "torgandoxonzaclaxyntothueouzabachalgetha";
    out << "uevionzadloxaphrantantoxalziuvahchephnerad";
    out << "noxornachoadaxaplaxvdraxtaphallebangalpvnzac";
    out << "hvranchorarnoxadnobvaxaclandaxolgabraxnotontox";
    out << "apvalgantagathaxarnapaxhvtargraphacholdaxvnchadu";
    out << "avravaldoothauopotgethalchednopaxalpaladraxolgat";
    out << "navadramaxaxaldvnchephadvantadgnoxanclantantox";
    out << "labalgapharnarganoxalchothachouzachaplapvaga";
    out << "gvraxoronzachapvantolvdraxaloxydlopathalga";
    out << "lachandoxapnaphamaradnachvaxameroxvlnoth";
    out << "agesvararchilodrantonnaphachalaxtarvah";
    out << "zegorrionamvagelalpamnaphaxalpontalo";
    out << "navonziphalgednodanparchadavalgeth";
    out << "laxameronampodaxalpelgargaldoxoh";
    out << "norchastorachnaxadraxvlgenphan";
    out << "vimadranolchayxorgredzebelga";
    out << "lolophararchanconsadglaxor";
    out << "vimnalophargethnocoxonco";
    out << "vnzinvolabraxamontanto";
    out << "latnrgraphacraxtanax";
    out << "vormahalhvdnaphaxo";
    out << "chiladaxardranga";
    out << "londanxarproda";
    out << "viodabrothal";
    out << "zadconarga";
    out << "hvxalcha";
    out << "nordep";
    out << "palg";
    out << "ar";
    return out;
}

QList<QString> table33b(){
    QList<QString> out;
    out << "z";
    out << "anc";
    out << "axarn";
    out << "deodang";
    out << "alpvdraxt";
    out << "naxalpoloxt";
    out << "viodraxadcrox";
    out << "convonglahpaard";
    out << "alchanaphaxarplif";
    out << "dahvdrahnochalzacha";
    out << "vrzidvaxaplaxtotemarb";
    out << "dahvxalgrannabvarchalzo";
    out << "mohontoxaplarrapharnidoya";
    out << "oxzochamphangesalzindodrapo";
    out << "chantaxargnoschylaldongadvoro";
    out << "alzonalhvxlohgedgramporhoxtnalp";
    out << "vorhaxaclontoxadnavarnadonhvuogat";
    out << "charachadougaloxaxoladvornoprasalco";
    out << "aviodarrampsacholhaxloxynobadrautanto";
    out << "lazadnargagenfaxvrcostorgalchemacharpar";
    out << "nirodanhaxolgpimischifaldroxvvaruottahonz";
    out << "galzataxaplatanpanvalopvxargganoxtouamvagon";
    out << "hvdalgothaporgannalvaxaxonaroxylbraxtoxadnopt";
    out << "vohordroxaclaxrachzintaxaclouzachapronaprasalza";
    out << "chiomnaclohzidnopadvrsahcheploxadougethalgethaxoh";
    out << "avoltothlacoxaroxtexolgadongaralchohanvaxarapha";
    out << "zidnoplotvoarnabvolgethachvdnopvolchednopalga";
    out << "chionagrahzaxlarponchastraxalbraxlodautompa";
    out << "nochastoradchemardrongolaxornopothavnadar";
    out << "vxaldothadcothaxargathlecorgalziduothal";
    out << "demapholaxarbcvthrnhchabachcroxzicnob";
    out << "alchingangvlgefaplauclaxzicloxormno";
    out << "gevirobratattaplaxvscolalvamnopra";
    out << "tegahaxarnaxolgeprothaxalgrohah";
    out << "navaronzintagalchebloxarnohad";
    out << "carnothgeloxadoxylchephando";
    out << "vohodrabahcadnocoxalgraxt";
    out << "lethcombanzanpahzidnolp";
    out << "avarontaxolbadcothalg";
    out << "norzichodothalplalo";
    out << "derdragelarvmatua";
    out << "virmargelzidcla";
    out << "hoxameronnarb";
    out << "gelthrograx";
    out << "laxormado";
    out << "hvnchax";
    out << "norva";
    out << "viu";
    out << "a";
    return out;
}

QList< QList<QString> > table33() {
    QList< QList<QString> > out;
    out << table33a();
    out << table33b();
    return out;
}

#endif // TABLE33_H
