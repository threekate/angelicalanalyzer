#ifndef TABLE17_H
#define TABLE17_H
#include <QList>

QList<QString> table17a(){
    QList<QString> out;
    out << "donalglahzintagnachoaldaxargeodnaclaxonachgeothan";
    out << "nagdanzataphnablagethnodaxonadlonvnatophanynzacha";
    out << "vabilzachvndanzachanzyntagnagoandlaxachardanzvnaa";
    out << "ilzachanzachalhvzadachildaxardoxnoxadnadeontanget";
    out << "naxaldlahnodaphanaxtraxnoplaxnachoanzynochachadan";
    out << "zanchalosvdnachachaldachorzadnachastorvantalmacha";
    out << "noxadrahahidnacloxzantagnagoalchidonaclansadaxira";
    out << "zachalzidnochaadaxoplaghnablagothvdraxtalgenvadrv";
    out << "nattagolyesantagnacloxarchansacharsantanaxphinoxa";
    out << "dachadmacharzantagefalzednochapalzanazvdraxzinget";
    out << "axvlgethacodnacroszidnacraspanchefalaxynsadnagena";
    out << "granzantagzvlachordrachadnaclaxpartenophaxvlzacho";
    out << "apphinodaxvrnachoadnodalahgehenzachzvrechvlzadcha";
    out << "nozadnamviotnaxaddraxagamvalgethnathablahgohanzac";
    out << "lechonsagalnaxalpagentagnacohlaxvngethnoxadnachas";
    out << "viglahvoxadnohzentalsehlacharzachinoxanaxynadanta";
    out << "zyntagalzoadnachadargamachasvrzadnaphavlchastoroh";
    out << "zamblahychaldroxachadnoxachildanatvortagalvdraxoh";
    out << "chantrozachyngalvschadnavndanvagelaxameronvntagla";
    out << "vocastorvtrapraoxadnochangeldaxochastornoxadclach";
    out << "natadnochephzildlaxazintaxagahzimonacanzylabortog";
    out << "segonvagelardroxaadontagenlazachidoxnaxaldlabatho";
    out << "vandanchanochastoradantamnalzachidaxtablahchidnon";
    out << "gahvdraxnolzintagohapplexameronnachadnachonparsah";
    out << "chidantahnoxaldrohynzalavarnaxtohychadnoxalphaget";
    out << "zvchastorachanzanoxtartarachinganzantanphanoclado";
    out << "vargethnadorsednoxalgelvahdrvxalgraxtapharneclaxa";
    out << "argednolaphysmacholzyclahvzantagennaconsadaxvdnax";
    out << "apholzachvdantangehnoxaltaxaclantalgonahzidnoxarc";
    out << "volchastoradroxachvolgethaclaxadoxvidragathnoxtra";
    out << "andransagathloxaddrintaffadalnolzipyclaxardrodnap";
    out << "vigvlzachanobadabraxtolgentantagethnotagnagrantoa";
    out << "nabrevenaxagraxtoplaxtragensaglathaclynathathachr";
    out << "doxalgethacloxtargethnohondoxvortablaxzinachadans";
    out << "chalfalazzogradalnaxvartonaxyblagathzidnochoaxarg";
    out << "nalzimnachefalchanotalzabyblaxordroxtabzidnaxtrag";
    out << "olchaganoxydqvatannolzaufaycladamaxchidnonvansast";
    out << "vxvrgahlexardapheadnichansagraxtalmogentaxargraph";
    out << "volzodgaclinsanodaxvarygesnadaxgronzachaphnaxageh";
    out << "adchidaxalnatalzachemadminsdantaxaprachlaxargaaxs";
    out << "vicrassadamphaganolbeagedongenoxtraphevmanzanaxor";
    out << "alphemneoxadracheyblessadaxolychadanloxagetvargeh";
    out << "vzaldaxvortantagethvlzadchanoxadvargethaclaxtorax";
    out << "pridodlacorgemahnechastoryntagnahvolzenblothaclax";
    out << "tapransadoxandrephvntagabnacorynsagethnoxadaglaxo";
    out << "vichadnaxaprixadalzadnachorytablohaxardroxantagol";
    out << "ynzacharmaroxylzachaxtogantalaxychanzanogedaalexo";
    out << "chanfarnachaziphahloxardrohprigasnadaxyblagethara";
    out << "nochanzaphalexadnachansapinodantaganylchnaxornaya";
    return out;
}

QList<QString> table17b(){
    QList<QString> out;
    out << "coxanzachanlaxalgedothzidlathachanzbvzalothagnass";
    out << "raxalgragadraxardaxandalganzangaligathanatharglax";
    out << "contalaxirzadaxyrnachechadnachednaxalgraxtolgatha";
    out << "chalchedongedoxchalaxnecordanzachydnaphaoxardraga";
    out << "volzintaqnaxolgethydnachaphidaxalaxzidnohylzachas";
    out << "phiontasnazednochadnachadnodaxyunonachslassadoclo";
    out << "vorgednocasdelgethaclaxdraxydnayaxalgedohclaxarna";
    out << "gethadqladaxclynzachapnachascligangazadnochasdoch";
    out << "chephecnoclardrantagethvldraxadnaclaxynsadygantax";
    out << "ypraxalgehodgadnoxadnodhodzantanglathzantagnaclax";
    out << "zidnoclaxtragethaxadnachephaxardraxydnaxonalgeoda";
    out << "chydoxidalzapichalaxtargetornedolachyusagelzoalcl";
    out << "daxnadonalyblaxzintombladanzidnochapaxladogednaih";
    out << "yerchanlaxadnochapzidnoxildlanoxadnachenzantagnal";
    out << "yeldorgemclaxtaxordraxtalgetharthcladnoxadangeold";
    out << "galzednodahgehodnadaxpalzednocasvargethalclaxolza";
    out << "chadnochadcladaxnocladanziantagethadraxvangethanz";
    out << "adnachednoclaxtigladaxonzantalgathacladnogathnada";
    out << "zidnathchidnotplagethzintaclaxmaranchidaxnaclansa";
    out << "xliodanzachadnacloxydlaxydaxacharnaladnaclaxydons";
    out << "aprantafnaconsadalgethydnarvrachanvorgendavahzech";
    out << "altagnaphaxylzachorchastorzadchinzadaxolzidnophan";
    out << "achelpayonzandachefalgensodaxanracathnolabvlaxvda";
    out << "ladorgethnacornacohgedragephaclaxtaraxzintilzanva";
    out << "chiodnachanalzednagenvachednoxalaxtaloxyntaxaryeu";
    out << "nolzantagethaclaxtanoxvdrodaxvartahzednomzantahad";
    out << "nelzonadaxyndantagenaxantaglaxynsapraxchidanaxara";
    out << "chadnoplataxnatanvargendaxochenvachnolzidnocharza";
    out << "ynchalploxacherontantaxaroxynachoadnapladaxydnoxa";
    out << "vontagnacalpydlaxtonoxynzachardantagenvndecnabaha";
    out << "golzednonachednodaxylzadnochazadnamvagesvdnoxalga";
    out << "doxlexontantoxlachvmarvdaxtoronpichasazvrachgeoda";
    out << "nabolgedrontadadnachovnadlaxtophaxvsvlaxtrahalexo";
    out << "acloxnolnegontalaxynzanochastorynsanepladaxvorono";
    out << "matalzachidantagothvadrahnoxadlahvichathzvdnachav";
    out << "zentalgethnogatvarmougemnexodnaclaxyparohvontagno";
    out << "chadnoxtantalaxyntagensadnochaphadroxadabvatargah";
    out << "noxadnaxdraxvldahnechastorraxadmachaadranvagesvdv";
    out << "chansandoxylplachataxnolzachidnodladaxyntantahons";
    out << "zinvilieechanzadaxantaxnaclohazemnaphednachednocl";
    out << "adnachesardrangahnechastoryntaldohapratzagnolzego";
    out << "antonadynvachnalzinocharzabvlantantaxoldragraparo";
    out << "plicosacordanoxvolzenodaxvachidontagalzadnozanvah";
    out << "chidansachadnoxalgethzydlachytaxonaxylzadnophaaxa";
    out << "degradnochoandvnachefzdnzaclahzanzaclaxarnahzigas";
    out << "prinsantolchadantaxacanvalgedadaxnoclansateronos.";
    out << "daxargethanzintagnacholaxvartagalydrasapragnaxals";
    out << "dnxadraxydnochanzandloxachadnoxvartaxyblaxdaroxad";
    out << "volchodnodydrosadagnagoviantaxydraxtohnechastoroh";
    return out;
}


QList< QList<QString> > table17() {
    QList< QList<QString> > out;
    out << table17a();
    out << table17b();
    return out;
}

#endif // TABLE17_H
