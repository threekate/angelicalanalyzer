#ifndef TABLE3_H
#define TABLE3_H
#include <QList>

QList<QString> table3a(){
    QList<QString> out;
    out << "olnóxapa.algeh.var.satqvámvah.noxíchmada.olzádge.nabots";
    out << "algv'maxvomr.Achýnsada.nórvah.oxámbloth.vstmágenox.vvah";
    out << "zoncórba.ieh.vorsa.omfráganox.tártaqvil.asmágnadah.orha";
    out << "calcal.dagnácvlap.vlloh.get.garzen.axárpamax.ollinoábab";
    out << "domnágolo.asgónva.nobbríftádax.órzaz.amnacoax.lardáh.od";
    out << "nangéhodah.opínacap.árpharan.godvah.oschínodax.larvaho";
    out << "sóldochar.ambidv'ap.orcásnaga.ladnáx.órmaquáns.sexvorga";
    out << "amnamgálam.dransel.orchinzáh.mallóad.donva.vors.satqval";
    out << "semchérnan.oldav'áget.dramneh.olva.lah.zvrahhah.oxamget.f";
    out << "tarpás.on'danqvans.syplágax.cornóh.dalgédvag.óxarza.alpó";
    out << "nohad.elcómva.gansélqu.ároh.aggáhoh.dártmad.oniblah.oxoh";
    out << "larsel.agiora.dancres.apríssaga.loxgrábbab.hv'nzánbloh.a";
    out << "vanvachárah.dngehódmax.vbclán.fafes.oxmarzo.gemvagoh.ha";
    out << "nosladah.vrádman.gglsapna.gonhanfal.gem.sandbá.loxácrah";
    out << "totsabnel.ohoga.namvansanahtersáqvi.ondixánvángelsod";
    out << "pamácres.ordóddos.gamnágo.iadáah.hvnplaqvinsab.sebv'rah";
    out << "gededvá.oxánpanchel.gv'bradg.apiqváy.orzáqval.loxáxvaxa";
    out << "qróndal.adv'arneoh.zavýngem.baxnómva.badmachéfalon.sant";
    out << "gadvar.záxvmloh.archnygag.londápha.nor.sámblansch.ocaha";
    out << "ránsatqvans.óx.maoh.varzed.gámvalges.orpymglv'zad.onchal";
    out << "zenfáx.orchánvah.noxérdro.hamsédbled.órdomag.lvnzánals";
    out << "cólsorda.lmlócor.falítocha.doxzáx.dranchalpámagen.nopl";
    out << "chirmal.ónelpoh.corgáoh.onbandálo.faféfas.hánzadádgman";
    out << "cálpar.odánvaod.zanládchem.orpáda.voámaa.gongássah.omna";
    out << "dalpádeh.horsýblad.dorsónfaman.hvngánnox.vargénvadaxt";
    out << "olphezamádron.gethánvacor.adrýnglax.orch.vórsaz.gempho";
    out << "ladmadoémagel.vórsomvnt.anphaldun.ivrgal.labclónvacal";
    out << "arphónons.denvamnoh.gastat.vor.galtah.oxardrah.paligeft";
    out << "archýna.donh.cheuva.ol.zedvan.olzáchádap.vngal.ompadanso";
    out << "oládogah.vorchal.moxarbvtagen.odax.ordónva.geldapragoh";
    out << "vólmaphex.arádmanch.aldágan.vorgándafóschnof.palaxvoh";
    out << "zanglánvar.onchamábvlab.dvlzadmáche.orphigámvámnadox";
    out << "parchélsa.orsaglór.vórs.ibroh.amphang.longádvomnavchel";
    out << "emaálacor.gvnzánvah.chebémiroch.arclan.dantámar.vdbras";
    out << "angox.placon.anvah.moxádro.vórsible.arad.namzorgas.alpal";
    out << "semnéraphon.avíntah.gvdbadrol.vómadron.zág.lozad.vrsáps";
    out << "vndv'ncha.lopómixax.vnchvnch.adabra.oprígalordanvox.vrs";
    out << "ampag.ordíssaple.ohahadal.glásmavorn.vrchánsel.gádvarh";
    out << "lonphýcham.vórtibles.arcab.anparsab.ord.ingál.vagam.nolg";
    out << "vn'sangephaoxádvar.blansélganag.órdoblas.galsáa.gongar";
    out << "vornáphal.gángres.lympath.zvréhodar.gónzvráb.palségons";
    out << "ár.maxvar.olqráncaleph.odnal.vongéragen.amplíodad.vaver";
    out << "ostr'advap.ódmaches.dazýmaplex.orch.ástraphe.orzýmacals";
    out << "pax.gálpoh.gánvagem.obló.ganam.harzadoplédg.arons.drvmah";
    out << "porsánva.gans.áddox.orrorges.gal.voxax.mordládvanochans";
    out << "loobábbalox.gogmádogog.aspv'ra.gemnansve.dahhádvarch.al";
    out << "gvdmáchen.orphýndal.golzádvor.gv'lzep.pardoldón.vomrons";
    out << "achéfmal.dodvor.maxadvrah.panch.ladqvemb.adrádmaloxarb";
    out << "capórzad.volzámphen.acórdax.poblógal.dvrgámvem.aconzah";
    return out;
}

QList<QString> table3b(){
    QList<QString> out;
    out << "zemgrápha.oxanádoch.gársan.onagébah.vorsémblah.gorlígo";
    out << "axvárneh.ocadmácox.lensádma.hol.giboláah.prónsesqvánso";
    out << "avimachad.ordomnáges.arges.nopha.dn'ran.angeodáx.parceth";
    out << "amsamnáche.dodas.táchel.orzínvachan.loxónges.adrohánsa";
    out << "marsívolah.génsava.dórmaqveth.azalbol.ahoxánvah.nageft";
    out << "agármax.ondánvágeh.doxabv'lax.zonchry.onzancápna.vorsah";
    out << "gaefámchaf.oníxamas.dvrah.vaxárnogef.talpámazan.zv'blax";
    out << "arpícal,dóxan,doxanvágah.galsv'blah.engánseph.orzv'bachan";
    out << "dónzalza.gémnefes.árdomal.vrtagansánpha.soxantan.gémpo";
    out << "naxv'dra.v'ornigódax.parchántaloh.gébracah.ostamáco.gans";
    out << "vorgónzanch.aspvráphab.golzanqváh.gopnypódagehasclán";
    out << "dónple.axáxvarax.tótnacheb.vámroges.ámnacopt.zenzédvah";
    out << "olzádchar.cónsanvah.vlnácheph.doxirsónax.pangenfaléxo";
    out << "dorzínbledámnaphor.gémnab.vranzách.vrcábbvlab.naptóns";
    out << "arzímney.oxángels.ángeld.ornath.adámvil.oxameron.danteq";
    out << "apnábrach,oxýnglapath.zábvlonch.cadvámnacormángedort";
    out << "astpn'lamphéres.donsántapoh.corc.hýnadab.vdbrácorgh.oho";
    out << "zanzompálgeh.vdabrácons.valgémnephar.vrmanáplan.gorza";
    out << "oxinvágenzoh.porzábbvlar.doncháldap.oxángel.dónzagans";
    out << "orcheno.vómeron.delpladganfángafets.orzangalzýpanoxt";
    out << "zvbnachadnápal.ox.vrmax.longénphamart.drvnzahvoh.argoe";
    out << "dah.axel.danchetabránzaba.gednoph.arcómzaga.geleóhadax";
    out << "gorsígon.arsémbláh.orzámblah.vorts.admáchar.dvxádphart";
    out << "ivbárgeh.oxadvamneh.godángela,or.záplagef.orchynzadaxt";
    out << "donlahvomsyméron.ardángenoh,vortempálex.arsebaróxadf";
    out << "vorsal.odíthax.volgef.adámnah.vdv'lzan.maxárzageth.vlcap";
    out << "volgalpádox.argen.vaxmar.ocaplah.vorsánvoh.adphvnachon";
    out << "mansétnah.oxángal.nodárpa.gemnádmazox.vn'tvmblah.axmalo";
    out << "vónefre.galzazánerom.alpagedórtafax.v'brachadángenphs";
    out << "alzédgar.oxináxol.dentáxma.oldam.odángex.var.sémsate.oha";
    out << "del.zadnax.ordringábvlah.gehv'schols.advernoh.gansánfah.";
    out << "lodatqvatam.óxibleh.yamvagam.galpax.ordínachadádnabar";
    out << "damsélqvo.axinvataf.orzabv'lchah.norvágeph.oxdóngendóh";
    out << "páphagam.nabzábans.orchádavon.tarto.pacámiz.anfágmaloh";
    out << "gemnáh.orchadnah.oxybálox.danvántegel.chardánvagorcha";
    out << "postv'rah.donzédqvax.ox.ángeph.ordri.oho.da.vax.marsa'genve";
    out << "dalsézgangen.vbrápnageph.al.dýnocan.vrmadephálege.vrsa";
    out << "panságna.oxardnoh.vomsángef.orvángeth.daxmárgens.odrab.";
    out << "arzénchv.záblazab.orphingens.oxarzágef.doxaldádex.geba";
    out << "von.varmah.golzazbach.vládman.genzábvlah.oxárvageh.dalz";
    out << "vortmart.oxagánaph.donzembles.dorzýmapha.geth.oxarvach.";
    out << "noxv'ldah.vortábnah.golgámah.galzágeph.noxádnab.oxýnclc";
    out << "daxárcha.loh.vónglah.drigénsapha.oxylapagéndomax.vrdra";
    out << "vontórgo.gemfálgeh.oxýndrah.voxámvlab.dedmácho.danzáng";
    out << "oschald.óxabrachánzv.dolzánch.noxánveh.dórdanaz.amgens";
    out << "zydýplagánzagan.oxadváxdan.olchy.admv'zax.orchádva.goot";
    out << "arzvgla.tepnáfagemth.nabv'dax.zóngenph.archánsah.nóxvar";
    out << "fangendoh.shadv'ansad.oxymacho.lórdavahgolzán.vdraháda";
    out << "zémnegoth.armax.odólphan.gethádmax.órdanqval.gels.arhyr";
    return out;
}

QList< QList<QString> > table3() {
    QList< QList<QString> > out;
    out << table3a();
    out << table3b();
    return out;
}

#endif // TABLE3_H
