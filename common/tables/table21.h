#ifndef TABLE21_H
#define TABLE21_H
#include <QList>

QList<QString> table21a(){
    QList<QString> out;

    out << "vontulgluhleodnaxtorgruphuxvdruntuntoxolhidrularp";
    out << "zendochalzidnochuludroxtachuphneoteroxaclaxtorges";
    out << "volcastorachunzuchaldronzunolpanchefulchvdabluxod";
    out << "nevolgemnadonuhhvdruxzidontulpaxurtertaxtolgruxoh";
    out << "nulgadnochaphuxargegruphulrlonexoterontantonglaxt";
    out << "chitaxnotuxneoteronpraxadloxadnaphuluntuntangethu";
    out << "neolzadnochadnuzudnoclaxadnoclazadnodantalpluxuhu";
    out << "deorgrugudneodvurtugalhednohupruxturgethulchidnox";
    out << "aclantaxaclioxudnachesdoxurgraphneoterontangednol";
    out << "vpruncasadchidnothundaxadnochapplaxtonextoterontu";
    out << "zintagnochappatoxtalaxichustorzintoxantalyllaxtor";
    out << "uchadnotetoronclanzvmachontaxargelaxvargethadoxta";
    out << "neolgeadnugraxphionzacliontugnacorvidlachutargala";
    out << "chepaphonzingluxadornuxturtontuxuclaxterohnecront";
    out << "upheldonluxundraxupluxtonuhvigladnochaphulchednuc";
    out << "achenzaclontugnohachunzuneocuntopradnaferonzintob";
    out << "urchurchallexornateroxudnaclantoxateronantugloxad";
    out << "viodrudrugrodoxzintolaxangenolaphuduxtochennatexo";
    out << "upruchednodalpolgenuphosvxardraxnucharzidnoehusul";
    out << "chidnomerontantulpagranzidnodaxalgethulgethornaxo";
    out << "vionrahudranclazuntugnuclaxtoturgeloxuchurnabruht";
    out << "zingluduxtoracucloclunzuhnevacrantalgemnaphealduh";
    out << "vxalgubudyungluxtoxydnolganacorgazartalzednoclaxo";
    out << "clontaphadulcluxnotuteronchunvahnechastoroxadlaxt";
    out << "viantalgarzantlaxadnochephurdruntalaxargentodanta";
    out << "nevorgamnaluxvicoradroxtanzinochalyntugnaclaxynge";
    out << "vortrobbutaclaxnogrunoltungafviontalchednaxtroxan";
    out << "chuplaxtonaltugednochapnutaxneographuxalglahchido";
    out << "archapnutalgednagenzaclunchintagrunzaollaxtoracha";
    out << "vlzalpluchathuchaldumaneclaxonentonglanglaxtontun";
    out << "zydnadvachunuxvntuxanaclochunzucluzantolgrazadnox";
    out << "alzidnuxtoteronulzadnochapuchanzalaxartuxudnochun";
    out << "vialgluxvdraxnochathneoduganzichastoruploxtantont";
    out << "neovssagtalulchednoguntalalgluhgehvdraxachephnoda";
    out << "vitaxadnotalziduvergenoxalchednotaxarnoclaxydraxo";
    out << "chentulgenadanzuchvndaltugnoteronuplagraphantunta";
    out << "longulgazachyntolaxudnochaphyblaxtoandongethavolz";
    out << "uchansachnoclantufucludnochapypnoduxalgethnechust";
    out << "uvortaxacludaxtonaxydnochantuntoxavonzuchunzednam";
    out << "voxalgethudalbalgazudnoxyntagnoxaclazantagentalux";
    out << "mrachlachudnochuladoxadoxylneydanchunzachaplongah";
    out << "vultvruchuchlaxtonoxydnoxadolgathnechanzuchvldant";
    out << "ubluxtonaxynzadnoxuprugnatontulzachanyntograduxod";
    out << "vorchastoradoxurneogruladonzuchneovotaplaxzintoro";
    out << "pvludnutexuldalaxvortanodvdrahachepnathocluthudno";
    out << "denonguhnexuteroncontalguthenolzidpruchednodaxoda";
    out << "vichostorneoterosvostargalachednoxalzvblucapnatox";
    out << "zinvalgethucluxtogranzadnochaphaclaxtoterohvduxar";
    out << "nolgethanzoaldadragrantulgethnocustorzentalguhoza";
    return out;
}

QList<QString> table21b(){
    QList<QString> out;

    out << "vorchemnadaxvdablahnoldaladornadvxadraxaclaxtonah";
    out << "clatargemaxvdantaxogladaxtorgathachandachalzednoc";
    out << "avorgethapladantoxaclaxypparontantoxacherdonalvia";
    out << "cohadnondenzanblaxacherdagahzentagnographalzvdlax";
    out << "ychordanoldaxvarochaclaxtoteroxalchadnogalzednoxt";
    out << "algednodaxidonzaneodragrahalchidnodaxzentalazvdra";
    out << "plichanzanotantonadranzacliodnaphiaximerontalgeth";
    out << "agvlzachnoldaxvaloxchilabalachednochalzintoxclant";
    out << "piadnagensadlvtaraxphingethnoxachednoytantaxadlac";
    out << "geonzachachaldahneogaldanzadaclannodanvagethalgox";
    out << "avarnayednoxadlahgeonzidchadantaxographaxvdloxarc";
    out << "cheodnadoxaglahzintolgradaxzintoxaxvngenlolzaclax";
    out << "phiodnachefadronaclandaxachanzacontraxalanzinglad";
    out << "vovamnadeodraxachednoxacheldraxylzednopalgalaxora";
    out << "denodagartaxnolapadaxvdranchanzachalchadnoxalgeph";
    out << "achednodaxplichastoraxvdnoxadonzachneclohdeodnagh";
    out << "aprongalnadoxtolaxydlagabzadchenzachalzednodaxtar";
    out << "phinotargendalaxvicoldlaxarnedadnaclionazintaxald";
    out << "geongraphaxzeodaxtargemnabalgradaxchidortagamlado";
    out << "gansagnocharpaxtolaneodadonchephneoldalacherdoxad";
    out << "anzendoxargenzachalnedolapontaxaclaxtarohgensacha";
    out << "devarcathnedoxtargeongradnolvdragantantoxaclaxtar";
    out << "aplaxtogangalontantoxadrahnecoladnoxvargeongrapar";
    out << "halgalzechnacolgathnedolhaxviordacatnebaladvdraxa";
    out << "necolgathachednodaxviodargeodnachephaxarglaxtarah";
    out << "nachalzeandanchasnabortaxalgranolvdrantogalpadorb";
    out << "alchanzaclaxadnacloxchinzadadroxtothaldraxtontaxa";
    out << "necladgetharbaaclaxtorgradnodaxphingradaxylzadnoh";
    out << "aplladnochapnachondeongaladnadgradontantolapraxah";
    out << "leodnograntaxalgrahnaodvanchaldaxtorgethraxochida";
    out << "thallangoraxalgrinnachastorantantoxacheldanaxarba";
    out << "geonzagallexodnachapnagradoxvndanoxparchednodalsa";
    out << "garnachadoxvicraxnolzadnondeontaglaxzionatargenno";
    out << "ynchodnaxaclaxtorantentoxanalvitaxacharbardeodnal";
    out << "viclaxtornachephzadclanzoamindaltaxalglaxaclosada";
    out << "nochalpraxadnochaphalchadnogransahdeomnagedohalch";
    out << "dograxtantolaclaxtothagednogalvrzachadnogaladnoxa";
    out << "vigednotaxaltanchidanzanovastoralmanzacharzidnach";
    out << "olzidnachephaxargenolgemnapheagaldonnatongedahalz";
    out << "valchastorzemlaxacharnaxdeodragranoxalgaphnataxar";
    out << "geongladviconzaclaxdeonzaclantantoxadralydnaclaxo";
    out << "chamniadaxzidnodaphalgethargathzidnoxalachednoxta";
    out << "volziblaxaclaxarohzintaxanargalhalzachacloxarchar";
    out << "nadoxtalgradaxviadnochansanfaxlexorgranoxandornac";
    out << "vialtanocarzazicladoxandrodaxanglonadorvblaxtaran";
    out << "nagalzadaxyntlaxidadnochapalchadnoxarnadydchamnah";
    out << "gorgethdortagedalgronadaxvalantantoxanalgethaxalo";
    out << "dabavilalchednoxaplaxtaronzintalgraphadnoxtalzidn";
    out << "chansalnachanviarbaragolzachnecladnoxapronsadonta";
    return out;
}

QList< QList<QString> > table21() {
    QList< QList<QString> > out;
    out << table21a();
    out << table21b();
    return out;
}

#endif // TABLE21_H
