#ifndef TABLE18_H
#define TABLE18_H
#include <QList>

QList<QString> table18a(){
    QList<QString> out;
    out << "zivialchahnoxadnachandaxarganaxvdnaclaxarnachioxa";
    out << "nachvdaxvartagnonydnagydlogogentantaxolyblasyrnah";
    out << "vocarnorynsandaxoclanpagenaclaxtapharinchoraxtaho";
    out << "michardaxynsadnocladaxnagempalchepnadaxvdrahachyl";
    out << "dortagnagemphiadnoclantantonzachelydlacasvlnadarq";
    out << "viortalaxynzechachanzadoxadnocasvintaxilgamzeanch";
    out << "nechastorydnaxydnodahalazvzadnochadaxvlnapalzacha";
    out << "draxvmerontantaxasembloxddnonadongahydrachadnocla";
    out << "chidaxalvicastraphenolzadnagethvolgetaxalshadanax";
    out << "voltagolandraxnacondargephaonzanoxadnaxichastorzv";
    out << "ntagnagephalgednachonazontagalyrnaxodantantaxorox";
    out << "dargethadraxvarchenodanzadaxpichadnachangehvnacla";
    out << "gehvdraxnexolgenzachadnaxarnachephalzachidaxtoxal";
    out << "naclinopraxtalaxvantaxanachosadramzachinachidaxal";
    out << "violtamaxnexadnaclaxvicornacloxpvnchaxlochanzadax";
    out << "alchanzantagnachoyutagnoclanvangednodaxtrohaxillo";
    out << "axarnaxyntantaxanechastroxynzangenazvrgedathnocas";
    out << "ydnalgedaxlozanchavolganpheazvdaxnachadnochasvrax";
    out << "decoxalgahnotaxmanaplaxydriahdeqohanzachanapagala";
    out << "gentaxnacloxnatolgenoydnoxapraxydnocharpyblaxadax";
    out << "ynzadantaxladnochalvatorgendavaxvdrahaplaxnoclaah";
    out << "vonzendalagednodaxvargethnodavantagechzidnachvtha";
    out << "ylzedochalzachidaxnoclaxyldrontagenvaxtardegothax";
    out << "volzidnahamvarzanechastoradnobychafchadnoldanzanv";
    out << "aplvnchafnoxalgethadroxachaxvidalgrabanodangaahox";
    out << "phianasmaxychoalzednocapaxcatoxvidalzidnochanzach";
    out << "chantaladrongahvioxadlaxnodantaxyldrooxanoxphicha";
    out << "noldantagenadnachansaclaxpraxadnaholzicharphacolz";
    out << "ynastroxachildoxardraxniclaxnogothvalzednochaxvar";
    out << "tontaxnachoabratnadaxydnondantagnachyaglaxcodansa";
    out << "neoperonzaduadgagensevolsadnacheansvraxadanzidnoh";
    out << "achirdoxydnolgazachylconabrohynzantanglahnodaxyth";
    out << "yntagnochapvitadzachytroxadarvargethnolzadnoxadno";
    out << "achadnocathviotagexadnoxaldagrahvicholdaxvraschod";
    out << "yntaxalgehvdraxadantaxagentalpaxvdraxonzantahclan";
    out << "santagnoangenzoidnochadanynzidnochephazvnazvnoclo";
    out << "chadmaxtargentaxlantalapvnaxtorohydnoxydanzachido";
    out << "alzidnoclantantaxinoclaxvolgantaxalzichadnacloxah";
    out << "yvomroclaxnechadnopartaxalgrapodangednochasvrnaxa";
    out << "deolgathyntagnadnoxtandrogadmangethynzyngaloxadno";
    out << "vocastornoxalnohyblaxydnoclansadnoxynsabvlaxagens";
    out << "yncladnachadgeotharanchildaxchanoxaldrahnechastor";
    out << "amphigedethnolzadnachephachvrachadnachanzintagnax";
    out << "ynraxalpractaffaroxydnogalaxvitorgebahvdroxpantax";
    out << "ynchanzachidnantagelydraxypantaxnachodaxargraxyda";
    out << "nozandaxyplaxnographziochalophadnargethalhidnache";
    out << "loxadnoxandrolapransaphnolzingalydlagentachadnoch";
    out << "avirdoxynsagnaxylsabnlaxtortoxanaxviatnagonydnnol";
    out << "parendethnochastorazintagahnoxalgethaldrvxaphinta";
    return out;
}

QList<QString> table18b(){
    QList<QString> out;
    out << "zadnaclartanaxolaschonzanchaloxandachalgedrahgena";
    out << "hvzandanchalaxtaxnachonzalachathnoxadnacoxandagna";
    out << "coxalgalaxdrothadnoclaxvargednodahlexandraxzedona";
    out << "chednoxaplangladaxtacorzalbachnechastorlednogatha";
    out << "vnbanchansanchalaxdraxanglahpraxandrahnehalzancos";
    out << "adraxlacnnagentalabroxandongephaxarnachoaclaxtolo";
    out << "viconlahgehvdaxahnecloxtaphangethnoglazadnachedox";
    out << "ploblatnogednocharzintagnocaxillaxarvorzednaxvdro";
    out << "aphinagendaxantaloxvnzanzachiadnolvdraxaploxacrah";
    out << "nachasmachorzalgathnochadnocanzaladgzintalapraxor";
    out << "vadoaxnatolvndahprachapnagentalexonzachantaxolapo";
    out << "gethadgmachinadnoxaldaxrorargethnoxtalgethanecrox";
    out << "adagnochydalbahchednogendaxvorteglaspricasnapheel";
    out << "amsendaxynpagnacloxzangesadonzanechastoryndaxoroh";
    out << "volchastoraxvdaxnocadnoapplaxaclinodaxyngendaxado";
    out << "noclanpanchadnodontagenalchemagvlaxinichargahnoxa";
    out << "polzemblothacrozaclantantonglaxapraxnodaxalgrapha";
    out << "norvidaxolgethaxnochapvalsedapornoddiaxclothnodal";
    out << "ydnopraglothzvdranvagelzadglaxcorgascanoxandeotha";
    out << "gethvrarzintagnochacholzadahvitvrbraxzindantaglax";
    out << "nobilateclanarzentolgenzadaxrogentolaproxadarchal";
    out << "nalgednachephazarzvclinodraxilgroxthendontangetha";
    out << "prinodangeldoclaxzintagnachephrolzednochaladoxarg";
    out << "vantagnacostraxviendaxclatoxanoxynsatoraxnoldacla";
    out << "piathnolaxvangednaxolachvsclanotangethzabvlontaxa";
    out << "nolzingesachantafvoxarnagrafzingildanozanchafatox";
    out << "plagastieiednodaxvdraclaxpangednochalvdraxnargeot";
    out << "avortaloxyndroxnadolphialgethazednothachianzacols";
    out << "nadaxtheontagnazvyngraxaclaxnocharrvtachnalvnzach";
    out << "volaschannegrantolaxyndagrantantelzaybroxnachybra";
    out << "deodnaygethanzechadalzachaxphinsaplatoxynzaclants";
    out << "lochastoracharmauneodnadaxyldvblagemandethachansa";
    out << "nochastorahgethanzachennoxalgedaproxnachdeolgatho";
    out << "marvanglaxnorartranaxvilzadnachedadlontagnoconzah";
    out << "phiantasazvrahzinacliaochaldahgebraxtalgednovaroz";
    out << "amaxnaclaxchenodantagenalchidanclabvrnatoxzingale";
    out << "vdantaxneclaxappvlazachionzacloxdeodnaxdeonanzeac";
    out << "lahgednographalzicorpraxaxnadabzvlaxyndantaglanio";
    out << "cartanglaxzingladeodnachedrolgethneocranoxtaltazo";
    out << "vdangetaxnolzachyallalexalgethnolapnacoxecalzinda";
    out << "oxoldachachanzazedachamenaclaxtonaxadchapnachalge";
    out << "agradanzacladnochanzichaadnachaladnodaxapraxadals";
    out << "valgemnaphealchedachednandengalaxetlongendoxalgat";
    out << "geothangaladoxadnogalzidnoachednoacladnaxargethax";
    out << "nedoszachidantaxipraxnaclasvicraxsedoxadontagelth";
    out << "aprontalgenoxalglahynsangohodnachsnastorvachausal";
    out << "yblaxtonnacradnogelaxvraxdaclannebvradaxorgethalo";
    out << "dednogalaxzvzanarzedlaxaproxnotantomptaszidnozado";
    out << "acladmachenzingloxnadantaxalgranocastorazinblogal";
    return out;
}

QList< QList<QString> > table18() {
    QList< QList<QString> > out;
    out << table18a();
    out << table18b();
    return out;
}

#endif // TABLE18_H
