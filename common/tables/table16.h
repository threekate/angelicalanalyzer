#ifndef TABLE16_H
#define TABLE16_H
#include <QList>

QList<QString> table16a(){
    QList<QString> out;
    out << "alzadnachadaxvagohalzadnohvohandnoxzechvrahadaxar";
    out << "zednotalzachaplaxnographalchednocheandvzadmacharg";
    out << "lananrachvaxalgethnogladnoxzintagnachnochanzachar";
    out << "plvdrahvonordograhnegolaxargedoxnaxantagvlazangeo";
    out << "aphragnochasterlazantogelamphiradoxnadontagoellax";
    out << "argoaplantaphnechosteradadnagohnabvlaxzentagnalpo";
    out << "archvdrahnozentlahzichanorvndranzachadnopolongvla";
    out << "bardededuagoaxartagensalladnoalzypatexorchastvoxo";
    out << "zantagnochalvdanzelcoxtvanypmagefcataplasydraxoco";
    out << "nalzachvchanzaclydontagamvalplasazalgethnochartax";
    out << "ziadnochalzvdaxvargednoadpalgednachoandrantagonax";
    out << "plantagnocarecoxvoletoxaplaxtargrahadnochastorana";
    out << "gedonzachvdraxvahnoxanplahzichastoranziclohnaxado";
    out << "carzapvlaxvdraxorchatmachonzynthalmachepnochastor";
    out << "advlzachnevornachaxlaxargahnochasbonsantanplaxvra";
    out << "cozadnoladonzachzadchednoclaxproxitophannacostrax";
    out << "ylzadnachzednalzagemvargethzecladnochaxvarnextalg";
    out << "nasembladdontangedoxarchansaclandorzeclaxorchedox";
    out << "pligantolaxvartonamanganahnoldaphacharvangethonzo";
    out << "alchadnoclaxzanclaxaprognantantraxolzintagnacloza";
    out << "malcoladroxagantantoxydornagedvaxalapraxandontant";
    out << "lachastonvdantangethgehvdrahvoxardrahnoxanblaxall";
    out << "famcothnacostragemtantaxloxadonfageldaxlacaxordra";
    out << "negodavapolzichadnoparletontantagethnachetnothels";
    out << "vaxandroxvdnapvagelzoacharparantantoxybladnacholg";
    out << "napvansehdoxarchadantontantaglahnoxadnaboxalpharo";
    out << "devdragraphnochastoryntagnacoxpalgethorclaxtopero";
    out << "zednograpdgelzadadclahaxaudragohzabvlaxvrnaxadnap";
    out << "topaschapalzvdnapadvrnacheflamfednoglayblesadolze";
    out << "chatargatonacladaxtoteronyntraxvardeodnaxydantaga";
    out << "voxtrahgehvdatalgadnochastorothalchydonachangolza";
    out << "amphantalaxyntaxanalyngalsapaxadnachefgalzadnacha";
    out << "doltentaphaxantagonabvlaxaldantantagnahyeldornecl";
    out << "adnoxtraxvoltrabbadoxvdraxadolzachydarvaxadolzard";
    out << "vnadrophaxphentolchadzvdachaclazangenoxtalgednaxt";
    out << "plandaphnagovalgethagadnaxzachoxalgaphalabthvazac";
    out << "vndantaglahnoxadnavachardnoxoldrahdoxednavarsadna";
    out << "orchameroatantantoglahvaxmeladnadcondazadlaglahod";
    out << "gebvrelalpradgmachlochadnohiehednazersalzednochap";
    out << "palgethnolaxnaaltapnagenodaxaronyntablagahnoxalge";
    out << "vadathnogradvdradohnolzinglanvntagnachepacheldama";
    out << "golgothnartabvlaxdlodradachnathaldrambreddomnavol";
    out << "getbalgexaxarnochapzvdlachaxandahchanzadnazvdacho";
    out << "laxanvahgedontaglaxparbacanleontafferonvnagedotha";
    out << "zintalgephendaxoraldvragethnoxaglaxpargethnoxalda";
    out << "polgafnanhondontagalsapatornexodadroxadnaxalzepor";
    out << "chvdanzaaldyodazachamefildaplachapnaconzentagnaxo";
    out << "alphvdagelvolzechaplavdaxoaanzyntaglahnoxadnaclax";
    out << "vargednavxorzachedonlavanzedaplantagnapantafforox";
    return out;
}

QList<QString> table16b(){
    QList<QString> out;
    out << "ozadnochaphaxadnalaxpargethnagalvdraxtalpataxonza";
    out << "alzidnadaxchadvandaxalgednoxtalgednatoxcaloxadnap";
    out << "vxadnochodaxadnochalzadnantagolzintaglagoxarnapho";
    out << "hedoxadnaclioxplaxclantantaphazadnontagelvdraclax";
    out << "phizadnachaoldaxvargednochapalchidnochalplaxardro";
    out << "noldaxtragohangednadoxalgephadonzachnachalzacanno";
    out << "danalziplahnedontagnaclaxalgednodaxpatroclantaxto";
    out << "volzednotaphvrdroxagalladvarghadoxnatovtichelvdna";
    out << "geondaganzandoylmachaloxadnachnocharbrantoxalgara";
    out << "voxaprahgethadnachaphaplaxzaganchzvdnadgagidonaho";
    out << "vnzantanzealzedaxvraprahanalgalzantagnaclaxzanget";
    out << "algethnoclaxadnachauaxargraphlachastraxnoxartalax";
    out << "zintalpalonyngedoxvancarzedoxannachaldaradnoxydno";
    out << "ydaztohplaxataphanolzadnamvndemvagesnodalzachidax";
    out << "volzegoraxpharmachonaltraxitolnatnraxtalgalzadnot";
    out << "andadgnanochampahgethalzachydortagalydnophanalzec";
    out << "logadraqxarnolzadblahavertocaxcapnamvoasemblaxtor";
    out << "genzadnop;ansageothaparhaxarnachodantasvdraxnopra";
    out << "chanfaxvngelzodaxvalytolapantannachefmatoxydnotal";
    out << "vorgetytanglaxnochastoryntorgendalchednophalaxtoh";
    out << "gemnesvehachadnaxgeodanaxtalqvithnachastrazadnoch";
    out << "alantaxvortangethgeodnataffaxorgallaxodtalchidnad";
    out << "anzantaxvdagalparclasydrosmochnochastorgaphintage";
    out << "nolzadvdragagelhadnatortaxahplansenvachenolzadnoc";
    out << "ydnolgabadvantaglahzidchadnazantzichastorvdraxtor";
    out << "valgednocharvansanbanobortagafvdrahgeothnachaddon";
    out << "nabzaiedoxalaxtarchnaxzantlyangedonanvansaxqvarta";
    out << "noldadgnochalgentahzichastorapnapadrantoxalzadnox";
    out << "pichanzachnolzadnoclantantaxnoclaxafradnaxydablah";
    out << "dolgidaxvrgradoxavantantosnelagenovaxnachephoxado";
    out << "nepalagvansantalafvnzonchadaxnaclonyntaxnachantox";
    out << "viouzachvdraxtapahnochardanchaladnachephazvchacla";
    out << "dorgethagalzachidaxtorantognachapaltafachalaxardr";
    out << "alzydnoclavoxartoganzidnogephallazebnachadoxvylza";
    out << "nodelphaanarganohledegohanzadzibvdabravxtoxanalyo";
    out << "aplatontantaxvalchanzanochastoradrioxziaclanoprax";
    out << "teonzanvachefnotaxaglahgeodnaqladvioxacrostaxydab";
    out << "laxtonagedlapaphadchadantagnacladaxnoplaghalzedro";
    out << "chvzacladaxtogolzochoapprachadaxtoglathapnaptagon";
    out << "zantagmaclohaxazerondantagnalvdraxvacharsadnaxtoa";
    out << "gastargedeodnaphantangaxaldradgahnoxalgedoxameroa";
    out << "ydnabvlaxapnodaphazintalaxarganaxydnotangarvantax";
    out << "chvdrachalzichadnochidanzachiantamnaxolgeodaxalox";
    out << "volgedohalraclydnadranoxanraphaeldozanchalvtanzac";
    out << "lepleggadnachoarnahzvoraxichochadanzaychanzanocha";
    out << "stroxalzednapvalgednocharneclantaxalgednochadmach";
    out << "vortellaroxvlzachapnagodanvlzachadaxtoroyuzanoxad";
    out << "zantagnachephalzvdaxnochadalzadvdnapaloxydnochana";
    out << "vantalzadnoveronzyclaxyntalgehvdagethclothaxalgeo";
    return out;
}

QList< QList<QString> > table16() {
    QList< QList<QString> > out;
    out << table16a();
    out << table16b();
    return out;
}
#endif // TABLE16_H
