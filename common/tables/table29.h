#ifndef TABLE29_H
#define TABLE29_H
#include <QList>

QList<QString> table29a(){
    QList<QString> out;

    out << "vl";
    out << "gldn";
    out << "aclaxt";
    out << "vranotan";
    out << "oalgethanz";
    out << "aclaxateront";
    out << "nochadnoclaxad";
    out << "viadraxaduacheph";
    out << "arzendabahaxardrax";
    out << "vlzintaxatolgathuoxa";
    out << "deodoxatolgetharoaarar";
    out << "chausagnoxalgrautaxalaga";
    out << "vochandaxnolziduothaxadaho";
    out << "geoschafilalnothaxandraxadox";
    out << "vlzeduachapuaxotouzinzaugranto";
    out << "nayvontagloxmachefferefadoxuoclo";
    out << "pichargahaxaduoclaxtotarouseudoxah";
    out << "chidnotaxatortaxuatautautogeuthalaxo";
    out << "vioudragraphaxadniclaxauatoxalgeduocla";
    out << "vichadaxateroxalchefuadonzentoxaudroxado";
    out << "volzednochepsaldeudoxantagnaphaxalchaduaxo";
    out << "valtagathnageldoutaladontoxalaprousephuadoxa";
    out << "vortoxaclaxachednotantoxaclaxtamerouzvdaglahax";
    out << "nochastorachaduachadalgephaxodolgednochapalzedoh";
    out << "chadathnathachonzauoladchaduacladaxvruaxvornabla";
    out << "geodautaxalaprodoxalcheouzanochauzaduothachaus";
    out << "volgednocaxaldeduoclautantoxachanzachalvdrax";
    out << "ybauzachnocladuaxaclaxvaroxtantaxuocharzid";
    out << "nalaxadnoclantaxotarnaphagziduoclaxtater";
    out << "caldextraxaduaphansaguoclaxaclaxtograp";
    out << "phocautoraudoutaxueovalgethuadaxacla";
    out << "vixardoxadzichastoroutautoxadvlgeu";
    out << "axameronziduophazidchastoraxtalo";
    out << "aldexadraxadoutagethuolzenblot";
    out << "aldoxanacladotharuodangeduox";
    out << "talzedcorahvixatargalaxato";
    out << "nachepnaxablacoxadnoxato";
    out << "zidnomachethaldroxatho";
    out << "uacheduothautaxalget";
    out << "nodalgednacharauza";
    out << "claxoutacauuoxah";
    out << "voxadouzaduoch";
    out << "aploxadrohad";
    out << "zabvlaxath";
    out << "chidaxtov";
    out << "ladaut";
    out << "vadi";
    out << "ox";
    return out;
}

QList<QString> table29b(){
    QList<QString> out;
    out << "a";
    out << "nox";
    out << "tagra";
    out << "notaxat";
    out << "valgethau";
    out << "neorgathada";
    out << "noxargradaxoh";
    out << "ochaudauaxtalga";
    out << "vaxandaxacloxtara";
    out << "condaxaclauzauoxald";
    out << "avdaxtaloxaclonzaclax";
    out << "phidaxtotaxtouzauoxauza";
    out << "votaruataclaxapraglaxacol";
    out << "taxelgladaxaclanuovautaloth";
    out << "azvuazantlaxochantaxnopatalax";
    out << "zantalouaxtagethaxadnopharnalda";
    out << "votraduadaxaldaxuagrautaxaldlaxod";
    out << "chemiugethaxornapatoutlidoxateaolza";
    out << "chevadolaxaldronadolzadchautaxacladax";
    out << "volchastoranvbiaxdeogradaxlodoxuapolget";
    out << "natoutaxarnaglaxziduoclantaxangroxnadroca";
    out << "legathnotaxalougeuoxadraphamclautoxaglauzac";
    out << "zaglaxtaloxadoutaluacladaxatorouacliuodoxadra";
    out << "nabvargedoxaclaxtontaglaxateryouapharmaclaxvdra";
    out << "nochastoraclausahvoltaxaglaxueodaxtautaxalothaxag";
    out << "geualadnopalaxtonolzintagrauzidnochapaclaxtolax";
    out << "vychastorauzeutlaxtonoxadroxalahziduocaladuax";
    out << "aclaxtouorpianchefmaxordronzacloutaxvmarout";
    out << "ueodaxalgethadoxuaxvioltagalzidnoxaprausa";
    out << "naclaxtarachvolgedoxalchinzauoclauzdcha";
    out << "vioutaglaxtateroxadnaphauochaldlaxaco";
    out << "phiulchesardrausahuoxautagnoxaldaho";
    out << "vragathzablaxtogradaxalgethnagoth";
    out << "claxtaruocapaloxardrouaxoterout";
    out << "viodragautaxnohacheldamphacla";
    out << "uochauzachuoldadnachauaclax";
    out << "vdraxnodalgradaxaclaxatar";
    out << "uatargrausahalplaxuatha";
    out << "chalderagledonaxtorua";
    out << "gevornahadontagacla";
    out << "nochastorazintoxa";
    out << "navalgethaduoth";
    out << "clidaxuaphant";
    out << "clinocadaxo";
    out << "ardrauzac";
    out << "zintoxa";
    out << "uecas";
    out << "acl";
    out << "a";
    return out;
}

QList< QList<QString> > table29() {
    QList< QList<QString> > out;
    out << table29a();
    out << table29b();
    return out;
}

#endif // TABLE29_H
