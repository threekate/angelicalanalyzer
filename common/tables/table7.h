#ifndef TABLE7_H
#define TABLE7_H
#include <QList>

QList<QString> table7a(){
    QList<QString> out;

    out << "onsonaxdalabragevanexodandolzemacheodalgensadeoth";
    out << "olzanvadladbraondamagendafavadeclangeneolrapharox";
    out << "dalchadmaraginosamendachedoxangepalegeconsadaxamp";
    out << "lavanzachadextuaondemphannexongemaxparcholadrahod";
    out << "vonchanoxadradmachoaldemphagesongonsadaxvardampha";
    out << "olzidanclaohanchasvardanvagahlodgaphnoxalgededvah";
    out << "ichanzaclinadaxvarnedogahapridaxarmagefaldvnabago";
    out << "alhanclanvanoxenbladonchanchaonaphangethnablaxanb";
    out << "nodolphagenodapnadonvarnehalexvarchednagephadelge";
    out << "nohadrachachlanzachavolzancheonchansanohabrahodah";
    out << "vortabnazaryzembloxacharnadongenvehaxarnachandong";
    out << "apresonadvagemmoxacladaxardnondemvarochaldahochal";
    out << "nodaxvarvbalongedeonchanodartagefvaxnarbalodadbra";
    out << "adrvzachanglandanvagesoxameronachadmahnochastorpo";
    out << "gamvehadalgodasdaxlochanchanodaxvagemnadohachioxa";
    out << "libanohdevangenodaldenchidroxaprodannebrvgahaxarg";
    out << "ochastornadvmagedapranvachanoxdronaldebrachalzada";
    out << "gvbrahlabrahvbrahhogadbroxlexarnabadgemnamvagelox";
    out << "dalgesvahochalvanodanzaandenbazemoxardexalvbagens";
    out << "olzichadaxlarvacheplapnagettraglansadmagvarsneoda";
    out << "lgedhadargenvagenogladnochastoraplangephoxarchans";
    out << "nodomronnagephachephnabaothalvdanzeachandachanozd";
    out << "acladvannoxadnargaldenvagelaxardodaxvarghnodalgen";
    out << "aprongasahnoldanvalabthagemvadargedohaloxandranol";
    out << "achypragmarttotgelnachoadronvaladborroxachanvahzv";
    out << "gamnapheolchanzaclandancholgemnagedvartnachoadran";
    out << "cholzadmachephachynadaxzvrehachaimahnozanvagempha";
    out << "noldanchadarphochanzagephzabvlolzacharnaxodransoh";
    out << "chaldronvaclangesnogramnachapthagraspotargemnaxto";
    out << "alphizachanodalbazaoxarneodalgemnachoaxarnadabnol";
    out << "zabchapathcardexnoxarnatlaxgolzachypnalgemprehaha";
    out << "doxargexachranglannodalvanahgedonachephaxphilzach";
    out << "norbndabaclaxtohpracastorzacharahgalzachanondaxav";
    out << "rohamnolzebbadoxlatnrgeldoxadnephaphangendangenza";
    out << "chaxaxgemnadoxagenvadaxvrabladoxchalzachvreangled";
    out << "abvrmahchanzachvrandemphnadomphachapnazachorzanga";
    out << "nodpvmaronalconapzembloradroxtahnoaahodadvbraxvor";
    out << "galgedonzanchvachvrdaxanlepagenoxadvnadodnabcoxar";
    out << "abymalondavadnorgethagebvartahlongenvaoxargedacha";
    out << "storlabaxtaroraxvartemplahochadmanodonzabahchapna";
    out << "doxtrahvortahclaxtommeronchevnadaxorgenvahgenzach";
    out << "achalzanodanchanvonzachemnohadnaxvahgedradvagemph";
    out << "alzednodaxachalzaoxarnahprogergadaxvannahlebancha";
    out << "dolzachvlonachanzanoxarvachandrvttablazdohaxloxto";
    out << "naphixaphedanloxarnachvaplichaxvartibaxnabvlazeng";
    out << "ongalzadoxpranchadnadolzidvancholdanchanolzadvach";
    out << "oladnadohaxymaclangentoapraxvanobalahvdrahdegadlo";
    out << "aprannapanangendannoytontagephazvdaxnozabvlandanv";
    out << "adanzachenodoxaldaxpradmaxclastroaemnachyolzychad";
    return out;
}

QList<QString> table7b(){
    QList<QString> out;
    out << "agednohchaplangaganoxadvangefaffratolzadchephadon";
    out << "ledoxmaraxparcheplahdoxangenohachynadlonpangessox";
    out << "volgednadababrahodnacholzagelaxvmpharondendaendoh";
    out << "claogdaxvrgebvargedohachapnahachyldanlochanzaclox";
    out << "laxargephaldemphagednabvlachanzadmachefapvlchapnl";
    out << "agendaxorzaclanvarnedochanzadoxalglassadoxvargefo";
    out << "affradonlathmachonnochastoragenvadahzabvlanzvraho";
    out << "vorzibvladachadnodovzvbachagednabahdeldamaphancho";
    out << "efonzadnagenzodaxalphadaunechonzachanzadoxalphaxo";
    out << "orzvmachafnobladonvangefafradmaxzvdrahclaxaxmaror";
    out << "pradvahgendalchansangephaclaxvarongedvachornablel";
    out << "aplaxvangemnabvlaapnodachildonzebachadaxlongephat";
    out << "nolzazchadvalchepalgedaxlongednaaxarvanondemphana";
    out << "nochamzaclydannozangeffaxortlydamvnvademnacholalo";
    out << "lvxardephadnachephzvlehaclangepahporgendaxloxarar";
    out << "adramvahofatnhrthhvfmkothnaasvvyhalyfodvahirhavot";
    out << "ineatbdelvrhicaneaeheymywirsneesoybdrgsichweltfre";
    out << "ttmtiiotvsteerhivldytncylntyynvdnredanotresasfoit";
    out << "loponrveromaevelelbnhgobezhthgsarpasesvergevtheit";
    out << "hvafyyrmlaeemnaioyaydmdoelieetydtressagsaedeosna";
    out << "lisofvigtrahctehebchelessleodartdrtpaeynvsnbivoto";
    out << "gemphadaxvlzachamfangednachefnodalgaphaadoxarneng";
    out << "valgaagonddmlaednivssanbolteytonttleaoltmenanepro";
    out << "gadachynsamvahsedmachylagenoxarphachadnaxvlnachet";
    out << "phazvrachadnazachcolgranzachynodenvachanalchadnax";
    out << "apalgezaaclaxorchaxachardaxlogdnvachoclaxtophorop";
    out << "zvblongenaxvrmacoanabvlaphachalzannechastaroradna";
    out << "ichydaxvargenvachefnoblaxvargeldrvngenpagedadvrax";
    out << "panchadnachoadvlzachednazabvlaapruzadnahvonranaha";
    out << "plvnchasvorgemnephaachastapharzichodaxvanzichanap";
    out << "olybnadahvolchancharvongenzachephnolzadanvaaldvts";
    out << "rabachendoxalgeaprohvondenoahchandexachlanzodnalg";
    out << "votarbvlaclynoxvacharnadolzymnaphalzimacheffallan";
    out << "ohadaxlochastorodmachvadornazavochadmachefazvlalt";
    out << "polongefadvnachanodalzachenzanzeopalgeddraxlongos";
    out << "achanchvapranclyolzinachomalchexpalohachefnadolah";
    out << "pragmaglagengemnadolzachastoraphymanadabvolzachrn";
    out << "gorchanserachefnadoxaldagefvolvdabrangehvdrachard";
    out << "xvrgephaclasednogabahaviodaxgolgedvorgelzachadnax";
    out << "aolchynochynadaxvolachvdavavonzanchestamranoxadra";
    out << "phiallochapmaaprvdamvodgaglaschonzachedaldvmarahv";
    out << "chephanzacradmachorpolzachvrapnagefaldrocapnaznan";
    out << "algednodapranzachvpranzaaldoxvargednohaxyrbavolox";
    out << "tamnarapholzabvlagednohcaphnoxargethnabladanzacho";
    out << "algedcapharmachanochabbvlaadvarnagepholzadnaachvn";
    out << "donzadadmaardrvmadaxolchamnachoradadadgmachefalan";
    out << "chanzvchachadargenphvolchednapheszendodgmaachasat";
    out << "volzadnaxnoxaldraphaachvzadelsphannoxalgenoclanap";
    out << "opronzampzedgragephaldvragedchavargednodaxoclanar";
    return out;
}


QList< QList<QString> > table7() {
    QList< QList<QString> > out;
    out << table7a();
    out << table7b();
    return out;
}

#endif // TABLE7_H
