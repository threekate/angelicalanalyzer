#ifndef TABLE41_H
#define TABLE41_H
#include <QList>

QList<QString> table41a(){
    QList<QString> out;
    out << "qi";
    out << "lzac";
    out << "locham";
    out << "mannacha";
    out << "lochasrump";
    out << "dochulguthar";
    out << "nozinupururgem";
    out << "lozirunchtothang";
    out << "nevornolapuxubruch";
    out << "vicharnuxtempnuchalg";
    out << "lacoxurontunsluxyporno";
    out << "taxurgetralaorrochalclon";
    out << "truncboramvamuaphangalgeth";
    out << "arcochchidorgaluphzidnopharg";
    out << "laduxurbhvmacolvinodruphangath";
    out << "chidonupolgruphaxnerecothaxaronc";
    out << "deoxnaprugluxorudnorgalphuxorgalno";
    out << "pulgephaconrudzidnopharurphalgurbvma";
    out << "vohoxoronchelchoxrescorgethpurzethnoxa";
    out << "viudlabubortaphansathchimaclonvxundepbar";
    out << "gvrabhalziclorbfavegnarapocanzaloxvornonph";
    out << "chidompnuhaxorglathgolupruxladurchinumvornax";
    out << "hvnborhilcvrupruxilzichvrzemfamvelchaxurnzidno";
    out << "pothoroxyblupvoxmechcunvumpnucoludrunvarnilzecha";
    out << "pulchvdururnopthzinbaxzintuxuplozinorurnuntontox-";
    out << "olzarnondungoph^gethaxornchidobruchcurnzipha---";
    out << "laznorvamuxpurthoxlancuphacloxornothviodubru";
    out << "charumpnolgrthgorsuxpolhvxtarponupvirgetha";
    out << "loxarliudvuraccrumzibtharchorudunduldupt";
    out << "Facoscoharnodoxulbarumvompugeldurdnodp";
    out << "zablucuxturnubruchchvrrunchgolzadgna";
    out << "pvruxtoxubvugelhidruxlorogruvhaxar";
    out << "colpharnexupvatarglvrugavhabazyn";
    out << "malbothurconvurontalgethnuxabr";
    out << "odnucolgurchanzachloxunderhi";
    out << "chenrodgalzaphuxylporodonp";
    out << "chablazachlvrgethardunph";
    out << "concluxzidnophaxulgrux";
    out << "themupharhindoxabrac";
    out << "podurhoschchervang";
    out << "lapulmachuthardr";
    out << "zincoxulbruxno";
    out << "vixurpuladga";
    out << "donphiruba";
    out << "hapugath";
    out << "coxtog";
    out << "laza";
    out << "co";
    return out;
}

QList<QString> table41b(){
    QList<QString> out;
    out << "v";
    out << "ilm";
    out << "ondur";
    out << "axaplug";
    out << "gorduprag";
    out << "hibolgamaxa";
    out << "ripomeronanva";
    out << "lorodrohcheldbo";
    out << "zimnorucprugnuruc";
    out << "chidroxulphalchalde";
    out << "zidlaxurmuchaphchiong";
    out << "algedroxupvarontanchlor";
    out << "tupvrumalhvmichlaqnascolg";
    out << "lurvanavangeborvinmaduxilga";
    out << "hvparolodugrouxubrohselgadsna";
    out << "velsmexmachvbalonhorplaxlarduga";
    out << "gefdurglorvinzicharhanrunchrigido";
    out << "larngonruchuaxudurggedvanglabbarnoc";
    out << "vxurchapolgethnucrostruxvilmuxlabnlax";
    out << "hidvarngemunchildroxpilgepthcarzadzuora";
    out << "vihubvrbmonzambnivodbacfagravmamproxaclom";
    out << "virgnazoldvxurmanneputothcaduxuranantochalc";
    out << "rumzachlongenvahzibuborhvpruxclutorgamphzicha";
    out << "vorcapnurgalzidnopharurvaxumvangenvaxtoplaangox";
    out << "chripholdothagruxvarmaruchzichbluxzidnomphalgetha";
    out << "himacharoppocropavripolgvaxmurchaxurchanzachalz";
    out << "viuxlargpvgaproxumprinogellardothhigadbrafron";
    out << "nurzidnophurglorvaxodoxuronchpurutrumphalga";
    out << "loxucrogruphzabnrantantoxenorgvumzachaclo";
    out << "taxanochaphnurvaxhidrodplochaphrenongha";
    out << "dunzanchalnodurunvongrephzababpolonhi";
    out << "ruphagnaloximytephargersodgplvcarna";
    out << "doudrugruphhixonruphurgednupvraph";
    out << "lardontaganulgethorzinvagonaxar";
    out << "revancaxcorgrumfruglixorchido";
    out << "tatavalnebronzinchiloxaudra";
    out << "zymetreharbrofarnegedroma";
    out << "drithvelontrabfolghexol";
    out << "apvarnotaxchironpnlga";
    out << "rempangaxvrnothadol";
    out << "dexuchaphaxnalhix";
    out << "udlaxnarbvolcas";
    out << "chizanpvlgago";
    out << "vlzinoppung";
    out << "vorchaxor";
    out << "taxarch";
    out << "lorvi";
    out << "not";
    out << "a";
    return out;
}

QList< QList<QString> > table41() {
    QList< QList<QString> > out;
    out << table41a();
    out << table41b();
    return out;
}

#endif // TABLE41_H
