#ifndef TABLE34_H
#define TABLE34_H
#include <QList>

QList<QString> table34a(){
    QList<QString> out;
    out << "cx";
    out << "hoba";
    out << "lezado";
    out << "gengelpa";
    out << "chavanzina";
    out << "leoparantant";
    out << "lothaxarbbotzo";
    out << "ancotaxarpalaxci";
    out << "zvddrabapranfancla";
    out << "viochepaxoraxtagepht";
    out << "ondroxalblaxanchonvaha";
    out << "zidocathalzedorvangephal";
    out << "noxabaladtantolpacordothal";
    out << "zidclothazinvachvdroxlionapa";
    out << "chardaxalgednobraganvahhanzipa";
    out << "larnanchavortsvsborghaldloxzirro";
    out << "drampaxnocarbaclopthougrintolgolts";
    out << "avrobnvrepraclefvoscaropraxcharnahzi";
    out << "onzilanpvnacoxaldaunodarblopaduorgalox";
    out << "pranclaxtoavantziduapargvbblothtartoxorg";
    out << "bachalantantefadvauacothlapsacharnolproxta";
    out << "chidviracheldroxnalpohchornoxorbvargathalpod";
    out << "nochalmadraxvrzachiadraxvlpomagliscardaupaxapv";
    out << "chionzalchaxtraxolothmoavrodathgebladanvihobraxt";
    out << "lachanzachinzoltaxaldraxprionochefaxolbavornotha";
    out << "darvangemnopolaxtorhothlescohdauzedbarchalvaha";
    out << "chidnoxaplinocantantoxalvamnachalvstraxappor";
    out << "nevortalangepaxadnocathchidnochapvorgraxta";
    out << "vidravaxolcloxchimanchalothgenphzacoxlox";
    out << "ardvbaroxuaxolgachornaxtalplaxacloxtar";
    out << "viodraxilblaxarmanazednochadoxvrgenp";
    out << "valcothaxarbachnoldethprochastorga";
    out << "viodraxaxarnouvinzednothaxorvago";
    out << "aldothachadnoxpolgethaproxalax";
    out << "vidrathaclanzachnochalontarb";
    out << "viaschallvrgraanorgripvoro";
    out << "tempolganophvxarcothalga";
    out << "pomchedmaxaldaclalorba";
    out << "vornexaplargraxvxorb";
    out << "avramacoxalgebthax";
    out << "olapanvanoldoxar";
    out << "teontalamphzid";
    out << "vargaloxadra";
    out << "chodaxlapt";
    out << "nobalant";
    out << "charga";
    out << "coxo";
    out << "no";
    return out;
}

QList<QString> table34b(){
    QList<QString> out;
    out << "l";
    out << "apa";
    out << "deomp";
    out << "chanzac";
    out << "lepargeno";
    out << "viradraxmal";
    out << "hvrcathaxalga";
    out << "lontoxaraphvlgo";
    out << "axprothanclaxarah";
    out << "gedovargavornedoxal";
    out << "neparonhvnzachalanvah";
    out << "carnoxtalgodnorhvdroxro";
    out << "geodraphaxalgraxachardaxt";
    out << "vichanzauphalgathloxarbhvna";
    out << "chalzidobraxzincathaxargethal";
    out << "procastoraxaroxneobalchapaxlarg";
    out << "galzenphaldroxnehvschananvompzvra";
    out << "nechodnephabbarabazemanachaxarpalax";
    out << "chidnoarnopalanchalzachchiodnoxaxarba";
    out << "nebathvalcothadhornevolzincothagethalga";
    out << "racraxtartoxagranlongephaxolgraxzichastor";
    out << "achantonabrofgofolnerzacachontoprepsanchald";
    out << "avronphaneorgedvaxalzinvanchardauzauchallongo";
    out << "arzanaxteomnaphalhvdlaalgedvarchargathziutoxnal";
    out << "adronoxandogalexarglaboncathzvdcathalaxmarglocaxt";
    out << "lidragothnaxorgviconzazimaxalgeodaxnopharalgedo";
    out << "nompongalaxalontantoxarzvdnamphgenodaxzidnoth";
    out << "voarcolstonaugerbagaprenozararnamphhaxaldax";
    out << "rechoudanghabalanxardrougabaltoxameroutal";
    out << "viodragrasnescolulgvramtoxnephalcorarno";
    out << "chemvompoloxaxarbvoliodothasclontouge";
    out << "avornodpiamchastoradougathnoxaplato";
    out << "vidragamphaxardroxarproueodangeth";
    out << "zidvarchathadoxnorbalcothaxarca";
    out << "vaxmaraphzidzalarmvrocoxornzi";
    out << "auvongouerbaxtalgothlordaxa";
    out << "viornamgalzidnoxachargaho";
    out << "lesqvithgohonzachlebrox";
    out << "aporgaxloxnorbadnamph";
    out << "zidarbaxalchonatoxa";
    out << "reonapulatoxvrget";
    out << "apvornatoxchido";
    out << "hopocoronclax";
    out << "nargaroxard";
    out << "voschetha";
    out << "nervaxo";
    out << "phich";
    out << "lad";
    out << "a";
    return out;
}

QList< QList<QString> > table34() {
    QList< QList<QString> > out;
    out << table34a();
    out << table34b();
    return out;
}

#endif // TABLE34_H
