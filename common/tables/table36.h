#ifndef TABLE36_H
#define TABLE36_H
#include <QList>

QList<QString> table36a(){
    QList<QString> out;

    out << "ca";
    out << "rsap";
    out << "axodal";
    out << "pothogau";
    out << "alzichadax";
    out << "zorchorephau";
    out << "alchinzochaxal";
    out << "depraxtalgethnox";
    out << "armanvaxlengodaduo";
    out << "apvlachaphzibalaxorg";
    out << "charmaraxaxorglethgoxt";
    out << "volzidnothalplothaxornat";
    out << "togonphaxargenphaxalpaxora";
    out << "temiphoclaandoxammaroutvlgax";
    out << "lepotarchalchidnodanphaalclaxt";
    out << "changethalzidnomproxgeouacharsoh";
    out << "lebarnaxomporthascothapolgenphauzi";
    out << "vinzinbodoxalchethzimacronalphonglax";
    out << "neobrantaxlaparganexolglexorgalebacoxa";
    out << "nevarothaxnorgallandongvxarnocraxalcloxt";
    out << "chammaphaargangalclaxlidonphacheblaxordomp";
    out << "anonzichadalclothaxornoboncharnaphalcrinddax";
    out << "vibraahgemnocalaxorggeothalcoxtorgaproxtalgeth";
    out << "nobavaxordanphaxmachalaxalglodaxlagrauzinbothanc";
    out << "charzachaphorunodalladchauzoclaxtaruothalpraxtah";
    out << "gemenephilolzidchidoxargalgroxtamnoldathloporg";
    out << "hvcharstornarvagoutalgethacloxaronphzidmacol";
    out << "zvrescothaxarnoxvntaxalggradoxapolgchidnox";
    out << "amornalontapvirnamphvrguarziutoalphanpho";
    out << "chidrochothargethchadnarbranziudroxalg";
    out << "labvargonhoxalglocausachvrnothamvato";
    out << "lechadnoraxchipoxamarvaramphzidoua";
    out << "orchinproxaxornontautafargednoxt";
    out << "alvorchethalchoraxarpoganzacha";
    out << "chvdrachalgrondoxerglarvahax";
    out << "zvbalaxargepharnothvoxyugl";
    out << "antripoufamnopraphalyngy";
    out << "rancolaxchinvoxornabva";
    out << "demantothzalgahlexon";
    out << "apvornatonglopadro";
    out << "phinabadrathanzo";
    out << "narviduaxplica";
    out << "toxameronchv";
    out << "marqveldax";
    out << "cliupaxo";
    out << "ucvorg";
    out << "amva";
    out << "ne";
    return out;
}

QList<QString> table36b(){
    QList<QString> out;
    out << "q";
    out << "var";
    out << "gatha";
    out << "noxalda";
    out << "vonchotha";
    out << "cladoxmarox";
    out << "ynavalhvdraxo";
    out << "nervaxandegopth";
    out << "axalgorguarcothal";
    out << "hvraxnaveronchinzoc";
    out << "alpharnoxvxargavondor";
    out << "alplancraphdaxargleopra";
    out << "datagoarnonchivansalaphax";
    out << "chvmaroncaldaxornamvagethax";
    out << "coldragraphaxolgarzimnopharda";
    out << "chvrzachaldaxnopalvalontoxargat";
    out << "poloncathlebvarapharzintaxaxorgat";
    out << "mataphargethnoxalgradcodanphalaxorg";
    out << "pirachanoxalgethaxardroxvlnoportoxarg";
    out << "lecastorgandroxvnaraphaxarchanphzidnorb";
    out << "axalamphgedragraxalgraphachantoxneopadaph";
    out << "lednoptacheldaxornoddamplicodancormoxabrach";
    out << "zidnoplaxarzimvamfaxvrgethnepolgarpardroxallo";
    out << "carhapnaxvlnepthanclohchimanchaladachauzvdrouch";
    out << "azidnopharaxalgraphlabaldoraxynnoppioraphgedroxo";
    out << "lanfarnoxzvbraxaldroxaprmnaphaxalcothachanvach";
    out << "nelzadpacharnochapascolgatharnonphaldoxarnap";
    out << "vichastorranzachlonsaublaxlebaronphchefnax";
    out << "vrdraxtoxammaronmvhvdronvoxgemphillaybay";
    out << "aieialapieiaicansoranvoirsetcarsachlox";
    out << "rauziblangloxardvioncharchascharnaph";
    out << "vornedgaclinvaxorchephaclaxvarchad";
    out << "lemonacoxardronphzichastoramvage";
    out << "lobvlapardoxarnoclaxargethalga";
    out << "lozinpalaphzidnochastoraldot";
    out << "nevargalonhvndrothzichasto";
    out << "vomravanzidlathzempaxlax";
    out << "necolgethaxarglecostor";
    out << "amphachaxlermoudnepo";
    out << "daxartagalvdraxarp";
    out << "navexoalchemphau";
    out << "geodagraxolaps";
    out << "vrnoxarnapla";
    out << "toxarbarno";
    out << "demaxart";
    out << "navort";
    out << "zido";
    out << "al";
    return out;
}

QList< QList<QString> > table36() {
    QList< QList<QString> > out;
    out << table36a();
    out << table36b();
    return out;
}

#endif // TABLE36_H
