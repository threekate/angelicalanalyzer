#ifndef TABLE31_H
#define TABLE31_H
#include <QList>

QList<QString> table31a(){
    QList<QString> out;

    out << "al";
    out << "goth";
    out << "nochad";
    out << "admachad";
    out << "laxornadox";
    out << "yneporgelzac";
    out << "ocheldaxlarzac";
    out << "vnadrahachephalg";
    out << "notagalalchethalga";
    out << "loxandonvagelziutoxa";
    out << "nabarothzidnochalaxarg";
    out << "vadabrahsaxaxoroxlapraxo";
    out << "chadantaxepnogalaxybvangal";
    out << "zidnophararchantaxalgathoxar";
    out << "neopalgedroxyxalaclaxamargedra";
    out << "zanchephlaxongethlabaxtalpethonz";
    out << "anvarnochephzidnobagethaxaladnoxac";
    out << "chidnolarvachapvlgethaclaxateronnota";
    out << "pvladnogalavorgethzachastoraboldonamva";
    out << "geothnoxtaplaxmatoraldempuayveadgalgehaz";
    out << "charmatoxalgetholchaphlacvtauolandanopaxah";
    out << "zichastorangalaxorgethaxadnopadanlaxaruapardv";
    out << "chadnopalagengaloxyntagolzachlahdvrachachephar";
    out << "doxomaronvinotalalbrahaxarnoxalchephnadoxanoxima";
    out << "volgethapvlzachalchvdrahaclazadnapharychaddalzeh";
    out << "lopagedarnoxanduopalhanchadoxalgethnotaxalceth";
    out << "avirnodalphagnopolontautoxarnapalgethaxoroxa";
    out << "degrabahchvrachachanzachlothadguothargvnch";
    out << "alchefneyalchodouzendouzidvblachimachold";
    out << "orbethanpangathalgednachapuachatoronax";
    out << "vchadaxornadonvongenalziduapadnachep";
    out << "chiduolaxadonopvangaloxinchauzaoxa";
    out << "dorigalaxvpagemvantacothascholad";
    out << "vorremanolpangethaxarbrahloxor";
    out << "pvdantopaphanneolgethgalaxor";
    out << "vdrangadaxvidabraxalchetha";
    out << "ziugednocalvuzanvachgvla";
    out << "teoparounozalvuzichalg";
    out << "arvaxolgethzimmetrax";
    out << "aldexouapviachepha";
    out << "danziugethnobalg";
    out << "axoroxalchepha";
    out << "nogadaxvnzac";
    out << "chioxauald";
    out << "piodanta";
    out << "labeth";
    out << "arfa";
    out << "uo";
    return out;
}

QList<QString> table31b(){
    QList<QString> out;

    out << "a";
    out << "xod";
    out << "nocha";
    out << "donzach";
    out << "lochasnap";
    out << "vonogathaxo";
    out << "davtagaladnoc";
    out << "vocastoradnoxoh";
    out << "aldevaraxangephax";
    out << "nolzadnochaphalzach";
    out << "chiontognapalgethnoga";
    out << "andvnazachalchethaschar";
    out << "nodapalohaxaudochantautox";
    out << "labascavahnolgethaxarnozach";
    out << "aplantagnogalalpazolzinochaph";
    out << "lepaloxorzanvahgordnoxtamaxabla";
    out << "deontaxlabvacothzanachethalzachor";
    out << "zintorarfronobladleopargenochapalge";
    out << "axameronalchefnochasaluexaplandaxonza";
    out << "vbanzavarnochastoralzedbagelorchonzacho";
    out << "ordephaxarnochabvoltamagelaxarnocathvlzac";
    out << "viodanzalaplaxanoxadnavaxordaxolzaganhouzac";
    out << "longahonaxalapvdraxhvnazachinodangeldodoxalph";
    out << "chidantaxallapaxgenodaxamvionabdalzachvrnohadax";
    out << "gethmagolachadnochapvolzichardradouchanobvarbalpa";
    out << "sinolgenoxaxalprichastoramgephedantolaxammeronv";
    out << "vlzachacholdaxnevaxapvangeladnoxaprauzachgord";
    out << "audaxmonacaphaguahtoxateronplodoxvrcethalph";
    out << "ardonglaclangeduodarvchasmarautantoxolaxo";
    out << "pachednocalgehotagloxanoxvolalbalgethax";
    out << "olzemnagephaxornadoxapolgethaxordraxo";
    out << "neopragnoxalchednophardohantazavlga";
    out << "vobsahchevachlophaxamarontatpolga";
    out << "oxinodoxalbamvaxadroxyntogladox";
    out << "vargenovantautaxachervaxoldra";
    out << "choandoxalbrahdedroransagah";
    out << "chimaxpardrophalgednorvnc";
    out << "alplaguaxarnothaxarbrah";
    out << "chamvaxalpaloxdvchald";
    out << "avornouascholdapath";
    out << "machephachacanval";
    out << "vmvagephzachado";
    out << "laparaxalgenp";
    out << "zinvachalgo";
    out << "achadnoco";
    out << "ampagra";
    out << "houzo";
    out << "anz";
    out << "a";
    return out;
}

QList< QList<QString> > table31() {
    QList< QList<QString> > out;
    out << table31a();
    out << table31b();
    return out;
}

#endif // TABLE31_H
