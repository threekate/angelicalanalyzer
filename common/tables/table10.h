#ifndef TABLE10_H
#define TABLE10_H
#include <QList>

QList<QString> table10a(){
    QList<QString> out;

    out << "alchadamvagoandargethadgmathonlogendaxvarnedophan";
    out << "vrzanvachanoxalgedohazvzadnaxolzadchansacholdanva";
    out << "naxalpalahoxalgebarnozembladonchlyndoxangefollaxo";
    out << "darfonfazachydarbrahadnaxardronachednabvolgemplax";
    out << "orzednachestorazvrehalendovahnotangehachordaxmach";
    out << "logodnodaxvalgednachanpalchendaxolchanapronsapaha";
    out << "olzidnachinaxmaconaldextravnapladoxamphingephadoh";
    out << "nachvbaholadnahgendoxadnapaclahvaxildamachyonabah";
    out << "voltabnagolgastoganvolgendaxargethachathnochalsoh";
    out << "doxargephaacheldaxvortnachaclanzabrathadnachalglo";
    out << "varzonchaladmachedaxvortamaroxalgradahcorganzache";
    out << "proxaldraonganzaclohvornabnlaxadrabahnocastoronga";
    out << "laxpargethasclydnochanvanzanchanoxadvargeachednac";
    out << "dolzydnagethachadnachorvongrefadvlgethachordahgeo";
    out << "angededmachvlavinachvvrchanorclangeldaxvrangehnox";
    out << "agladaxvartepluxzadnochanvansepornechaoldangethen";
    out << "naxolgrapheadmichadarlvbanaxvortemplesazongemnaco";
    out << "avornagempalgohaxandrahnochastoraclydanayyensacha";
    out << "lansantagovardoxaclydaxargenohlapadmachanozemblah";
    out << "voxanzachaaxiplacannavadymacholgenzachansolchadax";
    out << "placebatnothabnaharnanvancarmaxvlgedvarapronsaohp";
    out << "ascladnachoardraxolgednacheapnadoclesapvrielandep";
    out << "alphangegalsodgabrolgemaxbonchedonhandadgmaexeqva";
    out << "nechostormanglodedsoroharclangnoplallatonantonigo";
    out << "gendachamahvolzadaxvrneyvlzandayyeldabrahgeontala";
    out << "plvngenvadohaxpalachvsafealdenaxvrnagefablvdagens";
    out << "axalgransandochorcabvlacoxapnachondradgmahnochanz";
    out << "olphantagefvadagelargehozachadahlondronamphigalav";
    out << "noxadvaranfantefavangenocapaprongehvondanvageahod";
    out << "zymychadomvaoraxtohapragmanolabvlontantagemnolzah";
    out << "chodnolachxosappraxochalnotabtosvnzechaldextonzet";
    out << "pangednodahvonsentogemfalgethzachedondamphachepes";
    out << "noxiblazadvrdantagoaplagansanolascopohvnzazamvoto";
    out << "galtallopaandagadmagogangapanchaphvortexvdrolapha";
    out << "alphagramnolsegeoldahvocapnasebdochelavonrahgetha";
    out << "nodolzachyplachednaclanvosemblondraxvlnachonvanra";
    out << "gotnagosavotarbachahcontagnahcolgemphaxocheldaxoh";
    out << "segortagalphangonsalalgedronzychadaxzvchastorprax";
    out << "noclavonsapnagethzidachardanclaxadnadoanmicodanze";
    out << "alzochapronagemadolpronzedoxarblahcoragazlebnogem";
    out << "argrahglospromichansadgnagehgedionabvolzachazvreh";
    out << "cansadgnacholabvgahalpagethphachadnachoxaxclazina";
    out << "domvangeohaxplahgedohaclaxpracontanohgemphachonza";
    out << "zvclazocladaxvaganzednachedalgednochavaxnaorchedo";
    out << "planchasmachaphnolzemplavoxadrahgehvdzachabzvdaho";
    out << "aphalgehnochastorvdanchasvorgemadaxvdlabzvchalazi";
    out << "ondangethnolzednacheachefnoxadriangehvortrigonaxo";
    out << "algemzavealspracapnocharredoxaxlohzinaclionzichax";
    out << "vorgempalchastramatoxamphigiladayonchanzachioldah";

    return out;
}

QList<QString> table10b(){
    QList<QString> out;

    out << "achadnochapnachalozadnachanzechnozalgethornachona";
    out << "aphichadanzendapradaxpagelolzachadnamvageladnacha";
    out << "doxalgaparcholaxvachadnochaxrosalarvornightonacha";
    out << "logednohapranzachapnagethalzadnachadaxlohanzeocha";
    out << "alzichadanvoxargaphalodaxnohvorgednachaadrahadnah";
    out << "volziblesadnodgastarelzipharesaproximoxzvchalgepa";
    out << "zadchalzedoxuagodnochapnolartagemvaxartagedohanar";
    out << "volziblachadnaphararchouhaanclandaxnodrahalpvdach";
    out << "algeschonadalpagemnochanlaxnarvaandrraxoudolpanch";
    out << "axargeohadnoxphichanzanahvortomonaxargenvageochad";
    out << "aladnaoxalpadaxnochalzicoryvpatorcharsangraphaxoh";
    out << "nechadnochaualdenvagesolzibvlaxtoxntonacolzangeth";
    out << "alednaphavdrahzingendahzydnaphaolchadnaproxalgeph";
    out << "acharsannochastoraldvzachadoldimachoualcydoxvorta";
    out << "algethadgarzahomnachodaxachyoxondahadrochanadoxah";
    out << "nochapnachanzoploxargemvalchadnoxalvaxargethanzah";
    out << "arclantaphanceprachapnabonganzachonnolchydoxalget";
    out << "orminochaalgetamaxcholantangalaxvahaphinachenzach";
    out << "vdrogalzadmachnogamzachadnoxalgathoordaxongephnal";
    out << "zednoxargendahzymphanchanaxodrahvohaminodamaphalo";
    out << "vlzadchedaxarbpolcadaxornohphachaloartedgnacopola";
    out << "vrgemnedohanzadvrangadronzinocloxonornadontagesno";
    out << "chyfonablazartpargenvachanzachadnohgemnacholzanta";
    out << "voncharmazednooxondachahplaxtorgenoyphanacholadva";
    out << "vlplacharmachadralchadnahvortraxvlozichasvdrahonz";
    out << "astorgenoxanahprachanzachymyolzachaaldymazadnacha";
    out << "vogestrabaxtohdrvxadnachovolzadnaacvantadgmacopha";
    out << "abloanvantaxolzinodanaldexvargendahalgednogeniaia";
    out << "plvtantoganvahholzadnozacappocaxaldolzibozacharca";
    out << "geonthalmaxvrazanchanoldroxpalytoahozalzadzacvtho";
    out << "noblazachanzacaxorgenotantobalgamalprohhanzaiotha";
    out << "ardochadvalgenaldvraxzanchvzadodrahnoxolgradontar";
    out << "acharnophanzohoxcydonaldoxvdraahvolvomnechoalgedo";
    out << "nabvlaxagethrogalzachadnotapnodaxoxvochastordraho";
    out << "olzadnachephanzvnadovangensochansaagemzealgedaxol";
    out << "axargepalgedoxnoxargatlabnodavaxartaracolgamvaget";
    out << "aldagednachocablachadnachaparchefaadoxarnachaolza";
    out << "talgethoxarnachanaldenzachenaxarmachepmaxarchanah";
    out << "aldredohadnagorgempagelarchasvraxgenscorzachalgah";
    out << "colganradachoclyntangehvomlionaldahodnaaxarnached";
    out << "vortynacheflaxadnagethabladaxnochapaschelamahayah";
    out << "donaltadgmachanzanobvragensalchangaphacronzachona";
    out << "drantantageaxargenoalgechadnahgolzadvahalgethroha";
    out << "gonzadabochadnocophvorgethalchydaalgethonablatoaa";
    out << "ortarchavomnorganalzydnaphaoxolzadaxplaxonchachan";
    out << "dolzytaphanvdrantaphanaldixonaphvdrahgehendahalgo";
    out << "vorgendocloaproxuachonaldentaxonachephalgethoxarv";
    out << "nochastangentachealchadnaphanzingemnadaxvantabah";
    out << "achildanolzadalgednaolchaphadoxanochanzoramplacha";
    return out;
}

QList< QList<QString> > table10() {
    QList< QList<QString> > out;
    out << table10a();
    out << table10b();
    return out;
}

#endif // TABLE10_H
