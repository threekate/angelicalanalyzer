#ifndef TABLE25_H
#define TABLE25_H
#include <QList>

QList<QString> table25a(){
    QList<QString> out;
    out << "adnochastoraxantagnoxadnaphaadnochanzachadnochast";
    out << "oroxidolnachoardroxnechastorazidnoxaplanzachenget";
    out << "nolzachethnaxadoxavionacestraxacheldoxantognachon";
    out << "violzachefnaxadonzachnozaldloxamvagendagathaxelzo";
    out << "nozichadnachazachalpadaaxlantagnodagentaxalloxath";
    out << "aviontaglaxismachadanzinaxadrachantantonaclaxater";
    out << "oxadnotantaxanoladantaxylzadnochazichadnadoxviota";
    out << "naloagnathichastorazichadnoxalzachanzachadydnopha";
    out << "zidoxadnotalgethzicontaxnozionaladnoxtantolgethan";
    out << "andnaxtaxlachodnodachathylzadnoxychanzachazildaxa";
    out << "lionzachichanodoxchiaplagnotaroxachanzaclaxidnoch";
    out << "achodzachadonzachinzichadaplataxacheroxphinodaxah";
    out << "geographaxnozildachonazentolgenaxyparonzidoblaxah";
    out << "intoglaxacheroxnochanzachnaclanpathlabaialantaxoh";
    out << "vilchantaxnochalzidnochalzidnadaxaclaxarnothanzah";
    out << "aldvdachaphzinadabrahziochastoralzednobrahzibalox";
    out << "aldechaclonzachintaxacharladoxanzicladnaphachedno";
    out << "dedongraphaanzednoxalzedcuthyblaxtachateroxantoxa";
    out << "denopanaladenoxadnophaxilzachadnoxadnathemaxvioha";
    out << "alqvithaclaxtrahnebastaravenodaxlednodaxtartolonz";
    out << "chiontagaohazintaxantogalzanvaronzidablaxydnochax";
    out << "chindradaxaplanzaylzednohachardoxtarleochanzachax";
    out << "palatinohapranzanydnochapananlolgethazandoxvarunt";
    out << "violtachapadnohadsydnolzintaxnaxaclantoxadavinzac";
    out << "unzentolgethacloxadnochaphalchardeodragraphaplaxa";
    out << "neopalganohzidnochaxalzechachethalzednoclonzochal";
    out << "gahanzagantaxolzachzichastoraldithanzachnolzichox";
    out << "alclaxachornidoltaxarnahyntogranzadbraxaxargethax";
    out << "oalacohcadpahcnadchinzachathalzidnoxalzednodagrax";
    out << "vitolgathaxarnapaxydnogalzintaxalgethaxnachapalze";
    out << "avornaxaclanzachaloxarnaphaxneoteronyntagalzachan";
    out << "zivastoraxniodaxtartontongethabrontoxalgradaxvoca";
    out << "lochastnachaclanzachachnaxochvorchastoxadolgathax";
    out << "olzenvacathaclaxoneclantantoxadoxnechaladroxadnoh";
    out << "volgaphazintoxalzednochaphzabvlaxadnochaphonsacha";
    out << "natolgethzadchanzachothintaxadnaclantantoxadroxal";
    out << "chindaxalchantaxaclaxtoplaxateronvionzachnecholza";
    out << "gidalzednopachadnachonadnothanzachaloxadnotaxtote";
    out << "rolachalzednoxtaterolapraxoclanzachonzintaxadnoxa";
    out << "avolchinodavantaltentaxaroxaplaxtaphaxtonaxargeth";
    out << "oxelgethnazachabaloxadnodanzachnotalgathachoxadno";
    out << "anpohaaortnazidnodaxadnacharalzednochapaxtalgetha";
    out << "notaxateroxviltoxazenbloxadolzednotaxtoterolavans";
    out << "nopraxatoxatoxneoteronangethavolzednodaxtolgethax";
    out << "niontaxathneolzadnoxadnachazintoxaperoxchinadafax";
    out << "zintolzednobahalzedadoxachansachacharnoxtaxtolzav";
    out << "chidnodanzachalzednocladantantolgethanzaphanatols";
    out << "vichastoradloxadnoxapolzachacloxangethalzednolzac";
    out << "aphalchadnoxadnaxtotenonazentaxantaxadnachanzocha";

    return out;
}

QList<QString> table25b(){
    QList<QString> out;

    out << "omnemphagesadonanophando";
    out << "natoxadraxnochanzachananv";
    out << "acholdaxAnchadaxardrotha";
    out << "xalpahgethanoxardranvanao";
    out << "nachesaxamarontantaxvlza";

    return out;
}

QList< QList<QString> > table25() {
    QList< QList<QString> > out;
    out << table25a();
    out << table25b();
    return out;
}

#endif // TABLE25_H
