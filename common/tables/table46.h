#ifndef TABLE46_H
#define TABLE46_H
#include <QList>

QList<QString> table46a(){
    QList<QString> out;
    out << "g";
    out << "lao";
    out << "neopi";
    out << "chudorm";
    out << "urolchena";
    out << "praxlafagar";
    out << "aphgesgelhoxn";
    out << "abvulgebboxvrna";
    out << "vurtupulgnexoruvu";
    out << "chulmurphingenphaxo";
    out << "pvruddonogrifdraxziar";
    out << "runubragenphaldlapvirto";
    out << "vngemphalholnedruvalchilg";
    out << "ornevarorgritrubarolnuxzicl";
    out << "argednodurglibvranziphanzelch";
    out << "laxnurgphichusreontuxnervronclu";
    out << "phichodurnhidroxulorphzudomulchur";
    out << "vitrubruchgernoldephvmudolphzirchet";
    out << "cumnoxyrnavargothlonphzaxarprollivang";
    out << "nemruvuhchabudruphzithgerdnurronphcuulg";
    out << "luxordevunchorappvdoranfargefalzirgranvan";
    out << "nedroluprincuduchephlergronzilloxixarnnamph";
    out << "zalgephrexcongludnorguxurbvatramuhchirvanphal";
    out << "exorunlithgerediansaxlempvlemunhircolozizrilgal";
    out << "ohaxarruxgalbalcapurcoxdudvur?emnupvruxfalgepgarz";
    out << "zurrunuliaxloplorvexnamintapulnercothalgruporna";
    out << "phigalzichardranphqvellaqnexorengexsabvirmanc";
    out << "totogunphvoldomphabzivanargbutnaborcrinzach";
    out << "dodogrulhinziclunelgremphzircharftorforra";
    out << "gahaxlepvlubruxhanrophiardonuchoperunph";
    out << "lapvradorphminzinchlacoloxzirconzacha";
    out << "purnulphzirchalapviotanphbethgumert";
    out << "nerodoxzimreythtanphuxrehoxu'nu'lga";
    out << "hvrgenbaxampcolnecorondoxilgred";
    out << "emvurfixpugedru'xgelzidphharno";
    out << "lamfogracorgalfapolthanmaru'";
    out << "zilympodylzirophuralcongu'";
    out << "nerqvatzidoxzvdlupcapp?";
    out << "ziortalgvaxarnnexolpu'";
    out << "demvachnervongthaxo";
    out << "remocarnopunmudra";
    out << "zirophururconge";
    out << "nixolgrexdurg";
    out << "zilcoxchiar";
    out << "zaroxalot";
    out << "phiodux";
    out << "lebac";
    out << "gio";
    out << "a";
    return out;
}

QList<QString> table46b(){
    QList<QString> out;
    out << "n";
    out << "uro";
    out << "zildo";
    out << "pothnoc";
    out << "chuligano";
    out << "drunvaxneol";
    out << "gethmuxluonga";
    out << "nevaronphduxuro";
    out << "norchrosdextulgah";
    out << "zimuchergphzalcoxou";
    out << "puncruxnelvudorndulco";
    out << "zichustorgpungulurnopoa";
    out << "argedroxalclothnuxarnogal";
    out << "vidubalphaxalpothzvrgelphal";
    out << "zvdrubruchnervexleugenphzalga";
    out << "viotarunchchomlothmurgaalphaxne";
    out << "durgathlexadurggursangaxarmuronga";
    out << "phiodubroxnephulorgvantrulpdethcomp";
    out << "angongduvunchumurchaneovomnupolgarnup";
    out << "devorstgalhvdruphurgrempudugalglexecong";
    out << "onvaxnurvaxcothlabranphdexturglethgruxorg";
    out << "vongelphzirophurgnervaxcheldonphzurubonurba";
    out << "chervangalpuxlarchzichustorgnergothzadronchal";
    out << "hvmuvarndethgonuxonaroxphicothgalbaladornagarph";
    out << "ozubunoxdethhoporongenvachlexaronpuncholaxaronpha";
    out << "enepvludoxaterophcvrungazidromphzvlcothnuxorgal";
    out << "avitramavdemugelvrobzidcharmongzirbrolvdvorce";
    out << "oneragrunzilnafatzirchgonraplinhazihocarong";
    out << "edulgredurnopthzvrgemphulnurbduxarulphuar";
    out << "vzidoblungduhvndu'xlexonpinabirbzircildi";
    out << "cvilgethlatnepqvarlzvrumunglanronganp";
    out << "ozirmonchlongonphzvrchanphzirdonalp";
    out << "padrulvarhoxnelduthcombavonziraze";
    out << "canphironphaxalondorchethchidrv";
    out << "ageverchnolphirmuclebolurdnex";
    out << "aromzuvropogydorchilurnorpo";
    out << "gylziru'nchnercrosupluvurn";
    out << "vzadhvdumurghlebvaronph";
    out << "odethconphamugurnlang";
    out << "balzidnalviochurpha";
    out << "aremphzilchuldred";
    out << "ododrumphziloph";
    out << "odexudronolgo";
    out << "abornoxtzir";
    out << "opulgruph";
    out << "uzidoga";
    out << "adexa";
    out << "cle";
    out << "u";
    return out;
}

QList< QList<QString> > table46() {
    QList< QList<QString> > out;
    out << table46a();
    out << table46b();
    return out;
}

#endif // TABLE46_H
