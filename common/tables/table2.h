#ifndef TABLE2_H
#define TABLE2_H
#include <QList>

QList<QString> table2a(){
    QList<QString> out;

    out << "abimacha.vort.nalvm.angenvadah.nota.geldomcha.orpricaah";
    out << "semdaphan.londarsa.mvigah.ornab.gansastrophe.oxarva.svl";
    out << "lamphra.onel.dar.seqnah.vorti.balgemah.oxab.napv'sagetvro";
    out << "danselavi.oligax.bridages.Ormon.vans.capnagol.arvah.gols.";
    out << "ardnah.oh.sabligan.vonloh.adv'rel.nocapligah.vortsal.apho";
    out << "nomrvstah.voxvolsa.cab.nagemg.lemar.gepsageph.orsonadah";
    out << "gilbafa.oxif.alasqam.odibrah.gehohadah.vortolah.vn'dapar";
    out << "apinas.ox.arvagatanquanzoh.gebv'rnax.vols.gedvranobalse";
    out << "tarqvinar.orandah.vorsabagel.armavah.golsah.prah.gelzad";
    out << "ocharvanol.vanchanol.vrndah.vastermax.gaspichadan.lora";
    out << "doxmarva.han.garvah.oh.adnab.epanflas.orstampalor.nodage";
    out << "v'oxafra.naglah.orchadvah.nampichadon.varnegem.dolphaso";
    out << "cardans.or.semblohhar.gabv'rbaget.axvanel.ordam.hapascha";
    out << "damprax.olseganar.ordephal.canvibvloh.gavargeth.othaxg";
    out << "nablap.orsesqyamax.virdral.ompringem.maxvar.ol.calphada";
    out << "omphiloh.yomnavox.Arsv'bral.nax.orna.vilglolax.arg.gamnah";
    out << "ornamvay.odgalsaqvax.vord'asah.oschamel.ondah.varstatqs";
    out << "galgi:nachax.lornefa.campidav.vons.vrdvglaq.sebna.gemfal.";
    out << "orchminax.vostarq.consanglah.orpingev.vomnargem.ologat";
    out << "arman.odorphan.adv'lges.astodah.dabah.vons.ardnabragehoh.";
    out << "ansv'ngaba.lodargemahah.nobthv.vdrvmah.olzaxcoh.zedbaah";
    out << "norgeh.alphad.nvrnganfagen.ontant.seldah.oxirdee.odfald";
    out << "nasv'latqvamaran.gelscoh.adornpah.gelsaba.ordambh.oxorg";
    out << "vnsavomfagem.norvah.ostorn.abgedachemansebah.orsagens";
    out << "lantaf.ox.agei.arpqvans.tensagoh.oppohachan.dors.napotta";
    out << "vlzech.orda.von.sembladox.varsistaman.oncly.arged.orchan";
    out << "nabpha.dolsapogem.parchafaf.alnohanah.capnar.colsal.ave";
    out << "nob.semblacmar.onola.satagens.archv:bamnigeh.olchat.voro";
    out << "max.sentyganabah.dadadab.orxam.gedchv.varfes.orchaplvh.o";
    out << "qvaltaq.hontas.gethv'dabah.or'naches.donsy.chamlc.tatfals";
    out << "isthny.froms.abvah.tors.a.gladdor.amphan.gedhadmah.lv'z.ans.";
    out << "archv'pol.napfaf.oxmal.zv'raged.dalplaps.anfax.vrnoh.lamza.";
    out << "donfah.lv'zonche.adnah.zazmafal.gangep.arnal.vomferocaxs";
    out << "zv'mnal.zebloh.odadmar.nogastropah.olchinadax.varvese.ox.";
    out << "nolsap.appril.sanfax.vrzangez.olpilzacah.zvrdaph.oldaho.";
    out << "darvan.ondembleh.nablarvmax.zonghoxar.panfas.orzax.mals.";
    out << "zaldblah.onza.porchef.lapnage.ardvman.lonsaglig.arbvnah.";
    out << "tempah.oxlages.archv.nolzax.arnid.onbrah.daxlaf.orgilaba";
    out << "oxpaseh.granssen.onapen.dolbabalax.nodchadarph.aschlah.";
    out << "dalvmnagens.orchaldabrah.capnacofan.algedvah.zonglafa";
    out << "vxchals.orphinganph.ampvgedh.galzept.orzazax.prommocha";
    out << "dolbad.adagran.hvnzarah.oxarvagem.doldax.polipodns.avna";
    out << "volsedbranochadax.olphanches.orsam.phochal.vdrah.polza";
    out << "noxvam.abbrohdah.gepsab.oldampnageb.dolbadab.arbymadon";
    out << "talzebrah.oxchilazed.ondrinzanqvah.lv'dvbraxogamax.oda";
    out << "aoltaqval.longels.as.chv'nadonsabvlaxt.orgedon.anqv'arta";
    out << "pandochabamanoh.gesdaman.olpalged.doddabvlad.aschv'man.";
    out << "arnvah.gvbrv'hho.galsaitonam.pamlaseth.orsablax.dothmal.";
    out << "hanzach.orphanzoh.gelbah.orchynah.doxamal.pamlap.voxtoa.";

    return out;
}

QList<QString> table2b(){
    QList<QString> out;

    out << "baziocadamacol.zedgah.ongeh.alzapimacheph.latqvansvls";
    out << "gotnachapolah.gembelioh.ardamvagel.orcaminoh.vam'nac'oc";
    out << "dasqve.oh.gemnacoh.dalvanzadah.oxipal.gef.dorchanor.vana";
    out << "galseqvidaho.arbvmax.alza.zomryth.nabacodan.algephansl";
    out << "nodgorvah,pachedonadageh.axamlidah.notar.baaholilsoh.o";
    out << "gampvlzabaah.gondachadah.oximonoxgarsapotres.vlnaxt.e";
    out << "vorsymbloth.achadmanah.aximbrah.lodvangleh.galzagelh.v";
    out << "cordonaphaal.lvzadmaphe.andemax.onzachadmah.zorblesoe";
    out << "convancoh.laxanvageth.vorzibagra.dolzymaxornacoh.vora";
    out << "samlemph.adnaxarv.axomchaza.gibladan.or'naphes.demphans";
    out << "zoroglaah.dalzepah.oniblascoh.ardonfageh.gormachasvrg";
    out << "apny.onzamblaca.vorsvres.adnoph.daxypaleh.ganpacat.oro.e";
    out << "pals.genvachadans.orastaphons.ganzyges.orcham.vax.cvrel";
    out << "drvx.hapracaminolat.vordembageh.ondoxonzes.capnagenso";
    out << "vortnedg.aprindabah.oximagen.damvages.orzymacoph.alla.p";
    out << "donzambleh.achidabriochadax.voffages.gelsafex.orsago.l";
    out << "nalebaf.es.donibodax.claxtorvagem.gaglanvagoh.dvresch.a";
    out << "oxonfra.gvmycha.notabragenodaah.haplidomaxtarqvidand";
    out << "gonsibaligemino.ardeph.agvrnah.zamblahadox.zvgmacaxol";
    out << "tanvaxma.ondaglan.carpan.dapeh.galpages.oppachad.ox.zvh.s";
    out << "chad.vamz.zvnblagemnochapamicas.orseh.vars.dorqva.daxo.p";
    out << "lodgemnaph.vrnachola.hynsagel.ardv'raah.goldax.vonsaxo.l";
    out << "dah.lon.gem.nahoprocap.gehv'nzacap.nagozem.vanleffah.zvzc";
    out << "adrax.ordemnahod.capnagoah.lvssyth.hobligonab.Abnapahq";
    out << "zanqveth.afnoffageh.damqvoh.laxordam.vonsangvath.orna.a";
    out << "gamzeph.oxadrah.var.gedcoh.vxvmbrah.nadom.pageh.napvlaht";
    out << "sadcham.ondorvar.vos.manzaneladrv'xoglas.carpamnaghosa";
    out << "cvlpaz.ardryngel.noxadnah.gors.asqveth.labfanda.nogamv.q";
    out << "vnsv'ncha.notablage.larqv'eth.ardomagen.nazlogazeh.vdanx";
    out << "laz.gonfa.nolpam.gem.omnachodah.vorsagenah.gvnzapha.zvna";
    out << "archyr.ablasco.doxyndragept.taxqvarnacoh.gelzoh.capnan";
    out << "vxargrapha,demogel.vaxor.dan.galgey.noytaf.gansah.lvngaz";
    out << "zybleth.ardygansa.gamletoth.latqvansagaf.vdroh.adavnrl";
    out << "geb.aschalp.noxan.damph.ortanah.gephvzanch.ordrvzadgol.o";
    out << "partenopa.genvado.archaxmalzadqvam.noxybleh.ardneyohz";
    out << "dalph.oxingraph.dadmaqva.oxanzepah.gonzeh.capnah.ordraa";
    out << "gansv'brah.lod.gastmah.noxadmana.genseph.arphydona.gedod";
    out << "axical.lvryngeh.apprinocah.vordomphagen.nabzedma.geodl";
    out << "vamreh.tethochael.lamfrangeph.naxadox.vargemph.armadag";
    out << "cosdahtax.labnaconah.geldacor.paufram.oxinglapax.zvraa";
    out << "madnahtraphandongebleh.acharmah.voxardoh.gamre.odanvl";
    out << "consal.gethacadnoga.volseqvart.tolgemph.aschadnah.ohol";
    out << "omzybles.vax.nomeron.adnava.axenge.carzedah.voxard.nabla";
    out << "voltenbrachan.vrv'ngalsax.zedloh.achax.lozangra.vax.norb";
    out << "agnacoh.gamles.odanda.gehvdvah.lvzancheoxymachola.dava";
    out << "gadnabra.oxychan.dedmaycax.zambloth.achaldaxma.nocasta";
    out << "lvdazved.agyntogamal.naxvar.zvrebothasdong.adnay.gonap";
    out << "vnzah.vordeqvoah.dedmacho.lvrvasmacol.adney.golzan.vnza";
    out << "setgamvah.noxalda.nagemno.vordemah.aglidoah.vangerbaho.";

    return out;
}

QList< QList<QString> > table2() {
    QList< QList<QString> > out;
    out << table2a();
    out << table2b();
    return out;
}

#endif//TABLE2_H
