#ifndef TABLE14_H
#define TABLE14_H
#include <QList>

QList<QString> table14a(){
    QList<QString> out;

    out << "olchadanvahnochastoraschadnachodaxarnaphevldaxpar";
    out << "necheparothaxadnocaxplaxargednocantantaxalgednaho";
    out << "alchantaxadnachoalzvdaliahchantadgnachoadlagethax";
    out << "praxnocharvorsaphantogalzvbrahnoxendangedahvdaxar";
    out << "volziblachothnatorgenoxandoxnachanzachaladoxadnoh";
    out << "volchastoradnachonmalchadnochavantagnagrasvdadlab";
    out << "geomnavildantafageldoxaxarnagramampvracoxandraget";
    out << "nodaxacharnaphavolziblaxtoteronampvdaxnacosnapaho";
    out << "volzidaxnadontaxalaxzingramnochadnochaphalzvdacho";
    out << "vontegladnapolitanglasohgethorgraphanolzedadroxad";
    out << "vocastoranexolantantagethalzvdraxachapnagothzadvx";
    out << "apnaxcalzebachadaxvnaplaxtopheroxdavaxnodaelgadno";
    out << "tantaxoraglastonachadnochanvolsimagenadnoxadnavah";
    out << "gabvlabanotalgazadnocharvaxardepranocalzachantaxa";
    out << "nochargedaldoxphadapvnadaxvalcendachanaxtanteroxa";
    out << "zednocaraldrantantagongvlablatazengladroxadnahvax";
    out << "tergensodrahzvdnochadontavanyntasamalchapaxorchar";
    out << "vontolzadonzentagephnohamarontantargethnolzadnoca";
    out << "apmachadoxaldentagnoapladoxvanredopragnoxangethad";
    out << "volhalmahachepnodaxargrantantongaloxvolgebraxvdro";
    out << "prageldohgvnaxocapargedoxalchannoxadnochaxvlzadno";
    out << "prachastoralzedaproxnaphannevcastoryntagnahvdalza";
    out << "pranchefnadoxalgeporvornadaxvntanbragvlachannoxad";
    out << "vorgemvesadorgednechastorvmaxvaranobledoxadnachon";
    out << "chaldagmaxorzidamahnolatanoxvnzachapnochaphonadia";
    out << "seorgraphnolzadnochavaltexodnadoxadvantaxparolzad";
    out << "zvnadagephadolchaplagradaxongenoxvlchensoplataxar";
    out << "plataxtoragednovoxardrahaxargrahnochamtansanpogeh";
    out << "aqvlzahnechalavornachapvervntoxaloxienzocapraxoda";
    out << "vohornochanzantagnohaplantoganvolzichadaxchardaxa";
    out << "nolzidnachefnatostichelaxargednochastorvantalgeda";
    out << "napvramaxvoxodnadoxplaxtargedoxalgorhastrahvolzeo";
    out << "amnadomaxvortexachidnochanlarangethnoladchanazino";
    out << "pracontaxnodahvohadnahachadnaxadnogemdaxoldaphans";
    out << "piornopladoxaslantathnaxorzadnochaxelzidangenohal";
    out << "zichanorpratoxplvragapargephrasvnsadnachaxvalnexo";
    out << "daxvnbracordontahvolgednoxarzandaphnochastrahlaxa";
    out << "donclaxpraxcladnoclaxvortantalosvdraxprodantagnox";
    out << "vxalvahnocaldaxvorgendolonadnapladoxprichastoroxa";
    out << "navilmachephaldrvzabahnolzachoronzmalfadnaseapvlz";
    out << "acharsadnocraxorgemnagethalgvlaxaroprocastargenax";
    out << "zadnaphedoxalgranoxaltagnonvndantagemnoxadnochaps";
    out << "valtaxordrahnotablochprotagnagohzanchansadvlzadoh";
    out << "nolzadnochansvolhadnophravoxandroxaplantagemnolza";
    out << "voxadnochabvolhodnadeogradaxargetheodnadontagemph";
    out << "plvdaxnachintagethaplatagnoclantantopadnochadnaps";
    out << "gepholzacraxvaxardraxvpraxnaphanlorgransophaxardr";
    out << "ghehanzachepsnaxardromalzednochapapraxladnovachar";
    out << "pvzedazingethnoxadnophantalzadnochastorvlaxnadeon";
    return out;
}

QList<QString> table14b(){
    QList<QString> out;

    out << "devndravoxaldacharnobzadanlvdaxonaxvargentaxlohan";
    out << "ledonagvlazarnoxadnachapaprathoxalglahvoxarnodvna";
    out << "deontaleoplastorsednachaparaxnodrohgethanzychanox";
    out << "tantafadvlzaganziadnochanapvratoxnataadnapnlonzid";
    out << "rochantaxanadnochaplaxarnachafzvdadgmaalgensonxar";
    out << "reoxtalplaxtaroxvnastraphadeonadvrnaxzichastaramp";
    out << "gvlachacloxtanoxplaxtoteronzvntadgnachastoradolph";
    out << "zvntalgethaxvdrahnochantagnochapnagedovolsiphales";
    out << "arphamacharnexodnabahloxadnochaxploxadrotastachen";
    out << "amnachalviattogethnobladoxagradangetharlexordraho";
    out << "nolgamahgehvnagenplatoxtameronapragnachepheadhare";
    out << "chapmagolgemphantagnachoaldimachoxarnydaxplantoxa";
    out << "gemagradaxnoladnochanandadnophantantvrionalclahox";
    out << "pragednochadaxadmachaxplaxnacharampronaxtolagepha";
    out << "lodgesnohardrachadanlvgadaxprachadnochasteralzemb";
    out << "claxtornagraxgraphadnochaplvstagenadagnachoargedn";
    out << "charfagnacheontazavolgednochadnochalzadnodaxvrtoa";
    out << "orgadnogasvolgathnaxanortaxalgednodaxnapvramageot";
    out << "achaldaxoraxvolhvblahaxnorgemvageshasclaxnecladno";
    out << "vdantahvorgendaxapragnochaphlechosnachonzamalarto";
    out << "zidonataxlapodoxtaphangentoaxardragothnadoxtalaxo";
    out << "voxammaronzydachaxnolzadnoxvontagnograxplatontaxo";
    out << "gehvdraxaldaxnocharvalnexacharnanlachexadroxnapha";
    out << "deodnagethaldemachodaxplvclamarnaphaonzidnochanza";
    out << "lvzangahnoxalgadaxaplaxtogalaldraxmadengephnertho";
    out << "vldantagethnoxadnochadnaprodantagnochaphalzidloha";
    out << "deomvagellazantanaxodvdranphaxartroxalgradnachorg";
    out << "vnagraphateontaglanexiclanebthaxnoplacharnoxadnoh";
    out << "vantalpargeogradaxnoltalgradaxvolgarmorontantorxa";
    out << "gepneyadnolphazantippaloxandrohzedcornachefadnolg";
    out << "argednochadplagonsadoxateronprichasnahgeontantaho";
    out << "volgednachodaxaclahvolhadnochanzadaxalgradaxtopah";
    out << "noplansadaxvortavervdraxnochamvahzvchaldlaxvrtafp";
    out << "zedechadoxrenegordantagothadnochapnadoxantragelzo";
    out << "alexodrahvaxardongensodaxvalgednohapraxnachefalza";
    out << "gvnagradaxalgidontantantoxalzidrachadraxoxadnacho";
    out << "plantaxonalvoltagnocharzadnoxalzidnapracoxpodoxar";
    out << "zencladontagonaziblothnavamragonzandclanzavpraxad";
    out << "nochalcedoxanyodnaphaalchydnodoxalzachyfalzybygho";
    out << "daxalgranotascapaslexadnachanondantagnagensodnaba";
    out << "volgednotaxalgethroaxpransoaldaxnochanvartolzedno";
    out << "adnaphadexvalchetardraxaneoteroxaldolzachytargane";
    out << "vanseblaxachartalahdadnoxtalpagentvlgamvagothnado";
    out << "gathnexaxarvalgednochaphalzidnoclaxvargethadroxod";
    out << "achanzanecladnochastogathnalzipalaxzvdaxnechalzox";
    out << "apraxnaphadolzidnochaalhidnochaphnexotnagedavraxo";
    out << "daxargedavoltagnaxzadnacharoxvolgradontantaxtalto";
    out << "apvrahnoxondlazidadgnoxachadnopraxadnoladnorbatox";
    out << "plantolagensodaxalgetaxalgranoxadnaxapridnodaxolz";
    return out;
}

QList< QList<QString> > table14() {
    QList< QList<QString> > out;
    out << table14a();
    out << table14b();
    return out;
}

#endif // TABLE14_H
