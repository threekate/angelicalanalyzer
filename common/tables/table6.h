#ifndef TABLE6_H
#define TABLE6_H
#include <QList>

QList<QString> table6a(){
    QList<QString> out;

    out << "arzvgaoxardamvangesvorzagaltadmanoxvalephonzaches";
    out << "vorzimachodondalpafaxnodahvongedvangesoschandahod";
    out << "zachaldoxadvorfnagefvodogesasmadohathvonrechnogal";
    out << "voxardageonzadadmagafoxadmachoadondagemolnadoxadv";
    out << "gothalglefonzabnachedvorrigedohaxconsynglaphondah";
    out << "oxarblvghahordanzvbacholdaxodvordachodaxvornachon";
    out << "amphichadalammvrabazavahocharvaconzadongempalzaga";
    out << "oxadnadolhapnadvongefagvlgannolopnadrohporchangeo";
    out << "moxarclohorclinodoxmadoxvontopzvrchadmaholdabnaho";
    out << "axchapalonzvntangvacolgeonzadmachnolgedvacohoclad";
    out << "orconsaphagensvehnodgtrepetalslanvagladnoxadgrado";
    out << "vzangemvadaxnoxbnvichadnamachhoxorgebnapoladoxanc";
    out << "vxardrahazamnaphodoxardachalvohapvradoxacharmadon";
    out << "vonsagohalzanoxardanachanotparchenadanvanodalgefo";
    out << "gabzacorgapalgemnapheonnidanagendachalzangenglado";
    out << "appvracloxzydonghabradgedlobvadoxorchadmadolvorro";
    out << "bahnoxandorcharmacholzadcheddonchanfaolzablaoxarg";
    out << "nohalzodchaphangedabracholdachadvansanoxorgemphan";
    out << "chadmachvaldephonzadmachnochastoradoxnadongedvaho";
    out << "colzadgorzanvachorchaxpadongeoclazabachvoxordrabo";
    out << "olhaminondangephaonchadvontangalhvpravontalgefolz";
    out << "achoranchoxanacoxachordamanoblagahboxalgabadrvbal";
    out << "zamgemphannoxadnachonandrvmachespolidoxadodastvao";
    out << "golgolzaorcalzodnocasnodgechalmachasnoxvrzachaorh";
    out << "ohapphvonzablahdohadnaconnaschalormacoxvortanvage";
    out << "apocrapandongepholchadnadoxorblachodnodangedolzan";
    out << "zvrasacodanadvohpageldanoxambronconzagolvornoddah";
    out << "olzafaltasoxadfagemnobbracohaxvrgehahvdaxporsebor";
    out << "gemnafeoldandavesvllahoxcaxdonprvmachoorzangefola";
    out << "noddrangodpazanoblabacanalgenolamphacapnafamgebor";
    out << "zachadvanodahnolhadvangenzochadvrnalpachedvamcolo";
    out << "agropnodonsabvdaxardrachadosvomfamilohadziblocath";
    out << "axvbanohcongalaxonzibagendomtagvhalzinvescordoxah";
    out << "nolzachalordantalondonchadopedonachavansedoldgedo";
    out << "axphilmachonchanvahgethonzaclahnoxadahgorgaolzida";
    out << "volziclaxormanabondomvagelcolcavolpcapadoxalodnag";
    out << "geograldonsantabatholdemayongemvadopabaohnoxacoho";
    out << "dalzadvahordyblesoxardacheolchybabelachalzacorzam";
    out << "nodalzageomnipheoxalgathnotvartagledolchimacondax";
    out << "vargedolzibvlaxnoxadongedvahochadnamdonsangephoxa";
    out << "nolgemnachoadraxoldongefioncladvamaxnogestraphens";
    out << "goldezaorchibleyborgadzochadvabrolandrahoxendonal";
    out << "gedvisldaxarnoglanvioxtameronatopalodvarvgelzagoe";
    out << "aregladoxillamachondanvagemzvchaonchanolchadvaxor";
    out << "gedhanzvacharvantontacladabhvbarogendaxvoronagenc";
    out << "ocordacladnoxporchadaxorgefvradvanomnaphadangemne";
    out << "ochastorladanvooxadvalaphoxilgalotantoxolzabvgems";
    out << "noltapazvrgahocaldanlaxorchaladroxtangemneodaxvar";
    out << "geolgathanzvacharmahvolzadchvachanzenoxadraphalso";
    return out;
}

QList<QString> table6b(){
    QList<QString> out;
    out << "colsadnachodnabnlacoddachoachinodaxvortallachasto";
    out << "zachandaholzabvlacapnodaxvornagephaldvnathaxardao";
    out << "palgendaxorchasteraridnaclaxorgaphaoxamerontantol";
    out << "avantahgalsadnachoxadnachovornechalzadraclaxarnah";
    out << "palchastorachanzahnoladvanodonsaqvaonzadnoxalgeph";
    out << "oldanvagetholchadaxatpatordamacongigesonzacliodna";
    out << "chadabnagemavrehgovnacolengenozabmarchestoradnero";
    out << "zadlaxorgemvargemehzabvlchadaxcharneydenvagenohal";
    out << "pachadnacordanchaoxadvahcorganzagemadvmaraoxergah";
    out << "vortorobadnagenandohachadnagolchypodgrbladolphoxa";
    out << "nontantochanvarnadonanchadnacholzachanoxanvepabal";
    out << "gebirahoxandanoxdargemnochadvamoxamecorpotentogal";
    out << "lvganlabradnapheoxacladnachaorablavorsadnadanarza";
    out << "ophinodaxvongefadongemancloadbvnadoxymphanochanva";
    out << "zvcapladorgalsvolchardocharbacpangenograolchangep";
    out << "napvdaholgenvadoxandahcorgaldahgorzimochanphancha";
    out << "nolgenvaoldephacladvanecorvnrachnoldroscovomnolgo";
    out << "ampadangedachadnadolzarcladranclobrvzachochaldoxa";
    out << "noxadnophonzvzazaoxolnoxpiclabochadvantangentahox";
    out << "vortrehnolzanchobladagemvehnotalzaclamvantagoohad";
    out << "naxvargenzoaclahdohnabrahvoxarnohazanzachonchappo";
    out << "arddvragoldanvangehloribochapadabolchadnaghohanza";
    out << "phichanoraxeldopatroblahnochastorachanvahlochanoh";
    out << "ganzadbloxardrohnoxacarmorhpodloxrvdlangethrohado";
    out << "clanzexorgenvaxortaphanolzeoxamerodanzedoxangeohs";
    out << "gansadolpigenvadnolgraoxandachvonchiaprosacladvar";
    out << "qaxordadladqvanclansovrabangednahconsineagendahot";
    out << "gvraqranqvongenvelqvalsagaphnoldondabalgexorzadch";
    out << "oldonpabanvacalnotvenobladoxarnedonzangevoxadraho";
    out << "capnachondangedolzadmanolgemmamemolchexorchastoro";
    out << "pabasaoxadnodansadgnolzvbrascoadroxonadelvanolzan";
    out << "aphinochamarvenedaxprochastoralzvbranodangalpacar";
    out << "olzonvanoblapnaboxemerondantafvoxoxallorgepholdal";
    out << "corpongedlonganvagaolchessanqvaloxsanchphanpaloxa";
    out << "dolgenvahhodnaxopronsephachvriohaclohramsadnachap";
    out << "nodvagensablagetholdantagoarnehoxalgavortanqvahzo";
    out << "loschoadvradnobralzepachadnadolspatgemalgergevort";
    out << "nalveladvlgehcraxalgenalgedcohaxvrgaphachvrahloda";
    out << "nopalgenvoxardedonsantelapadmachaloxadnadonsablah";
    out << "gephalgednoxamreoxandaphcoxendahloxardahlodnalgeo";
    out << "pachanssadqvalsoachanzanoxenblaodanzachaoldamahor";
    out << "gedvachornadoxoacheloxargenopradnachadnaxvonsalge";
    out << "porchaoxarnabadrvechachalzanodargeolzachanzaolgez";
    out << "arpimnacloadnaclahoxandrahvordempacoladroxchilado";
    out << "aclangodansednaoxadvahoxalgedaqvantoaprageondahod";
    out << "noblagemaxergoadnachadmaclesaprichadnagendanzachv";
    out << "alnedgonsatqvageoadaxoxalgahvortafmadonanvaoxadna";
    out << "corsalgemplacoraclascaholdadnonvonsanfagotalgeoxa";
    out << "dontafnagothaxmovohgiblesadongethvorgefadroxdanza";
    return out;
}


QList< QList<QString> > table6() {
    QList< QList<QString> > out;
    out << table6a();
    out << table6b();
    return out;
}

#endif // TABLE6_H
