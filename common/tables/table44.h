#ifndef TABLE44_H
#define TABLE44_H
#include <QList>

QList<QString> table44a(){
    QList<QString> out;
    out << "l";
    out << "ubo";
    out << "nergo";
    out << "zidnupo";
    out << "urchvrumu";
    out << "donuvuxvrno";
    out << "zedchrilothco";
    out << "curvunchlandrox";
    out << "pvrungoxzemptullo";
    out << "zichuturglvrpruxnup";
    out << "zvbluxucholdnoxvrnuph";
    out << "ulvunchlirzvthaxurnoxta";
    out << "vixrunphclontoxlvmugelarp";
    out << "dodogrunloxornupolgeuxurnon";
    out << "ponguxlubvdorphcurvunuphidrid";
    out << "zidruxulnaphduvucloncongunphulg";
    out << "huxumarondednolhidengaphzilcoxurg";
    out << "vicluxurnoptrongrundoxulzvdothvrnox";
    out << "chumutruxnulgruphurdonvudvgruxlerzicl";
    out << "ucoppufrumzilgephnebvrotholcruntuntorbo";
    out << "virgethnubvlaxornephucolnothdemnudotharzu'";
    out << "roncrunvuchlornupurggedgruphvizcarstoronclu";
    out << "pvturnotruxlethcongulnorbvurnothgethlarphurdu";
    out << "chidobruphgulmuxnervonculbrunchlurexnechdoxurph";
    out << "lvrubrothclinvonchuxumutrondethmugoludnu'rbbolgrax";
    out << "dvrmutarziodnepluthurdnuleonvu'gelzidchethnuroxt";
    out << "lunrunchzichustoruxomucronzirdophurgledgru'mph";
    out << "nelbluldolzedpromyntuxylbvrdolpvrgaxlvrgoxt";
    out << "nevu'ronchulclondunvurgeruxnurbhvcurphzich";
    out << "luxudornelpulgdvrungduxurnopthlexnuridp";
    out << "crnguprunhuxluxlompochepchiubvorzeclu";
    out << "pvrgunphzicornolguxmuruphchalbunchu";
    out << "demuronhidubu'xnercolpermuclurglex";
    out << "himimanpzvmziclurphnexumeroncha";
    out << "vurhvdruxnecholunchdeplurgvdp";
    out << "dempnelzvchuvurnuzorbordulb";
    out << "vurnupuxzulcartulphomphal";
    out << "neguluzzichrunphunchanz";
    out << "dvmurglebnudorghvnclu";
    out << "dobbobelhaxurnuprul";
    out << "chirdorphullemung";
    out << "devornedghicaxo";
    out << "lampothnepzil";
    out << "udvurncluxo";
    out << "dudrunpho";
    out << "negulun";
    out << "chu'rn";
    out << "zir";
    out << "b";
    return out;
}

QList<QString> table44b(){
    QList<QString> out;
    out << "q";
    out << "ird";
    out << "dongu";
    out << "luxronc";
    out << "vichudurc";
    out << "piduphrunhv";
    out << "zymchorurmuxt";
    out << "lethgenphulcoxt";
    out << "tutnuphdecorgulga";
    out << "potoxnupvrivethzimp";
    out << "duclorponguxnurvchinu";
    out << "ruxungepthzlogredonhido";
    out << "porgempnuvortziprutorchur";
    out << "zipvurnu'lhindolghulcurchurg";
    out << "gulfvlbachurnupruxcharrudonga";
    out << "rvmu'phu'rmpchilcholgegronorvibvr";
    out << "dutruxlethgorncu'ldoxurnumphhibvrg";
    out << "oxu'lu'nchdophontricaru'prunnexcorphad";
    out << "nvlzilchlvbordu'rmu'cru'xhvnzildohordonp";
    out << "apvru'phru'nhu'ligvlu'rgvinmurchu'lcorndothu'";
    out << "vildomppu'roxhinodu'rzilchu'rgu'lhargrunchoxa";
    out << "hirchudolphzirgeleghuxurcorgdunvarchlvdubra";
    out << "chevulonduxnu'rgethucholdumpulchednopu'rmu'grump";
    out << "dordonchalvorchnoxu'mphiruphdethcornu'rgruxzilbon";
    out << "chudornudorgphilgrunphzirubrucoldethnurbhvdroncho";
    out << "gelduvurorpongephaxvmu'ronglu'druchnechu'storphing";
    out << "ludbu'rgnevornucorpolchephnudoxhidmurchlundorg";
    out << "viadbrochiclargethurchclaptotpognedorchalpo";
    out << "qvidsonfvrsaplvdziceleppalepogoxnergedonz";
    out << "gu'mruphardronopornochurnzilcoxu'ndoxilga";
    out << "lapvronyxirgethzuchlurborombaxhilzurn";
    out << "zvnaphardpiducruthculuphruxzilgroth";
    out << "nebvlurghuxurglvrchustorgaxidvurn";
    out << "ziblu'pulortuxiruncloxurbvdugru'n";
    out << "hidruphnecolphabadurrononolpo";
    out << "vixilglovanpfirgudmuhclinga";
    out << "pru'tnu'tzivuddothylconu'rda";
    out << "chimnepherothgednogrump";
    out << "dultuxnu'rgelbongaxtar";
    out << "chilopluthvudnurgal";
    out << "pvcuru'mu'nchvixnor";
    out << "dulhinyclynoddo";
    out << "gexzixapvlzic";
    out << "lervunpulga";
    out << "remuphurg";
    out << "hiclump";
    out << "dexol";
    out << "aln";
    out << "q";
    return out;
}

QList< QList<QString> > table44() {
    QList< QList<QString> > out;
    out << table44a();
    out << table44b();
    return out;
}

#endif // TABLE44_H
