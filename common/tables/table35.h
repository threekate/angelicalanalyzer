#ifndef TABLE35_H
#define TABLE35_H
#include <QList>

QList<QString> table35a(){
    QList<QString> out;
    out << "ay";
    out << "loro";
    out << "noxado";
    out << "vonchadv";
    out << "zinvoclaxt";
    out << "raxarbacloxo";
    out << "neorgabtzidcho";
    out << "vidrahaxclotharn";
    out << "chionavaracoxtalga";
    out << "leporgnaxzintaxoramp";
    out << "viodablaxardraxgeodalg";
    out << "zimathararcvthzidnopvorg";
    out << "alclaxtantoxaroltaxnaprang";
    out << "voltapnaxodranalghvnocaldaxa";
    out << "teongotharnoxydantaglaxvidronp";
    out << "adonphaloxargaphoxarnolhvadronth";
    out << "novidanaxvlgelbrachdoxarbapalgadno";
    out << "zemvinodalhalzednoxtarnonglanchanzad";
    out << "oxameronchvblaxapornoxanchedlaphzidoxo";
    out << "vorontelmanchethaxarbaronzvblaxtarqvinol";
    out << "getharglaconzachhvadranglozidnopaxarchongs";
    out << "arphangeaxorbgathnechastoralgemphallaxdorpax";
    out << "chidangedaxoraxamerontantalpvioclaxgednoclonga";
    out << "vimancargoalploxtoroxarnoxtalappodolgethnoxamvah";
    out << "gedraxchionzachvinaxavonrachgebvargeonzilornergo";
    out << "porodonalclanzzidnolamaxpartotogalvscloxarnoda";
    out << "violgethalpangedcharvanchchantoxandoglaxarno";
    out << "tampvolnoxarzinchothamvoxhvbroxapvranclydz";
    out << "aspoxaplatoxornolymvazclobotorgarcothalg";
    out << "navaraxorcroxalchenvaxzintaxazednolpla";
    out << "tamvagnodaxarghyscothamphvlgathaxorg";
    out << "nevadorvxantoutaplaxzidnamphachoxo";
    out << "neogradoxalzedalchephaxangathang";
    out << "lodobraxvscholnavortanaxorbron";
    out << "viodabalglebgraxaluadgeongar";
    out << "horcoxampoldaxlexbnrnadnot";
    out << "debagelalziduorastorgath";
    out << "chefanoxvfchanfolarbro";
    out << "loncodarchylopvornax";
    out << "chedabararvanzioch";
    out << "anclaxrounapolga";
    out << "horzicharvadra";
    out << "ziuponaporox";
    out << "aphiutaxal";
    out << "lednagol";
    out << "axarga";
    out << "hiox";
    out << "au";
    return out;
}

QList<QString> table35b(){
    QList<QString> out;

    out << "c";
    out << "odn";
    out << "oxarb";
    out << "agepext";
    out << "lavargrah";
    out << "lorazarchax";
    out << "chionartoxaug";
    out << "lemaxnapalgraxt";
    out << "vardegraphalgetha";
    out << "nebalonanzedphalmau";
    out << "zinopalaxvrnoxaclonza";
    out << "chimaxorantantoxalzadno";
    out << "ramavaxalgedvachanhvdraxt";
    out << "viodantaglaxaxouaraphcharva";
    out << "neomploxaclinzachlothcholaxar";
    out << "vichossorosalprogeffnadvornauco";
    out << "lepontolardroxnolvnzachlaxymporda";
    out << "vicofalarbronlszargaxalpradvanchlex";
    out << "champlothalontorbvicodnargedvananzich";
    out << "lolarabaxaxargachadnochaperonzidnaphalt";
    out << "affarageontoarzocdebvoraprouchiloxardralp";
    out << "phingothalcoxzinbothadomvadgarnothalplaxord";
    out << "vimaranhnxarchvipridolangalzechhvdrachidraxma";
    out << "vonchanzachhisconzimilidonclarnoparchautonothar";
    out << "gevdaxpolaphaloxvrnohtexardraxplatnagorhongathneo";
    out << "phiolchanaxzimouabvarnaxaxalgraxyuzachacloxnopa";
    out << "viscomphalongednapvaroxhvschaldapvarsephazvdv";
    out << "odandclonchodronvagephaxarnolapaxchidnobarg";
    out << "alclaxtanapodaxoronchiudamphamonvarghvdou";
    out << "apladaduabalaschvrroxamphardextogeouzah";
    out << "nelgedmenaclazexvlclaxavornotteoutotx";
    out << "nepollononardrouahgibalchzichynzord";
    out << "apnlgaxnochaplaxodanzaarnopthgeot";
    out << "charzammaclaxysnothauzidnopaxar";
    out << "viordraxalgedthamaxarnexadnox";
    out << "pochastoruavalhvnotapranzoh";
    out << "clinopraxalchanzachlexoxa";
    out << "zanvarglebothnotaxyrnol";
    out << "adorngohaxamerophalza";
    out << "chionchothaxargalep";
    out << "chodnopothzabuaga";
    out << "hvuaxpargethbou";
    out << "avantongolzac";
    out << "vorchymalap";
    out << "dabuagels";
    out << "vschomp";
    out << "haxar";
    out << "reo";
    out << "a";
    return out;
}

QList< QList<QString> > table35() {
    QList< QList<QString> > out;
    out << table35a();
    out << table35b();
    return out;
}

#endif // TABLE35_H
