#ifndef TABLE39_H
#define TABLE39_H
#include <QList>

QList<QString> table39a(){
    QList<QString> out;
    out << "ge";
    out << "poho";
    out << "nadoga";
    out << "alzidnap";
    out << "phamrothox";
    out << "labarvarocol";
    out << "urgvroduxlebvo";
    out << "neopolanphchanvo";
    out << "chapuchloxulnoxald";
    out << "virzabvargethaldoxar";
    out << "cherocaxnarvaxaldrapan";
    out << "nergathalplaxorgrophadno";
    out << "ronphgalpazicharnoxtalgath";
    out << "chironzachgvdraxvlnorbbarsac";
    out << "viodraphaxurontalgvathaxonarga";
    out << "chirvradgoldululvigarbalondroxax";
    out << "norazachalplunzintohaxumeronchanph";
    out << "calnaxleproxtappelhoxurbgedmathornod";
    out << "aldvnzachchionaphararchodoxulpingephlo";
    out << "nabalathdeodraxvibraxtexonraddugrascodra";
    out << "chonachalunchornudvonipochapluphvschodunar";
    out << "necholudnorbvancombuldvnadurchebachexurgedno";
    out << "virathalmanzipuxdorcaxulvompnaphaxchibrahaxord";
    out << "nedroblancanzacharchonavanchanzachaldroxvnrothge";
    out << "galsapurothcvdluphuxungaphzidluxurvotharnoxfarga";
    out << "zinvadugelamphacloxurphrangvlbraxtapormoroxvnd";
    out << "lachapumvugelublunchapnavolarcothacheldraxor";
    out << "nybaloncharzachadroxonepohorgaxutorgulbrax";
    out << "genzachlocrapaclvxtaxuntraphgeronahzidno";
    out << "pundulmunazachchivarbachcharzednograph";
    out << "zvdvbluxurnopthaclongathalzidnapuxor";
    out << "calcorurvonograxlednarhiadraphchia";
    out << "duxulbachaphhvdraxvlnoxurdronnah";
    out << "conzednocalvirbruxupvamnacoxta";
    out << "zirophanalpodaxupurapchintol";
    out << "armvrabachloxontoxnarvanch";
    out << "lvranzichoxudnorbudoxord";
    out << "volchastorudnaphavranc";
    out << "vrnascothalgephagnox";
    out << "arbvrachondnabalon";
    out << "hvxurbrothalziba";
    out << "relmaqzodolpro";
    out << "hanzanchaxta";
    out << "chiduxurno";
    out << "phianzah";
    out << "zempul";
    out << "vdno";
    out << "ch";
    return out;
}

QList<QString> table39b(){
    QList<QString> out;
    out << "z";
    out << "ado";
    out << "puxal";
    out << "ganhoca";
    out << "clopranch";
    out << "irograptalc";
    out << "laxynvangerdu";
    out << "puxuronzidvonre";
    out << "arvmarothduthalga";
    out << "levertoxupotranloxa";
    out << "nexomarontantualgehor";
    out << "gabramvachadnorbaloporn";
    out << "hadoxmagelarvanchvrnodrax";
    out << "alonchaziehastorudvantagoth";
    out << "chibalachorzedurdvnzichoxnalg";
    out << "haxupolamphinophardeothnarbarva";
    out << "roxupozidorhlethgranchapranziclax";
    out << "adruxlavaronthgerodraxvlnoxtarqvunz";
    out << "chinalvardurmuvanchchaxurbapvlgethard";
    out << "nodubruthvolnoxumpvracoxunglaxtraxleont";
    out << "vornodubalonaxurbroxdeodothgalzephardroth";
    out << "chdobraxomeronannachaphifraxnerochaxulploxt";
    out << "degronchanvadugeldrachloxnorvanchalplodvarnot";
    out << "apvamnoclonoxamarvageldrvxoduchproxnolpranchaph";
    out << "golgallagronynaphaduxoroxremavanpulcoladnoxacluxo";
    out << "durabalaxamerontantaxpichordranzacharnachionclu";
    out << "puxamvaraxtoldethgalprographnalonarmunapoarge";
    out << "chadmaxoxadmachleproxadvorchephnograxladnar";
    out << "vnaplaxuclutaxlubvrnagalornatraxargethoxa";
    out << "phichastorlarzachalbluhdoxlabvraxarcrot";
    out << "natoclatachelgarvanzidnoxulcharnaxaro";
    out << "dodrachancloxchinzocharpirodurhalza";
    out << "phichanclaxamnarahtoxorgumpolapha";
    out << "dextoradnaaolgalaxvdnophataxard";
    out << "rvdagennarvaxzidnapvschodnarv";
    out << "ocopgephoutzidchabbolhethno";
    out << "galnogelmogenesaxarraplox";
    out << "vidublaxvrnohtaxamaront";
    out << "naduvrahcoxalpharnoda";
    out << "dompnacorbadnodplax";
    out << "zanchiranzachorna";
    out << "phichastoramnax";
    out << "lethcoxnalgat";
    out << "vornothanla";
    out << "zinoprong";
    out << "dunvarg";
    out << "landr";
    out << "org";
    out << "v";
    return out;
}


QList< QList<QString> > table39() {
    QList< QList<QString> > out;
    out << table39a();
    out << table39b();
    return out;
}

#endif // TABLE39_H
