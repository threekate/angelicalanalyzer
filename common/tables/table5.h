#ifndef TABLE5_H
#define TABLE5_H
#include <QList>

QList<QString> table5a(){
    QList<QString> out;

    out << "hanzánpha.loxárvan.góstagen,arbvrahgemseh.oxárdoh.axoh";
    out << "orlórvan.gémnaphe.axymaba.vorgal.zangéhvrnal.van.sancha";
    out << "lanzenchv.arshánsa.golzamabachalsádchor.dolphachonza";
    out << "michanzadaxvarmanchanonlvmazadvoxmacholadnaphaolz";
    out << "iglodaxvarnabaondelphangedoxanvalodebrohaxvoradro";
    out << "oanzanchrvaglascodoxodambvrachaoldangephouchaldan";
    out << "gorzamnaconampvragepanchánadoxoxilgalahvortagelad";
    out << "drahonclanvanzangephoxalgradalgedolgameohadnaonza";
    out << "pancronalmvtabahoxalvagendadnodacladomphymarodeba";
    out << "oxangenamnagalzagenogállaphagolzalpagahorzamagedo";
    out << "vanrohaxilgenaxonchaoldavanchadonzadpraxalphaonzo";
    out << "omphílodan.oemaroxafchladanoxabnichadvansegmagelad";
    out << "cordangemvalzvrachadmachozanvagenlaxongramaxadney";
    out << "aprisadonamvrielaxinochadvachyzongalpaxarvamchals";
    out << "archvrachemnadolpatraconsamachorbridoxarchanlaxor";
    out << "paroadmichoxamplanecholganaxprischamadonzyblogemp";
    out << "arpneyadongelongefaschvdraalzanchonabnachadvamnap";
    out << "oxilgesasclohvoxarvagnaxvmarontanvahgolgessabargo";
    out << "axarmicasablahnoxarvagenadnarvachandontagleohadna";
    out << "vomzimaronangenoadvingedaoxardephannoxvanglahIeno";
    out << "armachasordangemvongelzoacrophanchaxmalgempalexol";
    out << "zadvamachardoxalpvgamerohaschaladrvgladoxarmachar";
    out << "argephadongelzaganzavachladmachondedragorgamaoxil";
    out << "aronnaxdolgenvaloxaxirzagephgolzagaordrionadrahor";
    out << "archanopdomlephaadongaphhvrataphelomvagenzadahora";
    out << "dronzymachesabladaxvarochcordantanabnadontagephal";
    out << "gedvamnoxademplagohvramacholhapnagohaxarnagelorda";
    out << "conganpleoxargedoxabrahvoxymarontantaxolzalgraxod";
    out << "calcengraphancolongenaphvgradaxvartangladaxordrox";
    out << "parchedgraplangranvaconselmaxoxanvaganolmagamplah";
    out << "orchlymarochalzeoxdanvanochandaxallchadmachnotalba";
    out << "chongenvahnolgenzachamnolplynadabraoxadmacoxvargo";
    out << "plihvchasorzemblohadnoppochaxalgemenonsamplabatro";
    out << "chanvimaransanglargroxadvaranogemnevinxadmachaldo";
    out << "drachemoxarvidamascalpatranzacharmacladorvonglano";
    out << "axirbighaxvrgedonzimachealgemvadaxnecrodanamandvm";
    out << "consanfagedongaoxalpacrongenapheadmichadlagrolpaq";
    out << "roqvalgenalgrangefvdrahoxarplvxzymblaidoncadmacon";
    out << "chazadmachaoldampligohvargedvachalambvrahaxvrgemp";
    out << "aladmageongaoxaldanvoltarqarsepnoxalgebeochadvals";
    out << "nogaprahoxargemnolzonnoponachristophandonqvamvaho";
    out << "oxarmacheoldadmachednggaolzyblageotrodoxvanqvrynx";
    out << "oldroxoxorgabonchamplagmacharaphadrosgemvamphoxar";
    out << "vahdoxgenradchedvalgeponovpalaxzanchaphochanzypal";
    out << "gemvehoxarvacadnodranmadrabcadvanochlangennoxalga";
    out << "pargenvacadmaoxalbachendaxaroldexvorangeadomragod";
    out << "braclocansanqvaxorzaxviconrahadmahaxilgalmalgedno";
    out << "ampvmachonadroxangehpracotalvorroxadronviadoxalad";
    out << "nobgaradohadnahprachapnoxalgenvargevadenochaclado";
    return out;
}

QList<QString> table5b(){
    QList<QString> out;

    out << "gansanfaxoxardrahvoxardongeldaxvarondanqvadgeblax";
    out << "olgastaphalmachangedouhanzancharoxvrneoxarvaranch";
    out << "admanchanoxvarmachypatchvvartangahoxardanneblazan";
    out << "zadbazongalzaonchinzabahvornahadadgmaoyanglosorza";
    out << "gvrahgrinynlodnnvoxardnosabnachorvornagenaldemnac";
    out << "deongaloghahanzachaladnacholzabahoxanvagemzandegt";
    out << "achynaperodonganoxadvanconzagalobnethexagerohpach";
    out << "ardonatharzandagenmobzachemomzachocolsgargazedaro";
    out << "aldamageograpgaonexanodabachirdaxvanchansorovadah";
    out << "vocepaleghorzachaphochadaxvargennohachadnadabraod";
    out << "borgemacheplagropahpringasoxilyasordoxabrigabvort";
    out << "galvelzacoriochoclanvahagedvangrahedmachonardrons";
    out << "amphiorohachisardrechadanladyedohazangephacordach";
    out << "faclonorarimbachandorzanagemvagohpacladvachorzana";
    out << "doxvarchazacholdanzachvordroxachandonzarddavalboh";
    out << "nachorgeordrachodaxvalbaogsastroxachaldonvonsanga";
    out << "corsagnagoaldaxobavzagemacorvahbolziblnhocanzacha";
    out << "nobsathvvrgandlesochanvaondoxvartorvagemptancarto";
    out << "advnzachocharvachladonvahnogergradomicladongemnod";
    out << "abvingandaroclarsamdolgelahaxpalobanmahgorsedgneo";
    out << "valzagemondaxorybladgahorzadacharolangiporsanoxar";
    out << "galsaropahchvmazonagmvagenpalephnocledachardahnox";
    out << "argemahngldamorzandagazpalontagorvdrahgenqvansoso";
    out << "ordamvagesozartonlagestagesorchadvarlongangapahod";
    out << "saltapaxlodogohoxarvahnotabzaqvyolhvbladdaxongeot";
    out << "galsetqvartgeographeadorgemadonzacloxordremracors";
    out << "satvalaoxardagesormagenondamvagenoxaqvafongvlahto";
    out << "adortagladvorneygohalchodanvexorxacholdaxarbvolza";
    out << "landantafzvblahorzadvacocomzachoacordalahordrigah";
    out << "olgedoxarnafeoniplanlvzechedzonglonzadchedoxarney";
    out << "semblothadamvageltevcrodacharadanlentagnoxagephan";
    out << "omzaligephachordaxzabnlondanvioxilgonnatfartoghda";
    out << "colgorzanchegoxandachoamichadaxvortemblothazvraho";
    out << "zadraphagenchanzvbladvangedochadathcolsadbathocho";
    out << "chanvachocorvaxdoxendixalgadahomnadongeothanvdrah";
    out << "zadcharzaochardaxvarbvlaxorgemtantopzvrethapronge";
    out << "chazadvanclodaxvangeondobnathchaldaxvrchedvacheno";
    out << "clodathacordagethnoxadvargamaxongrapaxnographedor";
    out << "chadvansalothachardonlaxpargathondanvageochadaxlo";
    out << "promphereadmanclonzanchaordamvangethoxybvlathoxah";
    out << "tladrachoxadnachardanoxpalligondanclothondonbacha";
    out << "torgethadnexalgethochanbleszachvmeronalostraphosa";
    out << "carvantadgemlothgalsegnapheoxibladathonzamnachoax";
    out << "oxpochadvanzedchadlvzadoradmachozantafgvdagonatpo";
    out << "oldachorpanchadmanoxaxarbalodnabnageffanglexzabbv";
    out << "achixochacharvandoxaldephzvdanvafagenvaoxadvaroth";
    out << "zemphareochanzaconsabnlaxodrazvagedorchasoxardano";
    out << "podaphaonchartaxvortembladordangefoxantachvmaroch";
    out << "voxotadnagepholdaxvagephvrgendonvraoxantagimacoth";

    return out;
}

QList< QList<QString> > table5() {
    QList< QList<QString> > out;
    out << table5a();
    out << table5b();
    return out;
}

#endif // TABLE5_H
