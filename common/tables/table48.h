#ifndef TABLE48_H
#define TABLE48_H
#include <QList>

QList<QString> table48a(){
    QList<QString> out;
    out << "a";
    out << "xum";
    out << "eront";
    out << "giluhod";
    out << "podonuxro";
    out << "gemnurgulgu";
    out << "vornophorgalp";
    out << "dedrohoxulbluxa";
    out << "neclidovunchuolga";
    out << "lepothulgeovurchuru";
    out << "phiangulgoxnepothixul";
    out << "dexluppunggerthroxyntav";
    out << "gedluxorgdulcothavilzicup";
    out << "deogrupinchelmondrunslepono";
    out << "genruphrungu'ldurgoxuvorruvunc";
    out << "lephorgu'ngluxornuphiontultexnap";
    out << "hidlonphtexurpdehoxnopumzintolgum";
    out << "dvdrusnergavorbledburghvturchlexico";
    out << "punconzivirbothgethloxnorvuncharcvton";
    out << "pru'lzipuxvdubluxzicpvrgeanvorneanzvchon";
    out << "dudrucongepulgenphzirmuphunlvrubruchnexou";
    out << "vldonguvindrulpernochlapontucothzichlunfaro";
    out << "gachignagelpirvunpocipoleldozanlencovarnonphu";
    out << "alblurzivonelgrulsziprontempugenphalmuchlegorge";
    out << "ruvunonchzirchethtulpluxconervunpphicolgednochurg";
    out << "lurqvinpiqvulbluxziconunphziphugeluxronunhvngux";
    out << "rexhendovrvpobalzirchunrebfagelbradovincronza";
    out << "chilodunzvrcuxlevurchsohgednorgduxu'rnzilcot";
    out << "zichorgudrulpudrunpuzipucunlexovuronpulga";
    out << "zirvunlophdextoroxtuncuphdeccornazvchup";
    out << "hircru'ngleponu'vursethgolgluchustorhil";
    out << "frozarmandvncarfalpdvhadraozixregup";
    out << "zibbelopalfidulalconguchlecornaah";
    out << "zvrubu'ldurconglervoncoaxuronplu";
    out << "demnuvelozidnupunchulzichorup";
    out << "dulucorulbolupthgedvornulor";
    out << "zircochanvbiducoxaxziludu";
    out << "vurduxalphungadvbruxzio";
    out << "nazvrechplichustornan";
    out << "zaducharhvlmanuzacv";
    out << "vibbabelzichongra";
    out << "norvinponcruxna";
    out << "taxalpudeonda";
    out << "lebazvruxor";
    out << "zichustor";
    out << "nurvron";
    out << "gvlzi";
    out << "deo";
    out << "a";
    return out;
}

QList<QString> table48b(){
    QList<QString> out;
    out << "m";
    out << "anz";
    out << "chero";
    out << "nubachu";
    out << "norvogald";
    out << "puxlurgeptu";
    out << "urcloptuzildo";
    out << "cornepoguluxodo";
    out << "vu'rgenudurchlu'xor";
    out << "phichustorhiblancho";
    out << "nuvuroxvtalpvuxaronpa";
    out << "demuphellichnongu'lpolox";
    out << "dudrunoxapolgethoxlibanch";
    out << "gadromphlipsirachaalapzvbot";
    out << "nalbodogruxvxfruldehoxamucher";
    out << "dubanoxzirnumphalgedphiodupnizo";
    out << "chamavahvpulbdevexornipugrunchord";
    out << "zabnlurdexmurrunpunsachlepothziclu'r";
    out << "dixnucharonpzirbilbalazvchalerdronvad";
    out << "pirvaranzanchulphioxnuvindumphgeluxrong";
    out << "vuxnolpuchorduxdehorgaxonuphondelpongarza";
    out << "nemrubuxulbongreldorchothdegunphaaxolurvaxo";
    out << "phichalmanzunchzvrbuncodruxanledgerphvbarrolp";
    out << "opnutovurzidroxlethcongnuxulpvixormurdexpolopta";
    out << "gehvnudronchipluxdehoxnurvagenchulcronphiodurbulg";
    out << "prochustorgnelolpodogrupugumvixnorglexopvonchol";
    out << "zirclumphlethgethnurvunhvnubrudophichurnophru";
    out << "loxorguxornuptzilphan'oladnupdvcorenvunduxna";
    out << "viruxmachvdruhzvruphumancholdurconalphu'lu'";
    out << "phicornothzirogrunolduxlonchchirmunchu'r";
    out << "ziruphugenphcalchudornupurtuxonurphu'n";
    out << "lethorunparemugoxzidrumptotu'xlurvxt";
    out << "demnagelardruncluxzirbuxormurgaxo";
    out << "legodonphzirnopolgearbunuphaxla";
    out << "gedvarnalchiruphungabalcornap";
    out << "vixollapnruchupzidogalduxna";
    out << "alporgdevanonynpollapront";
    out << "phunoxybongalboronphaqa";
    out << "ardrohuxomuclonzichzi";
    out << "nervoltzichloxarmon";
    out << "pontgethlardvduga";
    out << "chirovunchlaxco";
    out << "hvrnorzydorno";
    out << "dumphchelax";
    out << "clurzapth";
    out << "nervaca";
    out << "lexor";
    out << "dax";
    out << "v";

    return out;
}

QList< QList<QString> > table48() {
    QList< QList<QString> > out;
    out << table48a();
    out << table48b();
    return out;
}

#endif // TABLE48_H
