#ifndef TABLE4_H
#define TABLE4_H
#include <QList>

QList<QString> table4a(){
    QList<QString> out;

    out << "arbnávagel.orzádnamorgíriegábanath.oxyndámvageh.oroh";
    out << "apidnah.vonsabv'lxordírinhgónsaga.noxárbroh.abligánso";
    out << "donsal.opórchadab.sabvmilach.oldanvagor.zalv'ga.oxarv,ah";
    out << "iehódaah.ox.albvnza.nongénsogal.sigon.napvlábrahoschal";
    out << "dangénbleh.orgavolsgorsah.noldábnagef.arzy.abnold.vxar";
    out << "gefelme.opinádo.garnodáns.aglanza.vagráh.olchýmach.napa";
    out << "horzýmco.navan.garladvax.orse.gansnólro.appórge.achv'ral";
    out << "zanraga.halzed.organ.vaxárdachel.norgenadvmza.gebnahol.";
    out << "alchadax.lohanzagemnacholaschenohdaxnagehvarsanalp";
    out << "vdrah.nexolgamnapheadmachodaxmachágamrehoxadvamnox";
    out << "algamahoxalgalaxpacrogenvacondalphangeodhalpalaxo";
    out << "vargepaxarvanchansablahochadmachlozanglahvordonga";
    out << "lozangladvortongraphohhymalahdoxadmarchoxad.valloh";
    out << "cardaphanoxardramvansenglohaxbargavalchamfanchoch";
    out << "axardanlogapvlaxorchanvahalvdabronh.axchandifalgah";
    out << "sochanvaholzadmahlohcarphalvdrangephaolchandaxorc";
    out << "soholganchapomorohalgendavahnohanzachamalgvbrocah";
    out << "zazamalcharordeassapalgemvaxonamcharnochahvvltzad";
    out << "achymvahgvrahoxadmarchgalsaxorchandauoxarvanahoda";
    out << "prachadmachenoxapalchronfabmachouvdrangephaoxorno";
    out << "agildamompamchalzvboesaroomnaphdassvrachachonzava";
    out << "olcharomynongalpahaxorohcapnagempaneohalzvbradoxa";
    out << "genvarchbartographelaldaxvandeorgrabaxoxangempant";
    out << "halgraphaxargabaloxymachannageldaxorbladaxorchado";
    out << "talpyachalmogrohhadnacharadriohazvrahoxardavaxdeo";
    out << "galzanqvahoiganvahlemvanclahazvzabaxascomphacoxda";
    out << "ganzaxordraxvagenvageoldaxadrodvamgorsaacGadvardo";
    out << "golzaoxardonampallagendodangeohasperioxdrvzabnolg";
    out << "alcorgravdaxlophacolgohampalohchardaxomvageqvioha";
    out << "oxomvagepadoxachonbacomptasatsronaxvasarchonvioha";
    out << "zagvbraholhaphnogvoltabnadonalgvnageldathvanvlzvh";
    out << "achelpahzanchanvahlodagraxoxarnohprachadmachdolge";
    out << "volgedoxabbrachaxalpamachordrnxqvangelacoldavampo";
    out << "donlaphaoxargemephaigephazvrganaxvartaconlarnedvr";
    out << "aconzagoxadvorneaphargenohparcholdrvxaxadrahnollo";
    out << "achoazazonchynzaagamvradonphanchvaldvnzanoxadvamp";
    out << "archvnachaglomorontantafazvrehpachadvanzantamphle";
    out << "ozadvochorarnedadroxonaxarnagempansembloxtarsenge";
    out << "oldangahproxadnababorgaphascladodonporonzemblahad";
    out << "notonvageldrvxadvamgenorchrosadoxavolsedqvahloxad";
    out << "acharnarozemblaxoldanvahnochangelaxymacraalypaxda";
    out << "ocnageladanqvanzaoxagelvamchodonarvartemphangensb";
    out << "omvomnacadabnacoxendicaphnobladonarvantagmahgvreh";
    out << "aslondransvortemblahonzechaptargaschladonsdongens";
    out << "aradnondanvagemoxanchadnoldanganzvrehbodchandaoha";
    out << "praclohgebnacoxparchalpheonzeqdaxarchadvannaxtols";
    out << "agebnadoxardronglehaschvndaxargephavancheonzadvam";
    out << "noxarehvalodvamgonzambvlocadvalgestarqvangenoadva";
    out << "ngensongraphealdvrahgemnageohaschvraglomsagnahhoa";

    return out;
}

QList<QString> table4b(){
    QList<QString> out;
    out << "margémloge.aprínachas.óldlan.gansan.oxardámacho.naréso";
    out << "daxbna.nagláges.argýmnochas.algábroch.hanzvndamconsah.";
    out << "galzodlágo.arglox.vocántafanoblágen.doncha.malzadphah";
    out << "orzoch.lachándax.noxémbles.holságe.noplíph.ondólgen.nag.";
    out << "amphilodar.longefarchánadox.calzed.chornogallapha.dno.";
    out << "san.tachlvzanynaph.nvxabah.lábnege.norádmaba.catargéms";
    out << "oxíldvanax.totchonagraph.alchádan.vargéndeph.zv'blagel";
    out << "gorzanaborchánzvlanvárgem.abláhhodo.dánvagel.axximál";
    out << "rántala.zanviorgánvx.volgela.domphil.adron.gamfes.alpro";
    out << "daxarned.onchal.volsah.ozádmah.gorzímadrvxnáplagen.tan";
    out << "godnóh.oargens.ahband.oldáxma.zvrémbloh.axándrahoh.galp.";
    out << "notaxarch.oxáldeloh.galzy.ganváh.longvith.axáldan.nogar";
    out << "vorgon.amichar.tantraf.oxargenoh.zamleph.ordaxvah.nolpa";
    out << "dorzígah.olphínodal.ádrogan.vart.nagelvaoxárdah.volzac";
    out << "oxalviga.garv'dabrah.olchenohpaclephángepheolzar.vrza";
    out << "dólgoth.axymplah.noblázv.chardanvah.oxádmah.vorgémnes.o";
    out << "goldecha.mvbnv.asnodachcal.paxcorgaxvortímblah.zvange";
    out << "oxálva.dóngapaxnárlephan.ádmago.gálzanaxvortnabago.la";
    out << "vórsege.doriar.práchan.oxárvanal.donagel.aldvgal.zibteh";
    out << "zalbamachoagenodemages.ardana.oxardenvangefadrinoho";
    out << "axmarogedolasbohgatesornodachambesorzavagelodahge";
    out << "garzaneoxandrabavalgestargoalpaxdovnarh.empheadnel";
    out << "onzanobligahvaxtonangedoolpanagornodanabladaphoxa";
    out << "nogaladnahorzamvagestogenhahoorrohgenphanaldalcop";
    out << "aprochadonopogohazangladoxargeffaxfalgemeoldangot";
    out << "cardaxvanagedothablahgorsaschlahgervonsahgolgemna";
    out << "sabladazambalvmbrahloxadvamnahgemodanchalaxnodaxo";
    out << "pacolasdamnapatoxyangivagoalbvgehargedvanavonsans";
    out << "aplaxvornabmacopnagemadnachadodrastdabraoximichon";
    out << "dalphodonganzagohhabnagenvaxinodaxvarchadonahvoco";
    out << "adnagenaldemachoaximachandastragehalgabaoxardomal";
    out << "barbazachymacheoldanchenopnalzechozxangephaoldons";
    out << "avirohaxaxorchynodabrechadaxzenglandantegoalmaden";
    out << "vortyngamvraglabahoxardalexparmecoladmanahvoxindi";
    out << "ahonchalengemblesachynoxaldavontagnamvongemonadab";
    out << "alchephadrohaxirglalprohadmachanzangeldadamnachel";
    out << "adadgmachoaldymachorzamlageharzizangelalchinadabo";
    out << "tamnepheagendavolsadmacanyvramgemlahoxardrahcanca";
    out << "voxarvarohaldexadmacheormaroxadvanahoxingelpnalla";
    out << "doxtantonglazadmachelotaphinochangedvamaschardaxo";
    out << "abradaxnadonzangvorselgemaxarchalzaphaconzacharso";
    out << "aldvzagehaxarnachadonzavinolzadchalzymchonsaprach";
    out << "danvihonglazaheborahdoxarvagehadmochandaxaldangeh";
    out << "patraclahvonzangelaxargedondaphanzvmaclobangalaxa";
    out << "nodaxzanchadonlazymacohvortagenphalongacharnadbal";
    out << "holzacheadmachaxparchonvaormachalprodanvagehaldox";
    out << "oaproclangastamnaphaxlabrahamoldrvamphymaxhoxlazv";
    out << "naxohvolchanorsaplaphangorzahazaldaxparchaxordran";
    out << "vaxargebrahlodvangenohagemvadaolgenaxarmacholzage";
    return out;
}

QList< QList<QString> > table4() {
    QList< QList<QString> > out;
    out << table4a();
    out << table4b();
    return out;
}

#endif // TABLE4_H
