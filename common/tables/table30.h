#ifndef TABLE30_H
#define TABLE30_H
#include <QList>

QList<QString> table30a(){
    QList<QString> out;

    out << "ax";
    out << "ardr";
    out << "noxala";
    out << "phathanz";
    out << "aldvzantax";
    out << "neotaxononta";
    out << "volcapataxtout";
    out << "nocladaxtaxgetha";
    out << "nozadaclauzaclaxoh";
    out << "ueralhazantoxautagua";
    out << "viodagrathaxauerochans";
    out << "ziophalaxvualgeordraxald";
    out << "houtaxalanzadaxvutaxnopont";
    out << "valgethazoutaxanmerontalouta";
    out << "volcastorodoxnaclothaxaduaphax";
    out << "ziduothamvlgathaclouzachaclaxtor";
    out << "viaduoclaxtaxantagnagothalzaduocat";
    out << "zidnapathaclauzachaldlaxtacapharouta";
    out << "roxaldragraxadnoclaxadoxantaguographau";
    out << "ziougathalzeduochaphzintaxouaxalataxuara";
    out << "vidathanaxtaroxaduabvlaxodauzachyaldaxoroh";
    out << "abnaphatoxautonyalgethachephnaxolaxapraxtoth";
    out << "ziuanvageolglaxachephnoxadoxadroxaxaldaxoutals";
    out << "ordexalzvnaclauotaxanvaronyntoxalauvahzeduocatax";
    out << "ziuoblatouatoxantaguoxalgeothauzauochaduoxadraxo";
    out << "talglaxacouzachalglautagethuoxaduothaclaxtorox";
    out << "pichaldaxvdantaxuoagradaxvlgethaverouxaclpuo";
    out << "pacladoxandroxatongathnothagnocarzintolaxa";
    out << "geoutalagradnoxaldoxautoxnagafzlblaphazi";
    out << "acharnoxtalgeduoxtanatoutanamvansachna";
    out << "claxodraxadaxouayydnatoxalgeduothaxa";
    out << "teolachantaxautaxnotalgethuapadcha";
    out << "virsadaxantoteronzvuadaxalcladax";
    out << "vclaxtarzintoxadolgethandvngel";
    out << "ganacrapaxtoglaxotaxarontant";
    out << "vorcafnaxaldemnadoxziduaho";
    out << "vchalzoazadnochadaldexoh";
    out << "aclansautaxanoxadnaloh";
    out << "zazaduahavargetroxal";
    out << "galhadmayavernoxte";
    out << "ablacaxtotaroxta";
    out << "nelzadniochast";
    out << "zadchenvaxad";
    out << "deoutaxald";
    out << "nextapar";
    out << "vdraxa";
    out << "uada";
    out << "vo";
    return out;
}

QList<QString> table30b(){
    QList<QString> out;

    out << "o";
    out << "uza";
    out << "claxo";
    out << "anachad";
    out << "viaduocat";
    out << "nexotaxerou";
    out << "pinangethaxol";
    out << "aloxadraxtolgat";
    out << "olchadnadoxaldlax";
    out << "touaxtafatonneoptah";
    out << "voglaphargethueodragr";
    out << "oxaudaxatronotalgethado";
    out << "achephuacolzadaclaxtoroch";
    out << "algeudoxalphaudaclausothaxo";
    out << "adlaxtaloxtatoxauoxadnaplaxot";
    out << "aclontanopoltaclautoglaxtarocha";
    out << "gebnacladaxtongeduogalaxanvadaxoh";
    out << "zintouaphaxuoclantaxaugraphanuoxeud";
    out << "achanzachalaxnochchaduotaxaldlaxaduat";
    out << "volcadmachadonzacaldoxacransahaclaxouax";
    out << "gevoxtanoxtalaxchidaxtogalzaduaphanaclaus";
    out << "zautiphaxoloxaduoclaxvicolgethnatostaclauso";
    out << "vnaxtanalziutaxouaclaxtographaldexoclauobalqe";
    out << "oxaldeothadrachnadoxatoclatoxaduoclouyutagnolza";
    out << "viaclantaxazoutaxaclaxtogradoutalvdauoxalgethaxah";
    out << "varchastoruazenblaxadraxtatoxuadoxvomnaclaxtaxo";
    out << "neoxtagranzaclybargeuoxaclaxtarouvudauvahnocl";
    out << "doxadrachvdautaglaxnochastorazeubloxvudanza";
    out << "chevomrothaxadnodaxvoltoutonaglaxanovausa";
    out << "naodraxaldragrahzautaxalaxzvuoutogalzac";
    out << "yvancornabalgalaxoxargadalgethaxardro";
    out << "xlaxadnocharziutouamuangethaclaxtox";
    out << "uochadautoutolaxameroualgednacorg";
    out << "vaxalgethnavastorazanchanotalga";
    out << "aloxadroxaduadoxvolgethoxarna";
    out << "geodradadnaxalgraxacharuaco";
    out << "voltabalzadnaxachouzachal";
    out << "navotantoxacloxoxanacho";
    out << "nabalzachachordraxacl";
    out << "doutageuvagothuaxal";
    out << "adrvxadraxzintoga";
    out << "nadouzauoxadmac";
    out << "avaxuochardal";
    out << "gehvdraxvar";
    out << "uoxendixa";
    out << "qlclaxo";
    out << "uagot";
    out << "alc";
    out << "o";
    return out;
}


QList< QList<QString> > table30() {
    QList< QList<QString> > out;
    out << table30a();
    out << table30b();
    return out;
}

#endif // TABLE30_H
