#ifndef TABLE45_H
#define TABLE45_H
#include <QList>

QList<QString> table45a(){
    QList<QString> out;
    out << "u'l";
    out << "char";
    out << "doxund";
    out << "voronuha";
    out << "degumphalz";
    out << "nnxvmerongal";
    out << "piachurontunno";
    out << "dulechmurvirdlux";
    out << "zemchriorzonchadem";
    out << "vodru'xchvrumphallong";
    out << "demufechubluronchnergo";
    out << "ulhvcupuxnercoduchu'nzidu'";
    out << "locu'phnervgulchurchsolvdru";
    out << "phichu'rnopthvdugrupbdegornum";
    out << "u'cholvurpvbamunzilcothu'gedronc";
    out << "purdruxachvmuronzidubluxtotu'cher";
    out << "umvungeldroxu'ndru'xnelvu'chzirnu'bu'lu'";
    out << "piciondulvirgelphudnuxzichustorgnu'lv";
    out << "u'dornuvurgdempnuvuxlvrcoxzvpolgethnuxu";
    out << "rochu'lluxvrnothupolgethlodumvunglirbrulp";
    out << "hermu'phecarnoxzivnchetcurdriboneruxnurbgex";
    out << "urcoxulpruthgevoncroldempnuvorbydnothuxurgax";
    out << "virubruchlegoradvirochaphzuruphrunlvdzarbarnep";
    out << "vilgethaconzizachmongengalpvdaburgnerzitputhnorg";
    out << "gu'lluxcabalonchvbalaxvrimunchnelvungurgaxurpzich";
    out << "ledcorgcluxarnvannarulpinodrumphzipnulggernomp";
    out << "vutoxnulggurgunphulnorviturbonvrelvargebcoul";
    out << "chirromunchlubbunhvnzirdruxlvzurbvunmurupt";
    out << "totnu'chvlzihubvornegonpuludgfamironubalo";
    out << "gu'nruxvxnorbzivlzidripvrnunubhvgellaxo";
    out << "nergvethnorvinganumphhvnchahrelcoxno";
    out << "rumvrvanoxzidublu'hvngalmvrconchalg";
    out << "lu'pvu'rgnevorchlatorgrvdu'vuxnehol";
    out << "pironuvromunoudungothgermunglu";
    out << "dvhvrsturonulpvrunoxchirondu";
    out << "axocluphuxlunruchnervurcol";
    out << "du'hvngu'lletbvrnubunglomp";
    out << "pollidogruphzidubruxro";
    out << "urnugethcvdruxlethar";
    out << "nervumunchzidovu'rg";
    out << "u'rbruxlapvnuroxt";
    out << "pvcruphu'vonclo";
    out << "phicholgenph";
    out << "zidu'cregha";
    out << "vrchemph";
    out << "zarnox";
    out << "ddno";
    out << "lg";
    return out;
}

QList<QString> table45b(){
    QList<QString> out;
    out << "ol";
    out << "chur";
    out << "viromu'";
    out << "chu'nrudp";
    out << "geholgarpu";
    out << "chemu'xoldrun";
    out << "phinlextoponto";
    out << "alziropalhvtamu'r";
    out << "gengvu'rmbancluxorn";
    out << "tuntochunorgvidruxlo";
    out << "phirnonphulonzipurgalt";
    out << "nervuxu'xornupvruphurgaxo";
    out << "rummuduburgnethcolmdronpho";
    out << "churuchungnvburgdunvuchlvgax";
    out << "remigerechzvduburgulpluxnorvux";
    out << "vpvlnerampvruphantuxornupvolgrul";
    out << "crunchuluxledgrophudoxromonchvixur";
    out << "rvzidnophuxalgednurhondonphichu'storg";
    out << "nuvulonchzechlurgaxvdruchu'llervunchnor";
    out << "ubuntoxlurpvludrunphchirnoldumphphureong";
    out << "notogalvnchipolenghoxnorvigrunphzirmomalch";
    out << "lambonu'droxnelphuvrudrednurvachzibunzochurch";
    out << "lvruzorrvniroxtultoladmucheffaxlelbeniangmench";
    out << "loddodu'rdlipvoroneborumphnaxoludmurnothzicholanz";
    out << "zvchaphenduxnurgnebruphdexrunphalgargzichlornamp";
    out << "vundruhbyarbahvlclordepvevegadrumnozichacorndu";
    out << "firmenbulzvzolnercolarzirchvtrugmucalglurcha";
    out << "glu'xrucolhvxucorrommonondenudochulnothaxra";
    out << "viurglethgulphidrungnervulpaxondoldhigax";
    out << "rvmuphunneordurbvixolpnerduchupruxnorg";
    out << "viscongulziphu'lenpharrvdurgrunzuchu'l";
    out << "pirgenvuxnervonphunluxronphgethnu'r";
    out << "hvrchustorphinarglexurnonpalgrup";
    out << "chidruburcaxnolupvongeldunglux";
    out << "remphuxuphurnuydexorglubumba";
    out << "crianu'lghvxuryvicolmonphon";
    out << "hvxulpluchlexururbviodba";
    out << "girnuxxarcoldnxzimphan";
    out << "remonchizilpluthabvo";
    out << "nexposerurzicarval";
    out << "phiodruluxurunph";
    out << "devornongphing";
    out << "albvdu'nphalg";
    out << "nechu'staro";
    out << "phicholg";
    out << "arvunc";
    out << "nero";
    out << "du";
    return out;
}

QList< QList<QString> > table45() {
    QList< QList<QString> > out;
    out << table45a();
    out << table45b();
    return out;
}

#endif // TABLE45_H
