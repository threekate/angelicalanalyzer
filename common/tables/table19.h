#ifndef TABLE19_H
#define TABLE19_H
#include <QList>

QList<QString> table19a(){
    QList<QString> out;

    out << "sedontagentagnochaplagnoclaxydnachansednophaloxad";
    out << "vdraxtagentolantaxarnagentagnogethonodantagentage";
    out << "ophragnagedochanaxartagensoachionzachiadnohanzach";
    out << "yldoxadonzachantolydraxnochastorohydonzagathnolzl";
    out << "plachastoradroxnotafnagothvdraxodadnagenandagnach";
    out << "iplaxaliadantaglohnoxadnochavantrahnolzadnochanzo";
    out << "apraxanachendaxadnocladaxvalzonacladyblaxtopolzan";
    out << "vntagnocharnoxadaltanafafrongeontalecladoxtaphaxo";
    out << "yntoalodnodaxomerontantalaxvdamahnoxardaxodydnoch";
    out << "adonzaneoterohalclaxodaxtanebrachadolzachnedolcax";
    out << "ydnoxadynzaataymachidontalyprassodrvxdeodannecorg";
    out << "avolsednoclaxyblaxtogenaprigantoxantonagedothapla";
    out << "segorsagovoxadnoandentagephholzachadantaayplaxnoc";
    out << "vlanzachasplaxtonochalzadyndanoaconzadorydnophros";
    out << "zanvaxyrzabuloatantagnoxadarnochaydraxnolydnonvar";
    out << "yboutazaxylchadnonaplantagosydraxnochaldovaxtoroh";
    out << "plachibalzidnochasterohazvldaxtochordalaphantrgah";
    out << "maxtalgethnecoidantachanidnocharoxplaxtoleroxonah";
    out << "insantalgrantagnachoxadnoclantoxnabvrazantolzanto";
    out << "ydnoxadvdraxtohapraxtolaxtohynglatnnochalmadoxyba";
    out << "devantanalynzandeodraphalpranzednochalmanagearsac";
    out << "theonzadaxvorrontantoxacloxydlochanoxardoxlepatha";
    out << "nachadnochantantoxalzacharpricasmacharnaldoxtapro";
    out << "vdantolgethanarnographydnocharynsadgnoxtalzadnoah";
    out << "ydoblaxaproxtanoxalgednaxyntaxnorynsantagefaldrox";
    out << "prichastorazantalgeorgraduochanzachaclaxtorocland";
    out << "amanzaclaxnatoplaxydnoranvansednagorystaxolaprado";
    out << "nolednapracastagonzvrodachanzaneochetoroxaldorabi";
    out << "odalzachneldrothamvantagnodoxysclonnadrvmagvntago";
    out << "alzachaslossonnocharbachvontanvarahvongalsovontax";
    out << "vrchadnolanzadchappnacoxaldraxtapranzanoydnochars";
    out << "achenblohydnodaclaxvargethacheldondanzaldroxaclax";
    out << "nagethraxapriddonaloorrenzacloxadroxzinglaxaclaso";
    out << "vantoplasmagranyntlageordoragenynzoaconzanoxtalza";
    out << "alchynaclaxydnochadnaphexyscladnotanzachyntagnoca";
    out << "algaalgohanagtarprodantagenadnachoalchvzantaxorna";
    out << "vionzacharledzadroxdapraxolynsantafachenfonaxadna";
    out << "charzidalontantaploxycloazintoxadonzanavidolageoh";
    out << "nachanzaclothydnocharonachanzibonagephzantolaxado";
    out << "avaxadnachastorahydnoladnoxachaphnabnlaxtorgennap";
    out << "vlzacoznadorzidaplaxacapnotagnacrohadnochoronyndo";
    out << "plaxtribalzachidnogahynzachidorzadnochadaxargetha";
    out << "alzachydnobalontantoxacheldonviarbadroxmichanzaca";
    out << "nobvlacydnotraphalgehordraxyblothnacordoxagedroch";
    out << "georgaalchinzachannataxarachvolzidnochalzyntagnal";
    out << "yazangalaxvontagnataperodanzaacladchydrantontaget";
    out << "aldvzachanabvladvolhazpahgeoldanaclidnopalaxydnoc";
    out << "adroxtalgethanazadnoclapiaxychaxachonzachavtalzac";
    out << "ydnappargelzaclaxviolapordroxaclanzanodelzachepha";
    return out;
}

QList<QString> table19b(){
    QList<QString> out;
    out << "vprachasterantoganzanotalzachinzachonzaclaxtorgah";
    out << "noarbrasdemantageladnoradaxphianzachnochastoraxah";
    out << "ylchadnoplaxtameronzionzaclaoxarnopodontagensocha";
    out << "neodalaxacharzantafaynchonadrvxtolagextolapridola";
    out << "volzednoemtablaxtocenoxenarvanoladnataxalchixtape";
    out << "adnophilontantoxacheldranchidarzachonoxaloxapraxo";
    out << "tantaffeladnaxtachorydvnaclaxachapnadonydnochadra";
    out << "casagnogransadnachephalzednoclaxtalgeboraxtortanz";
    out << "uvantolantantogragephadoxandrvzadnocambriohvlnaxa";
    out << "poxchadnogethadnachalzadodroxaplichaxtapahnolzadn";
    out << "anthonaxtargenolvdraxvdranzavantantoxtaglaxantaga";
    out << "vioxalantalplaxtophanzaandroxazantalminobrachdalh";
    out << "hidrachonnacvorsephadolzachachildronnaforgenolado";
    out << "aviodaxcheponontantalsagrazichidnodantraxylzidlah";
    out << "volhantalachednachohalzemachanydroxadnechephalzac";
    out << "antagnaclohviantabrahgedlohachardaxynsoldanviodab";
    out << "gefneghaclodontantaglaxavionchadoxaraxviclaxalzed";
    out << "achartaxaloxaclodnontantoxyprachephnadonzachionza";
    out << "chialzachachodnodaxviochadoldedraxvoronopantafnac";
    out << "algephnechaldronzachealchidnochapagnpchanzidnoxta";
    out << "ylzachanlabvlachephadnaxtolaborgennodaxvorohadnol";
    out << "achanzachyntalqepahneotteroxadnochadnaphandoxalda";
    out << "phiordragloxappedaxnolzabachadnochorydnohavanzach";
    out << "ydnoholzazadachachachorzacontangamviaxalaxzedonga";
    out << "nechastorapnlzoadnaxtoravorgethnolzadnaphaneopola";
    out << "andvneantolqapaxalchiodraxtontagenzodnoxadaachara";
    out << "viaxaxaugesbrioptapolgededragraxaxalgelnoclaenzal";
    out << "navialntagnalnzacliolndadnaphienzaloxadnalpacloxt";
    out << "acolzachionzanodalbachorviongednacrodaxvitabranol";
    out << "genlahachantaxavolzidmachednogathgedothaprancheth";
    out << "nolziclodnaltaxvonnebrodnaclaantagnapraxydlablaho";
    out << "vadadnagraxadmachephalzednadeonzanecolnadeantagae";
    out << "naelgalzednochelonpalgemaphachvmachadaxlogemaphep";
    out << "ardomadoxnedraxmolontantergesadraxvalconhalziblox";
    out << "darchethanablantogethvbraxmaphathzichanaphayldrax";
    out << "apvlzachneogradlavdroxapridnapbarchednodantelenda";
    out << "ynogendavafanchasalaphgalsigmalehanzintoxalcrapah";
    out << "volzibartanuocharzintaglaxviodanneotepraxalgrapon";
    out << "marchefnaladronzachalvdraxachanzachohnalzidnachep";
    out << "archvdralaxadnochelalchinzachanvalhvdrahnohaxarch";
    out << "valzednopalgendaphalzegorgalspartrolachepnaxordra";
    out << "vioncladontantagnoxachvbraxaclinzadnxvialgetharga";
    out << "noladnogalhidrahachonzanviodnachadproxtalaclinoda";
    out << "parchansanotarbagehladvangenotanzachentagnoxaldra";
    out << "deograxnaxarbrachephadriodaxmalgephachalzagednolc";
    out << "algalchanneonzachabvolhadnachyrdrvxadablaxtoralga";
    out << "noddaxtraxyntagnocapolgethvdraxnachepvrzadnodlada";
    out << "vicontalzachanaclaxtraxtoachadnonynsabvlamenotado";
    out << "odnocladachephachednoxadnachephalzednochanzachals";
    return out;
}

QList< QList<QString> > table19() {
    QList< QList<QString> > out;
    out << table19a();
    out << table19b();
    return out;
}

#endif // TABLE19_H
