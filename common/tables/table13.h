#ifndef TABLE13_H
#define TABLE13_H
#include <QList>

QList<QString> table13a(){
    QList<QString> out;

    out << "vnadoxalnodangemnachalaxadrabalzenodanzichaxavaro";
    out << "achaldannaclodonaxvrnagendochapamachaphantoganzac";
    out << "lardrahaglanzoachalzanicladanaxargemnachonaldavah";
    out << "noxantagnachapaldontagohvolzibladoxangenvahenorda";
    out << "volzednochaclaxprachapnagedaldvblaxarchandaxnolza";
    out << "nevontalagenzoachinzochapnatolanvdnachamamvagenso";
    out << "apvlganaxardrachadaxlogohvolzigolhabranavoladgnox";
    out << "abraxaganlaproxadaxolavintagnaxaclaxachendazaalna";
    out << "chednobalaxvantagaladnochapanzarzachadnochanolcha";
    out << "noxardavahzibladangendolhadaxnachephalgendachedno";
    out << "alphinodaxalgednochanaldexarvachalotagnagensochoh";
    out << "chapnachonadoxalgenvadaxachaldannagoathageldracha";
    out << "labinochanalchilamzaphadardachalgednochargalchanz";
    out << "zvchastorandanchanatongethalchadnoxdardvbrahalget";
    out << "dvzandanoxtantafmachanzanoladnochapharchalnotagna";
    out << "phadelgethadanzachydoxapraxalgenoxalchaxarchosaxo";
    out << "nolzanchalebantageradnochaxarchanzantagnacronanda";
    out << "logednalopangethaxalgranoxadnochaeldoxaclanzachid";
    out << "vomsadarsadonsadaholdagranagolaxprachadnochadnoch";
    out << "zvndrongalazvnazemamphiganotaldanaxvlgadaxaplazod";
    out << "nochastoradnotalzachanvandrahagednochalplaxodnarb";
    out << "gehhvdrahaldexonatolgentalzearpvradexalzechaxallo";
    out << "natlesgadaxadnochadnopalaxviantaxalagladaxanachan";
    out << "amphvdaxalgednoladnoxantognabartogentagladoxavero";
    out << "anzanzadgmahzvnalchoadnichandoxandahalzichonadraa";
    out << "claxageladrachanzachydadnochalzadocladnachefadnod";
    out << "plantagnohadnachadanloxadmachapvatostachannatolge";
    out << "adrabrahgolhodnachadanzoadnoxandarzantagnatolgeth";
    out << "avradnachanalzinochadarvartagnapvlanzicoxadaphalg";
    out << "notzidnochaphadroxaxalgethadnochazantappanolzidno";
    out << "apnargemamphantalgeonohanzachadnochadaxalgedoxaro";
    out << "nolzidnoxapnargenadnachadanalgebrahgothagnogesneh";
    out << "alzanvahazvrgahloxadnochefaddvnachenalcladaxvenac";
    out << "vontalplaxartagemnolatnogeladnarbadnachonaldraxah";
    out << "alcychotaramvagenogradalpagnodaxalzichadnadaxalga";
    out << "vocastoraldvntagensoaldegnapraxadhvdrabrahloxadna";
    out << "gehvdraghapranzachalladnaxtohvocastorazachnoldaxa";
    out << "vonganzanacolatafmatoxadabalgednochafvalbolohadno";
    out << "degordahanapvmalechardanvagemolzadnopolapnacheaxv";
    out << "nademvagedhameroxaladnochanzachantagnagemphagelad";
    out << "gontalavanrahnoxendlaxorclonzadoxargedrahablaxaco";
    out << "dortongesamphalchodadroxanarplvnatefachemnagethad";
    out << "notamzaalgenzachipotagnachadaxalgrapronabvlapiona";
    out << "chennadragradnoxaproxtaxaltogamaxtargenotantalons";
    out << "onfantangahnoxadrahvolhodnachaalpvdaxargedraxnons";
    out << "volzinizantagnochadnopragentalapridnoxtradgrappha";
    out << "algvradaxnocradantantoxacrodaxalgedoxaldeloxadroc";
    out << "vvandolgemechalchidnadexorneanzemplatoxalgradonsa";
    out << "phiontalzadgmaanchafnotaxontagephalchednodraxalbl";
    return out;
}

QList<QString> table13b(){
    QList<QString> out;

    out << "lvzangladarnochadonadloxanvagethaldochadnoxantaho";
    out << "plvtarchadnoparsabrahdeolgedachvdaxnochandentagep";
    out << "avolzachantagnochanvalgenotargalaxvoxartoganloxar";
    out << "vorgethanablaxnoclansvardexclantagnodaxanclaparge";
    out << "valhadnochaxardoxambvradnochanavardohamphangendoh";
    out << "ampvladoxardragandaxvolzachapapnatoxadnovraxolzac";
    out << "leoxadrahvolzadnahchednopolotanionahchandentinged";
    out << "nodantagethalzachydoxalmvdaxalgehndraahamphigaaha";
    out << "noxalgerohaxadnochaphalchidonaxalplachephantagnah";
    out << "gehvnzaachalzachantagenoldadgnoamphiganaxolgladax";
    out << "volzantaglahvortantahnvxalgranotarvahgehvdranotax";
    out << "vrantafnagethadaxapladantagnagorzalvtantagvdraahx";
    out << "alchadnotantalgebrahvxodnachedaldrohaxadnoxplaxad";
    out << "voxcifmonatobladoxandargedonvndalaprassodaxlatero";
    out << "noganzachaphnotalgadavantalnolhanzachadnadoxaltal";
    out << "voxcopnapervdradlocoroxadnotaxalgedaxargednaplane";
    out << "cvlganzanoxattoradvoltabnagadnochapnodaxvartanton";
    out << "apladagnachoaldexodantagethnolzidchanohvolzaphels";
    out << "vraschaloxadnoxvntantafafbothagennoxadraphedoxalg";
    out << "nepvragedaddoxtaplaxnovidantagentaxalgradaxnochap";
    out << "chydrontagensoaldamacharnoxadnahvitaxplagethnocha";
    out << "alchvdaxadnacladoxadmachiondanvangethnolzadlached";
    out << "plantafeadnacholadnogemphadaxnadontagednochadnapa";
    out << "vlgethadnachoraschadnachadvalseblaxtolahalgednoca";
    out << "blangethroacloxtaynolginodaxalchasparvortantagent";
    out << "vldaolahgehorgrahnoxtriblachophidantagenaldixodna";
    out << "zemchirichapnagednathonzacharlepodantagnagraphalb";
    out << "genoxxazidnodaxalgednochadnapvlantantantagethroga";
    out << "voxandroxalgednochaldanaxvolhandaxchythargethalgr";
    out << "adavblaxontantaglasplaxnachorvdraxladaxnotargamah";
    out << "vionzanchanoxaldegranzadchepnaprachohalgedchodeco";
    out << "fantantagephacladnacheaxinrachardoxandrahvolzadno";
    out << "poldyfanoxaldexadnochateroxvlnatladgebladaxvxarba";
    out << "gethamnotaxagranadnochapnochaalgedochanadnochadno";
    out << "ambvragantantaxnacohvlvamnachampamphigeolabhvdrah";
    out << "gorzachadnoxtagethalzimachyladnoxadnochanzadaxaro";
    out << "volantantonapnagordaxvoholdantagnatosticlanpalzeo";
    out << "adragathagnachanvarnagentalapragednoxaltaplaxorne";
    out << "andraxadnochadnaphanfaaldeoxaproteronganzadaxorga";
    out << "lophannaxolchadvanoxvdladgradahvortexapraxnocappa";
    out << "zidaxalgladangethnopanachapadnaclonasmvradaxadnah";
    out << "geonzachatzeodnachoraxplaxadnochathvoladnolrahoda";
    out << "volgadnochanzednohardvrahgeoblahgeotgargeothallap";
    out << "pracecnephachodnadeomvagnachansoagignachadnochals";
    out << "pradongephadolnadabvontantagelvdrahabriglahnochas";
    out << "toradcladxmarondantagneladvantagenolataglahvordax";
    out << "vndrongarnoclaxordantagnophaalgemnapheadnvdrachap";
    out << "vorgalpaloxaddnachadadnocaphamphalgetharloxargeth";
    out << "notaxoclahzedcholondantagnaxzedadgrahapraxcoladso";
    return out;
}

QList< QList<QString> > table13() {
    QList< QList<QString> > out;
    out << table13a();
    out << table13b();
    return out;
}

#endif // TABLE13_H
