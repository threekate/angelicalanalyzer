#ifndef TABLE23_H
#define TABLE23_H
#include <QList>

QList<QString> table23a(){
    QList<QString> out;
    out << "golsalmodohzadnacladoxandragvmanoxaldraxadnachuns";
    out << "pilgepbadonzaclaxtonaporgennaphutarguphealzadacha";
    out << "lvscothudnonvangeladnocladoxtartoxadnolgathazachu";
    out << "cluduntagenazanclaxtarnagladaxvdraxnocladanvantax";
    out << "upluxnocharzantagnagothadnoxtantagladunaxvolzetha";
    out << "nozodcothaclaxturnochathnolzadnochuzuntyppodaxloz";
    out << "azincladoxantugallumeronzantagvanchapnaxdorgalzac";
    out << "neocladonpantungethuldaxzvdrachorphiantaxnoglahad";
    out << "vichorsanaxaldruxadnochalpalgednocathnulzintuglot";
    out << "upvrgednocluxtuntoteronunclansanohodnochudnotalza";
    out << "chidontageldonvagethapladontaxarnoguthluxandroxad";
    out << "urveniclaxzadnacladaxpliodnabalgulaxzintagnochart";
    out << "palitogalazadnoclansantaxnocharopruldogrababragol";
    out << "vrastaffaloxvduntuntuxalgataxangepanocalzahuchord";
    out << "lorhadnoravonsadolginzadoxatoralplachunsanolzanta";
    out << "viclaxtargentagladvongessoaxardracladgibalzamahod";
    out << "gansagnocarpanoxadvandalgvbradoxacheldonachansada";
    out << "honzacladoxadorneobalgraduntongalzachazodrahneoha";
    out << "dansantagexaxarnodantlaxplagentagnaplagentollalal";
    out << "vnchvnzalaxtargenorpachadontuntoxahlogednochalzio";
    out << "aldruxahnochustorzintagnachoalzidnochapalgabrahod";
    out << "zivastoraxudnocharardnaxplanfagnulgeledgaxigrasge";
    out << "hanzachitaxalclunvaxzinoduphanaxalgebrahnoxadnols";
    out << "phatonsanoldadagelhvdruchadvantagnotaxvongepnodax";
    out << "vadroxuclunsaphnoldangalaxzintolpraxaclaxtaraxtox";
    out << "macharnonfagensadalyblaxtogalzidnocladanzintolget";
    out << "aplaxcothvolchefnuchabvdrahachadnohalchepnadoxalh";
    out << "corsudnuxalondavaraxacromerongalhalziclaxzintolga";
    out << "vortalgafarnoxarongemnadoxalclaxtacorzintogathans";
    out << "geontalaxtanaxzuntugnachoaldaxtalaxphidantagnochr";
    out << "oldanvagesaxardalahgentungladoxvargenefuxardalanf";
    out << "gensadnochantaxardragladvdraxpraxangraphunoxandan";
    out << "consilnarvvandonultontagahzintulaxonacorvigargala";
    out << "branfodalgeblocalaldaxtuntaxtomerontulgradaxturge";
    out << "noxalpludonteroxmarondengeradaxoxadnachephzalzech";
    out << "alchunduntugnaphangeongalatatuxanglutoxzinclanzac";
    out << "ortalgoraclanzadnocoxploxadnedoxateronphinographa";
    out << "gednlzaachadnochadnaclaxzintoladroxnadantoxalgrax";
    out << "nodalgangalapthzednaxthednomeronalchephalcluxtrah";
    out << "vongednodaxtalgethaclaxtotunterodantuxplaxtogenno";
    out << "althedamvongethalclidontulblaxateroxuterodantalma";
    out << "genoduxtulgethadonzachephaclaxtrahnoxadransemblot";
    out << "ugnagethurthonacoxgrantalhadnocaphulgetandraxnaco";
    out << "zadchivalulchunzachnotastolaxardaclaxoteroxpalget";
    out << "vonsagnuclaxtontangladahvdruhunzantalgedaxarchath";
    out << "noladgradaxvolchathnodonanzvdulaxtogradantaniolga";
    out << "prudoxalclaxtontuxurnacronotuglaxtochantaxplaxtor";
    out << "zinglanfungesadnvbladnochunzamicolyduxtuteronange";
    out << "uphalnaxaclantuxoteroxablaxtogluntaxtonogansadoax";
    return out;
}

QList<QString> table23b(){
    QList<QString> out;
    out << "consudunagordonaxachadnocladaxladantaxalgradadnoh";
    out << "plichastorandaxnudoxtulaxchitalnacheantargalzadno";
    out << "michunzacladontugnomateronuplaxtarnuphangeontalax";
    out << "niocaduxtatontoglaxavantaglaxtacladoxnadoncalaxto";
    out << "vintaladrantalgradathnoxadvantaxalgradaxtonaclada";
    out << "hagendaluxvnadoxtantongalzazchadnogrudaxoncladnoc";
    out << "vonseldacluxtonuchephazvrachevoxarnadoxalnothucla";
    out << "toxuldlacothnaxadeontacuduthugalzudnodoxuntugnuxo";
    out << "phiontachanoxalgadurbudthonzadnopalachanzocluntax";
    out << "argethanzungaladoxtuphnonyntaxaluchertantameronts";
    out << "vigeldurunchonzachupathnuchaphalchinzucathalgothu";
    out << "noxudnoladnothaphonsachinoxacurdunvugethalconthat";
    out << "geholduthnoxudnuchathzabvlaxtaltaxonaxychardnochu";
    out << "vionzudnochuladnapnluxvrnonzuntolgethaxvitruxunox";
    out << "gethanzuchuluntugnophraxylduglaxupraxtolgethuchud";
    out << "nochunsudathnoxtuthuclidnochundunzabluxydnochanzo";
    out << "aplantagnoclaxtuteronzuchanchathzidnochapaxuloxoh";
    out << "levarnaxucladnaholonsahtulgethnocranudolpachurroh";
    out << "vidabraxameroxadnothanzachaleoxpadoltaxnapurgethu";
    out << "alchiduxtuphurachlubusvuthnozablithantuntaxtorohu";
    out << "vichustoryntulagednocathzibalbaluxyntuxanveronuda";
    out << "chevaxarneopluxtuntoxuntagennoludchiuntuganziblac";
    out << "gethurgathoxuldlugeodathugnochuphaldednogenvuchan";
    out << "loxondoxuclaxteruchchantuxantuxnuprontulzachiodna";
    out << "vulzabnachontuxurdraxalgethnuchustorallunoxarneyh";
    out << "chapadlaxuchuntuxanoxydablaxviulchanochastoradnoh";
    out << "zidnavagunulzibrudoxudrunonzintalaxvdruxodalzanta";
    out << "phiantagolzadnochaldvidoxaronsadeontagolaxadnachu";
    out << "geothnachastorydunzaneolpagethnachudnachabulzacho";
    out << "alzadnachutulzadchidaxtochuronaldalaxornanvagetha";
    out << "archidnuchathulchvpatoxachandaxtolgethuldruxnocal";
    out << "chiunvagathulchaduxtonophontuxurnoclanzidnolunsut";
    out << "vorsagnoxaladnophuloxydruxagentulgandruphunoxudno";
    out << "ydnabladuxviathucheldoxunodnachephazintagnochuphu";
    out << "zintalapronsadoxandagnolgethnacolzadnocoxardronta";
    out << "neoteronalgedoxtuluvornantagothaxaladnodvagunsapo";
    out << "daxudbluxudroxundaganzaphanotulbluxynzungathnoxal";
    out << "viurbuhgeotharnodruxucludnochuthzidnochantaxolapu";
    out << "geongalaxadnodagenaxurnethchanzvdaglaxvstolaclaxt";
    out << "achednochalzintolgednodaxuvilcuthzuntiguntoxucoth";
    out << "zednodaxalguthempaduxturuxnoculzidodaxarneoturnun";
    out << "zichulaglaxadnuphanyntagladoxanolvioturonzentoxan";
    out << "neocradnxpiolzuchedzantaxalgraphunadnochudaxvarro";
    out << "ydvalzachatantoxaclinzoundortantaxuldlothaxudnoth";
    out << "chevaxunalduxmuronphiudnodugartalantaxudnothuphan";
    out << "ylzadphuresudnophulozachansadneopolitaxadnochansu";
    out << "leodgradoxupolgethneotuxarnaxturgenothulzednochat";
    out << "zidnochunzachalgethognelulgednophundeodruxnachols";
    out << "voxornochapluxturgenothuldaxtoroxtuntunvageholdah";
    return out;
}

QList< QList<QString> > table23() {
    QList< QList<QString> > out;
    out << table23a();
    out << table23b();
    return out;
}

#endif // TABLE23_H
