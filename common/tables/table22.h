#ifndef TABLE22_H
#define TABLE22_H
#include <QList>

QList<QString> table22a(){
    QList<QString> out;

    out << "geonglaxardranzachednodavahgebvrahneldoxadagnalax";
    out << "chydortaxalgadnodroplanvracharnograxvolzadnocalpt";
    out << "anraxploxadnodaxirnocathalchathnaxarochadornagelt";
    out << "pionzaclantagnaxadrongathyldroxaclaxtolleodrantan";
    out << "tatnaxylzadnoclavidnalaclanzachiodantagnachoadros";
    out << "apirnathgeongalabnaxtaloxandlagothnaxvdraxaclaxto";
    out << "chemnadaxtoroxaxarnablachedoxnachionzachacordolor";
    out << "patagannolzadnochantantoxalgednoclantoxalgraphars";
    out << "handraxtogalblaxgeontaxviolzachadoxnatoxagraxclad";
    out << "oanzachindalphargedoxadnolgalzidnaxachorganavordo";
    out << "ybladnoclaxycnadoxarnethaganzaleodnochanvadrachal";
    out << "gedvansabvlzachaclaxtargentagonalzvdraxaclaxtocor";
    out << "viongradenodglaxtaxtoronvndaladolgethaclaxtograno";
    out << "vardolganoxadnapharchinaxaldanchalzadvornaplaaxod";
    out << "zichanzaclaxtocharonangendaxychladortontonaplaxto";
    out << "vigaldochaphnadgradoxylnarvantaxalexadanchidonzac";
    out << "naglaxtarorchindoncladoxadvargednochalvdrigathalz";
    out << "amzenvachloclanzachidnoradrarvolhadnochaphaldraxo";
    out << "geodnadoxaldlaxacontantoxalevantagnachoadelzechar";
    out << "colzedadnoclidaxvorgephadnodabraxzintagnograxalga";
    out << "zvrastorachanzachnathanzachvndargalaxphilcholadax";
    out << "viontalachvdrachaphzantaxalzednotalzednaxtalzedno";
    out << "agervanaxargethnochanzanalzachadnochasaplaxtopher";
    out << "zvngradnolzinparacladnagelradnagvelaschadnaxzinda";
    out << "zintagnocaphzvntalzincloxadnochantalzadnodaxadrah";
    out << "loxandrohvicholdahzvdnamvagesascherachodanvahodno";
    out << "dolchizanazadnoclaxypradoxanabvolgethanzvdnachali";
    out << "zinglohaplachnodaxyntagnographvilchethachraxtarah";
    out << "algabvrchgenodangalaxzintochapharmachonadoxaterox";
    out << "zvblahacharnocholaxadnoclantantaxadroxzidacalzant";
    out << "vnzachadroxnolachardomavagesvscladagenodaxarnacla";
    out << "vixoldraxacladnoxzniodnachaphangelaxadnochanzacha";
    out << "zilvachaclidnodaxyldavontagnahachertoxadnachoalch";
    out << "dornagradalzadnoxazintaxaronphichalzachacordraxod";
    out << "voltabphachadnaxodantagnogradaxdemnaphealzanachas";
    out << "tortaxlardronalaclansanoxadnochopvansonaclansahox";
    out << "genvengahahaldaxalontantoxalglaxtonataroxtalaxoth";
    out << "gothnachalgathahhaglaghanothagmachoalchadnabvagha";
    out << "chansachonolzadnaxaxlaxadnordedraxogladnochoryndo";
    out << "aphangethaclintagnaclonzentoxacraphaxalgenolaxado";
    out << "volchephnaxalchadaxtonnachansadnochefdrvxadnoclax";
    out << "pichalzachacladnodabvargendavahalsednovaxardracla";
    out << "chadnochanzachaxalgranzachnatolgadloxadracontanta";
    out << "levorgalaxvdroxaclothadnonangendoxachadnopractaxa";
    out << "dachanzachnozaldahzidclataxadnovahzintoadnoxagals";
    out << "patradnadantachnograxalantaxgranzidnodanchavolney";
    out << "achansahydnochastoraxaldruxadnocraxvolganahzednox";
    out << "archernehzadnavalaxzvdnangempalgetharchadraxtalzo";
    out << "nochanzadelgalgahaportaxadonzachnatosadnadaxtaalz";
    return out;
}

QList<QString> table22b(){
    QList<QString> out;
    out << "claxturgenothudnoguthaxadnochataldaxadroxaldlange";
    out << "noxudvargednochaduxarnacholzachadnachalzachidnoch";
    out << "adalzuphinaxdolazintagnaphadeoldragathagednoguthu";
    out << "drvxungathalzednuxartozonpucolzachadoxadornagotha";
    out << "vicolgathanubnlantaxangraphunoxaldluhgeodnaxtraxo";
    out << "appluchupnachontuntoxoldaxtornaxtaronulgednocharp";
    out << "palgramdanchethnaxtalgradaxtargentaxtolaxtagathan";
    out << "vldextanogathnogaladnoxtageodnapharzanchonchanoox";
    out << "archinvahaldoxantagornacoxildladoxtachadvantagedo";
    out << "galzanvachadoxateronnechastoraziblahnevomroxadonz";
    out << "achadnochastrolapladnoclandonvagesadnacholgladaxa";
    out << "paclaxtoterohgethnaxararchidnoxtalganzataxalgetha";
    out << "xalgradnoxaldontantoxantoglahvolzednachastoroxalp";
    out << "zintagnaoxadnochaldoxaprodoxtalagethanoxalgraphou";
    out << "adnachordanzachanzarchadnoxtaroxalphinachondaxtar";
    out << "oppalgedoxzintograntagnochaxalzidnochadaldephazar";
    out << "zeographanaxtolaxtoterongazemblothachadnaxtorgeno";
    out << "axargethanoxalgradanchadaxlargethaxlargethaclonah";
    out << "nechansoaldemnagohaxaldlahnechastarozadnachalaxad";
    out << "nolhadnoxgemvrchanalloxadroxadnechastoroxadaclado";
    out << "vongensodaxadnadnadlazinglozanclamafageorgonoxelo";
    out << "alchadnoxadantagnacholdalazintagnocharpasatmaqvat";
    out << "neoltathachadnaxaclidonaxalgednocastraxalzintarge";
    out << "apraxtalzedadnochathalzednoclazincholzanvarardong";
    out << "alclixonaxdonchallaphaamixavardohalzadrabgeongrah";
    out << "nachebalothandagnocantantoxalargethangodnoxalacla";
    out << "geonglaxadraxtalaxnorgednochastararchvdraxalgenta";
    out << "lachenzachadnoclaxvicalzaclaxtargephahnolzadnocha";
    out << "zintolazadnotaxvaronyntalgraphaxdongraphazodnacha";
    out << "alzhanoxadnoclazaclazadnoclantantoxalgeondalgatha";
    out << "nexodradoxaldrahahonzachidontaxonzacharnodlazadno";
    out << "aplaxtontoxadranleganzachnoxsoldahavontagalledaxo";
    out << "gnaxtalgedothazadnochastorzintagnozanadalzachidor";
    out << "vodolcophaxadnochaphzablachararzadnochamvaxardrox";
    out << "gevorchanozaldrontaxalgranolydrosassoldaxaprongah";
    out << "necolmaxadantaxaronmaclaxtaraxtateroxvnadragentax";
    out << "olgladaxtalglagothnoxtavontalzachnochastorachacla";
    out << "doxvargednodantagnaxacroxtalzachnolzadnachephalza";
    out << "deothnagorazintalaxonacrontalgednoxaldalpalgethno";
    out << "claxtontaxnacrapagnalondraxvalaxoncharnachalexond";
    out << "draxtantahvolchastorachansanozalvachmanchalaxadno";
    out << "gebnabulaxvdraxvanontantaplasascheronontantoxaneo";
    out << "pronchanzachazantaglahvolzadnoxaclaxtranoxzintlax";
    out << "aprontalchidaxtionavolgechandocharnopasviodraphan";
    out << "chartaxaclaxtarontanomeronsagensagentagnochaparge";
    out << "avalzednoxantaxanteroxalzednochastorychlaxanoxaro";
    out << "pronchasmalohazvdrahnoxalgaloxachandazachidaxaloh";
    out << "phianzocladaxtanoxvidoxaproxnachansagnoclaxzintar";
    out << "gantalgataxorommarvammatolgehvtraxinocraxyntalget";
    return out;
}

QList< QList<QString> > table22() {
    QList< QList<QString> > out;
    out << table22a();
    out << table22b();
    return out;
}

#endif // TABLE22_H
