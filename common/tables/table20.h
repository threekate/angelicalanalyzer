#ifndef TABLE20_H
#define TABLE20_H
#include <QList>

QList<QString> table20a(){
    QList<QString> out;

    out << "zidnocladaxvalzidxalgethadnochadanzoalnichadnocha";
    out << "pialzadaxodadraxtalgembaphorardronzachidnolaplans";
    out << "avanzaclaxtornoxadronzaclatontaxavilcoladnaxtraba";
    out << "viongafadladnodalychanzachenadmiclaxvicholzadanta";
    out << "ypladaxadnochaneographaxaldenographadzintaglaxapa";
    out << "dolzidachephnaxardraxtanonzantagnocharzempalgebra";
    out << "opadnopolgemnadoxtaloxydnochapalalcrodnotalgathal";
    out << "ydnoxtrauneotterolattorqemphaxaldrinodaxydnachedo";
    out << "appladaxtogathednacholzintofadolzidachidnoclanzoh";
    out << "volzimeronandorganolviotratonzeburadrvxalgehonzac";
    out << "aploxatangemnapheadnolgetothapnadgodanlebvdaxtaho";
    out << "lachafalaxvdnantentaxtolachadnoxadronzachiontagla";
    out << "fidonzantolgaraxaprantagolzaclidaxtoteronanzednol";
    out << "achardanoxyntogranobladochastorachanzachiandrogat";
    out << "olchantantoxnachantalchadniclatoxtaronalgedrachab";
    out << "gidnadoxaldagalvrchephaclaxtonaplongentolgednocha";
    out << "avidonachednataxtoxtalgentonaplachonzachvmerohyda";
    out << "nolzembloxacloxadnochantaxadronzachanviconchasado";
    out << "lexangethaclaxtragontagnochanlvpataxoclaxnatoraph";
    out << "zintagloataxablagentoladaxtantoladoxaviontaladnox";
    out << "chidonzanachastorantoxapolgethnotolzachachadnolad";
    out << "ziclaxaplaxtochiaterontantafnachoaldemnagethalcha";
    out << "daxtorgednoxtagranzenextalgradoranvicorzentagolla";
    out << "chiatraxolzachohanchadnodalvigethaclaxtonantognog";
    out << "aprintolaxtanteronachednochanzyntoxalgorgesvoxado";
    out << "lextontavalcheythnoldalachinzadclidoxtrohaproncha";
    out << "navartaxalgladanotaxtoloachinsadaxtantoxalgradoxa";
    out << "neotheroxalchvdronzinvantalpraminolychadnochahvor";
    out << "gemnoddansantchianzachidaxtoltalparaphinotalgetha";
    out << "alvidonatoxatorachildradvxadnochadoltagnochanzach";
    out << "violzachephalchiddraxalgepahviodnademnoclioxadrax";
    out << "chadvantaxnozalbavorgentaphesaldlotaxantantonacha";
    out << "volsednoxaclaxtroxvioltagnocheronadnochaxviolzado";
    out << "apladnochenzachaclonzachaclonzadnonchidaxtocheron";
    out << "apulzachaviothatnexolzachchidontagnateroxplicasto";
    out << "amnaddiontagnochantaploxgemnadoaclizadnochadnacha";
    out << "pargentafnaxoteronadladaxtogethachadnotantonontoh";
    out << "axalgraxtadnotargradaxpladonzachidanzacharzantaxo";
    out << "vichalzaplachadraxvnastraphavnadnochaphaldraxnoch";
    out << "neoterohaclidontagnodalgabraxtorgenvadaxorzinvage";
    out << "olchadoxlochadnoplaxcaltoxadroxtonaplagenadvionza";
    out << "chemnadonabvrdraxapolgethachednochateronalchinzac";
    out << "plantagnogephaldraxtaglaxtoxtargenodantalgethanza";
    out << "dlnabachardaxtoteroxvlchadnoxadoxviolataxanayadon";
    out << "neongesaphadnachvlzibalantantaxtorohvlzidnochalza";
    out << "phachansadeonsadaxvaltosagentolyndronzagohaphanzv";
    out << "dolzimneyadvrnadonvintraxtolgethargethandoxandolt";
    out << "aplantaxolgranadnochadavoxtoradchidlaprichastorah";
    out << "genosoladnoxaclaxphidantaladnochadroxtalgethargah";
    return out;
}

QList<QString> table20b(){
    QList<QString> out;

    out << "zeodchoalchednochadnochanzartolgendoxadraxtaloxoh";
    out << "czivannachoalchidnageladnocharoxtantaxtolgentageh";
    out << "avorrontanglaxtaronzadnoxydnacladontalgethraxodan";
    out << "geonapheadnoxtaplaxadchidnoladyntaxarteglaxandrax";
    out << "phantaxplatontagenadnochalrdphinalcheahemsadnaxod";
    out << "alzvzachachalchaldrazantagonadnohadnoclataxneoter";
    out << "aphinalgedaxtantoxangeodraxaclinzataxaloxtanoxtal";
    out << "yntolachednochapaldroxtantolgenodantagemaclaxtala";
    out << "chidaxtalaxchantaxnoxaclansasidansachenzadnachans";
    out << "aplaxcateronchidnochantagnoclaxavintogalzadnochar";
    out << "iblaxadolzachidantagnoxadroxavionzachalgednochals";
    out << "geodrantalchadaproxtaxnochateronzintagnocalydlogh";
    out << "acharnoxadnotalzadnohavintonatontarongenoxtalzadn";
    out << "alchidnopaxalchadnolgedvrgalaxornachonatoxtachela";
    out << "plichonzaneontagvantolgenoxalchidonaxdeonzantalza";
    out << "chidnotaxantarganohydnoxalgibrahnolzadzichastroxa";
    out << "plaxadnochadnabvlaxyntagnacholoxaproxtaxalgranata";
    out << "vionsataxalgradnotantonagnottanzachidaxalplaxarto";
    out << "neochadantaxalgethachenvagolabantagnachvdraxydnoc";
    out << "advlgetaxalgrahagolzachyndonzanochastorachinvahod";
    out << "nebvlzadnochoalchadnaxtoteronchidnachefadnoxadnoc";
    out << "vlzednodaxalglavortontonalchipameronchinsagnoladr";
    out << "agednoxtatetoronchadnochadolzachemvantolgemnadoxa";
    out << "apraxtolachidnodaxtartantaxalgradnochastorzimnach";
    out << "ylzadnachitaplaxaplacabtonogentaladnorvachaddvnta";
    out << "nechastorvnaendontagnochalzednachenaclaxtoteroxal";
    out << "viantaclaxinotalgentoroxalglannolzidnohachanvanta";
    out << "laxtorgentolachintolaprantasenalvnadraxtolgamnato";
    out << "avantoladraxtlaxadnochapzintaxonalviotalgethadnon";
    out << "volchadnaxachidnoxaclaxtoteronchinchadnaxocholzar";
    out << "zinannagehorgraxtontagonnecoladroxadphidaxnohylzo";
    out << "chitargexalchepnodaryntolaxanvaronzidnechapaxalzi";
    out << "chidnogradaxchintolzadamanzarcladonzaconsatnochal";
    out << "ydvblaxacloxtateronchinadoxtantorgenvaxtoraxadnac";
    out << "clantaxnoadintaglanchansachnochastoradolzachadran";
    out << "chedoxaplaxtoteronanchinzanaxadaxvionzandalaxmico";
    out << "avortalphichalzoadongephnolzintaxachadnochadnolza";
    out << "yntogantalpalageothadaxaclaxtortantaxanavorgethax";
    out << "chiadnaphianzanotolgrapadqnochaphalchvdaxargednoh";
    out << "calblaxadranchanosenblathchitantagalzvdnaxathedno";
    out << "archapnadoryntaxalplacatonatoraganyngefadroxadrox";
    out << "neolgraphaxgeodnapontantonacheldronnaxtalgethadox";
    out << "legednoandauzachiontaxallextrachedontaxachidontal";
    out << "yblantoganchidoxalgethacloxlempnagolacheldronzach";
    out << "vdangathalchonacharzartagnoclaxargephaldraxanvach";
    out << "geonzadaxtolgradnonyntlagradnachapmichadnoxadolza";
    out << "chionzaclazadnochaphneoteroxacloxadnavorgentagnac";
    out << "vdranvahaxalgredonnortantangohuhadnotasvanzadnoxt";
    out << "vrgethadronzanotargenolvnchaplagentaxonetoraldeph";
    return out;
}

QList< QList<QString> > table20() {
    QList< QList<QString> > out;
    out << table20a();
    out << table20b();
    return out;
}

#endif // TABLE20_H
