#ifndef TABLE12_H
#define TABLE12_H
#include <QList>

QList<QString> table12a(){
    QList<QString> out;

    out << "doxargennochaoldathanpolidioxarnochansadanzachaps";
    out << "vonosgrachenadonzachionadaxoclaxvorsilathanatgeha";
    out << "notalganahvolzichadnothanzachalodangalahvolzigloh";
    out << "plagensocapransanoxalpneardenzaveodanichasplanset";
    out << "notarmachvxoltantagemvdrosadrissoachadnatlodnacha";
    out << "aldenzachanzoclanzadanparchepnapahnoladaxnochorao";
    out << "chanzachansvdaxlochasnatarvannolaxvanvglahlodaxah";
    out << "nochanzadahnoladanvdnogetantavaladchinochamvanseg";
    out << "aprongehnozadnochapladnochaphzyntagnonadranvahzvd";
    out << "galgednochanzachanvoltablachardazachioptachennota";
    out << "vosadadexphanotalgebrahlvdnachadonzachvdronvagemp";
    out << "dolsaladahgehodnachamvansentablahoxandagnachonpla";
    out << "gonsandravacanachongenvdabrahlodegrahnoxoldohabla";
    out << "geodnadenandenazvblageodchadanzachypalganoxalzech";
    out << "aldranfanoxpamchalonvdrahvoxoldohchemedgvehardozo";
    out << "zvchaldazancapaloxvorsephachandahnolhvdragranzith";
    out << "vonclaxpavtogamadonzabaaolzichapalcodachaxvorxoth";
    out << "axenblodnodonzichadanzachidontagohapvagnacapragal";
    out << "zednochaphadnahvnzavoltaxocartantogemadnovocendah";
    out << "endangeochestahnegothanaxvrgethovemnachaadraxadab";
    out << "aldeqvatarzachamnotatavovnoxvdranvagetnochandamph";
    out << "zednogedohaprrahclinacodaadnopszvchascorynsanvach";
    out << "noldempagensagedioxavortabalzedoxalchvnadanzebaIl";
    out << "dozanblahachordahnaxpragnontemptachonahvolaedatha";
    out << "raxadzachamnoladnachemaphinotalgenadaxvonsadnocal";
    out << "diahadnodanzagempancholadringephoxameroxpridoxarb";
    out << "capnoganaxvdraclaxnoxadnogemnachalaxnochanvndanga";
    out << "voltadnochaxardelotaplasvolginoxadnochapalgalepox";
    out << "vdrastodgachensachannoltabagradnochanleoptapesons";
    out << "vndrangalepargelchohaclahadnochandrvzandahvochanz";
    out << "antagnogladaxorzinodanachanzadaxvoltapladoxvdroge";
    out << "tablabalonvnsagamfamsabangethorgaclantozanzvchaox";
    out << "laxplaxzocanneblodanzengemphapholdaxgeoxdafrahdox";
    out << "prongeohachadahdodachahplataligalgenspachadargeff";
    out << "achvdnachydonsacladnorvonsemplagoxginochadolpacha";
    out << "lagesodrassohclaposadnoclyoargeshabmagohsadnagelz";
    out << "pvrchednoladnochanoxildangaphanochansadonalchadax";
    out << "vorzipalanochastorvndremvagethvdrastnodalzechorar";
    out << "pilantantadgadaxarginnodanvalzedchovaxardemvagesa";
    out << "plaxochorontantohachirnochaphzvdeclangendaxochanz";
    out << "ardronzadontanfacalsednodabraolzichanasagnagonzeh";
    out << "valchesteaprahnecladansvongedohacharsohplichastor";
    out << "gahvldanoxandnohvoltantagemnabolahzonvahplaxoharg";
    out << "nevaschorvdrantognahnablapalahvoschyladnochadnadr";
    out << "vngensehvoltagnazyntagnochapharsephgelladrongenox";
    out << "galsedghaprasconadansadethadnohavalgaphachadnacho";
    out << "dalgednochapharchastortantantaglahvdragemphacholh";
    out << "vchanzadempharnohgethanzanoxalgradahvonsanohgarxa";
    out << "vaxpressonanochastragaplangelnotantagelthaxavprag";
    return out;
}

QList<QString> table12b(){
    QList<QString> out;

    out << "zidladanzoalchephachodnachondalvahgehvdnochalapha";
    out << "doxanblehdontalligonclamvacothaschadoxydnadabvols";
    out << "ardnotamfalgethvdradaclaxalandaxdaidachamfedgloht";
    out << "chanchalongrahadnochansadaxvolsybladaxcortazanoha";
    out << "volchibladoxaldampamchephneygolzablageordepageola";
    out << "gonseachephnochanzaalzangephladongethandvnzaglaah";
    out << "norgaabaphnoxgvbrahachednochadaldanganoxamnadaxol";
    out << "partochalopzvrahachenzahlvzachadaxodgensochadelva";
    out << "amlemphadantagenesframseblopagedrobaalcharaxodaah";
    out << "plvtantoganhadnazemnotaxvdraxplohgelgasrogensande";
    out << "platagnahvorsadnochaxargendohvolgemphapragnachads";
    out << "voltanodaxvaladnoxdadzemplanocaldograalblohalblox";
    out << "coxendonahplvdabrahabzenohalbrammachohalbladanabs";
    out << "geldochanslapharmaclonalzedvaragednochansadnophas";
    out << "valteriohaclanaxgedantagnochamvagesarsnohgelholda";
    out << "vobredonaxperodanzachenzaplonfrongeyatagnohchalax";
    out << "angemvacolatafnacongendrahohaschnalvolgephaldezod";
    out << "zvchastovangepheapladaxvrgenoxaldephachadnahvalso";
    out << "devnzazadgramvlchadvosangefardranchatalgepphesvda";
    out << "nalzengenotalgebrahcolchadnodaxargaclotantantozah";
    out << "vongeblachvdaxocropalphagensadonaygemphdevalnaado";
    out << "prochastorontantrapheadchanzaalchadanvorgemvesado";
    out << "lexargeoxnaxarnolatantographgeldoxacheldonataslol";
    out << "placonabamnachemadrogantalatoxanaxarchephnatoxato";
    out << "volzedozachadnalzindachanvalzechapnatolatadgmahoh";
    out << "gensaldahnihhodnaclanobledoxargefahprochimanataxa";
    out << "volglanaxtoryefeadnochadaxalgepragethornegensohaa";
    out << "alcladrvxadnoclaxvarnohclansagnochasterrvdanzacho";
    out << "plantanglanoxarnedeantalgephadronzachemvantagelza";
    out << "nebvlagednochadnachydonzanechastoradnorbadclanta?";
    out << "vdradenoxaldachadonzednoclapragaochaphanvdragedo?";
    out << "caparmachavnzinochanvarnodanahvlgedvanoxacradoxa?";
    out << "pacladmoconadranvagahvlzinochanoxanadaxolphaneot?";
    out << "valhvmarontantoxolgephanatoxanchanagenvadahvdnarb";
    out << "vortanvahvlgensodahvncheldoxaprochapnagenzoclanor";
    out << "phialgadolzednochansagnachaphaxadeblahnoladnochan";
    out << "zvblaxvrnochadandalzeachydraxachadnoxarmvrabradoh";
    out << "gemnastrodachyldanoxacladaphvrdragenzocacadnapher";
    out << "vlgentoganzapalmedonahvoltazanexoldaneochadadnoch";
    out << "aringithacharpraxadnochahvortsalnaxadronzachanzab";
    out << "nevornadanchaldochaxvoltagladaxarbrachadmachenzoh";
    out << "alpnlachaphnatolanvarnacladoxadavantagnochalohato";
    out << "volgiphexarmiihadantagnachephadnadaxalgenzacladoo";
    out << "nolzichadnocharonvolhandachanabvradabrahvlzachado";
    out << "vlzadnodaxalpragnachandontarphamphangethnoxalzeds";
    out << "prachephadrantagentalannataxargednochapnochensoah";
    out << "zedargaoxalpvlabrantaxaqvalzegohaldraxavrantahvro";
    out << "dansagnochansalpaladvdragahbrantonaxalgephachanza";
    out << "chantalzanadrantoxaplagedahvolzibladaxtantafnacha";
    return out;
}

QList< QList<QString> > table12() {
    QList< QList<QString> > out;
    out << table12a();
    out << table12b();
    return out;
}

#endif // TABLE12_H
