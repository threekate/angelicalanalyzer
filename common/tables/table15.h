#ifndef TABLE15_H
#define TABLE15_H
#include <QList>

QList<QString> table15a(){
    QList<QString> out;

    out << "geodazachalzednochaphnoxadnochalaxandagamphadaxod";
    out << "lebvdaglahoxadnachodalzednachaplaxadnochadnaxtoha";
    out << "lexodnagebathnachadgeodnachadeoxnachaphlexodnagap";
    out << "volgradaxvdrachasmarahvoltantagefadnadnoxagelsoah";
    out << "zidnoxadnabavolhadnoxadroxadvoxarmageladclaxtoroh";
    out << "progednochaparthantagexargradnopraxalgradaxvortax";
    out << "achidnodansagnachoaldemvagefaglazadnalexodrantano";
    out << "prochadnodoxaxarnahvocastnachorazvdnoxargedapraxo";
    out << "daxtalgephachadnachapnodalgaxacladnopamvangeloxah";
    out << "plaxateroxzvdlaxtorachenzadoxadnachefzidnoxadaxol";
    out << "argednodaxtaxarnedothaprexaldeodnagathzymchadnoda";
    out << "plichanzadeolzadahnochastraxplidonsadanpahnechaso";
    out << "alchedzabachadaxlogemnapheadnapvlaxdolganzadrozar";
    out << "noldemphachnadagehplodnachandantaxardvragendalpah";
    out << "zexodnacholchadoxlacormacoxparteriondandemplaxocq";
    out << "apvlzadaxadnochaplatadgnalochidaxandragemplagedon";
    out << "gormifaradaxvategolzadnachvdroxadnagentagnophalso";
    out << "vrdrahagintacolplagnocladixamiclontazantagentaxol";
    out << "aplaxtanzintagnochartaxladontagensonaxaldaglahvdo";
    out << "malgednapaxzidnotartaxnachadaxalgalgazugnaclogaah";
    out << "voytantolaglasvongergraxochanzaaplantoxadmorvanyo";
    out << "toxaldegensvncladantagnolpridaxvarchefnodaxlageph";
    out << "vscladnodaplachaxvdraladroxnophalzidnocladonpicha";
    out << "dlvorgethaclaxtoxnaclanzadaxvommadolaphchiodnadax";
    out << "zednodaxtargetaxvolhadnapraxtolgethathgehodnaxolo";
    out << "pladantagentalgradaxadnocladaxvorchadaxoldamvaget";
    out << "nacladmachanacharzachaphnotalzahaldvzabahgeodnaca";
    out << "axardrontantongradaxvolzadnachalprotaxtagenadnaxo";
    out << "deothnatoxalgednochanzidlagednoxaplaxontagelzadox";
    out << "vichordaxaplaxtanolphiontagnageonzanolaxmachapaxo";
    out << "deonacroxalzachozantalgachanzanochastorvolzebloth";
    out << "prachapnaxandrontagephnoldexadraxmachlodangethalo";
    out << "chodnaxartagalplagensopraxadnoxvartaxadaxnolaxton";
    out << "vachednodangednoclarvonsanphaachidnoclaxvorgentol";
    out << "prachaxtonymnachodaxvolhedchephadoxtoxnacvplagens";
    out << "almacheldolzadnadontagnoxapringadaxleotanneoxagra";
    out << "geldonzadnobladoxtantorzachaphziblaxtoraplaxnocha";
    out << "devornmachidnachodanaclinodaxvarsehgethargalaxorg";
    out << "gradransaxvngrancladploxarnoclaxvornefacoblaxtorg";
    out << "alraclaxtorglamvagrasvxordrohproxtalgrantammansan";
    out << "chalsadnoxacloxadrantexolgedadoxarvacordaxachidna";
    out << "pliconsatexadvarnechosaplansadolsednomvaxardralch";
    out << "achordahvolzednohzintagnolapraxangradaxolgebraxod";
    out << "pradlantagrasvdraxadransonotargethaclaxtroxandroh";
    out << "pridantagethnodalgepronsednochapalgadaxtargehoxox";
    out << "acladnaphanotaxalaxvanchefadrangolzidnomacomvalla";
    out << "claxtargradnoldaxarvogethzachledanzadonzanvachans";
    out << "oxalaxtottatnachroalpvdabladgeonagraxvolgethadroa";
    out << "machadnoxaloxadexargethnolzidlaxacharnalvortnatox";
    return out;
}

QList<QString> table15b(){
    QList<QString> out;
    out << "parglogethnaxavaheldachanavaxodnagedlachonzavioth";
    out << "nazalpaoxadnohachladnaxteontaxvalgemvagethnoxalgl";
    out << "varchcoparchaphnaxadladantantaxaclasnodalvortnada";
    out << "gemnaclosaxvortontagemphacloxadraxvorgesadraxtala";
    out << "noxalgladvoxardothchadontaxnohagladnachothzednoxa";
    out << "vortaxalaxtahgemelchandoxaldlaalabonadaphzyntagna";
    out << "olchimadaxvolgephachandaxorardroganaxplicaxvortax";
    out << "ychinzadaxaldaxantagohnehoterohadnaganzantegohado";
    out << "alchadanzadahgoldaxtarangladaxoraxtaglaxnatoxtana";
    out << "andemnaphadoxalgathgednodantagethachadnadaxzidado";
    out << "alzednadoxalgemphaclaxtorontantagnahgethargothacl";
    out << "adaxorgemvagansadaynolitantalgoaxargednachepnocla";
    out << "andnaxoladnodaxavarnaxiblathcalligalzanoxadnoxada";
    out << "plachastorarzechadaxoymnachadantanoxalglaxprachet";
    out << "avornohgemnachadoralgladnochantantaxnaplaxoteroxt";
    out << "maxpargathnoladnodaxvargednochaphalzednochararneh";
    out << "toxalgahlechastorneoplataggensodaxvoltargethadaxo";
    out << "nepsibalzachadnodoxandlagadnoclaxvorgenolpadanzio";
    out << "chaslabvahgehornoxadnomvagefvdlaxdargelzoadnochas";
    out << "voltantolacansanobvarzvnclaxvorgefadnaxtoadranzoc";
    out << "chidargladnochadnaclioxvortempadraxtoladnorgethah";
    out << "zidnochadadlaxvidnoxalpraxcodnadonzintagalzantaho";
    out << "chavarsephalzednodarvarnefeoxadnoclaxplachadnodax";
    out << "longlaxadronzaclinoxazicalzvadnobvarontantoxadoxa";
    out << "lebrahlemnrahaxarnahzednochadoxadahvolhelmancheph";
    out << "zandarglaxtohaplataxzinachvadnodaxtanvagensadoxal";
    out << "pladnobthadaxtorgedahoxandaphnocastorchidonzaclah";
    out << "apranzachonzadnoxaclaxtorzednogalahndantalamichas";
    out << "vdragalplacoxarnochastraphengenronsadnoxaclantaxo";
    out << "plaxaterohzintaxalclanzacloxadnohvargednohtaxadno";
    out << "vortatagehnaxoldagavolzimachoaxarphadolzindlacons";
    out << "aphradgnaclansachadnapvangensodnaphagvdrahaxargah";
    out << "legodnobaxvarzedargethannechanzadaxlegothaclaxtoh";
    out << "zingladaxtolyntavaxlontaganzvdnachachefnadolzinta";
    out << "plaxolzadartonzaclaxtontagenzadnochazvblahzednoch";
    out << "chadnacladnochapnaxooxadpraxodvantantaxalgradaxoh";
    out << "oldantaxalgethonzachandaxadnochanzadalzintaglaxoh";
    out << "plaxtarvadaxtoloxadnochadnolgentagnochaphzantagno";
    out << "zontalzocharzadnochaphvartaxlechastoraxintaponaco";
    out << "aplaxdahnotaladraxlogapnagenargethaxalgranochanza";
    out << "natordacladvaxorgethadolphantontacladnophelmaxola";
    out << "demigodahnotabavirgolaxtatvtaphanamichoramvantrag";
    out << "olchadnoxadahvoltaxahproclaxtontagenzintagnachaxo";
    out << "aplataxtontanaxpladaxvarontantontantontaponzvbban";
    out << "ordexannalodvagehaproxdaladadgnogahalzadnabzintox";
    out << "cladnovaldacharchadnopangendovahanzinodaxalglahvt";
    out << "apraxtontanoxvolgentaxnocladoxtantoxalgraxtograda";
    out << "nabvlatgehodaxtamalvonchephadonzadoxalgraxpracosa";
    out << "notemparachadnogansednoclanzadaxorchavornexothaxo";
    return out;
}

QList< QList<QString> > table15() {
    QList< QList<QString> > out;
    out << table15a();
    out << table15b();
    return out;
}

#endif // TABLE15_H
