#ifndef TABLE40_H
#define TABLE40_H
#include <QList>

QList<QString> table40a(){
    QList<QString> out;
    out << "ab";
    out << "ramo";
    out << "conzac";
    out << "chadoxna";
    out << "vidubracha";
    out << "ienofronaxal";
    out << "pvlorgedroxnep";
    out << "raxupranchaldunp";
    out << "lichordunziromacha";
    out << "purchadoxvngrapgaxor";
    out << "napvrathroxvlchermonoh";
    out << "sandephnagalbalaxvrgemph";
    out << "nathrodlanvanchanzachchvdo";
    out << "vaxupnlapruncodrundepoldrolp";
    out << "vircorzichlabaranphalcluxtorga";
    out << "vinzubronuxapluxtafnogaladcoxcor";
    out << "crocodronurzepvndemnagolaxurgbetho";
    out << "ramfgadnodraxdrondaxadnapolnemoroxoh";
    out << "vidurgulnoxupvorgephaltaxnorbarmaranph";
    out << "chiodraczircoltaivdnarayreminaratatgrolp";
    out << "vorchomzichannaphalhadchapzirdraxalnxrotha";
    out << "norbrastoxomeronhvdbrantantoxupvarvalapgvhnr";
    out << "aoitanaplidnnzicholgednogalhidraxnopagulsagrax";
    out << "vicholzachlapvrachonganzichlodroxlebvrnachanpunx";
    out << "lodvruntomphchirnophageldonchanzachalnolduxviroh";
    out << "taxumaroxvidracoxleplugrunhoxurglechastornapvr";
    out << "uvomranphzidcothacranzaeholdontuxomaroxphiln";
    out << "deodrugruphaxargolphlvrachaxumarothgednoph";
    out << "chilonttagnavaraxcarnaduxyrnaphonacholga";
    out << "cronzibanoxtuldoxnarvanchremonchadraxc";
    out << "phiantagnacluxzidnorbavarnoxtaxoldun";
    out << "rinzithmaglanchoulphladurmenpharra";
    out << "deoschellardrunpharnobadrunphilg";
    out << "urchochacluncoxnabalaxvrnodduo";
    out << "gehodnoxapvarnacolzidnoxurna";
    out << "deoxumvugluxnabvranzithcha";
    out << "toraphornophaxclunzachar";
    out << "virgethaxurbraduxolgar";
    out << "neocraxphinapulonrax";
    out << "renzachlochastorac";
    out << "vrnochchamaxongo";
    out << "nepvarchaxarch";
    out << "letharglogal";
    out << "liodranpha";
    out << "chinoyla";
    out << "arzich";
    out << "zvru";
    out << "le";
    return out;
}

QList<QString> table40b(){
    QList<QString> out;
    out << "x";
    out << "orc";
    out << "alpha";
    out << "gruncho";
    out << "lapvramax";
    out << "targamalpha";
    out << "lvraxzimnacho";
    out << "piraphalgdrotha";
    out << "nolviodarnachanor";
    out << "chadnorgethclonzidb";
    out << "arnoxtaxarbrachlochar";
    out << "voronocaxzichlothangalg";
    out << "arphranchaligvluxtorhorga";
    out << "libomarongendraxnolzidnapha";
    out << "alzidnocholgehalphalduxornava";
    out << "tentoxlapvrgataxurbarodvornetha";
    out << "chvdornalopthaxornduchlagesarnoxt";
    out << "vioturbachanzachvldrothardroxnarong";
    out << "chibadluxcartantleopugethnolhidaxalga";
    out << "leomvagelhidroxapalchatoxamaronvonroxno";
    out << "umlabarbvdnoxurnotheogradoxlingranvornaps";
    out << "vnchantprathaltochoruschanpvranphanolchasto";
    out << "adivrnaruphdeograxlapronchchinbaxlapvraphrads";
    out << "gerodoxulnolbaxumarontantoxavolnathtionaxtalged";
    out << "ardonchaproxanchnatapluxzilchompudrothalzinocoxar";
    out << "vidrabonganzichlorantogluxhixaramproncharoxtalg";
    out << "garzardudnochaloxvionaphaxynrothzidromphaxarg";
    out << "nomiorantotapnaphaphvrcoxliscastorgremancha";
    out << "loplodnoxubvornepraxyxurchalploxaronchalp";
    out << "ladoxnopraxydnapvichastorgplichadnavarg";
    out << "temocronpluxotaxamaplepothnarvaxphiol";
    out << "arvengolampuxonacchirvornadnothdaxo";
    out << "pricladunzabrantoxalbluxnorgephar";
    out << "nvradonpocholbnocapocarbaxalath";
    out << "nevramaxchidophargethnoglaxox";
    out << "leontaphicholhaxardropranch";
    out << "lirnophgebvargaxornopolph";
    out << "zimnorudvontopluxchindo";
    out << "pucharvanlochastorgal";
    out << "navambaramphzichast";
    out << "lexurnagrampulget";
    out << "vixarbalpvdagra";
    out << "remagephulcha";
    out << "naxorbrocho";
    out << "virnongax";
    out << "ardrpon";
    out << "demon";
    out << "gat";
    out << "v";
    return out;
}

QList< QList<QString> > table40() {
    QList< QList<QString> > out;
    out << table40a();
    out << table40b();
    return out;
}

#endif // TABLE40_H
