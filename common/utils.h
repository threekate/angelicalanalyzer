#ifndef UTILS_H
#define UTILS_H

#include <QtAlgorithms>
#include <QString>
#include <QList>
#include <QMap>
#include <cstdlib>
#include "tables.h"
QList< QList<QString> > tableData()
{
    QList< QList<QString> > out;
    out << table1();
    out << table2();
    out << table3();
    out << table4();
    out << table5();
    out << table6();
    out << table7();
    out << table8();
    out << table9();
    out << table10();
    out << table11();
    out << table12();
    out << table13();
    out << table14();
    out << table15();
    out << table16();
    out << table17();
    out << table18();
    out << table19();
    out << table20();
    out << table21();
    out << table22();
    out << table23();
    out << table24();
    out << table25();
    out << table26();
    out << table27();
    out << table28();
    out << table29();
    out << table30();
    out << table31();
    out << table32();
    out << table33();
    out << table34();
    out << table35();
    out << table36();
    out << table37();
    out << table38();
    out << table39();
    out << table40();
    out << table41();
    out << table42();
    out << table43();
    out << table44();
    out << table45();
    out << table46();
    out << table47();
    out << table48();
    out << table49();
    return out;
}

int computeWordScore( int length, int loagaethFrequency, int callsFrequency )
{
    return (length*1.5)+(loagaethFrequency*1.75)+(callsFrequency*1.85);
}

QList<QChar> letters()
{
    QList<QChar> out;
    out << 'b' << 'c' << 'g' << 'd' << 'f' << 'a' << 'e' << 'm' << 'i' << 'h' << 'l' << 'p' << 'q' << 'n' << 'x' << 'o' << 'r' << 'z' << 'u' << 's' << 't';
    return out;
}

QString tableName( int table )
{
    if(table == 0)
        return "1A";
    else if(table == 1)
        return "1B";
    else if(table == 97)
        return "Table Titles";
    if( table%2 )
        return QString("%1B").arg((table/2)+1);
    return QString("%1A").arg((table/2)+1);
}

QString combinationName( const QString & combination )
{
    QString out(combination);
    out.replace(" ", "_");
    return out;
}

QString normalize( const QString & word )
{
    QString out(word.toLower());
    out.replace('j', 'g');
    out.replace('k', 'c');
    out.replace('y', 'i');
    out.replace('v', 'u');
    out.replace("á","a");
    out.replace("ä","a");
    out.replace("â","a");
    out.replace("à","a");
    out.replace("é","e");
    out.replace("ë","e");
    out.replace("ê","e");
    out.replace("è","e");
    out.replace("í","i");
    out.replace("ï","i");
    out.replace("î","i");
    out.replace("ì","i");
    out.replace("ó","o");
    out.replace("ö","o");
    out.replace("ô","o");
    out.replace("ò","o");
    out.replace("ú","u");
    out.replace("ü","u");
    out.replace("û","u");
    out.replace("ù","u");
    out.replace("ý","y");
    out.replace("ÿ","y");
    out.replace("-","");
    return out;
}

double random( double min, double max )
{
    return (static_cast<double>(qrand()%RAND_MAX)/static_cast<double>(RAND_MAX))*(max-min)+min;
}

QMap<QChar, double> letterDoubleMappings()
{
    QMap<QChar, double> out;
    QList<QChar> l = letters();
    l.append(' ');
    for(int i=0;i<l.count();++i) {
        out.insert(l.at(i),(i*(3-1)/static_cast<double>(l.count()-1))-1);
    }
    return out;
}

double letterToDouble( QChar letter )
{
    return letterDoubleMappings().value(letter);
}

QChar doubleToLetter( double value )
{
    QList<double> values = letterDoubleMappings().values();
    qSort(values);
    foreach( double val, values) {
        if(value < val)
            return letterDoubleMappings().key(val);
    }
    return ' ';
}

QList<double> stringToDoubles( const QString & string )
{
    QList<double> out;
    foreach( QChar ch, string ) {
        out << letterToDouble(ch);
    }
    return out;
}

QString doublesToString( const QList<double> & list )
{
    QString out;
    foreach( double value, list ) {
        out.append(doubleToLetter(value));
    }
    return out;
}

#endif // UTILS_H
