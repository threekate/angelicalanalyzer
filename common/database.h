#ifndef DATABASE_H
#define DATABASE_H

#include <QObject>
#include <QString>

class Database : public QObject
{
    Q_OBJECT
public:
    enum Location {
        Loagaeth = 0x01,
        Calls = 0x02
    };
    Q_FLAGS(Location)
    Q_DECLARE_FLAGS(Locations,Location)

    Database();

    QList<QString> uniqueLetterCombinations( Locations locations, int number = -1 );

public slots:
    bool setFilename( const QString & filename );
    void rebild();
    void clear();
    void createStructure();
    void populateAllTables();
    void populateCombinations();
    void populateScores();

signals:
    void error( const QString & message );
    void status( const QString & message );
    void progress( int percent );
    void databaseOpened();

private:
    QString m_filename;
};

#endif // DATABASE_H
