#-------------------------------------------------
#
# Project created by QtCreator 2014-04-18T13:58:58
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = imageAnalyzer
TEMPLATE = app

INCLUDEPATH += ../common ../common/tables

SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h \
    ../common/tables.h \
    ../common/utils.h

FORMS    += mainwindow.ui

OTHER_FILES += \
    ../common/loagaeth/AOM-001/titles.txt \
    ../common/loagaeth/AOM-001/table01a.txt \
    ../common/loagaeth/AOM-001/table01b.txt \
    ../common/loagaeth/AOM-001/table02a.txt \
    ../common/loagaeth/AOM-001/table02b.txt \
    ../common/loagaeth/AOM-001/table03a.txt \
    ../common/loagaeth/AOM-001/table03b.txt \
    ../common/loagaeth/AOM-001/table04a.txt \
    ../common/loagaeth/AOM-001/table04b.txt \
    ../common/loagaeth/AOM-001/table05a.txt \
    ../common/loagaeth/AOM-001/table05b.txt \
    ../common/loagaeth/AOM-001/table06a.txt \
    ../common/loagaeth/AOM-001/table06b.txt \
    ../common/loagaeth/AOM-001/table07a.txt \
    ../common/loagaeth/AOM-001/table07b.txt \
    ../common/loagaeth/AOM-001/table08a.txt \
    ../common/loagaeth/AOM-001/table08b.txt \
    ../common/loagaeth/AOM-001/table09a.txt \
    ../common/loagaeth/AOM-001/table09b.txt \
    ../common/loagaeth/AOM-001/table10a.txt \
    ../common/loagaeth/AOM-001/table10b.txt \
    ../common/loagaeth/AOM-001/table11a.txt \
    ../common/loagaeth/AOM-001/table11b.txt \
    ../common/loagaeth/AOM-001/table12a.txt \
    ../common/loagaeth/AOM-001/table12b.txt \
    ../common/loagaeth/AOM-001/table13a.txt \
    ../common/loagaeth/AOM-001/table13b.txt \
    ../common/loagaeth/AOM-001/table14a.txt \
    ../common/loagaeth/AOM-001/table14b.txt \
    ../common/loagaeth/AOM-001/table15a.txt \
    ../common/loagaeth/AOM-001/table15b.txt \
    ../common/loagaeth/AOM-001/table16a.txt \
    ../common/loagaeth/AOM-001/table16b.txt \
    ../common/loagaeth/AOM-001/table17a.txt \
    ../common/loagaeth/AOM-001/table17b.txt \
    ../common/loagaeth/AOM-001/table18a.txt \
    ../common/loagaeth/AOM-001/table18b.txt \
    ../common/loagaeth/AOM-001/table19a.txt \
    ../common/loagaeth/AOM-001/table19b.txt \
    ../common/loagaeth/AOM-001/table20a.txt \
    ../common/loagaeth/AOM-001/table20b.txt \
    ../common/loagaeth/AOM-001/table21a.txt \
    ../common/loagaeth/AOM-001/table21b.txt \
    ../common/loagaeth/AOM-001/table22a.txt \
    ../common/loagaeth/AOM-001/table22b.txt \
    ../common/loagaeth/AOM-001/table23a.txt \
    ../common/loagaeth/AOM-001/table23b.txt \
    ../common/loagaeth/AOM-001/table24a.txt \
    ../common/loagaeth/AOM-001/table24b.txt \
    ../common/loagaeth/AOM-001/table25a.txt \
    ../common/loagaeth/AOM-001/table25b.txt \
    ../common/loagaeth/AOM-001/table26a.txt \
    ../common/loagaeth/AOM-001/table26b.txt \
    ../common/loagaeth/AOM-001/table27a.txt \
    ../common/loagaeth/AOM-001/table27b.txt \
    ../common/loagaeth/AOM-001/table28a.txt \
    ../common/loagaeth/AOM-001/table28b.txt \
    ../common/loagaeth/AOM-001/table29a.txt \
    ../common/loagaeth/AOM-001/table29b.txt \
    ../common/loagaeth/AOM-001/table30a.txt \
    ../common/loagaeth/AOM-001/table30b.txt \
    ../common/loagaeth/AOM-001/table31a.txt \
    ../common/loagaeth/AOM-001/table31b.txt \
    ../common/loagaeth/AOM-001/table32a.txt \
    ../common/loagaeth/AOM-001/table32b.txt \
    ../common/loagaeth/AOM-001/table33a.txt \
    ../common/loagaeth/AOM-001/table33b.txt \
    ../common/loagaeth/AOM-001/table34a.txt \
    ../common/loagaeth/AOM-001/table34b.txt \
    ../common/loagaeth/AOM-001/table35a.txt \
    ../common/loagaeth/AOM-001/table35b.txt \
    ../common/loagaeth/AOM-001/table36a.txt \
    ../common/loagaeth/AOM-001/table36b.txt \
    ../common/loagaeth/AOM-001/table37a.txt \
    ../common/loagaeth/AOM-001/table37b.txt \
    ../common/loagaeth/AOM-001/table38a.txt \
    ../common/loagaeth/AOM-001/table38b.txt \
    ../common/loagaeth/AOM-001/table39a.txt \
    ../common/loagaeth/AOM-001/table39b.txt \
    ../common/loagaeth/AOM-001/table40a.txt \
    ../common/loagaeth/AOM-001/table40b.txt \
    ../common/loagaeth/AOM-001/table41a.txt \
    ../common/loagaeth/AOM-001/table41b.txt \
    ../common/loagaeth/AOM-001/table42a.txt \
    ../common/loagaeth/AOM-001/table42b.txt \
    ../common/loagaeth/AOM-001/table43a.txt \
    ../common/loagaeth/AOM-001/table43b.txt \
    ../common/loagaeth/AOM-001/table44a.txt \
    ../common/loagaeth/AOM-001/table44b.txt \
    ../common/loagaeth/AOM-001/table45a.txt \
    ../common/loagaeth/AOM-001/table45b.txt \
    ../common/loagaeth/AOM-001/table46a.txt \
    ../common/loagaeth/AOM-001/table46b.txt \
    ../common/loagaeth/AOM-001/table47a.txt \
    ../common/loagaeth/AOM-001/table47b.txt \
    ../common/loagaeth/AOM-001/table48a.txt \
    ../common/loagaeth/AOM-001/table48b.txt \
    ../common/loagaeth/AOM-001/table49.txt
