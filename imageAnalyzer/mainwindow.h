#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
class QCheckBox;
class QComboBox;
class QSpinBox;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void updateImage();
    void randomizeColors();
    void randomizeTables();
    void randomizeOpacities();
    void randomizeAll();

    void setAllColors();
    void clearTables();
    void selectAllTables();

    void saveImage();
    void generateAndSaveImages();


private:
    QList<QCheckBox *> tableCheckBoxes() const;
    QList<QComboBox *> colorComboBoxes() const;
    QList<QSpinBox *> alphaSpinBoxes() const;
    void setColors( const QList<QComboBox *> & comboBoxes );
    QList<QString> colors() const;

    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
