#include <QPainter>
#include <QInputDialog>
#include <ctime>
#include "utils.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    qsrand(std::time(0));
    ui->setupUi(this);
    setColors(colorComboBoxes());
    randomizeColors();
    updateImage();

    foreach(QComboBox * box, colorComboBoxes()) {
        connect(box,SIGNAL(currentIndexChanged(int)),this,SLOT(updateImage()));
    }

    foreach(QSpinBox * box, alphaSpinBoxes()) {
        connect(box,SIGNAL(valueChanged(int)),this,SLOT(updateImage()));
    }

    foreach(QCheckBox * box, tableCheckBoxes()) {
        connect(box,SIGNAL(toggled(bool)),this,SLOT(updateImage()));
    }

    connect(ui->actionRandomize,SIGNAL(triggered()),this,SLOT(randomizeColors()));
    connect(ui->actionRandomize_2,SIGNAL(triggered()),this,SLOT(randomizeOpacities()));
    connect(ui->actionRandomize_3,SIGNAL(triggered()),this,SLOT(randomizeTables()));
    connect(ui->randomizeAllButton,SIGNAL(clicked()),this,SLOT(randomizeAll()));

    connect(ui->actionClear_selection,SIGNAL(triggered()),this,SLOT(clearTables()));
    connect(ui->actionSelect_all,SIGNAL(triggered()),this,SLOT(selectAllTables()));
    connect(ui->actionSet_all,SIGNAL(triggered()),this,SLOT(setAllColors()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::updateImage()
{
    setCursor(Qt::WaitCursor);
    QImage image(49,49,QImage::Format_ARGB32);
    image.fill(Qt::white);

    QPainter p(&image);

    const QList< QList<QString> > tables(tableData());
    const QList<QCheckBox * > checkBoxes(tableCheckBoxes());

    int shown(0);
    foreach(QCheckBox * box, checkBoxes) {
        if(box->isChecked())
            ++shown;
    }
    float factor = qMin(0.9,((shown/static_cast<double>(tables.count()))));

    for(int tableIndex=0;tableIndex<tables.count()-1;++tableIndex) {
        const QList<QString> table(tables.at(tableIndex));
        if(!checkBoxes.at(tableIndex)->isChecked())
            continue;
        for(int row=0;row<table.count();++row) {
            QString rowData(normalize(table.at(row)));
            while(rowData.count()<49) {
                rowData.append(' ');
                rowData.prepend(' ');
            }
            for(int column=0;column<rowData.count();++column) {
                switch(rowData.at(column).toLatin1()) {
                    case 'b':
                        {
                            QColor color(ui->colorB->currentData().value<QColor>());
                            color.setAlpha((ui->alphaB->value()*factor));
                            p.setPen(QPen(color));
                        }
                        break;
                    case 'c':
                        {
                            QColor color(ui->colorCK->currentData().value<QColor>());
                            color.setAlpha((ui->alphaCK->value()*factor));
                            p.setPen(QPen(color));
                        }
                        break;
                    case 'g':
                        {
                            QColor color(ui->colorGJ->currentData().value<QColor>());
                            color.setAlpha((ui->alphaGJ->value()*factor));
                            p.setPen(QPen(color));
                        }
                        break;
                    case 'd':
                        {
                            QColor color(ui->colorD->currentData().value<QColor>());
                            color.setAlpha((ui->alphaD->value()*factor));
                            p.setPen(QPen(color));
                        }
                        break;
                    case 'f':
                        {
                            QColor color(ui->colorF->currentData().value<QColor>());
                            color.setAlpha((ui->alphaF->value()*factor));
                            p.setPen(QPen(color));
                        }
                        break;
                    case 'a':
                        {
                            QColor color(ui->colorA->currentData().value<QColor>());
                            color.setAlpha((ui->alphaA->value()*factor));
                            p.setPen(QPen(color));
                        }
                        break;
                    case 'e':
                        {
                            QColor color(ui->colorE->currentData().value<QColor>());
                            color.setAlpha((ui->alphaE->value()*factor));
                            p.setPen(QPen(color));
                        }
                        break;
                    case 'm':
                        {
                            QColor color(ui->colorM->currentData().value<QColor>());
                            color.setAlpha((ui->alphaM->value()*factor));
                            p.setPen(QPen(color));
                        }
                        break;
                    case 'i':
                        {
                            QColor color(ui->colorIY->currentData().value<QColor>());
                            color.setAlpha((ui->alphaIY->value()*factor));
                            p.setPen(QPen(color));
                        }
                        break;
                    case 'h':
                        {
                            QColor color(ui->colorH->currentData().value<QColor>());
                            color.setAlpha((ui->alphaH->value()*factor));
                            p.setPen(QPen(color));
                        }
                        break;
                    case 'l':
                        {
                            QColor color(ui->colorL->currentData().value<QColor>());
                            color.setAlpha((ui->alphaL->value()*factor));
                            p.setPen(QPen(color));
                        }
                        break;
                    case 'p':
                        {
                            QColor color(ui->colorP->currentData().value<QColor>());
                            color.setAlpha((ui->alphaP->value()*factor));
                            p.setPen(QPen(color));
                        }
                        break;
                    case 'q':
                        {
                            QColor color(ui->colorQ->currentData().value<QColor>());
                            color.setAlpha((ui->alphaQ->value()*factor));
                            p.setPen(QPen(color));
                        }
                        break;
                    case 'n':
                        {
                            QColor color(ui->colorN->currentData().value<QColor>());
                            color.setAlpha((ui->alphaN->value()*factor));
                            p.setPen(QPen(color));
                        }
                        break;
                    case 'x':
                        {
                            QColor color(ui->colorX->currentData().value<QColor>());
                            color.setAlpha((ui->alphaX->value()*factor));
                            p.setPen(QPen(color));
                        }
                        break;
                    case 'o':
                        {
                            QColor color(ui->colorO->currentData().value<QColor>());
                            color.setAlpha((ui->alphaO->value()*factor));
                            p.setPen(QPen(color));
                        }
                        break;
                    case 'r':
                        {
                            QColor color(ui->colorR->currentData().value<QColor>());
                            color.setAlpha((ui->alphaR->value()*factor));
                            p.setPen(QPen(color));
                        }
                        break;
                    case 'z':
                        {
    /*TODO                        QColor color(ui->colorZ->currentData().value<QColor>());
                            color.setAlpha((ui->alphaZ->value()*factor));
                            p.setPen(QPen(color));*/
                            QColor color(Qt::transparent);
                            p.setPen(QPen(color));

                        }
                        break;
                    case 'u':
                        {
                            QColor color(ui->colorUV->currentData().value<QColor>());
                            color.setAlpha((ui->alphaUV->value()*factor));
                            p.setPen(QPen(color));
                        }
                        break;
                    case 's':
                        {
                            QColor color(ui->colorS->currentData().value<QColor>());
                            color.setAlpha((ui->alphaS->value()*factor));
                            p.setPen(QPen(color));
                        }
                        break;
                    case 't':
                        {
                            QColor color(ui->colorT->currentData().value<QColor>());
                            color.setAlpha((ui->alphaT->value()*factor));
                            p.setPen(QPen(color));
                        }
                        break;
                    default:
                        {
                            QColor color(Qt::transparent);
                            p.setPen(QPen(color));
                        }
                }
                if(p.pen().color() != Qt::transparent)
                    p.drawPoint(column,row);
            }
        }
    }
    ui->image->setPixmap(QPixmap::fromImage(image));
    setCursor(Qt::ArrowCursor);
}

void MainWindow::randomizeColors()
{
    const QList<QComboBox *> comboBoxes(colorComboBoxes());
    foreach(QComboBox * box, comboBoxes) {
        disconnect(box,SIGNAL(currentIndexChanged(int)),this,SLOT(updateImage()));
        box->setCurrentIndex(random(0,box->count()));
        connect(box,SIGNAL(currentIndexChanged(int)),this,SLOT(updateImage()));
    }
    updateImage();
}

void MainWindow::randomizeTables()
{
    const QList<QCheckBox *> checkBoxes(tableCheckBoxes());
    foreach(QCheckBox * box, checkBoxes) {
        disconnect(box,SIGNAL(toggled(bool)),this,SLOT(updateImage()));
        box->setChecked(random(1,10)>6);
        connect(box,SIGNAL(toggled(bool)),this,SLOT(updateImage()));
    }
    updateImage();
}

void MainWindow::randomizeOpacities()
{
    const QList<QSpinBox *> spinBoxes(alphaSpinBoxes());
    foreach(QSpinBox * box, spinBoxes) {
        disconnect(box,SIGNAL(valueChanged(int)),this,SLOT(updateImage()));
        box->setValue(random(0,255));
        connect(box,SIGNAL(valueChanged(int)),this,SLOT(updateImage()));
    }
    updateImage();
}

void MainWindow::randomizeAll()
{
    const QList<QComboBox *> comboBoxes(colorComboBoxes());
    foreach(QComboBox * box, comboBoxes) {
        disconnect(box,SIGNAL(currentIndexChanged(int)),this,SLOT(updateImage()));
        box->setCurrentIndex(random(0,box->count()));
        connect(box,SIGNAL(currentIndexChanged(int)),this,SLOT(updateImage()));
    }
    const QList<QCheckBox *> checkBoxes(tableCheckBoxes());
    foreach(QCheckBox * box, checkBoxes) {
        disconnect(box,SIGNAL(toggled(bool)),this,SLOT(updateImage()));
        box->setChecked(random(1,10)>=5);
        connect(box,SIGNAL(toggled(bool)),this,SLOT(updateImage()));
    }
    const QList<QSpinBox *> spinBoxes(alphaSpinBoxes());
    foreach(QSpinBox * box, spinBoxes) {
        disconnect(box,SIGNAL(valueChanged(int)),this,SLOT(updateImage()));
        box->setValue(random(0,255));
        connect(box,SIGNAL(valueChanged(int)),this,SLOT(updateImage()));
    }
    updateImage();
    ui->randomizeAllButton->setFocus();
}

void MainWindow::setAllColors()
{
    QList<QString> c(colors());
    c.append("Transparent");
    QString color = QInputDialog::getItem(this,"Choose color","Color",c,0,false);

    const QList<QComboBox *> comboBoxes(colorComboBoxes());
    foreach(QComboBox * box, comboBoxes) {
        disconnect(box,SIGNAL(currentIndexChanged(int)),this,SLOT(updateImage()));
        box->setCurrentText(color);
        connect(box,SIGNAL(currentIndexChanged(int)),this,SLOT(updateImage()));
    }
    updateImage();
}

void MainWindow::clearTables()
{
    const QList<QCheckBox *> checkBoxes(tableCheckBoxes());
    foreach(QCheckBox * box, checkBoxes) {
        disconnect(box,SIGNAL(toggled(bool)),this,SLOT(updateImage()));
        box->setChecked(false);
        connect(box,SIGNAL(toggled(bool)),this,SLOT(updateImage()));
    }
    updateImage();
}

void MainWindow::selectAllTables()
{
    const QList<QCheckBox *> checkBoxes(tableCheckBoxes());
    foreach(QCheckBox * box, checkBoxes) {
        disconnect(box,SIGNAL(toggled(bool)),this,SLOT(updateImage()));
        box->setChecked(true);
        connect(box,SIGNAL(toggled(bool)),this,SLOT(updateImage()));
    }
    updateImage();
}

void MainWindow::saveImage()
{

}

void MainWindow::generateAndSaveImages()
{

}

QList<QCheckBox *> MainWindow::tableCheckBoxes() const
{
    QList<QCheckBox *> out;
    out << ui->table1A << ui->table1B << ui->table2A << ui->table2B << ui->table3A << ui->table3B;
    out << ui->table4A << ui->table4B << ui->table5A << ui->table5B << ui->table6A << ui->table6B;
    out << ui->table7A << ui->table7B << ui->table8A << ui->table8B << ui->table9A << ui->table9B;
    out << ui->table10A << ui->table10B << ui->table11A << ui->table11B << ui->table12A << ui->table12B;
    out << ui->table13A << ui->table13B << ui->table14A << ui->table14B << ui->table15A << ui->table15B;
    out << ui->table16A << ui->table16B << ui->table17A << ui->table17B << ui->table18A << ui->table18B;
    out << ui->table19A << ui->table19B << ui->table20A << ui->table20B << ui->table21A << ui->table21B;
    out << ui->table22A << ui->table22B << ui->table23A << ui->table23B << ui->table24A << ui->table24B;
    out << ui->table25A << ui->table25B << ui->table26A << ui->table26B << ui->table27A << ui->table27B;
    out << ui->table28A << ui->table28B << ui->table29A << ui->table29B << ui->table30A << ui->table30B;
    out << ui->table31A << ui->table31B << ui->table32A << ui->table32B << ui->table33A << ui->table33B;
    out << ui->table34A << ui->table34B << ui->table35A << ui->table35B << ui->table36A << ui->table36B;
    out << ui->table37A << ui->table37B << ui->table38A << ui->table38B << ui->table39A << ui->table39B;
    out << ui->table40A << ui->table40B << ui->table41A << ui->table41B << ui->table42A << ui->table42B;
    out << ui->table43A << ui->table43B << ui->table44A << ui->table44B << ui->table45A << ui->table45B;
    out << ui->table46A << ui->table46B << ui->table47A << ui->table47B << ui->table48A << ui->table48B;
    out << ui->table49;
    return out;
}

QList<QComboBox *> MainWindow::colorComboBoxes() const
{
    //TODO add Z
    QList<QComboBox *> out;
    out << ui->colorB << ui->colorCK << ui->colorGJ << ui->colorD << ui->colorF << ui->colorA << ui->colorE;
    out << ui->colorM << ui->colorIY << ui->colorH << ui->colorL << ui->colorP << ui->colorQ << ui->colorN;
    out << ui->colorX << ui->colorO << ui->colorR << /*ui->colorZ <<*/ ui->colorUV << ui->colorS << ui->colorT;
    return out;
}

QList<QSpinBox *> MainWindow::alphaSpinBoxes() const
{
    //TODO add Z
    QList<QSpinBox *> out;
    out << ui->alphaB << ui->alphaCK << ui->alphaGJ << ui->alphaD << ui->alphaF << ui->alphaA << ui->alphaE;
    out << ui->alphaM << ui->alphaIY << ui->alphaH << ui->alphaL << ui->alphaP << ui->alphaQ << ui->alphaN;
    out << ui->alphaX << ui->alphaO << ui->alphaR << /*ui->alphaZ <<*/ ui->alphaUV << ui->alphaS << ui->alphaT;
    return out;
}

void MainWindow::setColors(const QList<QComboBox *> &comboBoxes)
{
    QList<QString> c(colors());
    foreach(QComboBox * box, comboBoxes) {
        foreach(QString color,c) {
            box->addItem(color,QColor(color));
        }
        box->addItem("Transparent",QColor(Qt::transparent));
        connect(box,SIGNAL(currentIndexChanged(int)),this,SLOT(updateImage()));
    }
}

QList<QString> MainWindow::colors() const
{
    QList<QString> out;
    out << "aliceblue" << "antiquewhite" << "aqua" << "aquamarine";
    out << "azure" << "beige" << "bisque" << "black" << "blanchedalmond";
    out << "blue" << "blueviolet" << "brown" << "burlywood";
    out << "cadetblue" << "chartreuse" << "chocolate";
    out << "coral" << "cornflowerblue" << "cornsilk" << "crimson";
    out << "cyan" << "darkblue" << "darkcyan" << "darkgoldenrod";
    out << "darkgray" << "darkgreen" << "darkgrey" << "darkkhaki";
    out << "darkmagenta" << "darkolivegreen" << "darkorange" << "darkorchid";
    out << "darkred" << "darksalmon" << "darkseagreen" << "darkslateblue";
    out << "darkslategray" << "darkslategrey" << "darkturquoise" << "darkviolet";
    out << "deeppink" << "deepskyblue" << "dimgray" << "dimgrey";
    out << "dodgerblue" << "firebrick" << "floralwhite" << "forestgreen";
    out << "fuchsia" << "gainsboro" << "ghostwhite" << "gold" << "goldenrod";
    out << "gray" << "green" << "greenyellow" << "grey" << "honeydew" << "hotpink";
    out << "indianred" << "indigo" << "ivory" << "khaki" << "lavender";
    out << "lavenderblush" << "lawngreen" << "lemonchiffon" << "lightblue";
    out << "lightcoral" << "lightcyan" << "lightgoldenrodyellow" << "lightgray";
    out << "lightgreen" << "lightgrey" << "lightpink" << "lightsalmon";
    out << "lightseagreen" << "lightskyblue" << "lightslategray" << "lightslategrey";
    out << "lightsteelblue" << "lightyellow" << "lime" << "limegreen";
    out << "linen" << "magenta" << "maroon" << "mediumaquamarine" << "mediumblue";
    out << "mediumorchid" << "mediumpurple" << "mediumseagreen" << "mediumslateblue";
    out << "mediumspringgreen" << "mediumturquoise" << "mediumvioletred";
    out << "midnightblue" << "mintcream" << "mistyrose" << "moccasin";
    out << "navajowhite" << "navy" << "oldlace" << "olive" << "olivedrab";
    out << "orange" << "orangered" << "orchid" << "palegoldenrod" << "palegreen";
    out << "paleturquoise" << "palevioletred" << "papayawhip" << "peachpuff";
    out << "peru" << "pink" << "plum" << "powderblue" << "purple" << "red";
    out << "rosybrown" << "royalblue" << "saddlebrown" << "salmon";
    out << "sandybrown" << "seagreen" << "seashell" << "sienna" << "silver";
    out << "skyblue" << "slateblue" << "slategray" << "slategrey" << "snow";
    out << "springgreen" << "steelblue" << "tan" << "teal" << "thistle" << "tomato";
    out << "turquoise" << "violet" << "wheat" << "white" << "whitesmoke";
    out << "yellow" << "yellowgreen";
    return out;
}
