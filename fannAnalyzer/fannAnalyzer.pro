#-------------------------------------------------
#
# Project created by QtCreator 2014-04-10T19:13:06
#
#-------------------------------------------------

QT       += core sql

QT       -= gui

LIBS += -lfann

TARGET = fannAnalyzer
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

INCLUDEPATH += ../common ../common/tables

osx {
    INCLUDEPATH += fannheaders
    LIBS += -L/usr/local/lib
}

HEADERS += ../common/utils.h \
    ../common/lexicon.h \
    ../common/database.h

SOURCES += main.cpp \
    ../common/database.cpp
