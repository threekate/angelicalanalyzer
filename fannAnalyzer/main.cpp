#include <QDebug>
#include <QFile>
#include <QStringList>
#include <fann.h>
#include <cstdlib>
#include <ctime>
#include "utils.h"
#include "lexicon.h"
#include "database.h"

typedef QPair<QString, QString> TrainingPair;

int main(int argc, char *argv[])
{
    srand(std::time(0));
    if(argc != 2)
        qFatal("create, train, or run?");

    if(qstrcmp(argv[1],"run")==0) {

        fann_type * calc_out;
        fann_type input[14];

        struct fann * ann = fann_create_from_file("angelical.net");
        QList<angelicalWord> words = wordList();
        QList<QString> fourteenLetterWords;
        foreach( angelicalWord word, words ) {
            if(word.word.count() <= 14) {
                fourteenLetterWords << word.word.leftJustified(14,' ');
            }
        }
        int correct(0);
        int wordCount(fourteenLetterWords.count());
        foreach( QString word, fourteenLetterWords ) {
            QList<double> dbls = stringToDoubles(word);
            input[0] = dbls.at(0);
            input[1] = dbls.at(1);
            input[2] = dbls.at(2);
            input[3] = dbls.at(3);
            input[4] = dbls.at(4);
            input[5] = dbls.at(5);
            input[6] = dbls.at(6);
            input[7] = dbls.at(7);
            input[8] = dbls.at(8);
            input[9] = dbls.at(9);
            input[10] = dbls.at(10);
            input[11] = dbls.at(11);
            input[12] = dbls.at(12);
            input[13] = dbls.at(13);

            calc_out = fann_run( ann, input );
            if(calc_out[0] > 0.0)
                ++correct;
            qDebug() << wordCount << correct << correct/static_cast<double>(wordCount);
        }
        Database db;
        db.setFilename("combinations.sqlite");
        QStringList loagethCombos = db.uniqueLetterCombinations(Database::Loagaeth);
        for(int i=0;i<loagethCombos.count();++i) {
            loagethCombos[i] = loagethCombos.at(i).leftJustified(14,0);
        }

        for(int i=0;i<5000;++i) {
            QList<QChar> l = letters();
            l.append(' ');
            QString str;
            for(int j=0;j<14;++j) {
                str.append(l.at(random(0.0,l.count()-1)));
            }
            if((!fourteenLetterWords.contains(str)) && (!loagethCombos.contains(str))) {
                ++wordCount;

                QList<double> dbls = stringToDoubles(str);
                input[0] = dbls.at(0);
                input[1] = dbls.at(1);
                input[2] = dbls.at(2);
                input[3] = dbls.at(3);
                input[4] = dbls.at(4);
                input[5] = dbls.at(5);
                input[6] = dbls.at(6);
                input[7] = dbls.at(7);
                input[8] = dbls.at(8);
                input[9] = dbls.at(9);
                input[10] = dbls.at(10);
                input[11] = dbls.at(11);
                input[12] = dbls.at(12);
                input[13] = dbls.at(13);
                calc_out = fann_run( ann, input );
                if(calc_out[0] < 0.0)
                    ++correct;
                else
                    qDebug() << str;
                qDebug() << wordCount << correct << correct/static_cast<double>(wordCount);
            }
        }

        qDebug() << wordCount << correct << correct/static_cast<double>(wordCount);
        return 0;
    }

    if(qstrcmp(argv[1],"train")==0) {

        //struct fann * ann = fann_create_standard( 3, 4, 8, 1 );
        struct fann *ann = fann_create_shortcut(2, 4, 1);
        //fann_set_activation_function_hidden( ann, FANN_SIGMOID_SYMMETRIC );
        //fann_set_activation_function_output( ann, FANN_SIGMOID_SYMMETRIC );
        fann_set_activation_function_hidden(ann, FANN_SIGMOID_SYMMETRIC);
        fann_set_activation_function_output(ann, FANN_LINEAR);
        fann_set_train_error_function(ann, FANN_ERRORFUNC_LINEAR);
        //fann_set_learning_rate( ann, 0.05 );
        //fann_train_on_file( ann, "training.data", 20000, 200, 0.0 );
        fann_cascadetrain_on_file( ann, "training.data", 777, 1, 0.01 );
        fann_save( ann, "angelical.net" );
        fann_destroy( ann );
        return 0;
    }

    if(qstrcmp(argv[1],"create")==0) {
        QFile trainingData("training.data");
        if(!trainingData.open(QIODevice::WriteOnly))
            qFatal("Could not write to training data file.");

        QList<angelicalWord> words = wordList();
        QList<QString> fourteenLetterWords;
        QList<QString> notWords;
        for( int i=0;i<words.count();++i) {
            angelicalWord word = words.at(i);
            if(word.word.count() <= 14) {
                fourteenLetterWords << word.word.leftJustified(14,' ');
            }
            qDebug() << "known words:" << i/static_cast<double>(words.count());
        }
        qDebug() << "load database";
        Database db;
        db.setFilename("combinations.sqlite");
        QStringList loagethCombos = db.uniqueLetterCombinations(Database::Loagaeth);
        for(int i=0;i<loagethCombos.count();++i) {
            loagethCombos[i] = loagethCombos.at(i).leftJustified(14,0);
        }
        for(int i=0;i<5000;++i) {
            QList<QChar> l = letters();
            l.append(' ');
            QString str;
            for(int j=0;j<14;++j) {
                str.append(l.at(random(0.0,l.count()-1)));
            }
            if( (!notWords.contains(str)) && (!fourteenLetterWords.contains(str)) && (!loagethCombos.contains(str)))
                notWords << str;
            qDebug() << "random word generation:" << i/5000.0;
        }
        qDebug() << "writing...";
        trainingData.write(QByteArray::number(fourteenLetterWords.count()+notWords.count())+" 4 1\n");
        foreach( QString word, fourteenLetterWords ) {
            QList<double> dbls = stringToDoubles(word);
            for(int i=0;i<dbls.count();++i) {
                trainingData.write(QByteArray::number(dbls.at(i),'g',15)+" ");
            }
            trainingData.write("\n1.0\n");
        }

        foreach(QString str, notWords) {
            QList<double> dbls = stringToDoubles(str);
            for(int i=0;i<dbls.count();++i) {
                trainingData.write(QByteArray::number(dbls.at(i),'g',15)+" ");
            }
            trainingData.write("\n-1.0\n");
        }
        return 0;
    }

    return 0;
}
