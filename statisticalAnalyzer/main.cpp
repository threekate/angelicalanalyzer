#include <QCoreApplication>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QDebug>
#include <QFile>
#include <iostream>
#include "utils.h"
#include "lexicon.h"

int main( int argc, char* argv[] )
{
    Q_UNUSED(argc)
    Q_UNUSED(argv)

    //The maximum combination length we are interested in
    static const int MAXLENGTH = 16;

    //The smallest number of occurences we are interested in for the main
    //output files. Anything occuring less often than this will go into
    //the uncommon.txt output file.
    static const int MINFREQ = 3;

    //Create database
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("combinations.sqlite");
    if(!db.open()) {
        qDebug() << "Could not open database";
        return -1;
    }

    //Create tables and indexes
    db.exec("CREATE TABLE COMBINATIONS("
            "COMBINATION    TEXT    NOT NULL," //The letter combination
            "TABLENUMBER    INT     NOT NULL," //The table number (0-98)
            "ROWNUMBER      INT     NOT NULL," //The row number (0 indexed)
            "STARTCOLUMN    INT     NOT NULL," //The column the combination starts in (0 indexed)
            "ENDCOLUMN      INT     NOT NULL"  //The column the combination ends in (0 indexed)
            ");");
    db.exec("CREATE TABLE SCORES("
            "COMBINATION   TEXT     NOT NULL," //The letter combination
            "LENGTH        INT      NOT NULL," //Length of the combination
            "FREQUENCY     INT      NOT NULL," //Frequency in Loagaetch
            "INCALLS       INT      NOT NULL," //Frequency in the 48 calls
            "SCORE         INT      NOT NULL"  //The computed score
            ");");
    db.exec("CREATE INDEX idx_COMBINATIONS ON COMBINATIONS (COMBINATION ASC);");
    db.exec("CREATE INDEX idx_COMBINATIONS2 ON COMBINATIONS (COMBINATION ASC, TABLENUMBER ASC);");

    //Go through every line of every table and add the combinations to the database
    {
        std::cout << "Processing Loagaeth tables..." << std::endl;
        QList<QList<QString> > tablesData(tableData());
        for(int table = 0; table < tablesData.count(); ++table) {
            for(int line = 0; line < tablesData.at(table).count(); ++line) {
                std::cout << "Processing Table " << qPrintable(tableName(table)) << " line " << line+1 << std::endl;
                QString normalizedLine = normalize(tablesData.at(table).at(line));
                for(int i=1;i<MAXLENGTH;++i) {
                    for(int j=0;j<normalizedLine.count();++j) {
                        QString chars = normalizedLine.mid(j,i);
                        //Prevent duplicate entries toward the end of a line
                        if(chars.count() == i) {
                            QString query = QString("INSERT INTO COMBINATIONS VALUES ('%1', %2, %3, %4, %5);")
                                    .arg(chars)
                                    .arg(table)
                                    .arg(line)
                                    .arg(j)
                                    .arg(j+i-1);
                            db.exec(query);
                        }
                    }
                }
            }
        }
    }

    QMultiMap<QString, angelicalWord> dict(dictionary());
    //Containers for different types of output
    QList<QString> combinations;
    QMultiMap<int, QString> frequency;
    QMultiMap<int, QString> length;
    QMap<QString, int> uncommon;

    //Get a list of all unique combinations from the database in alphabetical order
    QSqlQuery combinationsQuery("select distinct COMBINATION from COMBINATIONS ORDER BY COMBINATION ASC;",db);
    if(!combinationsQuery.exec()) {
        qDebug() << "query failed";
        return -1;
    }
    while(combinationsQuery.next()) {
        combinations << combinationsQuery.value(0).toString();
    }

    //Main output file
    QFile output("alphabeticaly.txt");
    output.open(QIODevice::WriteOnly);

    std::cout << "Processing letter combinations..." << std::endl;
    foreach( QString combination, combinations ) {
        //Get number of times a combination appears in Loagaeth
        QSqlQuery q(QString("select count(*) from COMBINATIONS where COMBINATION='%1';").arg(combination));
        q.exec();
        while(q.next()) {

            int keyCount(0);
            foreach( angelicalWord word, dict.values(combination) ) {
                keyCount += word.occurances;
            }

            std::cout << "Found " << qPrintable(combination) << " " << q.value(0).toInt() << " times." << std::endl;
            QString query = QString("INSERT INTO SCORES VALUES ('%1', %2, %3, %4, %5);")
                    .arg(combination)
                    .arg(combination.count())
                    .arg(q.value(0).toInt())
                    .arg(keyCount)
                    .arg(computeWordScore(combination.count(),q.value(0).toInt(),keyCount));
            db.exec(query);

            //We are only interested in strings that occur at least MINFREQ times
            if(q.value(0).toInt() >= MINFREQ) {
                std::cout << "Found " << qPrintable(combination) << " " << q.value(0).toInt() << " times." << std::endl;

                //Add it to the containers for the frequency and length output files
                frequency.insert(q.value(0).toInt(),combination);
                length.insert(combination.size(),combination);

                //Write the combination to the main output file
                output.write(qPrintable(combinationName(combination)));
                output.write(":\n");
                output.write("\tTotal occurences: ");
                output.write(q.value(0).toByteArray());
                output.write("\n");

                //Find out in what tables the combination occurs
                for(int i=0;i<tableData().count();++i) {
                    QSqlQuery details(QString("select count(*) from COMBINATIONS where COMBINATION='%1' and TABLENUMBER=%2;").arg(combination).arg(i));
                    details.exec();
                    while(details.next()) {
                        int cnt = details.value(0).toInt();
                        if(cnt > 0) {
                            output.write("\t\tTable ");
                            output.write(qPrintable(tableName(i)));
                            output.write(": ");
                            output.write(QByteArray::number(cnt));
                            output.write("\n");
                        }
                    }
                }
            }
            else {
                uncommon.insert(combination, q.value(0).toInt());
            }
        }
    }
    output.close();

    std::cout << "Generating list sorted by frequency..." << std::endl;
    QFile byFrequency("frequency.txt");
    byFrequency.open(QIODevice::WriteOnly);

    foreach( QString value, frequency.values() ) {
        byFrequency.write(qPrintable(combinationName(value)));
        byFrequency.write(": ");
        byFrequency.write(QByteArray::number(frequency.key(value)));
        byFrequency.write("\n");
    }
    byFrequency.close();

    std::cout << "Generating list sorted by length..." << std::endl;
    QFile byLength("length.txt");
    byLength.open(QIODevice::WriteOnly);
    foreach( QString value, length.values() ) {
        byLength.write(qPrintable(combinationName(value)));
        byLength.write(": ");
        byLength.write(QByteArray::number(frequency.key(value)));
        byLength.write("\n");
    }
    byLength.close();

    std::cout << "Generating list of uncommon combinations..." << std::endl;
    QFile uncommonCombinations("uncommon.txt");
    uncommonCombinations.open(QIODevice::WriteOnly);
    foreach( QString value, uncommon.keys() ) {
        uncommonCombinations.write(qPrintable(combinationName(value)));
        uncommonCombinations.write(": ");
        uncommonCombinations.write(QByteArray::number(uncommon.value(value)));
        uncommonCombinations.write("\n");
    }
    uncommonCombinations.close();

    return 0;

}
