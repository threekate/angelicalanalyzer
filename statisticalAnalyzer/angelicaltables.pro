#-------------------------------------------------
#
# Project created by QtCreator 2014-03-30T00:05:39
#
#-------------------------------------------------

QT       += gui core sql
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
TARGET = angelicaltables
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

INCLUDEPATH += ../common ../common/tables
DEPENDPATH += ../common

SOURCES += main.cpp \
    mainwindow.cpp \
    ../common/database.cpp

HEADERS += \
    table1.h \
    table2.h \
    utils.h \
    table3.h \
    table4.h \
    table5.h \
    table6.h \
    table7.h \
    table8.h \
    table9.h \
    table10.h \
    table11.h \
    table12.h \
    table13.h \
    table14.h \
    table15.h \
    table16.h \
    table17.h \
    table18.h \
    table19.h \
    table20.h \
    table21.h \
    table22.h \
    table23.h \
    table24.h \
    table25.h \
    table26.h \
    table27.h \
    table28.h \
    table29.h \
    table30.h \
    table31.h \
    table32.h \
    table33.h \
    table34.h \
    table35.h \
    table36.h \
    table37.h \
    table38.h \
    table39.h \
    table40.h \
    table41.h \
    table42.h \
    table43.h \
    table44.h \
    table45.h \
    table46.h \
    table47.h \
    table48.h \
    table49.h \
    tables.h \
    lexicon.h \
    mainwindow.h \
    ../common/database.h

FORMS += \
    mainwindow.ui
