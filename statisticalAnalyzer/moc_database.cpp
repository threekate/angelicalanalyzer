/****************************************************************************
** Meta object code from reading C++ file 'database.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.2.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../common/database.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'database.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.2.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Database_t {
    QByteArrayData data[19];
    char stringdata[192];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    offsetof(qt_meta_stringdata_Database_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData) \
    )
static const qt_meta_stringdata_Database_t qt_meta_stringdata_Database = {
    {
QT_MOC_LITERAL(0, 0, 8),
QT_MOC_LITERAL(1, 9, 5),
QT_MOC_LITERAL(2, 15, 0),
QT_MOC_LITERAL(3, 16, 7),
QT_MOC_LITERAL(4, 24, 6),
QT_MOC_LITERAL(5, 31, 8),
QT_MOC_LITERAL(6, 40, 7),
QT_MOC_LITERAL(7, 48, 14),
QT_MOC_LITERAL(8, 63, 11),
QT_MOC_LITERAL(9, 75, 8),
QT_MOC_LITERAL(10, 84, 6),
QT_MOC_LITERAL(11, 91, 5),
QT_MOC_LITERAL(12, 97, 15),
QT_MOC_LITERAL(13, 113, 17),
QT_MOC_LITERAL(14, 131, 20),
QT_MOC_LITERAL(15, 152, 14),
QT_MOC_LITERAL(16, 167, 8),
QT_MOC_LITERAL(17, 176, 8),
QT_MOC_LITERAL(18, 185, 5)
    },
    "Database\0error\0\0message\0status\0progress\0"
    "percent\0databaseOpened\0setFilename\0"
    "filename\0rebild\0clear\0createStructure\0"
    "populateAllTables\0populateCombinations\0"
    "populateScores\0Location\0Loagaeth\0Calls\0"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Database[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      11,   14, // methods
       0,    0, // properties
       1,   88, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   69,    2, 0x06,
       4,    1,   72,    2, 0x06,
       5,    1,   75,    2, 0x06,
       7,    0,   78,    2, 0x06,

 // slots: name, argc, parameters, tag, flags
       8,    1,   79,    2, 0x0a,
      10,    0,   82,    2, 0x0a,
      11,    0,   83,    2, 0x0a,
      12,    0,   84,    2, 0x0a,
      13,    0,   85,    2, 0x0a,
      14,    0,   86,    2, 0x0a,
      15,    0,   87,    2, 0x0a,

 // signals: parameters
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void, QMetaType::Int,    6,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Bool, QMetaType::QString,    9,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // enums: name, flags, count, data
      16, 0x1,    2,   92,

 // enum data: key, value
      17, uint(Database::Loagaeth),
      18, uint(Database::Calls),

       0        // eod
};

void Database::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Database *_t = static_cast<Database *>(_o);
        switch (_id) {
        case 0: _t->error((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: _t->status((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 2: _t->progress((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->databaseOpened(); break;
        case 4: { bool _r = _t->setFilename((*reinterpret_cast< const QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 5: _t->rebild(); break;
        case 6: _t->clear(); break;
        case 7: _t->createStructure(); break;
        case 8: _t->populateAllTables(); break;
        case 9: _t->populateCombinations(); break;
        case 10: _t->populateScores(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (Database::*_t)(const QString & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Database::error)) {
                *result = 0;
            }
        }
        {
            typedef void (Database::*_t)(const QString & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Database::status)) {
                *result = 1;
            }
        }
        {
            typedef void (Database::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Database::progress)) {
                *result = 2;
            }
        }
        {
            typedef void (Database::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Database::databaseOpened)) {
                *result = 3;
            }
        }
    }
}

const QMetaObject Database::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_Database.data,
      qt_meta_data_Database,  qt_static_metacall, 0, 0}
};


const QMetaObject *Database::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Database::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Database.stringdata))
        return static_cast<void*>(const_cast< Database*>(this));
    return QObject::qt_metacast(_clname);
}

int Database::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 11)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 11;
    }
    return _id;
}

// SIGNAL 0
void Database::error(const QString & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Database::status(const QString & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Database::progress(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void Database::databaseOpened()
{
    QMetaObject::activate(this, &staticMetaObject, 3, 0);
}
QT_END_MOC_NAMESPACE
